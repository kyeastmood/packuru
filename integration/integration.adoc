// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: 0BSD
= Packuru
Desktop environment integration

== freedesktop.org

* For current user only
    ** Keep entire application (including libs and plugins) in one place.
    ** Create links in ~/bin/ pointing to application executables
    ** Copy .desktop files from link:freedesktop.org/[] folder to ```~/.local/share/applications/```
    ** KDE/Dolphin: Copy .desktop files from link:freedesktop.org/kde/[] folder to ```~/.local/share/kservices5/```
* System-wide
    ** Keep executables and libraries in standard folders
    ** Copy .desktop files from link:freedesktop.org/[] to link:/usr/share/applications/[]
    ** KDE/Dolphin: Copy .desktop files from link:freedesktop.org/kde/[] folder to link:/usr/share/kservices5/[]
