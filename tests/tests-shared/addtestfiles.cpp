// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "addtestfiles.h"


namespace Packuru::Tests::Shared
{

void addTestFiles(const QString& folderPath,
                  const QString& nameFilter,
                  const QTestDataInspector& insp)
{
    QDirIterator it(folderPath, {nameFilter});
    while (it.hasNext())
    {
        const QString filePath = it.next();
        const QFileInfo info(filePath);

        QTestData& data = QTest::newRow(info.fileName().toLatin1())
                << filePath;
        if (insp)
            insp(data);
    }
}


void addTestFiles(const QString& folderPath,
                  const QRegularExpression& nameFilter,
                  const Packuru::Tests::Shared::QTestDataInspector& insp)
{
    QDirIterator it(folderPath);

    while (it.hasNext() )
    {
        const QString filePath = it.next();
        const QFileInfo info(filePath);

        if (!nameFilter.match(info.fileName()).hasMatch())
            continue;

        QTestData& data = QTest::newRow(info.fileName().toLatin1())
                << filePath;
        if (insp)
            insp(data);
    }
}

}
