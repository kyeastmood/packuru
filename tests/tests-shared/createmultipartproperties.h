// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "src/core/private/plugin-api/backendarchiveproperties.h"

#include "symbol_export.h"


namespace Packuru::Tests::Shared
{

PACKURU_TESTS_SHARED_EXPORT Packuru::Core::BackendArchiveProperties
createMultiPartProperties(int partCount, qulonglong totalSize);

}
