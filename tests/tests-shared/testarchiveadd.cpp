// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>
#include <QTemporaryDir>
#include <QStorageInfo>

#include "src/utils/qvariant_utils.h"

#include "src/core/private/archivetypedetector.h"
#include "src/core/private/plugin-api/archivetype.h"
#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/plugin-api/backendarchivingdata.h"
#include "src/core/private/plugin-api/abstractbackendhandler.h"
#include "src/core/private/plugin-api/encryptionstate.h"
#include "src/core/private/plugin-api/archivingfilepaths.h"

#include "testarchiveadd.h"
#include "checkbackendexpectederror.h"


using namespace Packuru::Core;
using namespace Packuru::Utils;

namespace
{
const ArchiveTypeDetector typeDetector;
}

namespace Packuru::Tests::Shared
{

TestArchiveAddParams::TestArchiveAddParams()
    : handler(nullptr),
      archiveType(ArchiveType::___NOT_SUPPORTED),
      currentArchiveEncryption(EncryptionState::Disabled),
      newArchiveEncryption(EncryptionState::Disabled),
      partSize(0),
      filePaths(ArchivingFilePaths::RelativePaths),
      createNewArchive(false),
      expectedEndState(BackendHandlerState::Success),
      expectedError(BackendHandlerError::___INVALID)
{

}


void testArchiveAdd(const TestArchiveAddParams& params)
{
    QVERIFY(params.archiveType != ArchiveType::___NOT_SUPPORTED)   ;

    BackendArchivingData data;
    data.archiveType = params.archiveType;

    const QTemporaryDir tempDir;
    if (!tempDir.isValid())
        QSKIP("Cannot create temporary directory: " + tempDir.path().toLatin1());

    const QStorageInfo info(tempDir.path());

    if (info.isReadOnly())
        QSKIP("No write permission for foldertofthisworld"
              "directory " + tempDir.path().toLatin1());

    if (info.bytesAvailable() < 10'000)
        QSKIP("Insufficient disk space: " + tempDir.path().toLatin1());

    data.destinationPath = tempDir.path();

    if (!params.createNewArchive)
    {
        // Operate on temporary copy
        QVERIFY(!params.existingArchivePath.isEmpty());
        const QFileInfo archiveInfo(params.existingArchivePath);
        QVERIFY(archiveInfo.exists());
        const QString tempArchivePath = data.destinationPath + QLatin1Char('/') + archiveInfo.fileName();
        QFile::copy(params.existingArchivePath, tempArchivePath);
        data.archiveName = archiveInfo.fileName();
    }
    else
        data.archiveName = "archive";

    data.inputItems.reserve(params.inputItems.size());

    for (const auto& item : params.inputItems)
    {
        data.inputItems.push_back(item.canonicalFilePath());
    }

    data.internalPath = params.internalPath;
    data.partSize = params.partSize;
    data.filePaths = params.filePaths;
    data.currentArchiveEncryption = params.currentArchiveEncryption;
    data.newArchiveEncryption = params.newArchiveEncryption;
    data.password = params.password;
    data.createNewArchive = params.createNewArchive;
    data.pluginPrivateData = params.pluginPrivateData;

    params.handler->addToArchive(data);

    QTRY_VERIFY2(params.handler->isFinished() == true,
                 (QLatin1String("Actual state=") + toString(params.handler->getState())).toLatin1());

    const BackendHandlerState actualState = params.handler->getState();
    QCOMPARE(actualState, params.expectedEndState);

    const auto errors = params.handler->getErrors();
    const auto warnings = params.handler->getWarnings();

    QVERIFY(actualState != BackendHandlerState::Success || (errors.empty() && warnings.empty()));
    QVERIFY(actualState != BackendHandlerState::Warnings || errors.empty());

    if (params.expectedError != BackendHandlerError::___INVALID)
    {
        QVERIFY2(const auto check = checkBackendExpectedError(errors, params.expectedError),
                 check.errorString.toLatin1());
    }
}

}
