// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once


namespace Packuru::Tests::Shared
{

enum class PasswordTestMode
{
    StartUpPasswordEntry,
    PromptAbort,
    PromptPasswordEntry
};

}
