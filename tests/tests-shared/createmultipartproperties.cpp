// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "src/core/private/plugin-api/backendarchiveproperty.h"

#include "createmultipartproperties.h"


using namespace Packuru::Core;


namespace Packuru::Tests::Shared
{

BackendArchiveProperties createMultiPartProperties(int partCount, qulonglong totalSize)
{
    return BackendArchiveProperties {
        {BackendArchiveProperty::Multipart, true},
        {BackendArchiveProperty::PartCount, partCount},
        {BackendArchiveProperty::TotalSize, totalSize}
    };
}

}
