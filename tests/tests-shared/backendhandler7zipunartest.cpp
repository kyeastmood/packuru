// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>

#include "src/core/private/plugin-api/abstractbackendhandler.h"
#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"

#include "backendhandler7zipunartest.h"
#include "passwordtestmode.h"
#include "passwords.h"
#include "testarchivetest.h"


using namespace Packuru::Core;


namespace Packuru::Tests::Shared
{

BackendHandler7ZipUnarTest::BackendHandler7ZipUnarTest(const CLIBackendHandlerFactory& factory, QObject *parent)
    : QObject(parent),
      bhFactory(factory)
{

}


void BackendHandler7ZipUnarTest::initTestCase()
{
    if (!bhFactory.executablAvailable())
        QSKIP("Executable not available");
}


void BackendHandler7ZipUnarTest::testArchive_data()
{
    QTest::addColumn<QString>("archivePath");

    const std::initializer_list<QLatin1String> paths
    {
        QLatin1String("../../data/7z/multipart.7z.001"),
                QLatin1String("../../data/zip/multipart.scheme1.zip"),
                QLatin1String("../../data/zip/multipart.scheme2.zip.001"),
                QLatin1String("../../data/7z/solid.unix_link.7z"),
                QLatin1String("../../data/zip/unix_link.zip")
    };

    for (const QLatin1String& path : paths)
        QTest::newRow(path.data())
                << path.data();
}


void BackendHandler7ZipUnarTest::testArchive()
{
    const auto handler = bhFactory.create();

    QFETCH(QString, archivePath);

    TestArchiveTestParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;

    testArchiveTest(params);
}


void BackendHandler7ZipUnarTest::testError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const std::initializer_list<QLatin1String> paths
    {
        QLatin1String("../../data/7z/corrupted1.7z"),
                QLatin1String("../../data/zip/corrupted3.zip"),
                QLatin1String("../../data/zip/corrupted.truncated.zip"),
                QLatin1String("../../data/7z/corrupted2.7z"),
                // 7-Zip and Unarchiver do not always report a missing part error
                // but rather a corrupted data error
                QLatin1String("../../data/7z/missing_part.7z.001"),
                QLatin1String("../../data/zip/missing_part.scheme2.zip.001")
    };

    for (const QLatin1String& path : paths)
        QTest::newRow(path.data())
                << path.data()
                << BackendHandlerError::DataError;
}


void BackendHandler7ZipUnarTest::testError()
{
    const auto handler = bhFactory.create();

    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveTestParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    testArchiveTest(params);
}


void BackendHandler7ZipUnarTest::testEncrypted_data()
{
    QTest::addColumn<QString>("archivePath");

    const std::initializer_list<QLatin1String> paths
    {
        QLatin1String("../../data/7z/encrypted.solid.7z"),
                QLatin1String("../../data/7z/encrypted_content.solid.7z"),
                QLatin1String("../../data/zip/encrypted_content.zip")
    };

    for (const QLatin1String& path : paths)
        QTest::newRow(path.data())
                << path.data();
}


void BackendHandler7ZipUnarTest::testEncrypted()
{
    QFETCH(QString, archivePath);

    auto handler = bhFactory.create();

    TestArchiveTestParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.password = validPassword;

    // Test with password provided in the command-line
    testArchiveTest(params);

    // BackendHandlers must be used only once
    handler = bhFactory.create();

    params.handler = handler.get();
    params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

    // Test with password provided in the prompt
    testArchiveTest(params);
}


void BackendHandler7ZipUnarTest::testEncryptedError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QString>("password");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const std::initializer_list<QLatin1String> validArchivePaths
    {
        QLatin1String("../../data/7z/encrypted.solid.7z"),
                QLatin1String("../../data/7z/encrypted_content.solid.7z"),
                QLatin1String("../../data/zip/encrypted_content.zip")
    };

    for (const QLatin1String& path : validArchivePaths)
        QTest::newRow(path.data())
                << path.data()
                << invalidPassword
                << BackendHandlerError::InvalidPasswordOrDataError;

    const std::initializer_list<QLatin1String> corruptedArchivePaths
    {
        QLatin1String("../../data/7z/corrupted.encrypted_content.solid.7z"),
                QLatin1String("../../data/7z/corrupted.encrypted2.7z"),
                QLatin1String("../../data/7z/corrupted.encrypted1.7z"),
                QLatin1String("../../data/7z/corrupted.encrypted_content.non_solid.7z"),
    };

    for (const QLatin1String& path : corruptedArchivePaths)
        QTest::newRow(path.data())
                << path.data()
                << validPassword
                << BackendHandlerError::InvalidPasswordOrDataError;
}


void BackendHandler7ZipUnarTest::testEncryptedError()
{
    QFETCH(QString, archivePath);
    QFETCH(QString, password);
    QFETCH(BackendHandlerError, expectedError);

    auto handler = bhFactory.create();

    TestArchiveTestParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.password = password;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    // Test with password provided in the command-line
    testArchiveTest(params);

    // BackendHandlers must be used only once
    handler = bhFactory.create();

    params.handler = handler.get();
    params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

    // Test with password provided in the prompt
    testArchiveTest(params);
}


void BackendHandler7ZipUnarTest::testEncryptedAbort_data()
{
    QTest::addColumn<QString>("archivePath");

    const char* path = "../../data/7z/encrypted.solid.7z";
    QTest::newRow(path)
            << path;
}


void BackendHandler7ZipUnarTest::testEncryptedAbort()
{
    const auto handler = bhFactory.create();

    QFETCH(QString, archivePath);

    TestArchiveTestParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.passwordTestMode = PasswordTestMode::PromptAbort;
    params.expectedEndState = BackendHandlerState::Aborted;

    testArchiveTest(params);
}

}
