// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "passwords.h"


namespace Packuru::Tests::Shared
{
const char* validPassword = "123";
const char* invalidPassword = "invalid_password";
}
