// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "clibackendhandlerfactory.h"
#include "symbol_export.h"


namespace Packuru::Tests::Shared
{

// Common archive read tests for 7-Zip and The Unarchiver
class PACKURU_TESTS_SHARED_EXPORT BackendHandler7ZipUnarRead : public QObject
{
    Q_OBJECT
public:
    explicit BackendHandler7ZipUnarRead(const CLIBackendHandlerFactory& factory,
                                        QObject *parent = nullptr);

private slots:
    void initTestCase();

    void readArchive_data();
    void readArchive();

    void readError_data();
    void readError();

    void readEncrypted_data();
    void readEncrypted();

    void readEncryptedError_data();
    void readEncryptedError();

    void readEncryptedAbort_data();
    void readEncryptedAbort();

private:
    CLIBackendHandlerFactory bhFactory;
};

}
