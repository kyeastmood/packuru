// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>
#include <memory>


namespace Packuru::Core
{
class AbstractBackendHandler;
}


namespace Packuru::Tests::Shared
{

struct CLIBackendHandlerFactory
{
    using CreateFunction
    = std::function<std::unique_ptr<Packuru::Core::AbstractBackendHandler>()>;

    using EcecCheckFunction
    = std::function<bool()>;

    CreateFunction create;
    EcecCheckFunction executablAvailable;
};

}
