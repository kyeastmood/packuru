# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-tests.pri)

QT += testlib
QT -= gui

TEMPLATE = lib
DEFINES += $${PROJECT_MACRO_NAME}_TESTS_SHARED_LIBRARY

TARGET = $$qtLibraryTarget($$TESTS_SHARED_LIB_BUILD_NAME)

DESTDIR += $${TESTS_SHARED_LIB_TARGET_DIR}

# project core libs
QMAKE_RPATHDIR += $ORIGIN/../../libs

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    addtestfiles.cpp \
    backendhandler7zipunarextract.cpp \
    backendhandler7zipunarread.cpp \
    backendhandler7zipunartest.cpp \
    checkbackendexpectederror.cpp \
    createarchivetempcopy.cpp \
    createmultipartproperties.cpp \
    loadplugin.cpp \
    passwords.cpp \
    testarchiveadd.cpp \
    testarchivedelete.cpp \
    testarchiveextract.cpp \
    testarchiveread.cpp \
    testarchivetest.cpp

HEADERS += \
    addtestfiles.h \
    backendhandler7zipunarextract.h \
    backendhandler7zipunarread.h \
    backendhandler7zipunartest.h \
    checkbackendexpectederror.h \
    clibackendhandlerfactory.h \
    createarchivetempcopy.h \
    createmultipartproperties.h \
    itemchecker.h \
    loadplugin.h \
    passwords.h \
    passwordtestmode.h \
    symbol_export.h \
    testarchiveadd.h \
    testarchivedelete.h \
    testarchiveextract.h \
    testarchiveread.h \
    testarchivetest.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
