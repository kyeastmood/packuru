// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>


#include "src/core/private/plugin-api/backendarchiveproperties.h"

#include "symbol_export.h"


namespace Packuru::Core
{
class AbstractBackendHandler;
enum class ArchiveType;
enum class BackendHandlerState;
enum class BackendHandlerError;
}


namespace Packuru::Tests::Shared
{

enum class PasswordTestMode;

struct PACKURU_TESTS_SHARED_EXPORT TestArchiveTestParams
{
    TestArchiveTestParams();

    Packuru::Core::AbstractBackendHandler* handler;
    QString archivePath;
    PasswordTestMode passwordTestMode;
    QString password;
    QVariant pluginPrivateData;
    Packuru::Core::BackendHandlerState expectedEndState;
    Packuru::Core::BackendHandlerError expectedError;
};


PACKURU_TESTS_SHARED_EXPORT void testArchiveTest(const TestArchiveTestParams& params);

}
