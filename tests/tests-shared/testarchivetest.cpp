// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>

#include "src/utils/qvariant_utils.h"

#include "src/core/private/archivetypedetector.h"
#include "src/core/private/plugin-api/archivetype.h"
#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/plugin-api/backendtestdata.h"
#include "src/core/private/plugin-api/abstractbackendhandler.h"

#include "passwordtestmode.h"
#include "testarchivetest.h"
#include "checkbackendexpectederror.h"


using namespace Packuru::Core;
using namespace Packuru::Utils;

namespace
{
const ArchiveTypeDetector typeDetector;
}

namespace Packuru::Tests::Shared
{

TestArchiveTestParams::TestArchiveTestParams()
    : handler(nullptr),
      passwordTestMode(PasswordTestMode::StartUpPasswordEntry),
      expectedEndState(BackendHandlerState::Success),
      expectedError(BackendHandlerError::___INVALID)
{

}


void testArchiveTest(const TestArchiveTestParams& params)
{
    BackendTestData data;
    data.archiveInfo.setFile(QFINDTESTDATA(params.archivePath));

    if (data.archiveInfo.fileName().isEmpty())
        QSKIP("");

    data.archiveType = typeDetector.detectType(data.archiveInfo.absoluteFilePath());
    data.pluginPrivateData = params.pluginPrivateData;

    if (params.passwordTestMode == PasswordTestMode::StartUpPasswordEntry)
    {
        data.password = params.password;
        params.handler->testArchive(data);
    }
    else
    {
        params.handler->testArchive(data);

        QTRY_VERIFY2(params.handler->getState() == BackendHandlerState::WaitingForPassword,
                     (QLatin1String("Actual state=") + toString(params.handler->getState())).toLatin1());

        if (params.passwordTestMode == PasswordTestMode::PromptAbort)
        {
            params.handler->stop();
            QTRY_VERIFY2(params.handler->getState() == BackendHandlerState::Aborted,
                         (QLatin1String("Actual state=") + toString(params.handler->getState())).toLatin1());
            return;
        }
        else
            params.handler->enterPassword(params.password);
    }

    QTRY_VERIFY2(params.handler->isFinished() == true,
                 (QLatin1String("Actual state=") + toString(params.handler->getState())).toLatin1());

    const BackendHandlerState actualState = params.handler->getState();
    QCOMPARE(actualState, params.expectedEndState);

    const auto errors = params.handler->getErrors();
    const auto warnings = params.handler->getWarnings();

    QVERIFY(actualState != BackendHandlerState::Success || (errors.empty() && warnings.empty()));
    QVERIFY(actualState != BackendHandlerState::Warnings || errors.empty());

    if (params.expectedError != BackendHandlerError::___INVALID)
    {
        QVERIFY2(const auto check = checkBackendExpectedError(errors, params.expectedError),
                 check.errorString.toLatin1());
    }
}

}
