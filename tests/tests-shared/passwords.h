// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once


namespace Packuru::Tests::Shared
{
extern const char* validPassword;
extern const char* invalidPassword;
}
