// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>

#include "src/core/private/plugin-api/abstractbackendhandler.h"
#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/plugin-api/backendarchiveproperties.h"
#include "src/core/private/plugin-api/backendarchiveproperty.h"
#include "src/core/private/plugin-api/encryptionstate.h"

#include "backendhandler7zipunarread.h"
#include "testarchiveread.h"
#include "createmultipartproperties.h"
#include "passwordtestmode.h"
#include "passwords.h"


using namespace Packuru::Core;


namespace Packuru::Tests::Shared
{

BackendHandler7ZipUnarRead::BackendHandler7ZipUnarRead(const CLIBackendHandlerFactory& factory, QObject *parent)
    : QObject(parent),
      bhFactory(factory)
{

}


void BackendHandler7ZipUnarRead::initTestCase()
{
    if (!bhFactory.executablAvailable())
        QSKIP("Executable not available");
}


void BackendHandler7ZipUnarRead::readArchive_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendArchiveProperties>("requiredProperties");
    QTest::addColumn<ArchiveItemChecker>("itemChecker");

    const char* path = "../../data/7z/multipart.7z.001";
    QTest::newRow(path)
            << path
            << createMultiPartProperties(3, 1126)
            << ArchiveItemChecker();

    path = "../../data/zip/multipart.scheme1.zip";
    QTest::newRow(path)
            << path
            << createMultiPartProperties(2, 1157)
            << ArchiveItemChecker();

    path = "../../data/zip/multipart.scheme2.zip.001";
    QTest::newRow(path)
            << path
            << createMultiPartProperties(3, 1148)
            << ArchiveItemChecker();

    const auto checkerSolidUnixLink7z = [] (const ArchiveItemMap& items)
    {
        QCOMPARE(items.size(), static_cast<std::size_t>(5));

        for (const auto& it : items)
        {
            const ArchiveItem& item = it.second;
            QCOMPARE(item.encrypted, false);
        }

        const auto* item = getItem(items, "files");
        QVERIFY(item);
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::Folder));

        QVERIFY(item = getItem(items, "files/file1.txt"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::File));
        QVERIFY(item->modified.isValid());
        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(4));
        QCOMPARE(item->checksum, "5A82FD08");

        QVERIFY(item = getItem(items, "files/link_to_dev_null"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::Link));
        QVERIFY(item->modified.isValid());
        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(9));
        QCOMPARE(item->checksum, "AC0BE185");
    };

    const BackendArchiveProperties propSolid = { {BackendArchiveProperty::Solid, true} };

    path = "../../data/7z/solid.unix_link.7z";
    QTest::newRow(path)
            << path
            << propSolid
            << ArchiveItemChecker(checkerSolidUnixLink7z);

    const auto checkerUnixLinkZip = [] (const ArchiveItemMap& items)
    {
        QCOMPARE(items.size(), static_cast<std::size_t>(5));

        for (const auto& it : items)
        {
            const ArchiveItem& item = it.second;
            QCOMPARE(item.encrypted, false);
        }

        const auto* item = getItem(items, "files");
        QVERIFY(item);
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::Folder));
        QCOMPARE(item->hostOS, "Unix");

        QVERIFY(item = getItem(items, "files/file1.txt"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::File));
        QVERIFY(item->modified.isValid());
        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(4));
        QCOMPARE(item->packedSize.value, static_cast<unsigned long long>(4));
        QCOMPARE(item->hostOS, "Unix");
        QCOMPARE(item->checksum, "5A82FD08");

        QVERIFY(item = getItem(items, "files/link_to_dev_null"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::Link));
        QVERIFY(item->modified.isValid());
        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(9));
        QCOMPARE(item->packedSize.value, static_cast<unsigned long long>(9));
        QCOMPARE(item->hostOS, "Unix");
        QCOMPARE(item->checksum, "AC0BE185");
    };

    path = "../../data/zip/unix_link.zip";
    QTest::newRow(path)
            << path
            << BackendArchiveProperties{}
            << ArchiveItemChecker(checkerUnixLinkZip);
}


void BackendHandler7ZipUnarRead::readArchive()
{
    const auto handler = bhFactory.create();

    QFETCH(QString, archivePath);
    QFETCH(BackendArchiveProperties, requiredProperties);
    QFETCH(ArchiveItemChecker, itemChecker);

    TestArchiveReadParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.requiredProperties = requiredProperties;
    params.itemChecker = itemChecker;

    testArchiveRead(params);
}


void BackendHandler7ZipUnarRead::readError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const std::initializer_list<QLatin1String> paths
    {
        QLatin1String("../../data/zip/corrupted3.zip"),
                QLatin1String("../../data/zip/corrupted.truncated.zip"),
                QLatin1String("../../data/7z/corrupted2.7z"),
                // 7-Zip and Unarchiver do not always report a missing part error
                QLatin1String("../../data/7z/missing_part.7z.001"),
                QLatin1String("../../data/zip/missing_part.scheme2.zip.001"),
    };

    for (const QLatin1String& path : paths)
        QTest::newRow(path.data())
                << path.data()
                << BackendHandlerError::DataError;
}


void BackendHandler7ZipUnarRead::readError()
{
    const auto handler = bhFactory.create();

    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveReadParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    testArchiveRead(params);
}


void BackendHandler7ZipUnarRead::readEncrypted_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QString>("password");
    QTest::addColumn<BackendArchiveProperties>("requiredProperties");
    QTest::addColumn<ArchiveItemChecker>("itemChecker");

    const auto checkerEncrypted = [] (const ArchiveItemMap& items)
    {
        for (const auto& it : items)
        {
            const auto* item = &it.second;
            QVERIFY2(item->encrypted.value() == true, item->name.toLatin1());
        }
    };

    const std::initializer_list<QLatin1String> fullyEncryptedPaths
    {
        QLatin1String("../../data/7z/encrypted.solid.7z"),
    };

    for (const QLatin1String& path : fullyEncryptedPaths)
        QTest::newRow(path.data())
                << path.data()
                << validPassword
                << BackendArchiveProperties{}
                << ArchiveItemChecker{checkerEncrypted};

    const BackendArchiveProperties propEncryptedContent =
    { {BackendArchiveProperty::EncryptionState, QVariant::fromValue(EncryptionState::FilesContentOnly)} };

    const auto checkerEncryptedContent = [] (const ArchiveItemMap& items)
    {
        for (const auto& it : items)
        {
            const auto* item = &it.second;
            if (item->nodeType != NodeType::Folder)
                QVERIFY2(item->encrypted.value() == true, item->name.toLatin1());
        }
    };

    const std::initializer_list<QLatin1String> contentEncryptedPaths
    {
        QLatin1String("../../data/zip/encrypted_content.zip"),
                QLatin1String("../../data/7z/encrypted_content.solid.7z"),
    };

    for (const QLatin1String& path : contentEncryptedPaths)
        QTest::newRow(path.data())
                << path.data()
                << ""
                << propEncryptedContent
                << ArchiveItemChecker{checkerEncryptedContent};
}


void BackendHandler7ZipUnarRead::readEncrypted()
{
    auto handler = bhFactory.create();

    QFETCH(QString, archivePath);
    QFETCH(QString, password);
    QFETCH(BackendArchiveProperties, requiredProperties);
    QFETCH(ArchiveItemChecker, itemChecker);

    TestArchiveReadParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.password = password;
    params.requiredProperties = requiredProperties;
    params.itemChecker = itemChecker;

    if (password.isEmpty())
    {
        const bool encryptionSpecified = params.requiredProperties
                .find(BackendArchiveProperty::EncryptionState) != params.requiredProperties.end();

        if (!encryptionSpecified)
            params.requiredProperties[BackendArchiveProperty::EncryptionState]
                    = QVariant::fromValue(EncryptionState::Disabled);

        testArchiveRead(params);
    }
    else
    {
        params.requiredProperties[BackendArchiveProperty::EncryptionState]
                = QVariant::fromValue(EncryptionState::EntireArchive);

        // Read with password provided in the command-line
        testArchiveRead(params);

        // BackendHandlers must be used only once
        handler = bhFactory.create();

        params.handler = handler.get();
        params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

        // Read with password provided in the prompt
        testArchiveRead(params);
    }
}


void BackendHandler7ZipUnarRead::readEncryptedError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QString>("password");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const std::initializer_list<QLatin1String> paths
    {
        QLatin1String("../../data/7z/corrupted.encrypted2.7z"),
                QLatin1String("../../data/7z/encrypted.solid.7z"),
    };

    for (const auto& path : paths)
        QTest::newRow(path.data())
                << path.data()
                << invalidPassword
                << BackendHandlerError::InvalidPasswordOrDataError;
}


void BackendHandler7ZipUnarRead::readEncryptedError()
{
    auto handler = bhFactory.create();

    QFETCH(QString, archivePath);
    QFETCH(QString, password);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveReadParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.password = password;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    if (!password.isEmpty())
    {
        params.requiredProperties[BackendArchiveProperty::EncryptionState]
                = QVariant::fromValue(EncryptionState::EntireArchive);

        testArchiveRead(params);
    }
    else
    {
        testArchiveRead(params);

        // BackendHandlers must be used only once
        handler = bhFactory.create();

        params.handler = handler.get();
        params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

        // Read with password provided in the prompt
        testArchiveRead(params);
    }
}


void BackendHandler7ZipUnarRead::readEncryptedAbort_data()
{
    QTest::addColumn<QString>("archivePath");

    const char* path = "../../data/7z/encrypted.solid.7z";
    QTest::newRow(path)
            << path;
}


void BackendHandler7ZipUnarRead::readEncryptedAbort()
{
    const auto handler = bhFactory.create();

    QFETCH(QString, archivePath);

    TestArchiveReadParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.passwordTestMode = PasswordTestMode::PromptAbort;
    params.expectedEndState = BackendHandlerState::Aborted;

    testArchiveRead(params);
}

}
