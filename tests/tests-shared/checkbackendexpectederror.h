// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_set>

#include <QString>

#include "symbol_export.h"


namespace Packuru::Core
{
enum class BackendHandlerError;
}


namespace Packuru::Tests::Shared
{

struct BackendExpectedErrorCheck
{
    explicit operator bool() const { return success; }

    bool success = false;
    QString errorString;
};



PACKURU_TESTS_SHARED_EXPORT BackendExpectedErrorCheck
checkBackendExpectedError(const std::unordered_set<Packuru::Core::BackendHandlerError>& errors,
                          Packuru::Core::BackendHandlerError expectedError);

}
