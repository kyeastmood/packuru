// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <unordered_map>

#include <QtTest>

#include "src/utils/private/qttypehasher.h"
#include "src/utils/private/unordered_map_utils.h"

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/plugin-api/backendarchiveproperties.h"
#include "src/core/private/plugin-api/backendarchiveproperty.h"
#include "src/core/private/plugin-api/encryptionstate.h"

#include "src/plugin-unarchiver-core/backendhandler.h"

#include "../../tests-shared/testarchiveread.h"
#include "../../tests-shared/createmultipartproperties.h"
#include "../../tests-shared/passwords.h"
#include "../../tests-shared/passwordtestmode.h"


using namespace Packuru::Core;
using namespace Packuru::Plugins::Unarchiver::Core;
using namespace Packuru::Tests::Shared;


namespace Packuru::Tests::Plugins::Unarchiver
{


class BackendHandlerRead : public QObject
{
    Q_OBJECT

public:
    BackendHandlerRead();
    ~BackendHandlerRead() override;

private slots:
    void initTestCase();

    void readSuccess_data();
    void readSuccess();

    void readError_data();
    void readError();

    void readEncrypted_data();
    void readEncrypted();

    void readEncryptedError_data();
    void readEncryptedError();
};


BackendHandlerRead::BackendHandlerRead()
{

}


BackendHandlerRead::~BackendHandlerRead()
{

}


void BackendHandlerRead::initTestCase()
{
    if (!BackendHandler::executableAvailable())
        QSKIP("Executable not found");
}


void BackendHandlerRead::readSuccess_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendArchiveProperties>("requiredProperties");
    QTest::addColumn<ArchiveItemChecker>("itemChecker");

    BackendArchiveProperties propComment =
    {{BackendArchiveProperty::Comment, "Line 1\nLine 2\nLine 3"}};

    const char* path = "../../data/zip/comment.zip";
    QTest::newRow(path)
            << path
            << propComment
            << ArchiveItemChecker();

    BackendArchiveProperties propRarMultiPart =
    { {BackendArchiveProperty::EncryptionState, QVariant::fromValue(EncryptionState::Disabled)},
      {BackendArchiveProperty::Solid, true},
      {BackendArchiveProperty::Multipart, true},
      {BackendArchiveProperty::PartCount, 3} };

    path = "../../data/rar/multipart.comment.lock.recovery.solid.rar_old.rar";
    propRarMultiPart[BackendArchiveProperty::TotalSize] = static_cast<qulonglong>(4439);

    const auto checkerRarOldMultipart = [this] (const ArchiveItemMap& items)
    {
        QCOMPARE(items.size(), static_cast<std::size_t>(5));

        for (const auto& it : items)
        {
            const ArchiveItem& item = it.second;
            QCOMPARE(item.encrypted, false);
        }

        const auto* item = getItem(items, "files");
        QVERIFY(item);
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::Folder));
        QCOMPARE(item->hostOS, "Win32");

        QVERIFY(item = getItem(items, "files/file1.txt"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::File));

        QCOMPARE(item->modified.toSecsSinceEpoch(), static_cast<qint64>(1592311924));

        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(3));
        QCOMPARE(item->packedSize.value, static_cast<unsigned long long>(90));
        QCOMPARE(item->hostOS, "Win32");
        QCOMPARE(item->checksum, "884863D2");

        QVERIFY(item = getItem(items, "files/blob1kb"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::File));

        QCOMPARE(item->modified.toSecsSinceEpoch(), static_cast<qint64>(1592312628));

        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(1000));
        QCOMPARE(item->packedSize.value, static_cast<unsigned long long>(889));
        QCOMPARE(item->hostOS, "Win32");
        QCOMPARE(item->checksum, "C8C2CC73");
    };

    QTest::newRow(path)
            << path
            << propRarMultiPart
            << ArchiveItemChecker(checkerRarOldMultipart);

    path = "../../data/rar/multipart.comment.lock.recovery.solid.rar_new.part01.rar";
    propRarMultiPart[BackendArchiveProperty::TotalSize] = static_cast<qulonglong>(4062);

    const auto checkerRarNewMultipart = [this] (const ArchiveItemMap& items)
    {
        QCOMPARE(items.size(), static_cast<std::size_t>(5));

        for (const auto& it : items)
        {
            const ArchiveItem& item = it.second;
            QCOMPARE(item.encrypted, false);
        }

        const auto* item = getItem(items, "files");
        QVERIFY(item);
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::Folder));
        QCOMPARE(item->hostOS, "Windows");

        QVERIFY(item = getItem(items, "files/file1.txt"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::File));

        QCOMPARE(item->modified.toSecsSinceEpoch(), static_cast<qint64>(1592311925));
        QCOMPARE(item->created.toSecsSinceEpoch(), static_cast<qint64>(1592900977));
        QCOMPARE(item->accessed.toSecsSinceEpoch(), static_cast<qint64>(1592900977));

        QVERIFY(item->accessed.isValid());

        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(3));
        QCOMPARE(item->packedSize.value, static_cast<unsigned long long>(89));
        QCOMPARE(item->hostOS, "Windows");
        QCOMPARE(item->checksum, "884863D2");

        QVERIFY(item = getItem(items, "files/blob1kb"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::File));

        QCOMPARE(item->modified.toSecsSinceEpoch(), static_cast<qint64>(1592312629));
        QCOMPARE(item->created.toSecsSinceEpoch(), static_cast<qint64>(1592905567));
        QCOMPARE(item->accessed.toSecsSinceEpoch(), static_cast<qint64>(1592905567));

        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(1000));
        QCOMPARE(item->packedSize.value, static_cast<unsigned long long>(892));
        QCOMPARE(item->hostOS, "Windows");
        QCOMPARE(item->checksum, "C8C2CC73");
    };


    QTest::newRow(path)
            << path
            << propRarMultiPart
            << ArchiveItemChecker(checkerRarNewMultipart);
}


void BackendHandlerRead::readSuccess()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(BackendArchiveProperties, requiredProperties);
    QFETCH(ArchiveItemChecker, itemChecker);

    TestArchiveReadParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.requiredProperties = requiredProperties;
    params.itemChecker = itemChecker;

    testArchiveRead(params);
}


void BackendHandlerRead::readError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const char* path = "../../data/zip/missing_part.scheme1.zip";
    QTest::newRow(path)
            << path
            << BackendHandlerError::DataError;

    path = "../../data/rar/missing_part.rar_new.part01.rar";
    // No errors for missing part in rar4
    // There must be at least two parts to report this error for rar5
    QTest::newRow(path)
            << path
            << BackendHandlerError::DataError;
}


void BackendHandlerRead::readError()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveReadParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    testArchiveRead(params);
}


void BackendHandlerRead::readEncrypted_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QString>("password");
    QTest::addColumn<BackendArchiveProperties>("requiredProperties");
    QTest::addColumn<ArchiveItemChecker>("itemChecker");

    const auto checkerEncrypted = [] (const ArchiveItemMap& items)
    {
        for (const auto& it : items)
        {
            const auto* item = &it.second;
            QVERIFY2(item->encrypted.value() == true, item->name.toLatin1());
        }
    };

    const std::initializer_list<QLatin1String> fullyEncryptedPaths
    {
        QLatin1String("../../data/rar/encrypted.solid.rar_new.rar"),
                QLatin1String("../../data/rar/encrypted.non_solid.rar_old.rar"),
    };

    for (const QLatin1String& path : fullyEncryptedPaths)
        QTest::newRow(path.data())
                << path.data()
                << validPassword
                << BackendArchiveProperties{}
                << ArchiveItemChecker{checkerEncrypted};

    const BackendArchiveProperties propEncryptedContent =
    { {BackendArchiveProperty::EncryptionState, QVariant::fromValue(EncryptionState::FilesContentOnly)} };

    const auto checkerEncryptedContent = [] (const ArchiveItemMap& items)
    {
        for (const auto& it : items)
        {
            const auto* item = &it.second;
            if (item->nodeType != NodeType::Folder)
                QVERIFY2(item->encrypted.value() == true, item->name.toLatin1());
        }
    };

    const std::initializer_list<QLatin1String> contentEncryptedPaths
    {
        QLatin1String("../../data/rar/encrypted_content.rar_new.rar"),
                QLatin1String("../../data/rar/encrypted_content.rar_old.rar"),
    };

    for (const QLatin1String& path : contentEncryptedPaths)
        QTest::newRow(path.data())
                << path.data()
                << ""
                << propEncryptedContent
                << ArchiveItemChecker{checkerEncryptedContent};
}


void BackendHandlerRead::readEncrypted()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(QString, password);
    QFETCH(BackendArchiveProperties, requiredProperties);
    QFETCH(ArchiveItemChecker, itemChecker);

    TestArchiveReadParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.password = password;
    params.requiredProperties = requiredProperties;
    params.itemChecker = itemChecker;

    if (password.isEmpty())
    {
        const bool encryptionSpecified = params.requiredProperties
                .find(BackendArchiveProperty::EncryptionState) != params.requiredProperties.end();

        if (!encryptionSpecified)
            params.requiredProperties[BackendArchiveProperty::EncryptionState]
                    = QVariant::fromValue(EncryptionState::Disabled);

        testArchiveRead(params);
    }
    else
    {
        params.requiredProperties[BackendArchiveProperty::EncryptionState]
                = QVariant::fromValue(EncryptionState::EntireArchive);

        // Read with password provided in the command-line
        testArchiveRead(params);

        // BackendHandlers must be used only once
        BackendHandler handler2;

        params.handler = &handler2;
        params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

        // Read with password provided in the prompt
        testArchiveRead(params);
    }
}


void BackendHandlerRead::readEncryptedError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QString>("password");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const std::initializer_list<QLatin1String> paths
    {
        QLatin1String("../../data/rar/corrupted.encrypted.rar_new.rar"),
    };

    for (const auto& path : paths)
        QTest::newRow(path.data())
                << path.data()
                << invalidPassword
                << BackendHandlerError::InvalidPasswordOrDataError;
}


void BackendHandlerRead::readEncryptedError()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(QString, password);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveReadParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.password = password;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    if (!password.isEmpty())
    {
        params.requiredProperties[BackendArchiveProperty::EncryptionState]
                = QVariant::fromValue(EncryptionState::EntireArchive);

        testArchiveRead(params);
    }
    else
    {
        testArchiveRead(params);

        // BackendHandlers must be used only once
        BackendHandler handler2;

        params.handler = &handler2;
        params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

        // Read with password provided in the prompt
        testArchiveRead(params);
    }
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::Unarchiver::BackendHandlerRead)

#include "tst_backendhandlerread.moc"
