// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>
#include <QDirIterator>

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"

#include "src/plugin-zpaq-core/backendhandler.h"

#include "tests/tests-shared/testarchiveextract.h"
#include "tests/tests-shared/addtestfiles.h"
#include "tests/tests-shared/passwords.h"
#include "tests/tests-shared/passwordtestmode.h"


using namespace Packuru::Core;
using namespace Packuru::Tests::Shared;
using Packuru::Plugins::ZPAQ::Core::BackendHandler;


namespace Packuru::Tests::Plugins::ZPAQ
{

class BackendHandlerExtract : public QObject
{
    Q_OBJECT

public:
    BackendHandlerExtract();
    ~BackendHandlerExtract();

private slots:
    void initTestCase();

    void extractEntireArchive_data();
    void extractEntireArchive();

    void extractSelectedFiles_data();
    void extractSelectedFiles();

    void extractError_data();
    void extractError();

    void extractEncrypted_data();
    void extractEncrypted();

    void extractEncryptedError_data();
    void extractEncryptedError();

    void extractEncryptedAbort_data();
    void extractEncryptedAbort();

private:
    const QString dataDir = QFINDTESTDATA("../../data/tar");
};


BackendHandlerExtract::BackendHandlerExtract()
{

}


BackendHandlerExtract::~BackendHandlerExtract()
{

}


void BackendHandlerExtract::initTestCase()
{
    if (!BackendHandler::executableAvailable())
        QSKIP("Executable not found");
}


void BackendHandlerExtract::extractEntireArchive_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QFileInfoChecker>("itemChecker");

    const auto checker1 = [] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), static_cast<std::size_t>(4));

        const QFileInfo* item = getItem(map, "files");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "files/file1.txt"));
        QCOMPARE(item->size(), 4);

        QVERIFY(item = getItem(map, "files/file2.txt"));
        QCOMPARE(item->size(), 4);
    };

    const char* path = "../../data/zpaq/files.zpaq";
    QTest::newRow(path)
            << path
            << QFileInfoChecker(checker1);

    const auto checker2 = [] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), static_cast<std::size_t>(5));

        const QFileInfo* item = getItem(map, "tmp");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "tmp/files"));
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "tmp/files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "tmp/files/file1.txt"));
        QCOMPARE(item->size(), 4);

        QVERIFY(item = getItem(map, "tmp/files/file2.txt"));
        QCOMPARE(item->size(), 4);
    };

    path = "../../data/zpaq/unix_absolute_paths.zpaq";
    QTest::newRow(path)
            << path
            << QFileInfoChecker(checker2);
}


void BackendHandlerExtract::extractEntireArchive()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(QFileInfoChecker, itemChecker);

    TestArchiveExtractParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.itemChecker = itemChecker;

    testArchiveExtract(params);
}


void BackendHandlerExtract::extractSelectedFiles_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<std::vector<QString>>("filesToExtract");
    QTest::addColumn<QFileInfoChecker>("itemChecker");

    const std::vector<QString> files1
    {
        "files/blob100b",
        "files/file1.txt"
    };

    const auto checker1 = [totalItems = files1.size() + 1] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), totalItems);

        const QFileInfo* item = getItem(map, "files");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "files/file1.txt"));
        QCOMPARE(item->size(), 4);
    };

    const char* path = "../../data/zpaq/files.zpaq";
    QTest::newRow(path)
            << path
            << files1
            << QFileInfoChecker(checker1);

    const std::vector<QString> files2
    {
        "/tmp/files/blob100b",
        "/tmp/files/file1.txt"
    };

    const auto checkerAbsolutePaths = [] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), static_cast<std::size_t>(4));

        const QFileInfo* item = getItem(map, "tmp");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "tmp/files"));
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "tmp/files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "tmp/files/file1.txt"));
        QCOMPARE(item->size(), 4);
    };

    path = "../../data/zpaq/unix_absolute_paths.zpaq";
    QTest::newRow(path)
            << path
            << files2
            << QFileInfoChecker(checkerAbsolutePaths);
}


void BackendHandlerExtract::extractSelectedFiles()
{
    BackendHandler handler;
    QFETCH(QString, archivePath);
    QFETCH(std::vector<QString>, filesToExtract);
    QFETCH(QFileInfoChecker, itemChecker);

    TestArchiveExtractParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.filesToExtract = filesToExtract;
    params.itemChecker = itemChecker;

    testArchiveExtract(params);
    qDebug()<<handler.clearLog();
}


void BackendHandlerExtract::extractError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const auto insp = [] (QTestData& data)
    { data << BackendHandlerError::DataError; };

    addTestFiles(dataDir, QRegularExpression("corrupted.\\d+.zpaq"), insp);

    const char* path = "../../data/zpaq/truncated.zpaq";
    QTest::newRow(path)
            << path
            << BackendHandlerError::DataError;
}


void BackendHandlerExtract::extractError()
{
    BackendHandler handler;
    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveExtractParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    testArchiveExtract(params);
}


void BackendHandlerExtract::extractEncrypted_data()
{
    QTest::addColumn<QString>("archivePath");

    const char* path = "../../data/zpaq/encrypted.zpaq";
    QTest::newRow(path)
            << path;
}


void BackendHandlerExtract::extractEncrypted()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);

    TestArchiveExtractParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.password = validPassword;

    // Test with password provided in the command-line
    testArchiveExtract(params);

    // BackendHandlers must be used only once
    BackendHandler handler2;

    params.handler = &handler2;
    params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

    // Test with password provided in the prompt
    testArchiveExtract(params);
}


void BackendHandlerExtract::extractEncryptedError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QString>("password");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const char* path = "../../data/zpaq/encrypted.zpaq";
    QTest::newRow(path)
            << path
            << invalidPassword
            << BackendHandlerError::InvalidPasswordOrDataError;

    path = "../../data/zpaq/corrupted.encrypted.1.zpaq";
    QTest::newRow(path)
            << path
            << validPassword
            << BackendHandlerError::InvalidPasswordOrDataError;

    path = "../../data/zpaq/corrupted.encrypted.2.zpaq";
    QTest::newRow(path)
            << path
            << validPassword
            << BackendHandlerError::DataError;
}


void BackendHandlerExtract::extractEncryptedError()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(QString, password);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveExtractParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.password = password;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    // Test with password provided in the command-line
    testArchiveExtract(params);

    // BackendHandlers must be used only once
    BackendHandler handler2;

    params.handler = &handler2;
    params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

    // Test with password provided in the prompt
    testArchiveExtract(params);
}


void BackendHandlerExtract::extractEncryptedAbort_data()
{
    QTest::addColumn<QString>("archivePath");

    const char* path = "../../data/zpaq/encrypted.zpaq";
    QTest::newRow(path)
            << path;
}


void BackendHandlerExtract::extractEncryptedAbort()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);

    TestArchiveExtractParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.passwordTestMode = PasswordTestMode::PromptAbort;
    params.expectedEndState = BackendHandlerState::Aborted;

    testArchiveExtract(params);
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::ZPAQ::BackendHandlerExtract)

#include "tst_backendhandlerextract.moc"
