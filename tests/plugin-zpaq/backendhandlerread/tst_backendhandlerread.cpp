// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <unordered_map>

#include <QtTest>
#include <QDirIterator>

#include "src/utils/private/qttypehasher.h"
#include "src/utils/private/unordered_map_utils.h"

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/plugin-api/backendarchiveproperties.h"
#include "src/core/private/plugin-api/backendarchiveproperty.h"
#include "src/core/private/plugin-api/encryptionstate.h"

#include "src/plugin-zpaq-core/backendhandler.h"

#include "tests/tests-shared/testarchiveread.h"
#include "tests/tests-shared/addtestfiles.h"
#include "tests/tests-shared/createmultipartproperties.h"
#include "tests/tests-shared/passwords.h"
#include "tests/tests-shared/passwordtestmode.h"


using namespace Packuru::Core;
using namespace Packuru::Tests::Shared;
using Packuru::Plugins::ZPAQ::Core::BackendHandler;


namespace Packuru::Tests::Plugins::ZPAQ
{


class  BackendHandlerRead : public QObject
{
    Q_OBJECT

public:
    BackendHandlerRead();
    ~BackendHandlerRead() override;

private slots:
    void initTestCase();

    void readArchive_data();
    void readArchive();

    void readError_data();
    void readError();

    void readEncrypted_data();
    void readEncrypted();

    void readEncryptedError_data();
    void readEncryptedError();

    void readEncryptedAbort_data();
    void readEncryptedAbort();

private:
    const QString dataDir = QFINDTESTDATA("../../data/zpaq");
};


BackendHandlerRead::BackendHandlerRead()
{

}


BackendHandlerRead::~BackendHandlerRead()
{

}


void BackendHandlerRead::initTestCase()
{
    if (!BackendHandler::executableAvailable())
        QSKIP("Executable not found");
}


void BackendHandlerRead::readArchive_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendArchiveProperties>("requiredProperties");
    QTest::addColumn<ArchiveItemChecker>("itemChecker");

    const auto itemChecker = [] (const ArchiveItemMap& items)
    {
        QCOMPARE(items.size(), static_cast<std::size_t>(4));

        for (const auto& it : items)
        {
            const ArchiveItem& item = it.second;
            QCOMPARE(item.encrypted, false);
        }

        const auto* item = getItem(items, "files/");
        QVERIFY(item);
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::Folder));

        QVERIFY(item = getItem(items, "files/blob100b"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::File));
        QCOMPARE(item->modified.toMSecsSinceEpoch(), 1593517636000);
        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(100));

        QVERIFY(item = getItem(items, "files/file1.txt"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::File));
        QCOMPARE(item->modified.toMSecsSinceEpoch(), 1593779134000);
        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(4));
    };

    BackendArchiveProperties properties {
        {BackendArchiveProperty::EncryptionState, QVariant::fromValue(EncryptionState::Disabled)} };

    const char* path = "../../data/zpaq/files.zpaq";
    QTest::newRow(path)
            << path
            << properties
            << ArchiveItemChecker(itemChecker);

    const auto itemCheckerAbs = [] (const ArchiveItemMap& items)
    {
        QCOMPARE(items.size(), static_cast<std::size_t>(4));

        for (const auto& it : items)
        {
            const ArchiveItem& item = it.second;
            QCOMPARE(item.encrypted, false);
        }

        const auto* item = getItem(items, "/tmp/files/");
        QVERIFY(item);
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::Folder));

        QVERIFY(item = getItem(items, "/tmp/files/blob100b"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::File));
        QCOMPARE(item->modified.toMSecsSinceEpoch(), 1593517636000);
        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(100));

        QVERIFY(item = getItem(items, "/tmp/files/file1.txt"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::File));
        QCOMPARE(item->modified.toMSecsSinceEpoch(), 1593779134000);
        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(4));
    };

    path = "../../data/zpaq/unix_absolute_paths.zpaq";
    QTest::newRow(path)
            << path
            << properties
            << ArchiveItemChecker(itemCheckerAbs);
}


void BackendHandlerRead::readArchive()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(BackendArchiveProperties, requiredProperties);
    QFETCH(ArchiveItemChecker, itemChecker);

    TestArchiveReadParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.requiredProperties = requiredProperties;
    params.itemChecker = itemChecker;

    testArchiveRead(params);
}


void BackendHandlerRead::readError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const char* path = "../../data/zpaq/corrupted.5.zpaq";
    QTest::newRow(path)
            << path
            << BackendHandlerError::DataError;

    path = "../../data/zpaq/truncated.zpaq";
    QTest::newRow(path)
            << path
            << BackendHandlerError::DataError;
}


void BackendHandlerRead::readError()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveReadParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    testArchiveRead(params);
}


void BackendHandlerRead::readEncrypted_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendArchiveProperties>("requiredProperties");
    QTest::addColumn<ArchiveItemChecker>("itemChecker");

    BackendArchiveProperties properties {
        {BackendArchiveProperty::EncryptionState, QVariant::fromValue(EncryptionState::EntireArchive)} };

    const auto checker = [] (const ArchiveItemMap& items)
    {
        for (const auto& it : items)
        {
            const auto* item = &it.second;
            QVERIFY2(item->encrypted.value() == true, item->name.toLatin1());
        }
    };

    const char* path = "../../data/zpaq/encrypted.zpaq";
    QTest::newRow(path)
            << path
            << properties
            << ArchiveItemChecker{checker};
}


void BackendHandlerRead::readEncrypted()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(BackendArchiveProperties, requiredProperties);
    QFETCH(ArchiveItemChecker, itemChecker);

    TestArchiveReadParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.password = validPassword;
    params.requiredProperties = requiredProperties;
    params.itemChecker = itemChecker;

    params.requiredProperties[BackendArchiveProperty::EncryptionState]
            = QVariant::fromValue(EncryptionState::EntireArchive);

    // Read with password provided in the command-line
    testArchiveRead(params);

    // BackendHandlers must be used only once
    BackendHandler handler2;

    params.handler = &handler2;
    params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

    // Read with password provided in the prompt
    testArchiveRead(params);
}


void BackendHandlerRead::readEncryptedError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QString>("password");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const char* path = "../../data/zpaq/encrypted.zpaq";
    QTest::newRow(path)
            << path
            << invalidPassword
            << BackendHandlerError::InvalidPasswordOrDataError;

    path = "../../data/zpaq/corrupted.encrypted.1.zpaq";
    QTest::newRow(path)
            << path
            << validPassword
            << BackendHandlerError::InvalidPasswordOrDataError;
}


void BackendHandlerRead::readEncryptedError()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(QString, password);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveReadParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.password = password;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    params.requiredProperties[BackendArchiveProperty::EncryptionState]
            = QVariant::fromValue(EncryptionState::EntireArchive);

    testArchiveRead(params);

    // BackendHandlers must be used only once
    BackendHandler handler2;

    params.handler = &handler2;
    params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

    // Read with password provided in the prompt
    testArchiveRead(params);
}


void BackendHandlerRead::readEncryptedAbort_data()
{
    QTest::addColumn<QString>("archivePath");

    const char* path = "../../data/zpaq/encrypted.zpaq";
    QTest::newRow(path)
            << path;
}


void BackendHandlerRead::readEncryptedAbort()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);

    TestArchiveReadParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.passwordTestMode = PasswordTestMode::PromptAbort;
    params.expectedEndState = BackendHandlerState::Aborted;

    testArchiveRead(params);
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::ZPAQ::BackendHandlerRead)

#include "tst_backendhandlerread.moc"
