// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>
#include <QThread>

#include "src/utils/qvariant_utils.h"
#include "src/utils/private/unordered_map_utils.h"

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/plugin-api/archivingfilepaths.h"
#include "src/core/private/plugin-api/archivetype.h"
#include "src/core/private/plugin-api/archivingcompressioninfo.h"
#include "src/core/private/plugin-api/encryptionstate.h"

#include "src/plugin-zpaq-core/backendhandler.h"
#include "src/plugin-zpaq-core/privatearchivingparametermapzpaq.h"
#include "src/plugin-zpaq-core/privatearchivingparameter.h"

#include "tests/tests-shared/testarchiveadd.h"
#include "tests/tests-shared/passwords.h"


using namespace Packuru::Utils;
using namespace Packuru::Core;
using namespace Packuru::Plugins::ZPAQ::Core;
using namespace Packuru::Tests::Shared;


namespace
{
// Check whether parameters passed to BackendHandler are reflected in command-line
// arguments of the backend process
void checkArguments(const TestArchiveAddParams& testParams, const QStringList& processArgs);

const int threadCount = QThread::idealThreadCount() / 2;
}


namespace Packuru::Tests::Plugins::ZPAQ
{

class BackendHandlerAdd : public QObject
{
    Q_OBJECT

public:
    BackendHandlerAdd();
    ~BackendHandlerAdd();

private slots:
    void initTestCase();

    void createNewArchive_data();
    void createNewArchive();

    void createNewEncrypted();

    void addToArchive_data();
    void addToArchive();

private:
    const std::vector<QFileInfo> inputItems = {QFINDTESTDATA("../../data/files/")};

};


BackendHandlerAdd::BackendHandlerAdd()
{

}


BackendHandlerAdd::~BackendHandlerAdd()
{

}


void BackendHandlerAdd::initTestCase()
{
    if (!BackendHandler::executableAvailable())
        QSKIP("Executable not found");
}


void BackendHandlerAdd::createNewArchive_data()
{
    QTest::addColumn<ArchivingFilePaths>("filePaths");
    QTest::addColumn<PrivateArchivingParameterMap>("privateParams");

    PrivateArchivingParameterMap privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel] = QVariant::fromValue(0);

    privateParams[PrivateArchivingParameter::FileAttributes] = QVariant::fromValue(false);

    privateParams[PrivateArchivingParameter::ThreadCount] = threadCount;

    QTest::newRow("archive1")
            << ArchivingFilePaths::RelativePaths
            << privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(5);

    privateParams[PrivateArchivingParameter::FileAttributes]
            = QVariant::fromValue(true);

    QTest::newRow("archive2")
            << ArchivingFilePaths::FullPaths
            << privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(1);

    privateParams[PrivateArchivingParameter::FileAttributes]
            = QVariant::fromValue(true);

    QTest::newRow("archive3")
            << ArchivingFilePaths::AbsolutePaths
            << privateParams;
}


void BackendHandlerAdd::createNewArchive()
{
    BackendHandler handler;

    QFETCH(ArchivingFilePaths, filePaths);
    QFETCH(PrivateArchivingParameterMap, privateParams);

    TestArchiveAddParams params;
    params.handler = &handler;
    params.archiveType = ArchiveType::zpaq;
    params.inputItems = inputItems;
    params.filePaths = filePaths;
    params.createNewArchive = true;
    params.pluginPrivateData = QVariant::fromValue(privateParams);

    testArchiveAdd(params);

    const auto args = handler.getBackendArgs();
    QCOMPARE(args.size(), static_cast<std::size_t>(1));
    checkArguments(params, args[0]);
}


void BackendHandlerAdd::createNewEncrypted()
{
    BackendHandler handler;

    PrivateArchivingParameterMap privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel] = QVariant::fromValue(5);

    privateParams[PrivateArchivingParameter::FileAttributes] = QVariant::fromValue(true);

    privateParams[PrivateArchivingParameter::ThreadCount] = threadCount;

    TestArchiveAddParams params;
    params.handler = &handler;
    params.archiveType = ArchiveType::zpaq;
    params.password = validPassword;
    params.inputItems = inputItems;
    params.filePaths = ArchivingFilePaths::FullPaths;
    params.createNewArchive = true;
    params.pluginPrivateData = QVariant::fromValue(privateParams);

    testArchiveAdd(params);

    const auto args = handler.getBackendArgs();
    QCOMPARE(args.size(), static_cast<std::size_t>(1));

    checkArguments(params, args.front());
}


void BackendHandlerAdd::addToArchive_data()
{
    QTest::addColumn<QString>("archiveName");
    QTest::addColumn<EncryptionState>("currentArchiveEncryption");
    QTest::addColumn<EncryptionState>("newArchiveEncryption");

    QTest::newRow("add")
            << "files.zpaq"
            << EncryptionState::Disabled
            << EncryptionState::Disabled;

    QTest::newRow("add_and_encrypt")
            << "files.zpaq"
            << EncryptionState::Disabled
            << EncryptionState::EntireArchive;

    QTest::newRow("add_to_encrypted")
            << "encrypted.zpaq"
            << EncryptionState::EntireArchive
            << EncryptionState::EntireArchive;
}


void BackendHandlerAdd::addToArchive()
{
    BackendHandler handler;

    QFETCH(QString, archiveName);
    QFETCH(EncryptionState, currentArchiveEncryption);
    QFETCH(EncryptionState, newArchiveEncryption);

    PrivateArchivingParameterMap privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel] = QVariant::fromValue(5);

    privateParams[PrivateArchivingParameter::FileAttributes] = QVariant::fromValue(true);

    privateParams[PrivateArchivingParameter::ThreadCount] = threadCount;

    TestArchiveAddParams params;
    params.handler = &handler;
    params.archiveType = ArchiveType::zpaq;
    params.existingArchivePath = QFINDTESTDATA("../../data/zpaq/" + archiveName);
    params.inputItems = inputItems;
    params.filePaths = ArchivingFilePaths::FullPaths;
    params.pluginPrivateData = QVariant::fromValue(privateParams);
    params.currentArchiveEncryption = currentArchiveEncryption;
    params.newArchiveEncryption = newArchiveEncryption;
    if (currentArchiveEncryption != EncryptionState::Disabled
            || newArchiveEncryption != EncryptionState::Disabled)
        params.password = validPassword;

    testArchiveAdd(params);

    const auto args = handler.getBackendArgs();
    // When adding files, the first invocation repacks and encrypts the archive
    // and the second adds files to this temporary/encrypted archive. The repack step is not checked.
    QCOMPARE(args.size(), static_cast<std::size_t>(2));
    checkArguments(params, args[1]);
}

}


namespace
{

void checkArguments(const TestArchiveAddParams& testParams, const QStringList& processArgs)
{
    QVERIFY(!processArgs.isEmpty());

    const auto privateData = getValueAs<PrivateArchivingParameterMap>(testParams.pluginPrivateData);

    const auto compressionLevel
            = getValueAs<int>(getValue(privateData,
                                       PrivateArchivingParameter::CompressionLevel));

    QVERIFY2(processArgs.contains(QLatin1String("-t") + QString::number(threadCount)),
             processArgs.join(" ").toLatin1());

    QVERIFY2(processArgs.contains(QLatin1String("-m") + QString::number(compressionLevel)),
             processArgs.join(" ").toLatin1());

    const auto storeAttributes
            = getValueAs<bool>(getValue(privateData,
                                        PrivateArchivingParameter::FileAttributes));

    QVERIFY2(processArgs.contains(QLatin1String("-noattributes")) != storeAttributes,
             processArgs.join(" ").toLatin1());

    if (!testParams.password.isEmpty())
    {
        QVERIFY2(processArgs.contains(QLatin1String("-key")),
                 processArgs.join(" ").toLatin1());

        QVERIFY2(processArgs.contains(testParams.password),
                 processArgs.join(" ").toLatin1());
    }

}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::ZPAQ::BackendHandlerAdd)

#include "tst_backendhandleradd.moc"
