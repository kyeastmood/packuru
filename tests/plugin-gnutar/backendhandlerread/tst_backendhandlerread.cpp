// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <unordered_map>

#include <QtTest>
#include <QDirIterator>

#include "src/utils/private/qttypehasher.h"
#include "src/utils/private/unordered_map_utils.h"

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/plugin-api/backendarchiveproperties.h"
#include "src/core/private/plugin-api/backendarchiveproperty.h"
#include "src/core/private/archivetypedetector.h"

#include "src/plugin-gnutar-core/backendhandler.h"
#include "src/plugin-gnutar-core/compressionhandlertype.h"
#include "src/plugin-gnutar-core/createcompressionhandler.h"
#include "src/plugin-gnutar-core/privatereadparameter.h"
#include "src/plugin-gnutar-core/privatereadparametermapgnutar.h"

#include "tests/tests-shared/testarchiveread.h"
#include "tests/tests-shared/addtestfiles.h"
#include "tests/tests-shared/createmultipartproperties.h"


using namespace Packuru::Core;
using namespace Packuru::Tests::Shared;
using namespace Packuru::Plugins::GnuTar::Core;


namespace Packuru::Tests::Plugins::GnuTar
{


class BackendHandlerRead : public QObject
{
    Q_OBJECT

public:
    BackendHandlerRead();
    ~BackendHandlerRead() override;

private slots:
    void readArchive_data();
    void readArchive();

    void readError_data();
    void readError();

private:
    const QString dataDir = QFINDTESTDATA("../../data/tar");
    ArchiveTypeDetector typeDetector;
};


BackendHandlerRead::BackendHandlerRead()
{

}


BackendHandlerRead::~BackendHandlerRead()
{

}


void BackendHandlerRead::readArchive_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendArchiveProperties>("requiredProperties");
    QTest::addColumn<ArchiveItemChecker>("itemChecker");

    const auto itemChecker = [] (const ArchiveItemMap& items)
    {
        QCOMPARE(items.size(), static_cast<std::size_t>(5));

        for (const auto& it : items)
        {
            const ArchiveItem& item = it.second;
            QCOMPARE(item.encrypted, false);
            QCOMPARE(item.user, "user");
            QCOMPARE(item.group, "user");
        }

        const auto* item = getItem(items, "files");
        QVERIFY(item);
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::Folder));

        QVERIFY(item = getItem(items, "files/blob100b"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::File));
        QCOMPARE(item->modified.toMSecsSinceEpoch(), 1593517620000);
        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(100));

        QVERIFY(item = getItem(items, "files/file1.txt"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::File));
        QCOMPARE(item->modified.toMSecsSinceEpoch(), 1593779100000);
        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(4));

        QVERIFY(item = getItem(items, "files/link_to_dev_null"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::Link));
        QCOMPARE(item->modified.toMSecsSinceEpoch(), 1592826720000);
        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(0));
    };

    const auto insp = [itemChecker = itemChecker] (QTestData& data)
    { data << BackendArchiveProperties()
           << ArchiveItemChecker(itemChecker); };

    addTestFiles(dataDir, "files.tar*", insp);

    QString path = QFINDTESTDATA("../../data/tar/multipart.tar.lz00001.lz");
    QTest::newRow(path.toLatin1())
            << path
            << createMultiPartProperties(1, 618)
            << ArchiveItemChecker(itemChecker);

    const auto itemCheckerAbs = [] (const ArchiveItemMap& items)
    {
        QCOMPARE(items.size(), static_cast<std::size_t>(5));

        for (const auto& it : items)
        {
            const ArchiveItem& item = it.second;
            QCOMPARE(item.encrypted, false);
            QCOMPARE(item.user, "user");
            QCOMPARE(item.group, "user");
        }

        const auto* item = getItem(items, "/tmp/files");
        QVERIFY(item);
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::Folder));

        QVERIFY(item = getItem(items, "/tmp/files/blob100b"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::File));
        QCOMPARE(item->modified.toMSecsSinceEpoch(), 1593517620000);
        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(100));

        QVERIFY(item = getItem(items, "/tmp/files/file1.txt"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::File));
        QCOMPARE(item->modified.toMSecsSinceEpoch(), 1593779100000);
        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(4));

        QVERIFY(item = getItem(items, "/tmp/files/link_to_dev_null"));
        QCOMPARE(toString(item->nodeType.value), toString(NodeType::Link));
        QCOMPARE(item->modified.toMSecsSinceEpoch(), 1596113280000);
        QCOMPARE(item->originalSize.value, static_cast<unsigned long long>(0));
    };

    path = QFINDTESTDATA("../../data/tar/unix_absolute_paths.tar");
    QTest::newRow(path.toLatin1())
            << path
            << BackendArchiveProperties()
            << ArchiveItemChecker(itemCheckerAbs);
}


void BackendHandlerRead::readArchive()
{
    QFETCH(QString, archivePath);
    QFETCH(BackendArchiveProperties, requiredProperties);
    QFETCH(ArchiveItemChecker, itemChecker);

    if (!BackendHandler::isTarExecAvailable())
        QSKIP("Tar executable not found");

    const auto archiveType = typeDetector.detectType(archivePath);

    const std::vector<CompressionHandlerExecInfo> handlerList = getCompressionHandlers(archiveType,
                                                                                       CompressionHandlerPickMode::AllSupported);

    for (const auto& info : handlerList)
    {
        if (!info.available)
        {
            QWARN("Executable not found: " + info.execName.toLatin1());
            continue;
        }

        BackendHandler handler;

        TestArchiveReadParams params;
        params.handler = &handler;
        params.archivePath = archivePath;

        PrivateReadParameterMap privateParams;
        privateParams[PrivateReadParameter::CompressionHandlerType] = QVariant::fromValue(info.handlerType);
        params.pluginPrivateData = QVariant::fromValue(privateParams);

        params.requiredProperties = requiredProperties;
        params.itemChecker = itemChecker;

        testArchiveRead(params);
    }
}


void BackendHandlerRead::readError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const auto insp = [] (QTestData& data)
    { data << BackendHandlerError::DataError; };

    addTestFiles(dataDir, "corrupted*.tar*", insp);
    addTestFiles(dataDir, "empty.tar*", insp);
    addTestFiles(dataDir, "truncated.tar*", insp);

    QString path = QFINDTESTDATA("../../data/tar/encrypted.tar.lrz");
    QTest::newRow(path.toLatin1())
            << path
            << BackendHandlerError::ArchivingMethodNotSupported;
}


void BackendHandlerRead::readError()
{
    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    if (!BackendHandler::isTarExecAvailable())
        QSKIP("Tar executable not found");

    const auto archiveType = typeDetector.detectType(archivePath);

    const std::vector<CompressionHandlerExecInfo> handlerList = getCompressionHandlers(archiveType,
                                                                                       CompressionHandlerPickMode::AllSupported);

    for (const auto& info : handlerList)
    {
        if (!info.available)
        {
            QWARN("Executable not found: " + info.execName.toLatin1());
            continue;
        }

        BackendHandler handler;

        TestArchiveReadParams params;
        params.handler = &handler;
        params.archivePath = archivePath;

        PrivateReadParameterMap privateParams;
        privateParams[PrivateReadParameter::CompressionHandlerType] = QVariant::fromValue(info.handlerType);
        params.pluginPrivateData = QVariant::fromValue(privateParams);

        params.expectedEndState = BackendHandlerState::Errors;
        params.expectedError = expectedError;

        testArchiveRead(params);
    }
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::GnuTar::BackendHandlerRead)

#include "tst_backendhandlerread.moc"
