// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>
#include <QDirIterator>
#include <QDebug>

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/archivetypedetector.h"

#include "src/plugin-gnutar-core/backendhandler.h"
#include "src/plugin-gnutar-core/compressionhandlertype.h"
#include "src/plugin-gnutar-core/createcompressionhandler.h"
#include "src/plugin-gnutar-core/privatetestparameter.h"
#include "src/plugin-gnutar-core/privatetestparametermapgnutar.h"

#include "tests/tests-shared/testarchivetest.h"
#include "tests/tests-shared/addtestfiles.h"


using namespace Packuru::Core;
using namespace Packuru::Tests::Shared;
using namespace Packuru::Plugins::GnuTar::Core;


namespace Packuru::Tests::Plugins::GnuTar
{

class BackendHandlerTest : public QObject
{
    Q_OBJECT

public:
    BackendHandlerTest();
    ~BackendHandlerTest();

private slots:
    void testArchive_data();
    void testArchive();

    void testError_data();
    void testError();

private:
    const QString dataDir = QFINDTESTDATA("../../data/tar");
    ArchiveTypeDetector typeDetector;
};


BackendHandlerTest::BackendHandlerTest()
{

}


BackendHandlerTest::~BackendHandlerTest()
{

}


void BackendHandlerTest::testArchive_data()
{
    QTest::addColumn<QString>("archivePath");

    addTestFiles(dataDir, "files.tar*");

    const QString path = QFINDTESTDATA("../../data/tar/multipart.tar.lz00001.lz");
    QTest::newRow(path.toLatin1())
            << path;
}


void BackendHandlerTest::testArchive()
{
    QFETCH(QString, archivePath);

    if (!BackendHandler::isTarExecAvailable())
        QSKIP("Tar executable not found");

    const auto archiveType = typeDetector.detectType(archivePath);

    const std::vector<CompressionHandlerExecInfo> handlerList = getCompressionHandlers(archiveType,
                                                                                       CompressionHandlerPickMode::AllSupported);

    for (const auto& info : handlerList)
    {
        if (!info.available)
        {
            QWARN("Executable not found: " + info.execName.toLatin1());
            continue;
        }

        BackendHandler handler;

        TestArchiveTestParams params;
        params.handler = &handler;
        params.archivePath = archivePath;

        PrivateTestParameterMap privateParams;
        privateParams[PrivateTestParameter::CompressionHandlerType] = QVariant::fromValue(info.handlerType);
        params.pluginPrivateData = QVariant::fromValue(privateParams);

        testArchiveTest(params);
    }
}


void BackendHandlerTest::testError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const auto insp = [] (QTestData& data)
    { data << BackendHandlerError::DataError; };

    addTestFiles(dataDir, "corrupted*.tar*", insp);
    addTestFiles(dataDir, "empty.tar*", insp);
    addTestFiles(dataDir, "truncated.tar*", insp);

    QString path = QFINDTESTDATA("../../data/tar/encrypted.tar.lrz");
    QTest::newRow(path.toLatin1())
            << path
            << BackendHandlerError::ArchivingMethodNotSupported;

    // Archive contains paths containing ../
    path = QFINDTESTDATA("../../data/tar/malicious.tar");
    QTest::newRow(path.toLatin1())
            << path
            << BackendHandlerError::___INVALID; // No error type defined yet
}


void BackendHandlerTest::testError()
{
    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    if (!BackendHandler::isTarExecAvailable())
        QSKIP("Tar executable not found");

    const auto archiveType = typeDetector.detectType(archivePath);

    const std::vector<CompressionHandlerExecInfo> handlerList = getCompressionHandlers(archiveType,
                                                                                       CompressionHandlerPickMode::AllSupported);

    for (const auto& info : handlerList)
    {
        if (!info.available)
        {
            QWARN("Executable not found: " + info.execName.toLatin1());
            continue;
        }

        BackendHandler handler;

        TestArchiveTestParams params;
        params.handler = &handler;
        params.archivePath = archivePath;

        PrivateTestParameterMap privateParams;
        privateParams[PrivateTestParameter::CompressionHandlerType] = QVariant::fromValue(info.handlerType);
        params.pluginPrivateData = QVariant::fromValue(privateParams);

        params.expectedEndState = BackendHandlerState::Errors;
        params.expectedError = expectedError;

        testArchiveTest(params);
    }
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::GnuTar::BackendHandlerTest)

#include "tst_backendhandlertest.moc"
