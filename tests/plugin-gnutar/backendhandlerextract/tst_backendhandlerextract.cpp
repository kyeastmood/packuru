// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>
#include <QDirIterator>

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/archivetypedetector.h"

#include "src/plugin-gnutar-core/backendhandler.h"
#include "src/plugin-gnutar-core/compressionhandlertype.h"
#include "src/plugin-gnutar-core/createcompressionhandler.h"
#include "src/plugin-gnutar-core/privateextractionparameter.h"
#include "src/plugin-gnutar-core/privateextractionparametermapgnutar.h"

#include "tests/tests-shared/testarchiveextract.h"
#include "tests/tests-shared/addtestfiles.h"


using namespace Packuru::Core;
using namespace Packuru::Tests::Shared;
using namespace Packuru::Plugins::GnuTar::Core;


namespace Packuru::Tests::Plugins::GnuTar
{

class BackendHandlerExtract : public QObject
{
    Q_OBJECT

public:
    BackendHandlerExtract();
    ~BackendHandlerExtract();

private slots:
    void extractEntireArchive_data();
    void extractEntireArchive();

    void extractSelectedFiles_data();
    void extractSelectedFiles();

    void extractError_data();
    void extractError();

private:
    const QString dataDir = QFINDTESTDATA("../../data/tar");
    ArchiveTypeDetector typeDetector;
};


BackendHandlerExtract::BackendHandlerExtract()
{

}


BackendHandlerExtract::~BackendHandlerExtract()
{

}


void BackendHandlerExtract::extractEntireArchive_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QFileInfoChecker>("itemChecker");

    const auto checker1 = [] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), static_cast<std::size_t>(5));

        const QFileInfo* item = getItem(map, "files");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "files/file1.txt"));
        QCOMPARE(item->size(), 4);

        QVERIFY(item = getItem(map, "files/file2.txt"));
        QCOMPARE(item->size(), 4);

        QVERIFY(item = getItem(map, "files/link_to_dev_null"));
        QCOMPARE(item->size(), 0);
    };

    const auto insp = [checker = checker1] (QTestData& data)
    { data << QFileInfoChecker(checker); };

    addTestFiles(dataDir, "files.tar*", insp);

    QString path = QFINDTESTDATA("../../data/tar/multipart.tar.lz00001.lz");
    QTest::newRow(path.toLatin1())
            << path
            << QFileInfoChecker(checker1);

    const auto checker2 = [] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), static_cast<std::size_t>(6));

        const QFileInfo* item = getItem(map, "tmp");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "tmp/files"));
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "tmp/files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "tmp/files/file1.txt"));
        QCOMPARE(item->size(), 4);

        QVERIFY(item = getItem(map, "tmp/files/file2.txt"));
        QCOMPARE(item->size(), 4);

        QVERIFY(item = getItem(map, "tmp/files/link_to_dev_null"));
        QCOMPARE(item->size(), 0);
    };

    path = QFINDTESTDATA("../../data/tar/unix_absolute_paths.tar");
    QTest::newRow(path.toLatin1())
            << path
            << QFileInfoChecker(checker2);
}


void BackendHandlerExtract::extractEntireArchive()
{    
    QFETCH(QString, archivePath);
    QFETCH(QFileInfoChecker, itemChecker);

    if (!BackendHandler::isTarExecAvailable())
        QSKIP("Tar executable not found");

    const auto archiveType = typeDetector.detectType(archivePath);

    const std::vector<CompressionHandlerExecInfo> handlerList = getCompressionHandlers(archiveType,
                                                                                       CompressionHandlerPickMode::AllSupported);

    for (const auto& info : handlerList)
    {
        if (!info.available)
        {
            QWARN("Executable not found: " + info.execName.toLatin1());
            continue;
        }

        BackendHandler handler;

        TestArchiveExtractParams params;
        params.handler = &handler;
        params.archivePath = archivePath;

        PrivateExtractionParameterMap privateParams;
        privateParams[PrivateExtractionParameter::CompressionHandlerType] = QVariant::fromValue(info.handlerType);
        params.pluginPrivateData = QVariant::fromValue(privateParams);

        params.itemChecker = itemChecker;

        testArchiveExtract(params);
    }
}


void BackendHandlerExtract::extractSelectedFiles_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<std::vector<QString>>("filesToExtract");
    QTest::addColumn<QFileInfoChecker>("itemChecker");

    const std::vector<QString> files1
    {
        "files/blob100b",
        "files/file1.txt"
    };

    const auto checker1 = [totalItems = files1.size() + 1] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), totalItems);

        const QFileInfo* item = getItem(map, "files");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "files/file1.txt"));
        QCOMPARE(item->size(), 4);
    };

    QString path = QFINDTESTDATA("../../data/tar/files.tar");
    QTest::newRow(path.toLatin1())
            << path
            << files1
            << QFileInfoChecker(checker1);

    const std::vector<QString> files2
    {
        "/tmp/files/blob100b",
        "/tmp/files/file1.txt"
    };

    const auto checkerAbsolutePaths = [] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), static_cast<std::size_t>(4));

        const QFileInfo* item = getItem(map, "tmp");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "tmp/files"));
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "tmp/files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "tmp/files/file1.txt"));
        QCOMPARE(item->size(), 4);
    };

    path = QFINDTESTDATA("../../data/tar/unix_absolute_paths.tar");
    QTest::newRow(path.toLatin1())
            << path
            << files2
            << QFileInfoChecker(checkerAbsolutePaths);
}


void BackendHandlerExtract::extractSelectedFiles()
{
    QFETCH(QString, archivePath);
    QFETCH(std::vector<QString>, filesToExtract);
    QFETCH(QFileInfoChecker, itemChecker);

    if (!BackendHandler::isTarExecAvailable())
        QSKIP("Tar executable not found");

    const auto archiveType = typeDetector.detectType(archivePath);

    const std::vector<CompressionHandlerExecInfo> handlerList = getCompressionHandlers(archiveType,
                                                                                       CompressionHandlerPickMode::AllSupported);

    for (const auto& info : handlerList)
    {
        if (!info.available)
        {
            QWARN("Executable not found: " + info.execName.toLatin1());
            continue;
        }

        BackendHandler handler;

        TestArchiveExtractParams params;
        params.handler = &handler;
        params.archivePath = archivePath;
        params.filesToExtract = filesToExtract;

        PrivateExtractionParameterMap privateParams;
        privateParams[PrivateExtractionParameter::CompressionHandlerType] = QVariant::fromValue(info.handlerType);
        params.pluginPrivateData = QVariant::fromValue(privateParams);

        params.itemChecker = itemChecker;

        testArchiveExtract(params);
    }
}


void BackendHandlerExtract::extractError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const auto insp = [] (QTestData& data)
    { data << BackendHandlerError::DataError; };

    addTestFiles(dataDir, "corrupted*.tar*", insp);
    addTestFiles(dataDir, "empty.tar*", insp);
    addTestFiles(dataDir, "truncated.tar*", insp);

    QString path = QFINDTESTDATA("../../data/tar/encrypted.tar.lrz");
    QTest::newRow(path.toLatin1())
            << path
            << BackendHandlerError::ArchivingMethodNotSupported;

    // Archive contains paths containing ../
    path = QFINDTESTDATA("../../data/tar/malicious.tar");
    QTest::newRow(path.toLatin1())
            << path
            << BackendHandlerError::___INVALID; // No error type defined yet
}


void BackendHandlerExtract::extractError()
{
    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    if (!BackendHandler::isTarExecAvailable())
        QSKIP("Tar executable not found");

    const auto archiveType = typeDetector.detectType(archivePath);

    const std::vector<CompressionHandlerExecInfo> handlerList = getCompressionHandlers(archiveType,
                                                                                       CompressionHandlerPickMode::AllSupported);

    for (const auto& info : handlerList)
    {
        if (!info.available)
        {
            QWARN("Executable not found: " + info.execName.toLatin1());
            continue;
        }

        BackendHandler handler;

        TestArchiveExtractParams params;
        params.handler = &handler;
        params.archivePath = archivePath;

        PrivateExtractionParameterMap privateParams;
        privateParams[PrivateExtractionParameter::CompressionHandlerType] = QVariant::fromValue(info.handlerType);
        params.pluginPrivateData = QVariant::fromValue(privateParams);

        params.expectedEndState = BackendHandlerState::Errors;
        params.expectedError = expectedError;

        testArchiveExtract(params);
    }
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::GnuTar::BackendHandlerExtract)

#include "tst_backendhandlerextract.moc"
