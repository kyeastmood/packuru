// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>
#include <QThread>

#include "src/utils/qvariant_utils.h"
#include "src/utils/private/unordered_map_utils.h"

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/plugin-api/archivingfilepaths.h"
#include "src/core/private/plugin-api/archivetype.h"
#include "src/core/private/plugin-api/archivingcompressioninfo.h"

#include "src/plugin-gnutar-core/backendhandler.h"
#include "src/plugin-gnutar-core/privatearchivingparametermapgnutar.h"
#include "src/plugin-gnutar-core/privatearchivingparameter.h"
#include "src/plugin-gnutar-core/tarformat.h"
#include "src/plugin-gnutar-core/getcompressioninfofortype.h"
#include "src/plugin-gnutar-core/tocliarg.h"
#include "src/plugin-gnutar-core/compressionhandlertype.h"
#include "src/plugin-gnutar-core/createcompressionhandler.h"

#include "tests/tests-shared/testarchiveadd.h"


using namespace Packuru::Utils;
using namespace Packuru::Core;
using namespace Packuru::Plugins::GnuTar::Core;
using namespace Packuru::Tests::Shared;


namespace
{
// Check whether parameters passed to BackendHandler are reflected in command-line
// arguments of the backend process
void checkArguments(const TestArchiveAddParams& testParams, const QStringList& processArgs);
void checkCompressorArguments(const TestArchiveAddParams& testParams, const QStringList& processArgs);
}


namespace Packuru::Tests::Plugins::GnuTar
{

class BackendHandlerAdd : public QObject
{
    Q_OBJECT

public:
    BackendHandlerAdd();
    ~BackendHandlerAdd();

private slots:
    void createNewArchive_data();
    void createNewArchive();

    void createNewMultipart_data();
    void createNewMultipart();

    void addToExisting_data();
    void addToExisting();

private:
    const std::vector<QFileInfo> inputItems = {QFINDTESTDATA("../../data/files/")};

};


BackendHandlerAdd::BackendHandlerAdd()
{

}


BackendHandlerAdd::~BackendHandlerAdd()
{

}


void BackendHandlerAdd::createNewArchive_data()
{
    QTest::addColumn<ArchiveType>("archiveType");
    QTest::addColumn<ArchivingFilePaths>("filePaths");
    QTest::addColumn<PrivateArchivingParameterMap>("privateParams");

    PrivateArchivingParameterMap privateParams;

    // Sparse files not supported for v7
    privateParams[PrivateArchivingParameter::SparseFileDetection] = QVariant::fromValue(false);
    privateParams[PrivateArchivingParameter::TarFormat] = QVariant::fromValue(TarFormat::V7);
    privateParams[PrivateArchivingParameter::ThreadCount] = QThread::idealThreadCount();

    auto type = ArchiveType::tar;
    QTest::newRow(toString(type).data())
            << type
            << ArchivingFilePaths::RelativePaths
            << privateParams;

    type = ArchiveType::tar_brotli;
    privateParams[PrivateArchivingParameter::SparseFileDetection] = QVariant::fromValue(true);
    privateParams[PrivateArchivingParameter::CompressionLevel] = QVariant::fromValue(getCompressionInfoForType(type).max);
    privateParams[PrivateArchivingParameter::TarFormat] = QVariant::fromValue(TarFormat::Gnu);

    QTest::newRow(toString(type).data())
            << type
            << ArchivingFilePaths::FullPaths
            << privateParams;

    type = ArchiveType::tar_bzip2;
    privateParams[PrivateArchivingParameter::CompressionLevel] = QVariant::fromValue(getCompressionInfoForType(type).max);
    privateParams[PrivateArchivingParameter::TarFormat] = QVariant::fromValue(TarFormat::Pax);

    QTest::newRow(toString(type).data())
            << type
            << ArchivingFilePaths::AbsolutePaths
            << privateParams;

    type = ArchiveType::tar_compress;
    // Sparse files not supported for ustar
    privateParams[PrivateArchivingParameter::SparseFileDetection] = QVariant::fromValue(false);
    privateParams[PrivateArchivingParameter::CompressionLevel] = QVariant::fromValue(getCompressionInfoForType(type).max);
    privateParams[PrivateArchivingParameter::TarFormat] = QVariant::fromValue(TarFormat::UStar);

    QTest::newRow(toString(type).data())
            << type
            << ArchivingFilePaths::AbsolutePaths
            << privateParams;

    type = ArchiveType::tar_gzip;
    privateParams[PrivateArchivingParameter::CompressionLevel] = QVariant::fromValue(getCompressionInfoForType(type).max);
    privateParams[PrivateArchivingParameter::TarFormat] = QVariant::fromValue(TarFormat::OldGnu);

    QTest::newRow(toString(type).data())
            << type
            << ArchivingFilePaths::RelativePaths
            << privateParams;

    type = ArchiveType::tar_lrzip;
    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(getCompressionInfoForType(type).max);

    QTest::newRow(toString(type).data())
            << type
            << ArchivingFilePaths::RelativePaths
            << privateParams;

    type = ArchiveType::tar_lz4;
    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(getCompressionInfoForType(type).max);

    QTest::newRow(toString(type).data())
            << type
            << ArchivingFilePaths::RelativePaths
            << privateParams;

    type = ArchiveType::tar_lzip;
    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(getCompressionInfoForType(type).max);

    QTest::newRow(toString(type).data())
            << type
            << ArchivingFilePaths::RelativePaths
            << privateParams;

    type = ArchiveType::tar_lzma;
    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(getCompressionInfoForType(type).max);

    QTest::newRow(toString(type).data())
            << type
            << ArchivingFilePaths::RelativePaths
            << privateParams;

    type = ArchiveType::tar_lzop;
    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(getCompressionInfoForType(type).max);

    QTest::newRow(toString(type).data())
            << type
            << ArchivingFilePaths::RelativePaths
            << privateParams;

    type = ArchiveType::tar_xz;
    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(getCompressionInfoForType(type).max);

    QTest::newRow(toString(type).data())
            << type
            << ArchivingFilePaths::RelativePaths
            << privateParams;

    type = ArchiveType::tar_zstd;
    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(getCompressionInfoForType(type).max);

    QTest::newRow(toString(type).data())
            << type
            << ArchivingFilePaths::RelativePaths
            << privateParams;
}


void BackendHandlerAdd::createNewArchive()
{
    QFETCH(ArchiveType, archiveType);
    QFETCH(ArchivingFilePaths, filePaths);
    QFETCH(PrivateArchivingParameterMap, privateParams);

    if (!BackendHandler::isTarExecAvailable())
        QSKIP("Tar executable not found");

    const std::vector<CompressionHandlerExecInfo> handlerList = getCompressionHandlers(archiveType,
                                                                                       CompressionHandlerPickMode::AllSupported);

    for (const auto& info : handlerList)
    {
        if (!info.available)
        {
            QWARN("Executable not found: " + info.execName.toLatin1());
            continue;
        }

        BackendHandler handler;

        TestArchiveAddParams params;
        params.handler = &handler;
        params.archiveType = archiveType;
        params.inputItems = inputItems;
        params.filePaths = filePaths;
        params.createNewArchive = true;

        privateParams[PrivateArchivingParameter::CompressionHandlerType] = QVariant::fromValue(info.handlerType);
        params.pluginPrivateData = QVariant::fromValue(privateParams);

        testArchiveAdd(params);

        const auto args = handler.getBackendArgs();
        QCOMPARE(args.size(), static_cast<std::size_t>(1));
        checkArguments(params, args[0]);

        if (params.archiveType != ArchiveType::tar)
        {
            const auto compressorArgs = handler.getCompressionHandlerArguments();
            checkCompressorArguments(params, compressorArgs);
        }
    }
}


void BackendHandlerAdd::createNewMultipart_data()
{
    QTest::addColumn<ArchiveType>("archiveType");
    QTest::addColumn<ArchivingFilePaths>("filePaths");
    QTest::addColumn<quint64>("partSize");
    QTest::addColumn<PrivateArchivingParameterMap>("privateParams");

    PrivateArchivingParameterMap privateParams;

    privateParams[PrivateArchivingParameter::ThreadCount]
            = QThread::idealThreadCount();

    auto type = ArchiveType::tar_lzip;
    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(getCompressionInfoForType(type).max);

    privateParams[PrivateArchivingParameter::SparseFileDetection] = QVariant::fromValue(true);
    privateParams[PrivateArchivingParameter::TarFormat] = QVariant::fromValue(TarFormat::Pax);

    QTest::newRow(toString(type).data())
            << type
            << ArchivingFilePaths::RelativePaths
            << static_cast<quint64>(100000)
            << privateParams;
}


void BackendHandlerAdd::createNewMultipart()
{
    QFETCH(ArchiveType, archiveType);
    QFETCH(ArchivingFilePaths, filePaths);
    QFETCH(quint64, partSize);
    QFETCH(PrivateArchivingParameterMap, privateParams);

    if (!BackendHandler::isTarExecAvailable())
        QSKIP("Tar executable not found");

    std::vector<CompressionHandlerExecInfo> handlerList = getCompressionHandlers(archiveType,
                                                                                 CompressionHandlerPickMode::AllSupported);

    if (archiveType == ArchiveType::tar_lzip)
    {
        const CompressionHandlerExecInfo info = getCompressionHandlerExecInfo(CompressionHandlerType::Plzip);
        if (info.available)
            QSKIP("plzip is installed and it does not support split archives; lzip invokes plzip if available");

        // remove plzip from the list to avoid the warning of unavailable executable
        std::remove_if(handlerList.begin(), handlerList.end(),
                       [] (const auto& info) { return info.handlerType == CompressionHandlerType::Plzip; });
    }

    for (const auto& info : handlerList)
    {
        if (!info.available)
        {
            QWARN("Executable not found: " + info.execName.toLatin1());
            continue;
        }

        BackendHandler handler;

        TestArchiveAddParams params;
        params.handler = &handler;
        params.archiveType = archiveType;
        params.inputItems = inputItems;
        params.filePaths = filePaths;
        params.partSize = partSize;
        params.createNewArchive = true;

        privateParams[PrivateArchivingParameter::CompressionHandlerType] = QVariant::fromValue(info.handlerType);
        params.pluginPrivateData = QVariant::fromValue(privateParams);

        testArchiveAdd(params);

        const auto args = handler.getBackendArgs();
        QCOMPARE(args.size(), static_cast<std::size_t>(1));
        checkArguments(params, args[0]);

        if (params.archiveType != ArchiveType::tar)
        {
            const auto compressorArgs = handler.getCompressionHandlerArguments();
            checkCompressorArguments(params, compressorArgs);
        }
    }
}


void BackendHandlerAdd::addToExisting_data()
{

}


void BackendHandlerAdd::addToExisting()
{
    if (!BackendHandler::isTarExecAvailable())
        QSKIP("Tar executable not found");

    // Only adding to uncompressed archives is supported
    BackendHandler handler;

    PrivateArchivingParameterMap privateParams;
    privateParams[PrivateArchivingParameter::SparseFileDetection] = false;

    TestArchiveAddParams params;
    params.handler = &handler;
    params.archiveType = ArchiveType::tar;
    params.existingArchivePath = QFINDTESTDATA("../../data/tar/files.tar");
    params.inputItems = inputItems;
    params.filePaths = ArchivingFilePaths::FullPaths;
    params.pluginPrivateData = QVariant::fromValue(privateParams);

    testArchiveAdd(params);

    const auto args = handler.getBackendArgs();
    QCOMPARE(args.size(), static_cast<std::size_t>(1));
    checkArguments(params, args[0]);
}

}


namespace
{

void checkArguments(const TestArchiveAddParams& testParams, const QStringList& processArgs)
{
    QVERIFY(!processArgs.isEmpty());

    const auto privateData = getValueAs<PrivateArchivingParameterMap>(testParams.pluginPrivateData);

    if (testParams.createNewArchive)
    {
        QVERIFY2(processArgs.contains(QLatin1String("-c")), processArgs.join(" ").toLatin1());
        QVERIFY2(processArgs.contains(QLatin1String("-H")), processArgs.join(" ").toLatin1()); // format

        const auto format = getValueAs<TarFormat>(getValue(privateData,
                                                           PrivateArchivingParameter::TarFormat));

        QVERIFY2(processArgs.contains(toCLIargTarFormat(format)), processArgs.join(" ").toLatin1());
    }
    else
    {
        QVERIFY2(processArgs.contains(QLatin1String("-u")), processArgs.join(" ").toLatin1());
        // tar will operate on the file directly
        QVERIFY2(processArgs.contains(QLatin1String("-f")), processArgs.join(" ").toLatin1());
    }

    const auto detectSparseFiles
            = getValueAs<bool>(getValue(privateData,
                                        PrivateArchivingParameter::SparseFileDetection));

    const auto sparseFilesArg = toCLIArgSparseFiles(detectSparseFiles);
    if (!sparseFilesArg.isEmpty())
        QVERIFY2(processArgs.contains(sparseFilesArg), processArgs.join(" ").toLatin1());

    if (testParams.filePaths == ArchivingFilePaths::AbsolutePaths)
        QVERIFY2(processArgs.contains(QLatin1String("-P")), processArgs.join(" ").toLatin1());
}


void checkCompressorArguments(const TestArchiveAddParams& testParams, const QStringList& processArgs)
{
    QVERIFY(!processArgs.isEmpty());

    const auto compressorType = fromExecName(processArgs[0]);
    QVERIFY2(compressorType != CompressionHandlerType::___INVALID, processArgs[0].toLatin1());

    const auto privateData = getValueAs<PrivateArchivingParameterMap>(testParams.pluginPrivateData);

    const auto compressionLevel = getValueAs<int>(getValue(privateData,
                                                           PrivateArchivingParameter::CompressionLevel));

    QStringList compressionArgs;
    compressionArgs = toCLIargCompressionLevel(compressionLevel, compressorType);

    for (const auto& arg : compressionArgs)
        QVERIFY2(processArgs.contains(arg), processArgs.join(" ").toLatin1());

    const auto threadCount = getValueAs<int>(getValue(privateData,
                                                      PrivateArchivingParameter::ThreadCount));

    const auto handlerType = getValueAs<CompressionHandlerType>(getValue(privateData,
                                                                PrivateArchivingParameter::CompressionHandlerType));

    const QString threadCountStr = toCLIargThreadCount(threadCount, handlerType);

    if (!threadCountStr.isEmpty())
        QVERIFY2(processArgs.contains(threadCountStr), processArgs.join(" ").toLatin1());

    if (testParams.partSize > 0)
    {
        const QString partSizeArg = toCLIargPartSize(testParams.partSize, compressorType);
        if (!partSizeArg.isEmpty())
            QVERIFY2(processArgs.contains(partSizeArg), processArgs.join(" ").toLatin1());
    }
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::GnuTar::BackendHandlerAdd)

#include "tst_backendhandleradd.moc"
