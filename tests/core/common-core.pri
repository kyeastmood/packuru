# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-testcases.pri)

DESTDIR += $${TESTS_TARGET_DIR}/core
