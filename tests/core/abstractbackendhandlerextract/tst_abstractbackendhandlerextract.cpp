// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>
#include <QStandardPaths>
#include <QTemporaryDir>

#include "src/core/private/plugin-api/abstractbackendhandler.h"
#include "src/core/private/plugin-api/backendextractiondata.h"


using namespace Packuru::Core;


namespace Packuru::Tests::Core
{


class MockBackendHandler : public AbstractBackendHandler
{
    void stopImpl() override {}
};


class AbstractBackendHandlerExtract : public QObject
{
    Q_OBJECT
public:
    AbstractBackendHandlerExtract();
    ~AbstractBackendHandlerExtract();

private slots:
    void extractToNonEmptyDirError();
    void extractToNonExistingDirError();


};


AbstractBackendHandlerExtract::AbstractBackendHandlerExtract()
{

}


AbstractBackendHandlerExtract::~AbstractBackendHandlerExtract()
{

}


void AbstractBackendHandlerExtract::extractToNonEmptyDirError()
{
    MockBackendHandler bh;
    BackendExtractionData data;

    for (int i = 0; i < QStandardPaths::StandardLocation::AppConfigLocation; ++i)
    {
        const QString path = QStandardPaths::writableLocation(static_cast<QStandardPaths::StandardLocation>(i));
        if (!path.isEmpty())
        {
            const QDir dir(path);
            if (!dir.isEmpty())
            {
                data.tempDestinationPath = path;
                break;
            }
        }
    }

    if (data.tempDestinationPath.isEmpty())
        QSKIP("Writable, non empty dir not found");

    bh.extractArchive(data);

    QCOMPARE(bh.getState(), BackendHandlerState::Errors);

    QVERIFY2(bh.hasError(BackendHandlerError::TemporaryFolderNotEmpty), data.tempDestinationPath.toLatin1());
}


void AbstractBackendHandlerExtract::extractToNonExistingDirError()
{
    MockBackendHandler bh;
    BackendExtractionData data;

    {
        QTemporaryDir dir;

        if (dir.isValid())
            data.tempDestinationPath = dir.path();
        // Dir is removed
    }

    if (data.tempDestinationPath.isEmpty())
        QSKIP("Temp dir not created");

    bh.extractArchive(data);

    QCOMPARE(bh.getState(), BackendHandlerState::Errors);

    QVERIFY2(bh.hasError(BackendHandlerError::TemporaryFolderNotFound), data.tempDestinationPath.toLatin1());
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Core::AbstractBackendHandlerExtract)

#include "tst_abstractbackendhandlerextract.moc"
