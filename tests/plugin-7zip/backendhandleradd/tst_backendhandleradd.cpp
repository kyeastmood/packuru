// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>
#include <QThread>

#include "src/utils/qvariant_utils.h"
#include "src/utils/private/tounderlyingtype.h"

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/plugin-api/archivetype.h"
#include "src/core/private/plugin-api/archivingfilepaths.h"
#include "src/core/private/plugin-api/encryptionstate.h"

#include "src/plugin-7zip-core/archiving/privatearchivingparametermap.h"
#include "src/plugin-7zip-core/archiving/privatearchivingparameter.h"
#include "src/plugin-7zip-core/archiving/archivingcompressionlevel.h"
#include "src/plugin-7zip-core/archiving/archivingcompressionmethod.h"
#include "src/plugin-7zip-core/archiving/archivingencryptionmethod.h"
#include "src/plugin-7zip-core/archiving/archivingupdatemode.h"
#include "src/plugin-7zip-core/archiving/archivingsolidblocksize.h"
#include "src/plugin-7zip-core/archiving/dictionarysize.h"
#include "src/plugin-7zip-core/archiving/wordsize.h"
#include "src/plugin-7zip-core/archiving/archivingoptiontocliarg.h"
#include "src/plugin-7zip-core/backendhandler.h"

#include "tests/tests-shared/testarchiveadd.h"
#include "tests/tests-shared/passwords.h"


using namespace Packuru::Utils;
using namespace Packuru::Core;
using namespace Packuru::Plugins::_7Zip::Core;
using namespace Packuru::Tests::Shared;


namespace
{
void checkArguments(const TestArchiveAddParams& testParams, const QStringList& processArgs);

const std::vector<QFileInfo> inputItems = {QFINDTESTDATA("../../data/files/")};

}


namespace Packuru::Tests::Plugins::_7Zip
{

class BackendHandlerAdd : public QObject
{
    Q_OBJECT

public:
    BackendHandlerAdd();
    ~BackendHandlerAdd() override;

private slots:
    void initTestCase();

    void createNew7z_data();
    void createNew7z();

    void createNewZip_data();
    void createNewZip();

    void createNewMultipart_data();
    void createNewMultipart();

    void createNewEncrypted_data();
    void createNewEncrypted();

    void addToExisting_data();
    void addToExisting();

    void addToEncrypted7z_data();
    void addToEncrypted7z();
};


BackendHandlerAdd::BackendHandlerAdd()
{

}


BackendHandlerAdd::~BackendHandlerAdd()
{

}

void BackendHandlerAdd::initTestCase()
{
    if (!BackendHandler::executableAvailable())
        QSKIP("Executable not found");
}


void BackendHandlerAdd::createNew7z_data()
{
    QTest::addColumn<ArchivingFilePaths>("filePaths");
    QTest::addColumn<PrivateArchivingParameterMap>("privateParams");

    PrivateArchivingParameterMap privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::NoCompression);

    privateParams[PrivateArchivingParameter::UpdateMode]
            = QVariant::fromValue(ArchivingUpdateMode::NewArchive);

    privateParams[PrivateArchivingParameter::ThreadCount] = QThread::idealThreadCount();

    QTest::newRow("archive1")
            << ArchivingFilePaths::AbsolutePaths
            << privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::Fastest);

    privateParams[PrivateArchivingParameter::CompressionMethod]
            = QVariant::fromValue(ArchivingCompressionMethod::BZip2);

    privateParams[PrivateArchivingParameter::DictionarySize]
            = toUnderlyingType(DictionarySizeBZip2::_100KiB);

    privateParams[PrivateArchivingParameter::SolidBlockSize]
            = QVariant::fromValue(ArchivingSolidBlockSize::NonSolid);

    QTest::newRow("archive2")
            << ArchivingFilePaths::FullPaths
            << privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::Fast);

    privateParams[PrivateArchivingParameter::CompressionMethod]
            = QVariant::fromValue(ArchivingCompressionMethod::LZMA);

    privateParams[PrivateArchivingParameter::DictionarySize]
            = toUnderlyingType(DictionarySizeLZMA::_2MiB);

    privateParams[PrivateArchivingParameter::WordSize]
            = toUnderlyingType(WordSizeLZMA::_2);

    privateParams[PrivateArchivingParameter::SolidBlockSize]
            = QVariant::fromValue(ArchivingSolidBlockSize::Solid);

    QTest::newRow("archive3")
            << ArchivingFilePaths::RelativePaths
            << privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::Normal);

    privateParams[PrivateArchivingParameter::CompressionMethod]
            = QVariant::fromValue(ArchivingCompressionMethod::LZMA2);

    privateParams[PrivateArchivingParameter::DictionarySize]
            = toUnderlyingType(DictionarySizeLZMA::_128MiB);

    privateParams[PrivateArchivingParameter::WordSize]
            = toUnderlyingType(WordSizeLZMA::_10);

    privateParams[PrivateArchivingParameter::SolidBlockSize]
            = QVariant::fromValue(ArchivingSolidBlockSize::MB128);

    QTest::newRow("archive4")
            << ArchivingFilePaths::AbsolutePaths
            << privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::Ultra);

    privateParams[PrivateArchivingParameter::CompressionMethod]
            = QVariant::fromValue(ArchivingCompressionMethod::PPMd);

    privateParams[PrivateArchivingParameter::DictionarySize]
            = toUnderlyingType(DictionarySizePPMd7z::_512MiB);

    privateParams[PrivateArchivingParameter::WordSize]
            = toUnderlyingType(WordSizePPMd7z::_32);

    privateParams[PrivateArchivingParameter::SolidBlockSize]
            = QVariant::fromValue(ArchivingSolidBlockSize::GB1);

    QTest::newRow("archive5")
            << ArchivingFilePaths::AbsolutePaths
            << privateParams;
}


void BackendHandlerAdd::createNew7z()
{
    BackendHandler handler;

    QFETCH(ArchivingFilePaths, filePaths);
    QFETCH(PrivateArchivingParameterMap, privateParams);

    TestArchiveAddParams params;
    params.handler = &handler;
    params.archiveType = ArchiveType::_7z;
    params.inputItems = inputItems;
    params.filePaths = filePaths;
    params.createNewArchive = true;
    params.pluginPrivateData = QVariant::fromValue(privateParams);

    testArchiveAdd(params);

    const auto args = handler.getBackendArgs();
    QCOMPARE(args.size(), static_cast<std::size_t>(1));

    checkArguments(params, args.front());
}


void BackendHandlerAdd::createNewZip_data()
{
    QTest::addColumn<ArchivingFilePaths>("filePaths");
    QTest::addColumn<PrivateArchivingParameterMap>("privateParams");

    PrivateArchivingParameterMap privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::NoCompression);

    privateParams[PrivateArchivingParameter::UpdateMode]
            = QVariant::fromValue(ArchivingUpdateMode::NewArchive);

    privateParams[PrivateArchivingParameter::ThreadCount] = QThread::idealThreadCount();

    QTest::newRow("archive1")
            << ArchivingFilePaths::AbsolutePaths
            << privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::Fastest);

    privateParams[PrivateArchivingParameter::CompressionMethod]
            = QVariant::fromValue(ArchivingCompressionMethod::BZip2);

    privateParams[PrivateArchivingParameter::DictionarySize]
            = toUnderlyingType(DictionarySizeBZip2::_200KiB);

    QTest::newRow("archive2")
            << ArchivingFilePaths::FullPaths
            << privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::Fast);

    privateParams[PrivateArchivingParameter::CompressionMethod]
            = QVariant::fromValue(ArchivingCompressionMethod::Deflate);

    privateParams[PrivateArchivingParameter::DictionarySize]
            = toUnderlyingType(DictionarySizeDeftate::value);

    privateParams[PrivateArchivingParameter::WordSize]
            = toUnderlyingType(WordSizeDeflate::_8);

    QTest::newRow("archive3")
            << ArchivingFilePaths::AbsolutePaths
            << privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::Normal);

    privateParams[PrivateArchivingParameter::CompressionMethod]
            = QVariant::fromValue(ArchivingCompressionMethod::Deflate64);

    privateParams[PrivateArchivingParameter::DictionarySize]
            = toUnderlyingType(DictionarySizeDeftate64::value);

    privateParams[PrivateArchivingParameter::WordSize]
            = toUnderlyingType(WordSizeDeflate64::_32);

    QTest::newRow("archive4")
            << ArchivingFilePaths::AbsolutePaths
            << privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::Maximum);

    privateParams[PrivateArchivingParameter::CompressionMethod]
            = QVariant::fromValue(ArchivingCompressionMethod::LZMA);

    privateParams[PrivateArchivingParameter::DictionarySize]
            = toUnderlyingType(DictionarySizeLZMA::_8MiB);

    privateParams[PrivateArchivingParameter::WordSize]
            = toUnderlyingType(WordSizeLZMA::_4);

    QTest::newRow("archive5")
            << ArchivingFilePaths::RelativePaths
            << privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::Ultra);

    privateParams[PrivateArchivingParameter::CompressionMethod]
            = QVariant::fromValue(ArchivingCompressionMethod::PPMd);

    privateParams[PrivateArchivingParameter::DictionarySize]
            = toUnderlyingType(DictionarySizePPMdZip::_256MiB);

    privateParams[PrivateArchivingParameter::WordSize]
            = toUnderlyingType(WordSizePPMdZip::_16);

    QTest::newRow("archive6")
            << ArchivingFilePaths::AbsolutePaths
            << privateParams;
}


void BackendHandlerAdd::createNewZip()
{
    BackendHandler handler;

    QFETCH(ArchivingFilePaths, filePaths);
    QFETCH(PrivateArchivingParameterMap, privateParams);

    TestArchiveAddParams params;
    params.handler = &handler;
    params.archiveType = ArchiveType::zip;
    params.inputItems = inputItems;
    params.filePaths = filePaths;
    params.createNewArchive = true;
    params.pluginPrivateData = QVariant::fromValue(privateParams);

    testArchiveAdd(params);

    const auto args = handler.getBackendArgs();
    QCOMPARE(args.size(), static_cast<std::size_t>(1));

    checkArguments(params, args.front());
}


void BackendHandlerAdd::createNewMultipart_data()
{
    QTest::addColumn<ArchiveType>("archiveType");

    QTest::newRow("7z")
            << ArchiveType::_7z;

    QTest::newRow("zip")
            << ArchiveType::zip;
}


void BackendHandlerAdd::createNewMultipart()
{   
    BackendHandler handler;

    QFETCH(ArchiveType, archiveType);

    PrivateArchivingParameterMap privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::NoCompression);

    privateParams[PrivateArchivingParameter::UpdateMode]
            = QVariant::fromValue(ArchivingUpdateMode::NewArchive);

    TestArchiveAddParams params;
    params.handler = &handler;
    params.archiveType = archiveType;
    params.inputItems = inputItems;
    params.partSize = 50;
    params.filePaths = ArchivingFilePaths::FullPaths;
    params.createNewArchive = true;
    params.pluginPrivateData = QVariant::fromValue(privateParams);

    testArchiveAdd(params);

    const auto args = handler.getBackendArgs();
    QCOMPARE(args.size(), static_cast<std::size_t>(1));

    checkArguments(params, args.front());
}


void BackendHandlerAdd::createNewEncrypted_data()
{
    QTest::addColumn<ArchiveType>("archiveType");
    QTest::addColumn<EncryptionState>("newEncryptionState");
    QTest::addColumn<ArchivingEncryptionMethod>("encryptionMethod");

    QTest::newRow("7z_content_encryption")
            << ArchiveType::_7z
            << EncryptionState::FilesContentOnly
            << ArchivingEncryptionMethod::___INVALID;

    QTest::newRow("7z_full_encryption")
            << ArchiveType::_7z
            << EncryptionState::EntireArchive
            << ArchivingEncryptionMethod::___INVALID;

    QTest::newRow("zip_content_encryption_zipcrypto")
            << ArchiveType::zip
            << EncryptionState::FilesContentOnly
            << ArchivingEncryptionMethod::ZipCrypto;

    QTest::newRow("zip_content_encryption_aes")
            << ArchiveType::zip
            << EncryptionState::FilesContentOnly
            << ArchivingEncryptionMethod::AES_256;
}


void BackendHandlerAdd::createNewEncrypted()
{
    BackendHandler handler;

    QFETCH(ArchiveType, archiveType);
    QFETCH(EncryptionState, newEncryptionState);
    QFETCH(ArchivingEncryptionMethod, encryptionMethod);

    PrivateArchivingParameterMap privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::NoCompression);

    privateParams[PrivateArchivingParameter::UpdateMode]
            = QVariant::fromValue(ArchivingUpdateMode::NewArchive);

    privateParams[PrivateArchivingParameter::EncryptionMethod]
            = QVariant::fromValue(encryptionMethod);

    TestArchiveAddParams params;
    params.handler = &handler;
    params.archiveType = archiveType;
    params.password = validPassword;
    params.newArchiveEncryption = newEncryptionState;
    params.inputItems = inputItems;
    params.filePaths = ArchivingFilePaths::FullPaths;
    params.createNewArchive = true;
    params.pluginPrivateData = QVariant::fromValue(privateParams);

    testArchiveAdd(params);

    const auto args = handler.getBackendArgs();
    QCOMPARE(args.size(), static_cast<std::size_t>(1));

    checkArguments(params, args.front());
}


void BackendHandlerAdd::addToExisting_data()
{
    QTest::addColumn<ArchiveType>("archiveType");
    QTest::addColumn<ArchivingUpdateMode>("updateMode");
    QTest::addColumn<EncryptionState>("newArchiveEncryption");

    const std::initializer_list<ArchiveType> types {ArchiveType::_7z, ArchiveType::zip};

    const std::initializer_list<ArchivingUpdateMode> updateModes {
        ArchivingUpdateMode::AddAndSkip,
                ArchivingUpdateMode::AddAndUpdate,
                ArchivingUpdateMode::AddAndReplace,
                ArchivingUpdateMode::UpdateExisting};

    for (auto type : types)
        for (auto updateMode : updateModes)
        {
            QTest::newRow((toString(type) + "_" + toString(updateMode)).toLatin1())
                    << type
                    << updateMode
                    << EncryptionState::Disabled;
        }

    QTest::newRow("7z_add_encrypt_EntireArchive")
            << ArchiveType::_7z
            << ArchivingUpdateMode::AddAndUpdate
            << EncryptionState::EntireArchive;

    QTest::newRow("7z_add_encrypt_FilesContentOnly")
            << ArchiveType::_7z
            << ArchivingUpdateMode::AddAndSkip
            << EncryptionState::FilesContentOnly;

    QTest::newRow("zip_add_encrypt_FilesContentOnly")
            << ArchiveType::zip
            << ArchivingUpdateMode::UpdateExisting
            << EncryptionState::FilesContentOnly;
}


void BackendHandlerAdd::addToExisting()
{
    BackendHandler handler;

    QFETCH(ArchiveType, archiveType);
    QFETCH(ArchivingUpdateMode, updateMode);
    QFETCH(EncryptionState, newArchiveEncryption);

    PrivateArchivingParameterMap privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::NoCompression);

    privateParams[PrivateArchivingParameter::UpdateMode]
            = QVariant::fromValue(updateMode);

    TestArchiveAddParams params;
    params.handler = &handler;
    params.archiveType = archiveType;

    if (archiveType == ArchiveType::_7z)
        params.existingArchivePath = QFINDTESTDATA("../../data/7z/solid.unix_link.7z");
    else if (archiveType == ArchiveType::zip)
    {
        params.existingArchivePath = QFINDTESTDATA("../../data/zip/unix_link.zip");
        QVERIFY(newArchiveEncryption != EncryptionState::EntireArchive);
    }
    else
        QFAIL(("Invalid archive type: " + toString(archiveType)).toLatin1());

    QVERIFY(newArchiveEncryption != EncryptionState::___INVALID);
    if (newArchiveEncryption != EncryptionState::Disabled)
    {
        params.newArchiveEncryption = newArchiveEncryption;
        params.password = validPassword;
        if (archiveType == ArchiveType::zip)
            privateParams[PrivateArchivingParameter::EncryptionMethod]
                    = QVariant::fromValue(ArchivingEncryptionMethod::AES_256);
    }

    params.inputItems = inputItems;
    params.filePaths = ArchivingFilePaths::FullPaths;
    params.pluginPrivateData = QVariant::fromValue(privateParams);

    testArchiveAdd(params);

    const auto args = handler.getBackendArgs();
    QCOMPARE(args.size(), static_cast<std::size_t>(1));

    checkArguments(params, args.front());
}


void BackendHandlerAdd::addToEncrypted7z_data()
{

}


void BackendHandlerAdd::addToEncrypted7z()
{
    BackendHandler handler;

    PrivateArchivingParameterMap privateParams;

    privateParams[PrivateArchivingParameter::CompressionLevel]
            = QVariant::fromValue(ArchivingCompressionLevel::NoCompression);

    privateParams[PrivateArchivingParameter::UpdateMode]
            = QVariant::fromValue(ArchivingUpdateMode::AddAndUpdate);

    TestArchiveAddParams params;
    params.handler = &handler;
    params.archiveType = ArchiveType::_7z;
    params.existingArchivePath = QFINDTESTDATA("../../data/7z/encrypted.solid.7z");
    params.password = validPassword;
    params.currentArchiveEncryption = EncryptionState::EntireArchive;
    params.newArchiveEncryption = EncryptionState::EntireArchive;
    params.inputItems = inputItems;
    params.filePaths = ArchivingFilePaths::FullPaths;
    params.pluginPrivateData = QVariant::fromValue(privateParams);

    testArchiveAdd(params);

    const auto args = handler.getBackendArgs();
    QCOMPARE(args.size(), static_cast<std::size_t>(1));

    checkArguments(params, args.front());
}

}


namespace
{

// Checks whether parameters passed to BackendHandler are reflected in command-line
// arguments of the backend process
void checkArguments(const TestArchiveAddParams& testParams, const QStringList& processArgs)
{
    qDebug()<<processArgs;

    if (!testParams.password.isEmpty())
        QVERIFY(processArgs.contains(QLatin1String("-p") + testParams.password));

    if (testParams.archiveType == ArchiveType::zip)
    {
        QVERIFY(testParams.currentArchiveEncryption != EncryptionState::EntireArchive);
        QVERIFY(testParams.newArchiveEncryption != EncryptionState::EntireArchive);
    }

    const QString encryptionArg = archivingOptionToCLIarg(testParams.newArchiveEncryption);
    if (!encryptionArg.isEmpty())
        QVERIFY2(processArgs.contains(encryptionArg), encryptionArg.toLatin1());

    for (const auto& item : testParams.inputItems)
    {
        const QString path = item.canonicalFilePath();
        QVERIFY2(processArgs.contains(path), "Path=" + path.toLatin1());
    }

    if (testParams.partSize > 0)
        QVERIFY(processArgs.contains(archivingOptionToCLIarg(testParams.partSize)));

    const QString filePathsArg = archivingOptionToCLIarg(testParams.filePaths);
    if (filePathsArg.size() > 0)
        QVERIFY2(processArgs.contains(filePathsArg), filePathsArg.toLatin1());

    const QString archiveTypeArg = archivingOptionToCLIarg(testParams.archiveType);
    QVERIFY2(processArgs.contains(archiveTypeArg), archiveTypeArg.toLatin1());

    QVERIFY(containsType<PrivateArchivingParameterMap>(testParams.pluginPrivateData));
    const auto privateMap
            = getValueAs<PrivateArchivingParameterMap>(testParams.pluginPrivateData);

    auto it = privateMap.find(PrivateArchivingParameter::UpdateMode);
    QVERIFY2(it != privateMap.end(), "Update mode not set");
    QVERIFY(containsType<ArchivingUpdateMode>(it->second));
    const auto updateMode = getValueAs<ArchivingUpdateMode>(it->second);
    const QString updateModeArg = archivingOptionToCLIarg(updateMode);
    QVERIFY2(processArgs.contains(updateModeArg), updateModeArg.toLatin1());

    it = privateMap.find(PrivateArchivingParameter::CompressionLevel);
    QVERIFY2(it != privateMap.end(), "Compression level not set");
    QVERIFY(containsType<ArchivingCompressionLevel>(it->second));
    const auto compressionLevel = getValueAs<ArchivingCompressionLevel>(it->second);
    const QString compressionLevelArg = archivingOptionToCLIarg(compressionLevel);
    QVERIFY2(processArgs.contains(compressionLevelArg), compressionLevelArg.toLatin1());

    if (compressionLevel != ArchivingCompressionLevel::NoCompression)
    {
        if (testParams.archiveType == ArchiveType::_7z)
        {
            it = privateMap.find(PrivateArchivingParameter::SolidBlockSize);
            QVERIFY2(it != privateMap.end(), "Solid block size not set");
            QVERIFY(containsType<ArchivingSolidBlockSize>(it->second));
            const auto solidBlockSize = getValueAs<ArchivingSolidBlockSize>(it->second);
            const QString solidBlockSizeArg = archivingOptionToCLIarg(solidBlockSize);
            QVERIFY2(processArgs.contains(solidBlockSizeArg), solidBlockSizeArg.toLatin1());
        }

        it = privateMap.find(PrivateArchivingParameter::CompressionMethod);
        QVERIFY2(it != privateMap.end(), "Compression method not set");
        QVERIFY(containsType<ArchivingCompressionMethod>(it->second));
        const auto compressionMethod = getValueAs<ArchivingCompressionMethod>(it->second);

        if (testParams.archiveType == ArchiveType::_7z)
        {
            QVERIFY(compressionMethod != ArchivingCompressionMethod::Deflate);
            QVERIFY(compressionMethod != ArchivingCompressionMethod::Deflate64);

        }
        else if (testParams.archiveType == ArchiveType::zip)
        {
            QVERIFY(compressionMethod != ArchivingCompressionMethod::LZMA2);
        }

        it = privateMap.find(PrivateArchivingParameter::DictionarySize);
        QVERIFY2(it != privateMap.end(), "Dictionary size not set");
        QVERIFY(containsType<qulonglong>(it->second));
        const auto dictionarySize = getValueAs<qulonglong>(it->second);

        int wordSize = 0;

        if (compressionMethod != ArchivingCompressionMethod::BZip2)
        {
            it = privateMap.find(PrivateArchivingParameter::WordSize);
            QVERIFY2(it != privateMap.end(), "Word size not set");
            QVERIFY(containsType<int>(it->second));
            wordSize = getValueAs<int>(it->second);
        }

        const auto methodArgs = archivingOptionToCLIarg(testParams.archiveType,
                                                        compressionMethod,
                                                        dictionarySize,
                                                        wordSize);

        for (const auto& arg : methodArgs)
            QVERIFY2(processArgs.contains(arg), arg.toLatin1());

        it = privateMap.find(PrivateArchivingParameter::ThreadCount);
        QVERIFY2(it != privateMap.end(), "Thread count not set");
        QVERIFY(containsType<int>(it->second));
        const auto threadCount = getValueAs<int>(it->second);
        const QString threadCountArg = threadCountToCLIarg(threadCount);
        QVERIFY2(processArgs.contains(threadCountArg), threadCountArg.toLatin1());
    }

    if (testParams.archiveType == ArchiveType::zip
            && testParams.newArchiveEncryption == EncryptionState::FilesContentOnly)
    {
        it = privateMap.find(PrivateArchivingParameter::EncryptionMethod);
        QVERIFY2(it != privateMap.end(), "Encryption method not set");
        QVERIFY(containsType<ArchivingEncryptionMethod>(it->second));
        const auto encryptionMethod = getValueAs<ArchivingEncryptionMethod>(it->second);
        const QString encryptionMethodArg = archivingOptionToCLIarg(encryptionMethod);
        QVERIFY2(processArgs.contains(encryptionMethodArg), encryptionMethodArg.toLatin1());
    }
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::_7Zip::BackendHandlerAdd)

#include "tst_backendhandleradd.moc"
