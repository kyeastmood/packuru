# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

TEMPLATE = subdirs

SUBDIRS += \
    backendhandleradd \
    backendhandlerdelete \
    backendhandlerextract \
    backendhandlerextractshared \
    backendhandlerread \
    backendhandlerreadshared \
    backendhandlertest \
    backendhandlertestshared
