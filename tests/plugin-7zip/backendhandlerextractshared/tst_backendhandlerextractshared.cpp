// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QObject>
#include <QtTest>

#include "src/plugin-7zip-core/backendhandler.h"

#include "tests/tests-shared/backendhandler7zipunarextract.h"


using namespace Packuru::Plugins::_7Zip::Core;
using namespace Packuru::Tests::Shared;

namespace Packuru::Tests::Plugins::_7Zip
{

class BackendHandlerExtractShared : public BackendHandler7ZipUnarExtract
{
    Q_OBJECT
public:
    BackendHandlerExtractShared()
        : BackendHandler7ZipUnarExtract({ [] () { return std::make_unique<BackendHandler>(); },
    [] () { return BackendHandler::executableAvailable(); } }) {}
};

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::_7Zip::BackendHandlerExtractShared)

#include "tst_backendhandlerextractshared.moc"
