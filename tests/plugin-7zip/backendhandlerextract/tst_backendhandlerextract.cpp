// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"

#include "src/plugin-7zip-core/backendhandler.h"

#include "tests/tests-shared/testarchiveextract.h"


using namespace Packuru::Core;
using namespace Packuru::Tests::Shared;
using Packuru::Plugins::_7Zip::Core::BackendHandler;


namespace Packuru::Tests::Plugins::_7Zip
{

class BackendHandlerExtract : public QObject
{
    Q_OBJECT

public:
    BackendHandlerExtract();
    ~BackendHandlerExtract() override;

private slots:
    void initTestCase();

    void extractionError_data();
    void extractionError();
};


BackendHandlerExtract::BackendHandlerExtract()
{

}


BackendHandlerExtract::~BackendHandlerExtract()
{

}


void BackendHandlerExtract::initTestCase()
{
    if (!BackendHandler::executableAvailable())
        QSKIP("Executable not found");
}


void BackendHandlerExtract::extractionError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const std::initializer_list<QLatin1String> paths
    {
        QLatin1String("../../data/zip/corrupted1.zip"),
                QLatin1String("../../data/zip/corrupted2.zip"),
    };

    for (const QLatin1String& path : paths)
        QTest::newRow(path.data())
                << path.data()
                << BackendHandlerError::DataError;

    const std::initializer_list<QLatin1String> paths2
    {
        QLatin1String("../../data/zip/missing_part.scheme1.zip")
    };

    for (const QLatin1String& path : paths2)
        QTest::newRow(path.data())
                << path.data()
                << BackendHandlerError::ArchivePartNotFound;

    const std::initializer_list<QLatin1String> paths3
    {
        QLatin1String("../../data/7z/solid.unix_link.7z"),
                QLatin1String("../../data/zip/unix_link.zip")
    };

    for (const QLatin1String& path : paths3)
        QTest::newRow(path.data())
                << path.data()
                << BackendHandlerError::DangerousLink;
}


void BackendHandlerExtract::extractionError()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveExtractParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    testArchiveExtract(params);
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::_7Zip::BackendHandlerExtract)

#include "tst_backendhandlerextract.moc"
