# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common.pri)

INCLUDEPATH += $$PROJECT_ROOT_DIR

# project main libs
LIBS += -L$$PROJECT_TARGET_LIB_DIR -l$${CORE_BUILD_NAME}

TESTS_TARGET_DIR = $${PROJECT_TARGET_DIR}/tests/
TESTS_SHARED_LIB_TARGET_DIR = $${TESTS_TARGET_DIR}/libs
TESTS_SHARED_LIB_BUILD_NAME = $${PROJECT_BUILD_NAME}-tests-shared
