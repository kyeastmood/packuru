// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include "../core/private/plugin-api/archivingparameterhandlerbase.h"


namespace Packuru::Plugins::_7Zip::Core
{

class ArchivingParametersHandler : public Packuru::Core::ArchivingParameterHandlerBase
{
    Q_OBJECT
public:
    explicit ArchivingParametersHandler(const Packuru::Core::ArchivingParameterHandlerInitData& initData,
                                            QObject *parent = nullptr);
    ~ArchivingParametersHandler() override;

    bool archiveSplittingSupport() const override;
    Packuru::Core::ArchivingCompressionInfo getCompressionInfo() const override;
    Packuru::Core::EncryptionSupport getEncryptionSupport() const override;
    QString getEncryptionMethod() const override;
    void setCompressionLevel(int value) override;
    QVariant getPrivateArchivingData() const override;

private:
    std::unordered_set<Packuru::Core::ArchivingFilePaths> getAdditionalSupportedFilePaths() const override;

    class CompressionLevelCtrl* compressionLevelCtrl = nullptr;
};

}
