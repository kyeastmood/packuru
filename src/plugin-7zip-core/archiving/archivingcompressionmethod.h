// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace Packuru::Plugins::_7Zip::Core
{

enum class ArchivingCompressionMethod
{
    BZip2,
    Deflate, // zip only
    Deflate64, // zip only
    LZMA,
    LZMA2, // 7z only
    PPMd,

    ___INVALID
};

QString toString(ArchivingCompressionMethod value);

}

Q_DECLARE_METATYPE(Packuru::Plugins::_7Zip::Core::ArchivingCompressionMethod);


