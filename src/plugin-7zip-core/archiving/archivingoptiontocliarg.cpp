// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "core/private/plugin-api/archivetype.h"
#include "core/private/plugin-api/archivingfilepaths.h"
#include "core/private/plugin-api/encryptionstate.h"

#include "archivingoptiontocliarg.h"
#include "archivingupdatemode.h"
#include "archivingcompressionlevel.h"
#include "archivingcompressionmethod.h"
#include "archivingsolidblocksize.h"
#include "archivingencryptionmethod.h"


using namespace Packuru::Core;


namespace Packuru::Plugins::_7Zip::Core
{

QString archivingOptionToCLIarg(ArchiveType value)
{
    switch (value)
    {
    case ArchiveType::_7z: return QLatin1String("-t7z");
    case ArchiveType::zip: return QLatin1String("-tzip");
    default: Q_ASSERT(false); return QLatin1String();
    }
}


QString archivingOptionToCLIarg(ArchivingFilePaths value)
{
    switch (value)
    {
    case ArchivingFilePaths::AbsolutePaths: return QLatin1String("-spf");
    case ArchivingFilePaths::FullPaths: return QLatin1String("-spf2");
    case ArchivingFilePaths::RelativePaths: return QLatin1String("");
    default: Q_ASSERT(false); return QLatin1String("");
    }
}


QString archivingOptionToCLIarg(EncryptionState value)
{
    switch (value)
    {
    case EncryptionState::EntireArchive: return QLatin1String("-mhe");
    case EncryptionState::Disabled:
    case EncryptionState::FilesContentOnly:
    case EncryptionState::___INVALID: return QLatin1String();
    default: Q_ASSERT(false); return QLatin1String("");
    }
}


QString archivingOptionToCLIarg(ArchivingUpdateMode value)
{
    switch (value)
    {
    case ArchivingUpdateMode::AddAndReplace: return QLatin1String("-up1q1r2x2y2z2w2");
    case ArchivingUpdateMode::AddAndSkip: return QLatin1String("-up1q1r2x0y0z0w0");
    case ArchivingUpdateMode::AddAndUpdate: return QLatin1String("-up1q1r2x1y2z1w2");
    case ArchivingUpdateMode::UpdateExisting: return QLatin1String("-up1q1r0x1y2z1w2");
    case ArchivingUpdateMode::NewArchive: return QLatin1String("-up0q0r2x2y2z2w2");
    default: Q_ASSERT(false); return QLatin1String("");
    }
}


QString archivingOptionToCLIarg(ArchivingCompressionLevel value)
{
    switch (value)
    {
    case ArchivingCompressionLevel::NoCompression: return QLatin1String("-mx=0");
    case ArchivingCompressionLevel::Fastest: return QLatin1String("-mx=1");
    case ArchivingCompressionLevel::Fast: return QLatin1String("-mx=3");
    case ArchivingCompressionLevel::Normal: return QLatin1String("-mx=5");
    case ArchivingCompressionLevel::Maximum: return QLatin1String("-mx=7");
    case ArchivingCompressionLevel::Ultra: return QLatin1String("-mx=9");
    default: Q_ASSERT(false); return QLatin1String("");
    }
}


QString archivingOptionToCLIarg(ArchivingSolidBlockSize value)
{
    switch(value)
    {
    case ArchivingSolidBlockSize::Solid:
        return QLatin1String("-ms=on");
    case ArchivingSolidBlockSize::NonSolid:
        return QLatin1String("-ms=off");
    default:
        return QLatin1String("-ms=")
                + QString::number(static_cast<qulonglong>(value))
                + QLatin1String("b");
    }
}


QString threadCountToCLIarg(int count)
{
    return QLatin1String("-mmt=") + QString::number(count);
}


QString archivingOptionToCLIarg(ArchivingEncryptionMethod value)
{
    switch(value)
    {
    case ArchivingEncryptionMethod::AES_128: return QLatin1String("-mem=aes128");
    case ArchivingEncryptionMethod::AES_256: return QLatin1String("-mem=aes256");
    case ArchivingEncryptionMethod::ZipCrypto: return QLatin1String("-mem=zipcrypto");
    default: Q_ASSERT(false); return QLatin1String("");
    }
}


std::vector<QString> archivingOptionToCLIarg(ArchiveType archiveType,
                                             ArchivingCompressionMethod method,
                                             qulonglong dictionarySize,
                                             int wordSize)
{
    std::vector<QString> args;
    const auto wordSizeStr = QString::number(wordSize);

    if (archiveType == ArchiveType::zip)
    {
        args.push_back(QLatin1String("-mm=") + toString(method));

        // Dictionary size
        if (method == ArchivingCompressionMethod::PPMd)
            args.push_back(QLatin1String("-mmem=")
                           + QString::number(dictionarySize) + QLatin1String("b"));
        else if (method == ArchivingCompressionMethod::LZMA)
            args.push_back(QLatin1String("-md=")
                           + QString::number(dictionarySize) + QLatin1String("b"));

        // Word size
        switch(method)
        {
        case ArchivingCompressionMethod::Deflate:
        case ArchivingCompressionMethod::Deflate64:
        case ArchivingCompressionMethod::LZMA:
            args.push_back(QLatin1String("-mfb=") + wordSizeStr); break;
        case ArchivingCompressionMethod::PPMd:
            args.push_back(QLatin1String("-mo=") + wordSizeStr); break;
        default:
            break;
        }
    }
    else if (archiveType == ArchiveType::_7z)
    {
        QString methodStr = QLatin1String("-m0=") + toString(method);

        // Dictionary size
        if (method == ArchivingCompressionMethod::PPMd)
            methodStr += QLatin1String(":mem=") + QString::number(dictionarySize) + QLatin1String("b");
        else
            methodStr += QLatin1String(":d=") + QString::number(dictionarySize) + QLatin1String("b");

        // Word size
        switch(method)
        {
        case ArchivingCompressionMethod::LZMA:
        case ArchivingCompressionMethod::LZMA2:
            methodStr += QLatin1String(":fb=") + wordSizeStr; break;
        case ArchivingCompressionMethod::PPMd:
            methodStr += QLatin1String(":o=") + wordSizeStr; break;
        default:
            break;
        }

        args.push_back(methodStr);
    }

    return args;
}


QString archivingOptionToCLIarg(qulonglong partSize)
{
    if (partSize > 0)
        return QLatin1String("-v")
                + QString::number(partSize)
                + QLatin1Char('b');

    return QString();
}

}
