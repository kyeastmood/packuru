// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../qcs-core/controller.h"


namespace Packuru::Plugins::_7Zip::Core
{

class ThreadCountCtrl : public qcs::core::Controller<QString, int>
{
public:
    ThreadCountCtrl();

    void update() override;

protected:
    void onValueSyncedToWidget(const QVariant& value) override;

private:
    int userValue;
};

}
