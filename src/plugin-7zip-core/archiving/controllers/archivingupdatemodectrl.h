// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../qcs-core/controller.h"

#include "../archivingupdatemode.h"


namespace Packuru::Plugins::_7Zip::Core
{

class ArchivingUpdateModeCtrl : public qcs::core::Controller<QString, ArchivingUpdateMode>
{
public:
    ArchivingUpdateModeCtrl();
};

}
