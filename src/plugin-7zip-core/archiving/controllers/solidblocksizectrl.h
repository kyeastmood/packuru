// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../qcs-core/controller.h"

#include "../archivingsolidblocksize.h"


namespace Packuru::Core
{
enum class ArchiveType;
}


namespace Packuru::Plugins::_7Zip::Core
{

class SolidBlockSizeCtrl : public qcs::core::Controller<QString, ArchivingSolidBlockSize>
{
public:
    SolidBlockSizeCtrl(Packuru::Core::ArchiveType archiveType);

    void update() override;

private:
    PublicValueList publicList;
    PrivateValueList privateList;
    Packuru::Core::ArchiveType archiveType_;
};

}
