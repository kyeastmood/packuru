// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../qcs-core/controller.h"


namespace Packuru::Core
{
enum class ArchiveType;
}

namespace Packuru::Plugins::_7Zip::Core
{

class DictionarySizeCtrl : public qcs::core::Controller<QString,qulonglong>
{
public:
    DictionarySizeCtrl(Packuru::Core::ArchiveType archiveType);

    void update() override;

private:
    PublicValueList publicListBZip2;
    PrivateValueList privateListBZip2;

    PublicValueList publicListLZMABoth;
    PrivateValueList privateListLZMABoth;

    PublicValueList publicListPPMd;
    PrivateValueList privateListPPMd;

    Packuru::Core::ArchiveType archiveType_;
};

}
