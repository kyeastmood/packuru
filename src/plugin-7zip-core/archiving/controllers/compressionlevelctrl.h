// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>

#include "../qcs-core/controller.h"

#include "../archivingcompressionlevel.h"


namespace Packuru::Plugins::_7Zip::Core
{

class CompressionLevelCtrl : public qcs::core::Controller<QString, ArchivingCompressionLevel>
{
public:
    CompressionLevelCtrl();

    void externalLevelInput(ArchivingCompressionLevel level);

private:
    PublicValueList publicList;
    PrivateValueList privateList;
};

}
