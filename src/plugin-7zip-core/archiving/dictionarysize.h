// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QtGlobal>


namespace Packuru::Plugins::_7Zip::Core
{

enum class DictionarySizeBZip2 : qulonglong
{
    _100KiB = 100 * 1024,
    _200KiB = _100KiB * 2,
    _300KiB = _100KiB * 3,
    _400KiB = _100KiB * 4,
    _500KiB = _100KiB * 5,
    _600KiB = _100KiB * 6,
    _700KiB = _100KiB * 7,
    _800KiB = _100KiB * 8,
    _900KiB = _100KiB * 9,
};


enum class DictionarySizeDeftate : qulonglong
{
    value = 32 * 1024
};


enum class DictionarySizeDeftate64 : qulonglong
{
    value = 64 * 1024
};


enum class DictionarySizeLZMA : qulonglong
{
    _64KiB = 64 * 1024,
    _1MiB  = 1024 * 1024,
    _2MiB = _1MiB * 2,
    _3MiB = _1MiB * 3,
    _4MiB = _1MiB * 4,
    _6MiB = _1MiB * 6,
    _8MiB = _1MiB * 8,
    _12MiB = _1MiB * 12,
    _16MiB = _1MiB * 16,
    _24MiB = _1MiB * 24,
    _32MiB = _1MiB * 32,
    _48MiB = _1MiB * 48,
    _64MiB = _1MiB * 64,
    _96MiB = _1MiB * 96,
    _128MiB = _1MiB * 128,
    _192MiB = _1MiB * 192,
    _256MiB = _1MiB * 256,
    _384MiB = _1MiB * 384,
    _512MiB = _1MiB * 512,
    _768MiB = _1MiB * 768,
    _1024MiB = _1MiB * 1024,
    _1536MiB = _1MiB * 1536,
};


enum class DictionarySizePPMd7z : qulonglong
{
    _1MiB  = 1024 * 1024,
    _2MiB = _1MiB * 2,
    _3MiB = _1MiB * 3,
    _4MiB = _1MiB * 4,
    _6MiB = _1MiB * 6,
    _8MiB = _1MiB * 8,
    _12MiB = _1MiB * 12,
    _16MiB = _1MiB * 16,
    _24MiB = _1MiB * 24,
    _32MiB = _1MiB * 32,
    _48MiB = _1MiB * 48,
    _64MiB = _1MiB * 64,
    _96MiB = _1MiB * 96,
    _128MiB = _1MiB * 128,
    _192MiB = _1MiB * 192,
    _256MiB = _1MiB * 256,
    _384MiB = _1MiB * 384,
    _512MiB = _1MiB * 512,
    _768MiB = _1MiB * 768,
    _1024MiB = _1MiB * 1024,
};


enum class DictionarySizePPMdZip : qulonglong
{
    _1MiB  = 1024 * 1024,
    _2MiB = _1MiB * 2,
    _4MiB = _1MiB * 4,
    _8MiB = _1MiB * 8,
    _16MiB = _1MiB * 16,
    _32MiB = _1MiB * 32,
    _64MiB = _1MiB * 64,
    _128MiB = _1MiB * 128,
    _256MiB = _1MiB * 256,
};

}
