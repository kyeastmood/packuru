// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>

#include "archivingcompressionmethod.h"


namespace Packuru::Plugins::_7Zip::Core
{

QString toString(ArchivingCompressionMethod value)
{
    switch (value)
    {
    case ArchivingCompressionMethod::BZip2: return "BZip2";
    case ArchivingCompressionMethod::Deflate: return "Deflate";
    case ArchivingCompressionMethod::Deflate64: return  "Deflate64";
    case ArchivingCompressionMethod::LZMA: return "LZMA";
    case ArchivingCompressionMethod::LZMA2: return "LZMA2";
    case ArchivingCompressionMethod::PPMd: return "PPMd";
    default: Q_ASSERT(false); return QString();
    }

}

}
