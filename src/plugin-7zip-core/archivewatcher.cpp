// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QFileSystemWatcher>
#include <QStorageInfo>

#include "archivewatcher.h"


namespace Packuru::Plugins::_7Zip::Core
{

struct ArchiveWatcher::Private
{
    enum class ArchiveState { Existed, Deleted, Unknown };
    ArchiveState archiveState = ArchiveState::Unknown;
    bool diskFull = false;
    QFileSystemWatcher* fsw = nullptr;
    QFileInfo archiveInfo;
    ArchiveWatcher::OperationMode mode;
};


ArchiveWatcher::ArchiveWatcher(const QString& archivePath, OperationMode mode, QObject *parent)
    : QObject(parent)
    , priv(new Private)
{
    priv->fsw = new QFileSystemWatcher(this);
    priv->mode = mode;

    connect(priv->fsw, &QFileSystemWatcher::directoryChanged,
            this, [publ = this] (const auto& path)
    {
        Q_UNUSED(path)

        auto priv = publ->priv.get();
        auto& archiveInfo = priv->archiveInfo;

        const QStorageInfo storageInfo(archiveInfo.absolutePath());
        if (storageInfo.bytesAvailable() == 0)
            priv->diskFull = true;

        if (priv->mode == OperationMode::WatchArchive)
        {
            archiveInfo.refresh();
            auto& archiveState = priv->archiveState;
            using ArchiveState = Private::ArchiveState;

            if (archiveState == ArchiveState::Unknown)
            {
                if (archiveInfo.exists())
                    archiveState = ArchiveState::Existed;
            }
            else if (archiveState == ArchiveState::Existed)
            {
                if (!archiveInfo.exists())
                    archiveState = ArchiveState::Deleted;
            }

        }
    });

    priv->archiveInfo = archivePath;
}


ArchiveWatcher::~ArchiveWatcher()
{

}


void ArchiveWatcher::start()
{
    const QFileInfo info(priv->archiveInfo);
    priv->fsw->addPath(info.absolutePath()); // watch parent dir
}


void ArchiveWatcher::stop()
{
    priv->fsw->removePath(priv->archiveInfo.absolutePath());
}


bool ArchiveWatcher::fullDiskDetected() const
{
    // The only case when 7-Zip deletes an archive during operation seems to be when the disk is full
    return priv->diskFull || priv->archiveState == Private::ArchiveState::Deleted;
}

}
