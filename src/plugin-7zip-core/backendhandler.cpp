// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause


#include <QRegularExpression>
#include <QDir>
#include <QTextStream>
#include <QVector>
#include <QStandardPaths>
#include <QThread>

#include "../utils/private/qvariantunorderedmap.h"

#include "../core/private/plugin-api/backendreaddata.h"
#include "../core/private/plugin-api/backendextractiondata.h"
#include "../core/private/plugin-api/backendtestdata.h"
#include "../core/private/plugin-api/backendarchivingdata.h"
#include "../core/private/plugin-api/backenddeletiondata.h"
#include "../core/private/plugin-api/backendexitcodeinterpretation.h"
#include "../core/private/plugin-api/textstreampreprocessor.h"
#include "../core/private/plugin-api/backendarchiveproperty.h"
#include "../core/private/plugin-api/multipartarchivehelper.h"

#include "archiving/privatearchivingparameter.h"
#include "archiving/privatearchivingparametermap.h"
#include "archiving/archivingupdatemode.h"
#include "archiving/archivingcompressionmethod.h"
#include "archiving/archivingcompressionlevel.h"
#include "archiving/archivingsolidblocksize.h"
#include "archiving/archivingencryptionmethod.h"
#include "archiving/archivingoptiontocliarg.h"
#include "backendhandler.h"
#include "invocationdata.h"
#include "archivewatcher.h"


using namespace Packuru::Core;


namespace
{
const QLatin1String backendExec("7z");
}

namespace Packuru::Plugins::_7Zip::Core
{

BackendHandler::BackendHandler(QObject* parent)
    : CLIBackendHandler(parent),
      archiveType(ArchiveType::___NOT_SUPPORTED)
{
    connect(outPreprocessor, &TextStreamPreprocessor::unfinishedLine,
            this, &BackendHandler::passwordPromptHandler);

    connect(errPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::errorStreamErrorLineHandler);

    connect(errPreprocessor, &TextStreamPreprocessor::unfinishedLine,
            this, &BackendHandler::progressLineHandler);
}


bool BackendHandler::executableAvailable()
{
    return !QStandardPaths::findExecutable(backendExec).isEmpty();
}


QString BackendHandler::createSinglePartArchiveName(const QString& archiveBaseName, ArchiveType archiveType) const
{
    QLatin1String extension;
    switch (archiveType)
    {
    case ArchiveType::zip:
        extension = QLatin1String(".zip");
        break;
    case ArchiveType::_7z:
        extension = QLatin1String(".7z");
        break;
    default:
        Q_ASSERT(false);
        break;
    }

    return archiveBaseName + extension;
}


QString BackendHandler::createMultiPartArchiveName(const QString& archiveBaseName, ArchiveType archiveType) const
{
    switch (archiveType)
    {
    case ArchiveType::_7z:
        return archiveBaseName + QLatin1String(".7z.001");
    case ArchiveType::zip:
        return archiveBaseName + QLatin1String(".zip.001");
    default:
        Q_ASSERT(false);
        return QString();
    }
}


QString BackendHandler::createMultiPartArchiveRegexNamePattern(const QString& archiveBaseName, ArchiveType archiveType) const
{
    Q_ASSERT(archiveType == ArchiveType::_7z || archiveType == ArchiveType::zip);

    /* 7-Zip does not allow creating a multi-part archive when single part archive
       with the same name already exists, e.g. if archive.7z exists it is not possible
       to create a multi-part archive with the same name
       (the parts would be archive.7z.001, archive,7z.002...)
       This might be reported as a bug */
    const QString name = QRegularExpression::escape(createSinglePartArchiveName(archiveBaseName, archiveType));
    // ? at the end of the pattern to take 7-Zip limitation into account and to make that part optional
    // exclude .000
    const QString pattern = name + QLatin1String("(?!\\.000$)(\\.\\d{3,}$)?");
    return pattern;
}


void BackendHandler::readArchiveImpl(const BackendReadData& data)
{
    if (archiveInfo.absoluteFilePath().isEmpty())
        archiveInfo = data.archiveInfo;

    archiveType = data.archiveType;

    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &BackendHandler::onListProcessFinished);

    connect(outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::listInitHandler);

    InvocationData invocationData;
    invocationData.command = "l";
    invocationData.switches.push_back("-slt");
    invocationData.archiveFilePath = archiveInfo.absoluteFilePath();
    /* Password should not be provided in InvocationData when reading archive because it is then
       not possible to distinguish archives with encrypted file names from archives with encrypted
       file content only. Instead helper variable is used and password is entered when needed
       which then means that file names are encrypted. */
    readPassword = data.password;

    startBackend(invocationData);
}


void BackendHandler::extractArchiveImpl(const BackendExtractionData& data)
{
    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &BackendHandler::onProcessFinished);

    connect(outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::outputStreamErrorLineHandler);

    if (archiveInfo.absoluteFilePath().isEmpty())
        archiveInfo = data.archiveInfo;

    InvocationData invocationData;
    invocationData.command = "x";
    invocationData.archiveFilePath = archiveInfo.absoluteFilePath();

    /* This handles the case of multiple identical paths inside an archive.
       For formats that can store multiple versions of the same file we favor the option
       to overwrite everything so the last file version added to the archive will overwrite
       all previous versions during extraction. Same thing when extracting files fore preview.
       The downside is that if an archive contains two identical paths eg. dir1/dir2/item
       but the first is a directory and the second is a file there will be a 'Type mismatch' error.
       There is no perfect solution since for extraction's behavior uniformity sake
       everything is extracted to a temp dir and in plugins we don't handle backend
       overwrite prompts. */
    // NOTE: 7-Zip plugin handles only uncompressed tar
    if (data.extractionForPreview || isTar(data.archiveType))
    {
        invocationData.switches.push_back(QLatin1String("-aoa"));
    }
    else
    {
        /* The remaining formats do not store multiple versions of the files but still can have
           same paths with different types (dir/file). This is not a problem on systems that
           allow identical names for files and dirs (e.g. Windows) so we set the option to
           overwrite everything on these systems but there should not be really anything to overwrite in the
           temp dir. For Unix we set the option to automatically rename the items that are
           extracted from the archive. */
#ifdef Q_OS_UNIX
        invocationData.switches.push_back(QLatin1String("-aou"));
#else
        invocationData.switches.push_back(QLatin1String("-aoa"));
#endif
    }

    invocationData.switches.push_back(QLatin1String("-o") + data.tempDestinationPath);
    invocationData.items = data.filesToExtract;

    // Will enter the password if asked for. This is to distinguish data error from invalid password
    // on corrupted unencrypted archives. If the password is always passed in the command-line even
    // for unencrypted archives, error detection is less precise.
    readPassword = data.password;

    startBackend(invocationData);
}


void BackendHandler::addToArchiveImpl(const BackendArchivingData& data)
{
    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &BackendHandler::onProcessFinished);

    connect(outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::outputStreamErrorLineHandler);

    const auto privateParams = Utils::getValueAs<PrivateArchivingParameterMap>(data.pluginPrivateData);

    QString archiveName = data.archiveName;

    // If there are dots in the archive name (e.g. archive.123)
    // 7-Zip doesn't add any file name extension
    if (data.createNewArchive)
    {
        archiveName = createSinglePartArchiveName(data.archiveName, data.archiveType);
    }

    Q_ASSERT(archiveName.length() > 0);

    QString destinationFolder = data.destinationPath;
    Q_ASSERT(destinationFolder.length() > 0);

    if (!destinationFolder.endsWith(QDir::separator()))
        destinationFolder += QDir::separator();

    InvocationData invocationData;
    invocationData.command = "a";
    invocationData.archiveFilePath = destinationFolder + archiveName;

    auto& switches = invocationData.switches;

    switches.push_back(QLatin1String("-snl")); // store symbolic links as links
    switches.push_back(archivingOptionToCLIarg(data.archiveType));

    if (data.partSize > 0)
    {
        const auto partSizeArg = archivingOptionToCLIarg(data.partSize);
        Q_ASSERT(!partSizeArg.isEmpty());
        switches.push_back(partSizeArg);
    }

    const auto updateMode
            = Utils::getValueAs<ArchivingUpdateMode>(privateParams,
                                                     PrivateArchivingParameter::UpdateMode);
    const auto updateModeArg = archivingOptionToCLIarg(updateMode);
    Q_ASSERT(!updateModeArg.isEmpty());
    switches.push_back(updateModeArg);

    const auto compressionLevel
            = Utils::getValueAs<ArchivingCompressionLevel>(privateParams,
                                                           PrivateArchivingParameter::CompressionLevel);
    const auto compressionLevelArg = archivingOptionToCLIarg(compressionLevel);
    Q_ASSERT(!compressionLevelArg.isEmpty());
    switches.push_back(compressionLevelArg);

    if (compressionLevel != ArchivingCompressionLevel::NoCompression)
    {
        const auto compressionMethod
                = Utils::getValueAs<ArchivingCompressionMethod>(privateParams,
                                                                PrivateArchivingParameter::CompressionMethod);
        Q_ASSERT(compressionMethod != ArchivingCompressionMethod::___INVALID);

        if (data.archiveType == ArchiveType::_7z)
        {
            Q_ASSERT(compressionMethod != ArchivingCompressionMethod::Deflate);
            Q_ASSERT(compressionMethod != ArchivingCompressionMethod::Deflate64);

        }
        else if (data.archiveType == ArchiveType::zip)
        {
            Q_ASSERT(compressionMethod != ArchivingCompressionMethod::LZMA2);
        }

        const auto dictionarySize
                = Utils::getValueAs<qulonglong>(privateParams,
                                                PrivateArchivingParameter::DictionarySize);

        int wordSize = 0;

        if (compressionMethod != ArchivingCompressionMethod::BZip2)
            wordSize = Utils::getValueAs<int>(privateParams,
                                              PrivateArchivingParameter::WordSize);

        const std::vector<QString> methodArgs = archivingOptionToCLIarg(data.archiveType,
                                                                        compressionMethod,
                                                                        dictionarySize,
                                                                        wordSize);

        switches.insert(switches.end(), methodArgs.begin(), methodArgs.end());

        if (data.archiveType == ArchiveType::_7z)
        {
            const auto solidBlockSize
                    = Utils::getValueAs<ArchivingSolidBlockSize>(privateParams,
                                                                 PrivateArchivingParameter::SolidBlockSize);
            const auto solidBlockSizeArg = archivingOptionToCLIarg(solidBlockSize);
            Q_ASSERT(!solidBlockSizeArg.isEmpty());
            switches.push_back(solidBlockSizeArg);
        }

        const auto threadCount = Utils::getValueAs<int>(privateParams,
                                                        PrivateArchivingParameter::ThreadCount);

        switches.push_back(threadCountToCLIarg(threadCount));
    }

    if (data.archiveType == ArchiveType::zip)
    {
        Q_ASSERT(data.currentArchiveEncryption != EncryptionState::EntireArchive);
        Q_ASSERT(data.newArchiveEncryption != EncryptionState::EntireArchive);
    }

    const auto encryptionArg = archivingOptionToCLIarg(data.newArchiveEncryption);
    if (!encryptionArg.isEmpty())
        switches.push_back(encryptionArg);

    if (data.newArchiveEncryption == EncryptionState::EntireArchive
            || data.newArchiveEncryption == EncryptionState::FilesContentOnly)
    {
        invocationData.password = data.password;

        // 7z uses only AES-256
        if (data.archiveType == ArchiveType::zip
                && data.newArchiveEncryption == EncryptionState::FilesContentOnly)
        {
            const auto encryptionMethod
                    = Utils::getValueAs<ArchivingEncryptionMethod>(privateParams,
                                                                   PrivateArchivingParameter::EncryptionMethod);
            const auto encryptionMethodArg = archivingOptionToCLIarg(encryptionMethod);
            Q_ASSERT(!encryptionMethodArg.isEmpty());
            switches.push_back(encryptionMethodArg);
        }
    }

    const auto filePathsArg = archivingOptionToCLIarg(data.filePaths);
    if (!filePathsArg.isEmpty())
        switches.push_back(filePathsArg);

    for (const auto& item : data.inputItems )
        invocationData.items.push_back(item.absoluteFilePath());

    QString watchArchiveName = createSinglePartArchiveName(data.archiveName, data.archiveType);
    ArchiveWatcher::OperationMode watchMode;

    if (data.createNewArchive)
    {
        if (data.partSize > 0)
            watchArchiveName = createMultiPartArchiveName(data.archiveName, data.archiveType);
        watchMode = ArchiveWatcher::OperationMode::WatchArchive;
    }
    else // 7-Zip doesn't support editing multipart archives
        watchMode = ArchiveWatcher::OperationMode::WatchParentDirectory;

    archiveWatcher = new ArchiveWatcher(destinationFolder + watchArchiveName, watchMode, this);
    archiveWatcher->start();

    startBackend(invocationData);
}


void BackendHandler::deleteFilesImpl(const BackendDeletionData& data)
{
    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &BackendHandler::onProcessFinished);

    connect(outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::outputStreamErrorLineHandler);

    InvocationData invocationData;
    invocationData.command = "d";
    invocationData.archiveFilePath = data.archiveInfo.absoluteFilePath();
    invocationData.items = data.absoluteFilePaths;
    invocationData.password = data.password;

    auto& switches = invocationData.switches;

    switch(data.archiveType)
    {
    case ArchiveType::_7z: switches.push_back(QLatin1String("-t7z")); break;
    case ArchiveType::zip: switches.push_back(QLatin1String("-tzip")); break;
    default: Q_ASSERT(false); break;
    }

    archiveWatcher = new ArchiveWatcher(invocationData.archiveFilePath,
                                        ArchiveWatcher::OperationMode::WatchParentDirectory,
                                        this);
    archiveWatcher->start();

    startBackend(invocationData);
}


void BackendHandler::testArchiveImpl(const BackendTestData& data)
{
    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &BackendHandler::onProcessFinished);

    connect(outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::outputStreamErrorLineHandler);

    if (archiveInfo.absoluteFilePath().isEmpty())
        archiveInfo = data.archiveInfo;

    InvocationData invocationData;
    invocationData.command = "t";
    invocationData.archiveFilePath = archiveInfo.absoluteFilePath();

    // Will enter the password if asked for. This is to distinguish data error from invalid password
    // on corrupted unencrypted archives. If the password is always passed in the command-line even
    // for unencrypted archives, error detection is less precise.
    readPassword = data.password;

    startBackend(invocationData);
}


void BackendHandler::startBackend(const InvocationData& data)
{
    changeState(BackendHandlerState::InProgress);

    logCommand(backendExec, composeCommand(data, false));

    if (!data.password.isEmpty())
        passwordReceived_ = true;

    const auto args = composeCommand(data, true);

    backendProcess->start(backendExec, args);
}


QStringList BackendHandler::composeCommand(const InvocationData& data, bool includePassword) const
{
    QStringList args {data.command};

    for (const auto& s : data.switches)
        args.push_back(s);

    if (!data.password.isEmpty())
    {
        if (includePassword)
            args << QLatin1String("-p") + data.password;
        else
            args << QLatin1String("-p*");
    }

    QStringList globalSwitches {"-bsp2", "-bse2", "-bso1"};

    args << globalSwitches << data.archiveFilePath;

    for (const auto& s : data.items)
        args.push_back(s);

    return args;
}


void BackendHandler::passwordPromptHandler(const QString& line)
{   
    if (line.startsWith(QLatin1String("Enter password")))
    {
        if (!readPassword.isEmpty())
        {
            const auto copy = readPassword;
            readPassword.clear();
            enterPassword(copy);
        }
        else
            changeState(BackendHandlerState::WaitingForPassword);
    }
}


void BackendHandler::progressLineHandler(QString& line, int newDataPosition)
{ 
    const int index = line.indexOf('%', newDataPosition);

    if (index < 0)
        return;

    QTextStream progressLineStreamer;

    // Using const_cast - there seems to be no other way to stream const QString with QTextStream
    progressLineStreamer.setString(const_cast<QString*>(&line), QIODevice::ReadOnly);

    // Seek to file count
    if (!progressLineStreamer.seek(index + 2) || progressLineStreamer.status() != QTextStream::Ok)
        return;

    bool validProgress = false;
    QChar c;
    progressLineStreamer >> c;

    if (progressLineStreamer.status() != QTextStream::Ok)
        return;

    // Skip file count
    if (c.isDigit())
    {
        while (!progressLineStreamer.atEnd())
        {
            progressLineStreamer >> c;

            if (progressLineStreamer.status() != QTextStream::Ok)
                return;

            if (!c.isDigit())
                break;
        }

        progressLineStreamer >> c;

        if (progressLineStreamer.status() != QTextStream::Ok)
            return;
    }

    // Check for marker after file count
    if (progressMarkers.contains(c))
        validProgress = true;

    if (!validProgress)
        return;

    const auto copy = line.midRef(index - 3, 3);
    bool ok = false;
    const int progress = copy.toInt(&ok);
    if (ok)
    {
        changeProgress(progress);
        // Prevents accumulation of previous chunks which might end up being processed in errorStreamErrorLineHandler
        line.clear();
    }
}


void BackendHandler::errorStreamErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        auto error = BackendHandlerError::___INVALID;

        if (line.endsWith(QLatin1String("Cannot open encrypted archive. Wrong password?")))
            error = BackendHandlerError::InvalidPasswordOrDataError;
        else if ((line.startsWith(QLatin1String("Headers Error"))
                  && line != QLatin1String("Headers Error in encrypted archive. Wrong password?"))
                 || line.startsWith(QLatin1String("Unexpected end of archive"))
                 // Annoying warning that doesn't really mean that archive is corrupted. But 7-Zip
                 // is not able to add files to such archive so it must be reported.
                 || line.startsWith(QLatin1String("There are data after the end of archive")))
            error = BackendHandlerError::DataError;
        else if (line.startsWith(QLatin1String("Open ERROR: Cannot open the file"))
                 || line.endsWith(QLatin1String("Can not open the file as archive")))
            error = BackendHandlerError::DataError;
        else if (line == QLatin1String("WARNING: Permission denied")
                 || line == QLatin1String("Permission denied"))
            error = BackendHandlerError::AccessDenied;
        else
        {
            // Lines can contain garbage at the beginning if e.g. progressLineHandler
            // doesn't clear the buffer but not only
            if (int pos = line.indexOf(QLatin1String("ERROR: Data Error : ")) >= 0)
            {
                auto error = BackendHandlerError::DataError;
                if (passwordReceived_)
                    error = BackendHandlerError::InvalidPasswordOrDataError;

                addError(error, line.mid(pos));
            }
            else if (int pos = line.indexOf(QLatin1String("ERROR: There are some data after the end"
                                                          " of the payload data : ")) >= 0)
                addError(BackendHandlerError::DataError, line.mid(pos));
            else if (int pos = line.indexOf(QLatin1String("ERROR: Unexpected end of data : ")) >= 0)
                addError(BackendHandlerError::DataError, line.mid(pos));
            else if (int pos = line.indexOf(QLatin1String("ERROR: CRC Failed : ")) >= 0)
            {
                auto error = BackendHandlerError::DataError;
                if (passwordReceived_)
                    error = BackendHandlerError::InvalidPasswordOrDataError;
                addError(error, line.mid(pos));
            }

            else if (int pos = line.indexOf(QLatin1String("ERROR: Unsupported Method : ")) >= 0)
                addError(BackendHandlerError::ArchivingMethodNotSupported, line.mid(pos));

            // When provided invalid password during extraction of zip archive
            // with content only encryption
            else if (int pos = line.indexOf(QLatin1String("ERROR: Wrong password : ")) >= 0)
                addError(BackendHandlerError::InvalidPasswordOrDataError, line.mid(pos));

            else if (int pos = line.indexOf(QLatin1String("ERROR: Data Error in encrypted file."
                                                          " Wrong password?")) >= 0)
                addError(BackendHandlerError::InvalidPasswordOrDataError, line.mid(pos));

            // Corrupted file in fully encrypted 7z archive
            else if (int pos = line.indexOf(QLatin1String("ERROR: CRC Failed in encrypted file."
                                                          " Wrong password?")) >= 0)
                addError(BackendHandlerError::InvalidPasswordOrDataError, line.mid(pos));

            // Corrupted zip
            else if (int pos = line.indexOf(QLatin1String("ERROR: Headers Error : ")) >= 0)
                addError(BackendHandlerError::DataError, line.mid(pos));

            // when trying to open an item as a directory but it's a file
            else if (int pos = line.indexOf(QLatin1String("ERROR: Can not open output file :"
                                                          " Not a directory : ")) >= 0)
                addError(BackendHandlerError::ItemTypeMismatch, line.mid(pos));

            // when trying to write a file over existing non empty directory
            else if (int pos = line.indexOf(QLatin1String("ERROR: Can not delete output folder :"
                                                          " Directory not empty : ")) >= 0)
                addError(BackendHandlerError::ItemTypeMismatch, line.mid(pos));

            else if (int pos = line.indexOf(QLatin1String("ERROR: Dangerous link path was ignored : ")) >= 0)
                addError(BackendHandlerError::DangerousLink, line.mid(pos));
        }

        if (error != BackendHandlerError::___INVALID)
            addError(error, line);
        else
            addLogLine(line);
    }
}


void BackendHandler::outputStreamErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
        checkStdOutLineForErrors(line);
}


void BackendHandler::checkStdOutLineForErrors(QStringRef line)
{
    if ((line.startsWith(QLatin1String("Headers Error"))
         && line != QLatin1String("Headers Error in encrypted archive. Wrong password?"))
            || line.startsWith(QLatin1String("Unconfirmed start of archive"))
            || line.startsWith(QLatin1String("Unexpected end of archive"))

            // Not really an error, most often archive format is different than its extension suggests
            // or it's an SFX archive and 7-Zip complains about the executable (PE) part of archive. */
            // || line.startsWith(QLatin1String("Open WARNING: Can not open the file as"))

            // Annoying warning that doesn't really mean that archive is corrupted. But 7-Zip
            // is not able to add files to such archive so it must be reported.
            || line.startsWith(QLatin1String("There are data after the end of archive")))
        addError(BackendHandlerError::DataError, line);
    else if (line.startsWith(archiveInfo.baseName()) && line.endsWith(QLatin1String(": No more files")))
        addError(BackendHandlerError::FileNotFound, line); // When adding files to archive
    else if (line == QLatin1String("Warning: The archive is open with offset"))
        addError(BackendHandlerError::DataError, line);
    else if (line.startsWith(QLatin1String("ERROR = Missing volume :"))
             || line.startsWith(QLatin1String("ERROR = Can't open volume:")))
        addError(BackendHandlerError::ArchivePartNotFound, line);
    else if (line.startsWith(QLatin1String("Open ERROR: Cannot open the file")))
        addError(BackendHandlerError::DataError, line);
}


void BackendHandler::listInitHandler(const std::vector<QStringRef>& lines)
{
    for (std::size_t i = 0; i < lines.size(); ++i)
    {
        const auto& line = lines[i];

        /* In buffered mode (which currently is not used because progress/errors are sent to error stream)
           7-Zip doesn't add a new line after password prompt so "--" is printed in the same line */
        if (line == (QLatin1String("--")) || line == (QLatin1String("Enter password (will not be echoed):--")))
        {
            disconnect(outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
                       this, nullptr);

            connect(outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
                    this, &BackendHandler::listHeaderHandler);

            return listHeaderHandler(lines, i + 1);
        }
    }
}


void BackendHandler::listHeaderHandler(const std::vector<QStringRef>& lines, std::size_t firstLine)
{
    for (std::size_t i = firstLine; i < lines.size(); ++i)
    {
        const auto& line = lines[i];

        if (line.startsWith(QLatin1String("Comment = ")))
        {
            archiveProperties[BackendArchiveProperty::Comment] = line.mid(10).toString();
        }
        else if (line.startsWith(QLatin1String("Method = ")))
        {
            auto& method = archiveProperties[BackendArchiveProperty::Methods];
            auto set = method.value<Utils::QStringUnorderedSet>();
            const auto strings = line.mid(9).trimmed().split(QLatin1Char(' '));
            for (const auto& str : strings)
                set.insert(str.toString());
            method.setValue(set);
        }
        else if (line.startsWith(QLatin1String("Characteristics = ")))
        {
            if (line.lastIndexOf(QLatin1String("Lock")) > 0)
                archiveProperties[BackendArchiveProperty::Locked] = true;

            if (line.lastIndexOf(QLatin1String("Recovery")) > 0)
                archiveProperties[BackendArchiveProperty::RecoveryData] = true;
        }
        else if (line.startsWith(QLatin1String("Solid = ")))
        {
            if (line.mid(8).startsWith(QLatin1Char('+')))
                archiveProperties[BackendArchiveProperty::Solid] = true;
            else
                archiveProperties[BackendArchiveProperty::Solid] = false;
        }
        else if (line.startsWith(QLatin1String("Multivolume = ")))
        {
            if (line.mid(14).startsWith(QLatin1Char('+')))
                archiveProperties[BackendArchiveProperty::Multipart] = true;
            else
                archiveProperties[BackendArchiveProperty::Multipart] = false;
        }
        else if (line == QLatin1String("Type = Split"))
        {
            splitType = true;
        }
        else if (line.startsWith(QLatin1String("Volumes = ")))
        {
            // In wim archives volumes can be printed twice with different values;
            // first is right
            archiveProperties.try_emplace(BackendArchiveProperty::PartCount,
                                          line.mid(10).toInt() );
        }
        // Total size is not always valid for multipart archives so it will be
        // computed separately
        // else if (line.startsWith(QLatin1String("Total Physical Size = ")))
        // {
        // archiveProperties[BackendArchiveProperty::TotalSize] = line.mid(22).toULongLong();
        // }
        else if (line.startsWith(QLatin1String("----------")))
        {
            disconnect(outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable, this, nullptr);

            connect(outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
                    this, &BackendHandler::listEntriesHandler);

            return listEntriesHandler(lines, i + 1);
        }
        else
            checkStdOutLineForErrors(line);
    }
}


void BackendHandler::listEntriesHandler(const std::vector<QStringRef>& lines, std::size_t firstLine)
{
    for (std::size_t i = firstLine; i < lines.size(); ++i)
    {
        const auto& line = lines[i];

        if (line.startsWith(QLatin1String("Path = ")))
        {
            tempItem.name = QDir::fromNativeSeparators(line.mid(7).toString());
        }
        else if (line.startsWith(QLatin1String("CRC = ")))
        {
            tempItem.checksum = line.mid(6).toString();
        }
        else if (line.startsWith(QLatin1String("Host OS = ")))
        {
            tempItem.hostOS = line.mid(10).toString();
        }
        else if (line.startsWith(QLatin1String("Size =")))
        {
            // In xar archives 7-zip doesn't print info about file type;
            // the only sign of a folder is that no size has been printed so the line length is 6
            if (line.length() == 6)
            {
                if (archiveType == ArchiveType::xar)
                    tempItem.nodeType = NodeType::Folder;
            }
            else
            {
                if (archiveType == ArchiveType::xar)
                    tempItem.nodeType = NodeType::File;
                tempItem.originalSize = line.mid(7).toULongLong();
            }
        }
        else if (line.startsWith(QLatin1String("Packed Size = ")))
        {
            tempItem.packedSize = line.mid(14).toULongLong();
        }
        else if (line.startsWith(QLatin1String("Method = ")))
        {
            Q_ASSERT(archiveType != ArchiveType::___NOT_SUPPORTED);

            // Methods are printed at the beginning of 7z archives
            if (archiveType != ArchiveType::_7z)
            {
                const auto methods = line.mid(9).split(QLatin1Char(' '));
                for (const auto& strRef : methods)
                    methodsSet.insert(strRef.toString());
            }

            tempItem.method = line.mid(9).toString();
        }
        else if (line.startsWith(QLatin1String("Mode = ")))
        {
            tempItem.attributes = line.mid(7).toString();
        }
        else if (line.startsWith(QLatin1String("Modified = ")))
        {
            tempItem.modified = QDateTime::fromString(line.mid(11).toString(), Qt::ISODate);
        }
        else if (line.startsWith(QLatin1String("Created = ")))
        {
            tempItem.created = QDateTime::fromString(line.mid(10).toString(), Qt::ISODate);
        }
        else if (line.startsWith(QLatin1String("Accessed = ")))
        {
            tempItem.accessed = QDateTime::fromString(line.mid(11).toString(), Qt::ISODate);
        }
        else if (line.startsWith(QLatin1String("User = ")))
        {
            tempItem.user = line.mid(7).toString();
        }
        else if (line.startsWith(QLatin1String("Group = ")))
        {
            tempItem.group = line.mid(8).toString();
        }
        else if (line.startsWith(QLatin1String("Attributes = ")))
        {
            tempItem.attributes = line.mid(13).toString();

            if (tempItem.nodeType == NodeType::File)
            {
                if (tempItem.attributes.contains(QLatin1Char('d'), Qt::CaseInsensitive))
                    tempItem.nodeType = NodeType::Folder;
                else if (tempItem.attributes.contains(QLatin1Char('l'), Qt::CaseInsensitive))
                    tempItem.nodeType = NodeType::Link;
            }
        }
        // Don't process if it's already been changed from 'file'
        else if (line.startsWith(QLatin1String("Folder = ")) && tempItem.nodeType == NodeType::File)
        {
            const auto mid = line.mid(9);
            if (mid.startsWith(QLatin1Char('+')))
                tempItem.nodeType = NodeType::Folder;
        }
        else if (line.startsWith(QLatin1String("Encrypted = ")))
        {
            tempItem.encrypted = line.mid(12).startsWith(QLatin1Char('+'));
            encryptedFiles = tempItem.encrypted;
        }
        else if (line.startsWith(QLatin1String("Comment = ")))
        {
            tempItem.comment = line.mid(9).toString();
        }
        // Don't process if it's already been changed from 'file'
        else if (line.startsWith(QLatin1String("Symbolic Link = ")) && tempItem.nodeType == NodeType::File)
        {
            if (line.size() > 16)
                tempItem.nodeType = NodeType::Link;
        }
        else if (line.isEmpty())
        {
            if (!tempItem.name.isEmpty())
            {
                /* In fully encrypted archives the folders are not marked as encrypted
                   (even though in reality they are). So we mark them as encrypted because
                   it allows to quickly recognize the archive as fully encrypted without
                   checking its properties. */
                if (passwordReceived_)
                    tempItem.encrypted = true;
                items.emplace_back(std::move(tempItem));
            }
            else
                tempItem = ArchiveItem();
        }
        else
            checkStdOutLineForErrors(line);
    }

    if (!items.empty())
        emit newItems(items);
}


void BackendHandler::onProcessFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    Q_UNUSED(exitStatus)

    const auto interpretation = exitCode == 0 ? BackendExitCodeInterpretation::Success
                                              : BackendExitCodeInterpretation::WarningsOrErrors;

    /* These are additional checks in case full disk was not detected while parsing
     * error lines nor by DiskSpaceWatcher. E.g. when creating archive no error is printed.
     * Also there are other processes running which can free or aquire disk space so these checks
     * do not guarantee 100% detection. */
    if (isWritingTask(getTaskType())
            && interpretation != BackendExitCodeInterpretation::Success
            && archiveWatcher)
    {
        if (archiveWatcher->fullDiskDetected())
            addError(BackendHandlerError::DiskFull);

        archiveWatcher->stop();
    }

    const BackendHandlerState state = handleFinishedProcess(interpretation);
    changeState(state);
}


void BackendHandler::onListProcessFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    auto encryptionState = EncryptionState::Disabled;

    if (passwordReceived_)
        encryptionState = EncryptionState::EntireArchive;
    else if (encryptedFiles)
        encryptionState = EncryptionState::FilesContentOnly;

    archiveProperties[BackendArchiveProperty::EncryptionState].setValue(encryptionState);

    if (splitType)
        archiveProperties[BackendArchiveProperty::Multipart] = true;

    auto it = archiveProperties.find(BackendArchiveProperty::Multipart);
    if (it != archiveProperties.end())
    {
        const auto multipart = it->second.toBool();

        if (multipart)
        {
            it = archiveProperties.find(BackendArchiveProperty::PartCount);
            const int partCount = it->second.toInt();

            archiveProperties[BackendArchiveProperty::TotalSize]
                    = computeArchiveSize(archiveInfo.absoluteFilePath(),
                                         archiveType,
                                         partCount);
        }
    }

    if (archiveType != ArchiveType::_7z)
    {
        if (!methodsSet.empty())
        {
            auto& methods = archiveProperties[BackendArchiveProperty::Methods];
            auto set = methods.value<Utils::QStringUnorderedSet>();
            set.insert(methodsSet.begin(), methodsSet.end());
            methods.setValue(set);
        }
    }

    onProcessFinished(exitCode, exitStatus);
}

}
