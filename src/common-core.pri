# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(common-lib.pri)

CONFIG += file_copies
COPIES += public_headers
