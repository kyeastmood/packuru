// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QDialog>


namespace Ui {
class QueueMessengerDialog;
}

namespace Packuru::Core::Browser
{
class QueueMessenger;
}


class QueueMessengerDialog : public QDialog
{
    Q_OBJECT
public:
    explicit QueueMessengerDialog(Packuru::Core::Browser::QueueMessenger* messenger, QWidget *parent = nullptr);
    ~QueueMessengerDialog();

private:
    Ui::QueueMessengerDialog *ui;
    Packuru::Core::Browser::QueueMessenger* messenger;
};
