// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../utils/qvariant_utils.h"

#include "../desktop-core/globalsettings.h"

#include "desktopbrowsersettingsconverter.h"


using namespace Packuru::Core;
using Packuru::Desktop::GlobalSettings;


DesktopBrowserSettingsConverter::DesktopBrowserSettingsConverter()
{

}


std::unordered_set<int> DesktopBrowserSettingsConverter::getSupportedKeys() const
{
    return {toInt(GlobalSettings::DoubleClickToOpenItems)};
}


QString DesktopBrowserSettingsConverter::getKeyName(int key) const
{
    const auto keyId = static_cast<GlobalSettings>(key);
    switch (keyId)
    {
    case GlobalSettings::DoubleClickToOpenItems:
        return QLatin1String("dc_double_click_to_open_items");
    default:
        Q_ASSERT(false); return QLatin1String();
    }
}


QVariant DesktopBrowserSettingsConverter::toSettingsType(int key, const QVariant& value) const
{
    const auto keyId = static_cast<GlobalSettings>(key);
    switch (keyId)
    {
    case GlobalSettings::DoubleClickToOpenItems:
        Q_ASSERT(Packuru::Utils::containsType<bool>(value));
        return value;
    default:
        Q_ASSERT(false);
        return QVariant();
    }
}


QVariant DesktopBrowserSettingsConverter::fromSettingsType(int key, const QVariant& value) const
{
    const auto keyId = static_cast<GlobalSettings>(key);
    switch (keyId)
    {
    case GlobalSettings::DoubleClickToOpenItems:
        if (value.canConvert<bool>())
            return value.toBool();
        else
            return false;
    default:
        Q_ASSERT(false);
        return QVariant();
    }
}
