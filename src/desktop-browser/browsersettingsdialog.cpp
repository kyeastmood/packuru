// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QIcon>

#include "../core-browser/settingsdialogcore.h"

#include "browsersettingsdialog.h"
#include "browsersettingspage.h"


using namespace Packuru::Core::Browser;
using namespace Packuru::Core;


BrowserSettingsDialog::BrowserSettingsDialog(AppSettingsDialogCore* appSettingsCore,
                                             SettingsDialogCore* browserSettingsCore,
                                             QWidget *parent)
    : SettingsDialog(appSettingsCore, parent)
{
    browserSettingsCore->setParent(this);

    const QString browserPageTitle = SettingsDialogCore::getString(SettingsDialogCore
                                                                   ::UserString::PageTitle);

    auto browserPage = new BrowserSettingsPage(browserSettingsCore->getControllerEngine(),
                                               browserPageTitle);

    insertSettingsPage(1,
                       QIcon::fromTheme("document-open"),
                       browserPageTitle,
                       browserPage);

    connect(this, &QDialog::accepted,
            browserPage, &BrowserSettingsPage::accept);

    connect(this, &QDialog::accepted,
            browserSettingsCore, &SettingsDialogCore::accept);
}


BrowserSettingsDialog::~BrowserSettingsDialog()
{
}
