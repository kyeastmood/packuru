﻿// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <vector>

#include <QGuiApplication>
#include <QSettings>
#include <QTimer>
#include <QDateTime>
#include <QFocusEvent>

#include "../utils/localholder.h"

#include "../qcs-qtwidgets/widgetmapper.h"

#include "../core/commonstrings.h"
#include "../core/globalsettingsmanager.h"

#include "../core-browser/archivenavigator.h"
#include "../core-browser/viewfiltercontrollertype.h"

#include "../desktop-core/globalsettings.h"
#include "../desktop-core/createcolumnconfigmenu.h"

#include "navigationwidget.h"
#include "ui_navigationwidget.h"


using namespace Packuru::Core::Browser;
using namespace Packuru::Core;
using namespace Packuru;


using Packuru::Desktop::GlobalSettings;


struct NavigationWidget::Private
{
    Private(NavigationWidget* publ, ArchiveNavigator* navigator);
    ~Private();

    void openItem(const QModelIndex& index);
    void prepareHeaderView();
    void saveHeaderViewState();
    bool restoreHeaderViewState();
    void onHeaderViewStateChanged();
    void closeFilter();

    Ui::NavigationWidget *ui;
    NavigationWidget* publ;
    ArchiveNavigator* navigator = nullptr;
    qcs::qtw::WidgetMapper* widgetMapper = nullptr;
    static const QString settingsKeyHeaderViewState;
    static const QString settingsKeyHeaderViewUpdateTime;
    QTimer* headerViewSaveTimer;
    qulonglong headerViewUpdateTime = 0;
    bool restoringHeaderViewState = false;
    bool doubleClickToOpen = true;
};


const QString NavigationWidget::Private::settingsKeyHeaderViewState = "browser_navigationwidget/contentview_headerview_state";
const QString NavigationWidget::Private::settingsKeyHeaderViewUpdateTime = "browser_navigationwidget/contentview_headerview_updatetime";


NavigationWidget::NavigationWidget(ArchiveNavigator* navigator, QWidget *parent)
    : QWidget(parent),
      priv(new Private(this, navigator))
{
    priv->doubleClickToOpen = GlobalSettingsManager::instance()
            ->getValueAs<bool>(GlobalSettings::DoubleClickToOpenItems);

    connect(GlobalSettingsManager::instance(), &GlobalSettingsManager::valueChanged,
            this, [priv= priv.get()] (int key, const QVariant& value)
    {
        const auto keyId = static_cast<GlobalSettings>(key);

        switch (keyId)
        {
        case GlobalSettings::DoubleClickToOpenItems:
            priv->doubleClickToOpen = Utils::getValueAs<bool>(value);
            break;
        }
    });

    connect(priv->ui->treeView, &QAbstractItemView::clicked,
            this, [priv = priv.get()] (const QModelIndex& index)
    {
        if (!priv->doubleClickToOpen)
            priv->openItem(index);
    });

    connect(priv->ui->treeView, &QAbstractItemView::doubleClicked,
            this, [priv = priv.get()] (const QModelIndex& index)
    {
        if (priv->doubleClickToOpen)
            priv->openItem(index);
    });

    connect(priv->ui->goUpButton, &QToolButton::clicked,
            priv->navigator, &ArchiveNavigator::goUp);

    connect(priv->ui->pathEdit, &QLineEdit::returnPressed,
            this, [priv = priv.get()] ()
    { priv->navigator->changeDir(priv->ui->pathEdit->text()); });

    // Not used yet
    priv->ui->notificationWidget->hide();
}


NavigationWidget::~NavigationWidget()
{

}


void NavigationWidget::openFilterWidget()
{
    if (!priv->widgetMapper)
    {
        using Type = Packuru::Core::Browser::ViewFilterControllerType::Type;

        priv->widgetMapper = new qcs::qtw::WidgetMapper(this);
        priv->widgetMapper->setEngine(priv->navigator->getFilterControllerEngine());

        priv->widgetMapper->addLabelAndWidget(Type::FilterScope,
                                              priv->ui->filterScopeLabel,
                                              priv->ui->filterScopeCombo);
        priv->widgetMapper->addLabelAndWidget(Type::FilterFiles,
                                              priv->ui->filterFilesLabel,
                                              priv->ui->filterFilesCombo);
        priv->widgetMapper->addLabelAndWidget(Type::FilterFolders,
                                              priv->ui->filterFoldersLabel,
                                              priv->ui->filterFoldersCombo);
        priv->widgetMapper->addWidgets(Type::FilterText, priv->ui->filterEdit);

        priv->widgetMapper->updateWidgets();
    }

    priv->ui->filterWidget->setVisible(true);
    priv->ui->filterEdit->setFocus();
}


bool NavigationWidget::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::FocusIn)
    {
        const auto focusEvent = static_cast<QFocusEvent*>(event);
        Q_ASSERT(event);

        const auto reason = focusEvent->reason();
        if  (reason == Qt::MouseFocusReason
             || reason == Qt::TabFocusReason
             || reason == Qt::BacktabFocusReason
             || reason == Qt::OtherFocusReason)
        {
            auto widget = qobject_cast<QWidget*>(watched);
            Q_ASSERT(widget);
            setFocusProxy(widget);
        }
    }

    return false;
}


NavigationWidget::Private::Private(NavigationWidget* publ, ArchiveNavigator* navigator)
    : ui(new Ui::NavigationWidget),
      publ(publ),
      navigator(navigator),
      headerViewSaveTimer(new QTimer(publ))
{
    ui->setupUi(publ);

    navigator->setPreviousPathIndexSelection(ArchiveNavigator::PreviousPathIndexSelection::SelectRow);

    ui->treeView->setModel(navigator->getModel());
    ui->treeView->setSelectionModel(navigator->getSelectionModel());

    QObject::connect(navigator, &ArchiveNavigator::currentPathIndexChanged,
                     publ, [publ = publ] (const QModelIndex& index)
    {
        auto view = publ->priv->ui->treeView;
        view->setRootIndex(index);
        view->collapseAll();
    });

    QObject::connect(navigator, &ArchiveNavigator::currentPathChanged,
                     ui->pathEdit, &QLineEdit::setText);

    ui->goUpButton->setEnabled(false);
    QObject::connect(navigator, &ArchiveNavigator::canGoUpChanged,
                     ui->goUpButton, &QWidget::setEnabled);

    ui->filterCloseButton->setText(navigator->getString(ArchiveNavigator::UserString::Close));

    ui->filterWidget->hide();

    auto actionCloseFilter = new QAction(publ);
    actionCloseFilter->setShortcutContext(Qt::WidgetWithChildrenShortcut);
    actionCloseFilter->setShortcut(Qt::Key_Escape);
    publ->addAction(actionCloseFilter);

    QObject::connect(actionCloseFilter, &QAction::triggered,
                     [priv = this] () { priv->closeFilter(); });

    QObject::connect(ui->filterCloseButton, &QAbstractButton::clicked,
                     [priv = this] () { priv->closeFilter(); });

    auto actionBack = new QAction(publ);
    actionBack->setShortcutContext(Qt::WidgetShortcut);
    actionBack->setShortcut(Qt::Key_Backspace);
    ui->treeView->addAction(actionBack);

    QObject::connect(actionBack, &QAction::triggered,
                     navigator, &ArchiveNavigator::goUp);

    auto actionEnter = new QAction(publ);
    actionEnter->setShortcutContext(Qt::WidgetShortcut);
    actionEnter->setShortcut(Qt::Key_Return);
    ui->treeView->addAction(actionEnter);

    QObject::connect(actionEnter, &QAction::triggered, [priv = this] ()
    {
        const auto index = priv->ui->treeView->selectionModel()->currentIndex();
        // The model might be empty; in that case the index would be invalid
        if (index.isValid())
            priv->openItem(index);
    });

    ui->treeView->setSortingEnabled(true);

    QObject::connect(navigator->getModel(), &QAbstractItemModel::modelReset,
                     publ, [priv = this] ()
    {
        priv->closeFilter();
        // Restoring header state will sort the model
        priv->prepareHeaderView();
    });

    ui->treeView->header()->setContextMenuPolicy(Qt::CustomContextMenu);

    const auto saveStateCallback = [priv = this] () { priv->onHeaderViewStateChanged(); };

    QObject::connect(ui->treeView->header(), &QAbstractItemView::customContextMenuRequested,
                     [view = ui->treeView,
                     model = navigator->getModel(),
                     widget = publ,
                     callback = saveStateCallback] (const auto pos)
    { createColumnConfigMenu(view, model, widget, pos, callback); });

    headerViewSaveTimer->setSingleShot(true);
    headerViewSaveTimer->setInterval(500);
    QObject::connect(headerViewSaveTimer, &QTimer::timeout,
                     publ, [publ = publ] ()
    { publ->priv->onHeaderViewStateChanged(); });

    QObject::connect(ui->treeView->header(), &QHeaderView::sectionResized,
                     headerViewSaveTimer, static_cast<void(QTimer::*)()>(&QTimer::start));

    QObject::connect(ui->treeView->header(), &QHeaderView::sectionMoved,
                     saveStateCallback);

    QObject::connect(ui->treeView->header(), &QHeaderView::sortIndicatorChanged,
                     saveStateCallback);

    prepareHeaderView();

    publ->setFocusProxy(ui->treeView);

    ui->pathEdit->installEventFilter(publ);
    ui->treeView->installEventFilter(publ);
    ui->filterEdit->installEventFilter(publ);
    ui->filterCloseButton->installEventFilter(publ);
}


NavigationWidget::Private::~Private()
{
    delete ui;
}


void NavigationWidget::Private::openItem(const QModelIndex& index)
{
    if (QGuiApplication::keyboardModifiers() & Qt::ControlModifier
            || QGuiApplication::keyboardModifiers() & Qt::ShiftModifier)
        return;

    navigator->openItem(index);
}

void NavigationWidget::Private::prepareHeaderView()
{
    if (!restoreHeaderViewState())
    {
        const int count = navigator->getModel()->columnCount(QModelIndex());
        for (int i = 0; i < count; ++i)
            ui->treeView->hideColumn(i);

        const std::vector<int> sections = navigator->getModelDefaultSections();
        int newVisualIndex = 0;
        for (const int section : sections)
        {
            ui->treeView->showColumn(section);
            int current = ui->treeView->header()->visualIndex(section);
            ui->treeView->header()->moveSection(current, newVisualIndex);
            ++newVisualIndex;
        }

        ui->treeView->header()->setSortIndicator(0, Qt::AscendingOrder);

        saveHeaderViewState();
    }
}


void NavigationWidget::Private::saveHeaderViewState()
{
    if (restoringHeaderViewState)
        return;

    QSettings s;
    const auto array = ui->treeView->header()->saveState();
    s.setValue(settingsKeyHeaderViewState, array);

    headerViewUpdateTime = static_cast<qulonglong>(QDateTime::currentSecsSinceEpoch());
    s.setValue(settingsKeyHeaderViewUpdateTime, headerViewUpdateTime);

}


bool NavigationWidget::Private::restoreHeaderViewState()
{
    Utils::LocalHolder<bool, false> lh(&restoringHeaderViewState, true);

    QSettings s;
    if (s.contains(settingsKeyHeaderViewState) && s.contains(settingsKeyHeaderViewUpdateTime))
    {
        auto updateTime = s.value(settingsKeyHeaderViewUpdateTime).toULongLong();
        if (updateTime != headerViewUpdateTime)
        {
            auto array = s.value(settingsKeyHeaderViewState).toByteArray();
            ui->treeView->header()->restoreState(array);

            headerViewUpdateTime = updateTime;

        }
        return true;
    }

    return false;
}


void NavigationWidget::Private::onHeaderViewStateChanged()
{
    saveHeaderViewState();
}


void NavigationWidget::Private::closeFilter()
{
    ui->filterEdit->clear();
    ui->filterWidget->hide();
}
