// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QWidget>


namespace Packuru::Core::Browser
{
class StatusPageCore;
}


class ArchiveBrowserStatusWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ArchiveBrowserStatusWidget(Packuru::Core::Browser::StatusPageCore* core, QWidget *parent = nullptr);
    ~ArchiveBrowserStatusWidget();

    void updateIcon(const QIcon& icon);

signals:
    void focusProxyChanged();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

