// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QMainWindow>

#include "../core-browser/archivebrowsercore.h"


namespace Ui {
class BrowserMainWindow;
}


namespace Packuru::Core::Browser
{
class MainWindowCore;
}


class BrowserMainWindow : public QMainWindow
{
    friend struct BrowserMainWindowPrivate;
    Q_OBJECT
public:
    explicit BrowserMainWindow(Packuru::Core::Browser::MainWindowCore* mainWindowCore, QWidget *parent = nullptr);
    ~BrowserMainWindow() override;

signals:
    void finished();

private:
    void closeEvent(QCloseEvent *event) override;
    void dragEnterEvent(QDragEnterEvent* event) override;
    void dropEvent(QDropEvent* event) override;

    struct Private;
    std::unique_ptr<Private> priv;
};
