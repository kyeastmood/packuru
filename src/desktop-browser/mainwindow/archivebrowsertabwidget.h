// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QTabWidget>


class ArchiveBrowserWidget;

namespace Packuru::Core::Browser
{
class ArchiveBrowserCore;
}


class ArchiveBrowserTabWidget : public QTabWidget
{
    Q_OBJECT
public:
    explicit ArchiveBrowserTabWidget(QWidget *parent = nullptr);

    bool eventFilter(QObject* watched, QEvent* event) override;

    void createBrowser(Packuru::Core::Browser::ArchiveBrowserCore *browserCore);
    ArchiveBrowserWidget* getCurrentBrowser() const;
    void previewRequested();
    void closeCurrentTab();

signals:
    void currentBrowserAvailableActionsChanged();
    void currentBrowserTitleChanged();

protected:
    void resizeEvent(QResizeEvent *event) override;

private:
    void activateBrowser(ArchiveBrowserWidget* browser);
    void setStatusIcon(ArchiveBrowserWidget* browser, const QIcon& icon);
    void onTabCloseRequested(int index);
    void onCurrentTabChanged();
    void updateTitle(ArchiveBrowserWidget* browser, const QString& title);
    void updateTabWidth();

    ArchiveBrowserWidget* currentBrowser = nullptr;
    QTimer* tabResizeTimer = nullptr;
};

