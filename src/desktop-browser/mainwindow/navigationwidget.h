// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QWidget>


namespace Packuru::Core::Browser
{
class ArchiveNavigator;
}

class NavigationWidget : public QWidget
{
    Q_OBJECT

public:
    explicit NavigationWidget(Packuru::Core::Browser::ArchiveNavigator* navigator, QWidget *parent = nullptr);
    ~NavigationWidget() override;

    void openFilterWidget();

private:
    bool eventFilter(QObject *watched, QEvent *event) override;

    struct Private;
    std::unique_ptr<Private> priv;
};

