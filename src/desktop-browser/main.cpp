// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QApplication>

#include "../core/appinfo.h"
#include "../core/globalsettingsmanager.h"

#include "../core-browser/init.h"

#include "mainwindow/browsermainwindow.h"
#include "desktopbrowsersettingsconverter.h"


using namespace Packuru::Core;
using namespace Packuru::Core::Browser;


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);
    app.setApplicationVersion(AppInfo::getAppVersion());
    app.setOrganizationName(AppInfo::getAppName());
    app.setApplicationName(AppInfo::getAppName() + " Desktop");

    Init coreInit(QUEUE_EXEC_NAME, QUEUE_EXEC_NAME);

    GlobalSettingsManager::instance()
            ->addCustomSettingsConverter(std::make_unique<DesktopBrowserSettingsConverter>());

    if (coreInit.messagePrimaryInstance())
        return 0;

    BrowserMainWindow window(coreInit.getMainWindowCore());
    window.show();

    coreInit.processArguments();

    return app.exec();
}
