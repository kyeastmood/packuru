<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>BrowserMainWindow</name>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="49"/>
        <source>&amp;Archive</source>
        <translation>&amp;Archief</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="66"/>
        <source>A&amp;pplication</source>
        <translation>&amp;Programma</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="74"/>
        <source>&amp;View</source>
        <translation>&amp;Beeld</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="84"/>
        <source>Toolbar</source>
        <translation>Werkbalk</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="212"/>
        <source>F7</source>
        <translation>F7</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="275"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="117"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="129"/>
        <source>Ins</source>
        <translation>Ins</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="141"/>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="158"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="170"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="200"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="227"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="239"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="251"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../mainwindow/browsermainwindow.ui" line="263"/>
        <source>F11</source>
        <translation>F11</translation>
    </message>
</context>
<context>
    <name>BrowserSettingsPage</name>
    <message>
        <location filename="../browsersettingspage.ui" line="114"/>
        <source>Double click to open items</source>
        <translation>Dubbelklikken om items te openen</translation>
    </message>
</context>
</TS>
