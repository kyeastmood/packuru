// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QDialog>


namespace Ui {
class ArchivePropertiesDialog;
}

class QAbstractTableModel;


class ArchivePropertiesDialog : public QDialog
{
    Q_OBJECT
public:
    explicit ArchivePropertiesDialog(QAbstractTableModel* model,
                                     const QString& archiveComment,
                                     QWidget *parent = nullptr);
    ~ArchivePropertiesDialog();

private:
    Ui::ArchivePropertiesDialog *ui;
};

