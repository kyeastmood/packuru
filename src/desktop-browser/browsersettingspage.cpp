// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../qcs-qtwidgets/widgetmapper.h"

#include "../core/globalsettingsmanager.h"

#include "../core-browser/settingsdialogcore.h"
#include "../core-browser/settingscontrollertype.h"

#include "../desktop-core/globalsettings.h"

#include "browsersettingspage.h"
#include "ui_browsersettingspage.h"


using Packuru::Core::Browser::SettingsDialogCore;
using Packuru::Core::GlobalSettingsManager;
using Packuru::Desktop::GlobalSettings;
using Type = Packuru::Core::Browser::SettingsControllerType::Type;


BrowserSettingsPage::BrowserSettingsPage(qcs::core::ControllerEngine* engine,
                                         const QString& pageTitle,
                                         QWidget *parent)
    : QWidget(parent),
      ui(new Ui::BrowserSettingsPage),
      engine(engine)
{
    ui->setupUi(this);

    ui->browserPageTitle->setText(pageTitle);

    QFont font;
    font.setPointSizeF(font.pointSizeF() * 1.2);
    font.setBold(true);
    ui->browserPageTitle->setFont(font);


    mapper = new qcs::qtw::WidgetMapper(this);
    mapper->addLabelAndWidget(Type::ForceSingleInstance,
                              ui->forceSingleInstanceLabel,
                              ui->forceSingleInstanceCheck);

    mapper->addLabelAndWidget(Type::ShowSummaryOnTaskCompletion,
                              ui->showSummaryOnTaskCompletionLabel,
                              ui->showSummaryCheck);
    mapper->setEngine(engine);

    const auto checked = GlobalSettingsManager::instance()
            ->getValueAs<bool>(GlobalSettings::DoubleClickToOpenItems);
    ui->doubleClickCheck->setChecked(checked);
}


BrowserSettingsPage::~BrowserSettingsPage()
{
    delete ui;
}


void BrowserSettingsPage::accept()
{
    GlobalSettingsManager::instance()->setValue(GlobalSettings::DoubleClickToOpenItems,
                                                ui->doubleClickCheck->isChecked());
}
