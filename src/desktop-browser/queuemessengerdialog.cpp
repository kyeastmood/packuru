// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QTimer>

#include "../core/commonstrings.h"

#include "../core-browser/queuemessenger.h"

#include "queuemessengerdialog.h"
#include "ui_queuemessengerdialog.h"


using namespace Packuru::Core::Browser;
using namespace Packuru::Core;


QueueMessengerDialog::QueueMessengerDialog(QueueMessenger* messenger, QWidget *parent)
    : QDialog(parent),
      ui(new Ui::QueueMessengerDialog),
      messenger(messenger)
{
    ui->setupUi(this);

    setWindowTitle(QueueMessenger::getString(QueueMessenger::UserString::DialogTitle));

    ui->runInBrowserButton->setText(QueueMessenger::getString(QueueMessenger::UserString::RunInBrowser));
    ui->abortButton->setText(messenger->getString(QueueMessenger::UserString::Abort));

    if (!messenger->getCanRunInBrowser())
        ui->runInBrowserButton->hide();

    connect(ui->abortButton, &QPushButton::clicked,
            messenger, [dialog = this, msg = messenger] ()
    {
        msg->abort();
        dialog->close();
    });

    connect(ui->runInBrowserButton, &QPushButton::clicked,
            messenger, [dialog = this, msg = messenger] ()
    {
        msg->runInBrowser();
        dialog->close();
    });

    connect(messenger, &QueueMessenger::deactivated,
            this, [dialog = this] ()
    {
        dialog->setEnabled(false);
        // Only delay close when messenger's state changed without user action; otherwise close instantly.
        QTimer::singleShot(1000, dialog, &QueueMessengerDialog::close);
    });

    connect(messenger, &QueueMessenger::statusChanged,
            ui->statusLabel, &QLabel::setText);

    ui->statusLabel->setText(messenger->getStatus());

    connect(this, &QueueMessengerDialog::rejected,
            messenger, &QueueMessenger::abort);
}


QueueMessengerDialog::~QueueMessengerDialog()
{
    delete ui;
}
