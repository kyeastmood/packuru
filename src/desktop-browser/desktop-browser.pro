# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)
include(../common-desktop.pri)
include(../common-exec.pri)

MODULE_BUILD_NAME_SUFFIX = $$BROWSER_BUILD_NAME_SUFFIX
MODULE_BUILD_NAME = $$BROWSER_BUILD_NAME

include(../embed-qm-files.pri)

TARGET = $$MODULE_BUILD_NAME

LIBS += -L$${PROJECT_TARGET_LIB_DIR}
LIBS += -l$$QCS_QTWIDGETS_BUILD_NAME
LIBS += -l$${CORE_BUILD_NAME} -l$${CORE_BROWSER_BUILD_NAME} -l$${DESKTOP_CORE_BUILD_NAME}

TRANSLATIONS += \
    translations/$${MODULE_BUILD_NAME_SUFFIX}_en.ts \
    translations/$${MODULE_BUILD_NAME_SUFFIX}_nl.ts \

HEADERS +=\
    desktopbrowsersettingsconverter.h \
    mainwindow/archivebrowsertabwidget.h \
    mainwindow/archivebrowserwidget.h \
    mainwindow/archivebrowserstatuswidget.h \
    mainwindow/archivebrowserbasiccontrolwidget.h \
    mainwindow/browserpasswordprompt.h \
    propertiesitemdelegate.h \
    queuemessengerdialog.h \
    mainwindow/navigationwidget.h \
    deletiondialog.h \
    archivepropertiesdialog.h \
    browsersettingsdialog.h \
    browsersettingspage.h \
    mainwindow/browsermainwindow.h
    
SOURCES += \
    desktopbrowsersettingsconverter.cpp \
    mainwindow/archivebrowsertabwidget.cpp \
    mainwindow/archivebrowserwidget.cpp \
    mainwindow/archivebrowserstatuswidget.cpp \
    mainwindow/archivebrowserbasiccontrolwidget.cpp \
    mainwindow/browserpasswordprompt.cpp \
    main.cpp \
    propertiesitemdelegate.cpp \
    queuemessengerdialog.cpp \
    mainwindow/navigationwidget.cpp \
    deletiondialog.cpp \
    archivepropertiesdialog.cpp \
    browsersettingsdialog.cpp \
    browsersettingspage.cpp \
    mainwindow/browsermainwindow.cpp

FORMS += \
    mainwindow/archivebrowserwidget.ui \
    mainwindow/archivebrowserstatuswidget.ui \
    mainwindow/archivebrowserbasiccontrolwidget.ui \
    mainwindow/browserpasswordprompt.ui \
    queuemessengerdialog.ui \
    mainwindow/navigationwidget.ui \
    deletiondialog.ui \
    archivepropertiesdialog.ui \
    browsersettingspage.ui \
    mainwindow/browsermainwindow.ui
