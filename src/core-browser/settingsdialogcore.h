// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include "../core/appsettingsdialogcore.h"

/// @file

namespace qcs::core
{
class ControllerEngine;
}


namespace Packuru::Core::Browser
{

/**
 @brief Allows to adjust %Browser settings.

 It is used in the settings dialog.

 Object of this class is @link Packuru::Core::Browser::MainWindowCore::createBrowserSettingsDialogCore()
 provided@endlink by Packuru::Core::Browser::MainWindowCore object.

 For core application settings you also need Packuru::Core::AppSettingsDialogCore.

 @headerfile "core-browser/settingsdialogcore.h"
 */
class SettingsDialogCore : public QObject
{
    Q_OBJECT
public:
    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        PageTitle, ///< Browser page title.
    };
    Q_ENUM(UserString)

    explicit SettingsDialogCore(QObject *parent = nullptr);
    ~SettingsDialogCore() override;

    /// @brief Allows destruction from QML.
    Q_INVOKABLE void destroyLater();

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE static QString getString(UserString value);

    /**
     @brief Returns pointer to ControllerEngine controlling input widgets of %Browser settings.

     The widgets must be mapped in WidgetMapper using SettingsControllerType::Type enum
     as the ID.

     Object ownership is not transferred to the caller.
     */
    Q_INVOKABLE qcs::core::ControllerEngine* getControllerEngine();

    /// @copydoc Packuru::Core::AppSettingsDialogCore::accept()
    Q_INVOKABLE void accept();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
