// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../utils/private/enumhash.h"

#include "viewfiltercontrollertype.h"


namespace Packuru::Core::Browser
{

uint qHash(ViewFilterControllerType::Type value, uint seed)
{
    return Utils::enumHash(value, seed);
}

}
