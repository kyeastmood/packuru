// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivenavigatorbackdoordata.h"


namespace Packuru::Core::Browser
{

bool ArchiveNavigatorBackdoorData::isValid() const
{
    return getSelectedItems
            && hasSelectedFiles;
}

}
