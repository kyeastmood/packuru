// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "pluginconfigchangenotifier.h"


namespace Packuru::Core::Browser
{

PluginConfigChangeNotifier::PluginConfigChangeNotifier(QObject *parent)
    : QObject(parent)
{

}

}
