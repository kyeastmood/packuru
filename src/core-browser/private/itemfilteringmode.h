// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>
#include <QString>


namespace Packuru::Core::Browser
{

enum class ItemFilteringMode
{
    Filter,
    ShowAll,
    HideAll,
};

QString toString(ItemFilteringMode value);

}

Q_DECLARE_METATYPE(Packuru::Core::Browser::ItemFilteringMode)
