// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>


namespace Packuru::Core::Browser
{

struct ArchiveDialogAcceptedResult;

template <typename TaskData>
using ArchiveDialogAcceptedCallback = std::function<ArchiveDialogAcceptedResult(const TaskData&)>;

}
