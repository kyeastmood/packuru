// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>


namespace Packuru::Core::Browser
{

class PluginConfigChangeNotifier : public QObject
{
    Q_OBJECT
public:
    explicit PluginConfigChangeNotifier(QObject *parent = nullptr);

signals:
    void pluginConfigChanged();
};

}
