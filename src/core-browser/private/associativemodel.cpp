// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "associativemodel.h"


namespace Packuru::Core::Browser
{

AssociativeModel::AssociativeModel(QObject *parent)
    : QAbstractTableModel(parent)
{

}


AssociativeModel::~AssociativeModel()
{
}


int AssociativeModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 1;
}


int AssociativeModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return static_cast<int>(items.size());
}


QVariant AssociativeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || role != Qt::DisplayRole)
        return QVariant();

    const auto row = static_cast<std::size_t>(index.row());
    return items[row].value;
}


QVariant AssociativeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Vertical || role != Qt::DisplayRole)
        return QVariant();

    return items[static_cast<std::size_t>(section)].key;
}


Qt::ItemFlags AssociativeModel::flags(const QModelIndex& index) const
{
    if (index.isValid())
        return Qt::ItemIsEnabled;
    else
        return Qt::NoItemFlags;
}


void AssociativeModel::setModelData(ItemList& items)
{
    beginResetModel();
    this->items = std::move(items);
    endResetModel();
}


void AssociativeModel:: clear()
{
    beginResetModel();
    items.clear();
    endResetModel();
}

}
