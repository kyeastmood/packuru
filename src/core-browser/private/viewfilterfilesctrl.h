// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QCoreApplication>

#include "qcs-core/controller.h"

#include "itemfilteringmode.h"


namespace Packuru::Core::Browser
{

class ViewFilterFilesCtrl : public qcs::core::Controller<QString, ItemFilteringMode>
{
    Q_DECLARE_TR_FUNCTIONS(ViewFilterFilesCtrl)
public:
    using SetFilteringFunction = std::function<void(ItemFilteringMode)>;

    ViewFilterFilesCtrl(SetFilteringFunction&& setFiltering);

private:
    void onValueSyncedToWidget(const QVariant& value) override;

    SetFilteringFunction setFiltering_;
};

}
