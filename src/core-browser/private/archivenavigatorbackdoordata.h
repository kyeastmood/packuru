// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>
#include <vector>


class QModelIndex;


namespace Packuru::Core::Browser
{

struct ArchiveNavigatorBackdoorData
{
    using GetSelectedItemsCallback = std::function<std::vector<QModelIndex>()>;
    using HasSelectedFilesCallback = std::function<bool()>;

    bool isValid() const;

    // Callbacks must be valid
    GetSelectedItemsCallback getSelectedItems;
    HasSelectedFilesCallback hasSelectedFiles;
};

}
