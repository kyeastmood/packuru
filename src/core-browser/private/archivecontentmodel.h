// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QAbstractItemModel>

#include "core/private/plugin-api/archiveitem.h"


class QAbstractTableModel;


namespace Packuru::Core
{
class ArchiveContentData;
}


namespace Packuru::Core::Browser
{

class ArchiveContentModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum class PathFormat
    {
        // Dir paths always end with '/'
        Backend, // Remove leading / ("/dir/" becomes "dir/", "//dir/" becomes "/dir/")
        Browser  // For path widget
    };
    Q_ENUM(PathFormat)

    explicit ArchiveContentModel(QObject *parent = nullptr);
    ~ArchiveContentModel() override;

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    bool hasChildren(const QModelIndex & parent = QModelIndex()) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    void clear();
    void setArchiveData(const Packuru::Core::ArchiveContentData& data);
    const Packuru::Core::ArchiveContentData& getArchiveData() const;

    // index can be invalid
    Q_INVOKABLE bool isDir(const QModelIndex& index) const;

    // index can be invalid
    Q_INVOKABLE QString getAbsoluteFilePath(const QModelIndex& index, PathFormat format) const;

    // index must be valid
    Q_INVOKABLE QString getName(const QModelIndex& index) const;

    // index must be valid
    QDateTime lastModified(const QModelIndex& index) const;

    // Returns invalid index for empty path, the root dir (/) and non-existent path
    Q_INVOKABLE QModelIndex getDirectoryIndex(const QString& absoluteFilePath) const;
    Q_INVOKABLE bool containsData() const;
    // Not implemented; returns nullptr for now
    Q_INVOKABLE QAbstractTableModel* getItemPropertiesModel(const QModelIndex& index);
    std::vector<int> getDefaultSections() const;

     bool lessThan(const QModelIndex& source_left, const QModelIndex& source_right, Qt::SortOrder order) const;

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
