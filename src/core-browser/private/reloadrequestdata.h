// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QFileInfo>
#include <QString>


namespace Packuru::Core::Browser
{

struct ReloadRequestData
{
    QFileInfo archiveInfo;
    QString password;
};

}
