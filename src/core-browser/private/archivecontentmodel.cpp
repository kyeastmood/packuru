// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <queue>

#include <QCollator>
#include <QDateTime>
#include <QUrl>
#include <QMimeType>
#include <QMimeDatabase>
#include <QIcon>
#include <QDir>
#include <QFont>

#include "core/globalsettingsmanager.h"
#include "core/private/globalsettings.h"
#include "core/private/tasks/archivedirnode.h"
#include "core/private/digitalsize.h"
#include "core/private/tasks/pathlookuptable.h"
#include "core/private/tasks/archivecontentdata.h"

#include "archivecontentmodel.h"
#include "associativemodel.h"


using namespace Packuru::Core;


namespace Packuru::Core::Browser
{

namespace
{

enum class Column
{
    Name = 0,

    Accessed,
    Attributes,
    Block,
    Comment,
    Checksum,
    Created,
    Encrypted,
    Extension,
    Group,
    HostOS,
    Method,
    Modified,
    NodeType,
    OriginalSize,
    PackedSize,
    Ratio,
    User,

    ___COUNT,
    ___INVALID
};

enum UserRole
{
    Name = Qt::UserRole,

    Accessed,
    Attributes,
    Block,
    Comment,
    Checksum,
    Created,
    Encrypted,
    Extension,
    Group,
    HostOS,
    Method,
    Modified,
    NodeType,
    OriginalSize,
    PackedSize,
    Ratio,
    User,

    ___COUNT,
};

QString getAlias(Column column);
Column translate(UserRole role);

}


struct ArchiveContentModel::Private
{
    Private(ArchiveContentModel* publ);

    ArchiveFileNode* indexToFileNode(const QModelIndex& index) const;

    ArchiveContentModel* publ;
    SizeUnits sizeUnits;
    /* The root node is nameless and it corresponds to an invalid QModelIndex which marks the root of
       the model in Qt's Model-View architecture. Therefore when operating on an invalid QModelIndex
       we should operate on the root node and vice versa. */
    ArchiveContentData archiveData;
    QMimeDatabase mimeDB;
    QMimeType folderType;
    QCollator col;
};


ArchiveContentModel::ArchiveContentModel(QObject *parent)
    : QAbstractItemModel(parent),
      priv(new Private(this))
{
    /* Create QMimeType for a folder without QFileIconProvider because it depends on QtWidgets;
       it looks like there's no other way to put "inode/directory" into QMimeType without a real folder;
       Might not work correctly on Windows where disk letter like "C:\" could be returned
       which can have different type. */
    priv->folderType = priv->mimeDB.mimeTypeForUrl(QUrl(QDir::rootPath()));
}


ArchiveContentModel::~ArchiveContentModel()
{

}


QModelIndex ArchiveContentModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_ASSERT(checkIndex(parent, QAbstractItemModel::CheckIndexOption::IndexIsValid) || !parent.isValid());

    ArchiveDirNode* parentNode;

    if (!parent.isValid())
        parentNode = priv->archiveData.getRootNode();
    else
        parentNode = static_cast<ArchiveDirNode*>(parent.internalPointer());

    Q_ASSERT(parentNode);

    ArchiveFileNode* childNode = parentNode->getChild(row);
    Q_ASSERT(childNode);

    return createIndex(row, column, childNode);
}


QModelIndex ArchiveContentModel::parent(const QModelIndex &index) const
{
    Q_ASSERT(checkIndex(index, QAbstractItemModel::CheckIndexOption::IndexIsValid
                        | QAbstractItemModel::CheckIndexOption::DoNotUseParent));

    ArchiveFileNode* node = static_cast<ArchiveFileNode*>(index.internalPointer());
    Q_ASSERT(node);

    ArchiveDirNode* parentNode = static_cast<ArchiveDirNode*>(node->getParent());

    if (parentNode == priv->archiveData.getRootNode())
        return QModelIndex();

    return createIndex(parentNode->getRow(), 0, parentNode);
}


bool ArchiveContentModel::hasChildren(const QModelIndex& parent) const
{
    ArchiveFileNode* parentNode = nullptr;

    if (!parent.isValid())
        parentNode = priv->archiveData.getRootNode();
    else
        parentNode = static_cast<ArchiveFileNode*>(parent.internalPointer());

    Q_ASSERT(parentNode);

    if (parentNode->isDir())
    {
        ArchiveDirNode* dirNode = static_cast<ArchiveDirNode*>(parentNode);
        return dirNode->getChildrenCount() > 0;
    }

    return false;
}


int ArchiveContentModel::rowCount(const QModelIndex &parent) const
{
    ArchiveFileNode* parentNode = nullptr;

    if (!parent.isValid())
        parentNode = priv->archiveData.getRootNode();
    else
        parentNode = static_cast<ArchiveFileNode*>(parent.internalPointer());

    Q_ASSERT(parentNode);

    if (!parentNode->isDir())
        return 0;

    ArchiveDirNode* dirNode = static_cast<ArchiveDirNode*>(parentNode);

    return dirNode->getChildrenCount();
}


int ArchiveContentModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return static_cast<int>(Column::___COUNT);
}


QVariant ArchiveContentModel::data(const QModelIndex &index, int role) const
{
    Q_ASSERT(checkIndex(index, QAbstractItemModel::CheckIndexOption::IndexIsValid));

    const ArchiveFileNode* const node = priv->indexToFileNode(index);
    auto column = static_cast<Column>(index.column());
    if (role >= Qt::UserRole)
        column = translate(static_cast<UserRole>(role));

    const auto& item = node->getItem();
    const bool itemIsDir = item.nodeType == NodeType::Folder;

    if (role == Qt::DisplayRole || role >= Qt::UserRole)
    {
        switch (column)
        {
        case Column::Accessed:
            return item.accessed.toString("yyyy-MM-dd hh:mm");
        case Column::Attributes:
            return item.attributes;
        case Column::Block:
            return item.block;
        case Column::Comment:
            return item.comment;
        case Column::Checksum:
            return item.checksum;
        case Column::Created:
            return item.created.toString("yyyy-MM-dd hh:mm");
        case Column::Encrypted:
            return static_cast<bool>(item.encrypted);
        case Column::Extension:
            return item.extension;
        case Column::Group:
            return item.group;
        case Column::HostOS:
            return item.hostOS;
        case Column::Method:
            return item.method;
        case Column::Modified:
            return item.modified.toString("yyyy-MM-dd hh:mm");
        case Column::Name:
            if (item.nodeType == NodeType::Folder
                    && item.name.isEmpty()
                    && node->getParent() == priv->archiveData.getRootNode())
                return "@UNIX_ROOT_DIR";
            else
                return item.name;
        case Column::NodeType:
            return toString(item.nodeType);
        case Column::OriginalSize:
            if (itemIsDir)
            {
                const ArchiveDirNode* dirNode = static_cast<const ArchiveDirNode*>(node);
                const int entries = dirNode->getChildrenCount();
                return Packuru::Core::Browser::ArchiveContentModel::tr("%n items", "", entries);
            }
            else
            {
                return DigitalSize::toString(item.originalSize, priv->sizeUnits);
            }
        case Column::PackedSize:
            if (!itemIsDir)
                return DigitalSize::toString(item.packedSize, priv->sizeUnits);
            else
                return QVariant();
        case Column::Ratio:
        {
            if (!itemIsDir)
                return QString::number(static_cast<double>(item.ratio), 'f', 2);
            else
                return QVariant();
        }
        case Column::User:
            return item.user;
        default:
            Q_ASSERT(false); break;
        }
    }
    else if (role == Qt::DecorationRole)
    {
        switch (column)
        {

        case Column::Name:
        {
            if (itemIsDir)
                return QIcon::fromTheme(priv->folderType.iconName());
            else
            {
                const auto type = priv->mimeDB.mimeTypeForFile(item.name, QMimeDatabase::MatchExtension);
                auto icon = QIcon::fromTheme(type.iconName());

                if (icon.isNull())
                    icon = QIcon::fromTheme("application-octet-stream");

                return icon;
            }
        }

        default:
            return QVariant();
        }
    }
    else if (role == Qt::TextAlignmentRole)
    {
        switch (column)
        {
        case Column::PackedSize:
        case Column::Ratio:
        case Column::OriginalSize:
        {
            const int flags = Qt::AlignRight | Qt::AlignVCenter;
            return flags;
        }
        default:
            return QVariant();
        }
    }
    else if (role == Qt::FontRole && item.encrypted)
    {
        QFont f;
        f.setStyle(QFont::StyleItalic);
        return f;
    }

    return QVariant();
}


Qt::ItemFlags ArchiveContentModel::flags(const QModelIndex &index) const
{
    Q_ASSERT(checkIndex(index, QAbstractItemModel::CheckIndexOption::IndexIsValid)
             || !index.isValid());

    return QAbstractItemModel::flags(index);
}


QVariant ArchiveContentModel::headerData(int section, Qt::Orientation orientation, int role ) const
{
    Q_ASSERT(section >= 0 && section < static_cast<int>(Column::___COUNT));

    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        const auto column = static_cast<Column>(section);
        return getAlias(column);
    }

    return QVariant();
}


QHash<int, QByteArray> ArchiveContentModel::roleNames() const
{
    auto map = QAbstractItemModel::roleNames();

    map[UserRole::Name] = "name";
    map[UserRole::Accessed] = "accessed";
    map[UserRole::Attributes] = "attributes";
    map[UserRole::Comment] = "comment";
    map[UserRole::Created] = "created";
    map[UserRole::Encrypted] = "encrypted";
    map[UserRole::Extension] = "extension";
    map[UserRole::HostOS] = "hostOS";
    map[UserRole::Method] = "method";
    map[UserRole::Modified] = "modified";
    map[UserRole::NodeType] = "nodeType";
    map[UserRole::OriginalSize] = "originalSize";
    map[UserRole::PackedSize] = "packedSize";
    map[UserRole::Ratio] = "ratio";
    map[UserRole::User] = "user";

    return map;
}


void ArchiveContentModel::clear()
{
    beginResetModel();

    priv->archiveData = ArchiveContentData();

    endResetModel();
}


void ArchiveContentModel::setArchiveData(const ArchiveContentData& data)
{
    beginResetModel();

    auto list = priv->archiveData.getLookupTable()->keys();

    priv->archiveData = data;

    list = priv->archiveData.getLookupTable()->keys();

    priv->sizeUnits = GlobalSettingsManager::instance()
            ->getValueAs<SizeUnits>(GlobalSettings::Enum::AC_SizeUnits);

    endResetModel();
}


const ArchiveContentData&ArchiveContentModel::getArchiveData() const
{
    return priv->archiveData;
}


bool ArchiveContentModel::isDir(const QModelIndex& index) const
{
    Q_ASSERT(checkIndex(index, QAbstractItemModel::CheckIndexOption::IndexIsValid) || !index.isValid());

    const ArchiveFileNode* const node = priv->indexToFileNode(index);
    return node->isDir();
}


QString ArchiveContentModel::getAbsoluteFilePath(const QModelIndex& index, PathFormat format) const
{
    // Index can be invalid when a view shows root directory
    Q_ASSERT(checkIndex(index, QAbstractItemModel::CheckIndexOption::IndexIsValid) || !index.isValid());

    const ArchiveFileNode* const node = priv->indexToFileNode(index);

    std::deque<QString> pathParts;

    auto tempNode = node;

    do
    {
        pathParts.push_front(tempNode->getItem().name);
    }
    while (static_cast<bool>(tempNode = tempNode->getParent()));

    QString path;

    std::size_t start = 0;
    if (format == PathFormat::Backend) // Skip root dir
        ++start;

    for (std::size_t i = start; i < pathParts.size() - 1; ++i) // Skip last part
        path += pathParts[i] + QLatin1String("/");

    path += pathParts.back();

    if (node->isDir())
        path += QLatin1String("/");

    return path;
}


QString ArchiveContentModel::getName(const QModelIndex& index) const
{
    Q_ASSERT(checkIndex(index, QAbstractItemModel::CheckIndexOption::IndexIsValid));

    const ArchiveFileNode* const node = priv->indexToFileNode(index);
    return node->getItem().name;
}


QDateTime ArchiveContentModel::lastModified(const QModelIndex& index) const
{
    Q_ASSERT(checkIndex(index, QAbstractItemModel::CheckIndexOption::IndexIsValid));

    const ArchiveFileNode* const node = priv->indexToFileNode(index);
    return node->getItem().modified;
}


QModelIndex ArchiveContentModel::getDirectoryIndex(const QString& absoluteFilePath) const
{
    if (absoluteFilePath.isEmpty() || absoluteFilePath == QLatin1String("/"))
        return QModelIndex();

    auto path = absoluteFilePath;
    if (!path.endsWith(QLatin1Char('/')))
        path.append(QLatin1Char('/'));

    ArchiveDirNode* const dirNode = priv->archiveData.getLookupTable()->getDir(path);

    // This should not happen as we handle empty path and / at the beginning
    Q_ASSERT(dirNode != priv->archiveData.getRootNode());

    if (!dirNode)
        return QModelIndex();

    const int row = dirNode->getRow();

    return createIndex(row, 0, dirNode);
}


bool ArchiveContentModel::containsData() const
{
    return priv->archiveData.containsData();
}


QAbstractTableModel* ArchiveContentModel::getItemPropertiesModel(const QModelIndex &index)
{
    Q_ASSERT(checkIndex(index, QAbstractItemModel::CheckIndexOption::IndexIsValid));

    return nullptr;
}


std::vector<int> ArchiveContentModel::getDefaultSections() const
{
    return {static_cast<int>(Column::Name),
                static_cast<int>(Column::OriginalSize),
                static_cast<int>(Column::Modified)};
}


bool ArchiveContentModel::lessThan(const QModelIndex& source_left,
                                   const QModelIndex& source_right,
                                   Qt::SortOrder order) const
{
    const auto* const node1 = static_cast<ArchiveFileNode*>(source_left.internalPointer());
    const auto* const node2 = static_cast<ArchiveFileNode*>(source_right.internalPointer());

    if (node1->isDir())
    {
        if (!node2->isDir())
            return order == Qt::AscendingOrder;
    }
    else if (node2->isDir())
        return order == Qt::DescendingOrder;

    Q_ASSERT(node1->isDir() == node2->isDir());

    const QCollator& col = priv->col;

    Q_ASSERT(source_left.column() == source_right.column());
    const auto columnId = static_cast<Column>(source_left.column());

    switch (columnId)
    {
    case Column::Accessed:
        return node1->getItem().accessed < node2->getItem().accessed;
    case Column::Attributes:
        return col.compare(node1->getItem().attributes, node2->getItem().attributes) < 0;
    case Column::Block:
        return col.compare(node1->getItem().block, node2->getItem().block) < 0;
    case Column::Comment:
        return col.compare(node1->getItem().comment, node2->getItem().comment) < 0;
    case Column::Checksum:
        return col.compare(node1->getItem().checksum, node2->getItem().checksum) < 0;
    case Column::Created:
        return node1->getItem().created < node2->getItem().created;
    case Column::Encrypted:
        return node1->getItem().encrypted < node2->getItem().encrypted;
    case Column::Extension:
        return node1->getItem().extension < node2->getItem().extension;
    case Column::Group:
        return col.compare(node1->getItem().group, node2->getItem().group) < 0;
    case Column::HostOS:
        return col.compare(node1->getItem().hostOS, node2->getItem().hostOS) < 0;
    case Column::Method:
        return col.compare(node1->getItem().method, node2->getItem().method) < 0;
    case Column::Modified:
        return node1->getItem().modified < node2->getItem().modified;
    case Column::Name:
        return col.compare(node1->getItem().name, node2->getItem().name) < 0;
    case Column::NodeType:
        return node1->getItem().nodeType < node2->getItem().nodeType;
    case Column::OriginalSize:
        if (node1->isDir())
        {
            const auto* dir1 = static_cast<const ArchiveDirNode*>(node1);
            const auto* dir2 = static_cast<const ArchiveDirNode*>(node2);
            return dir1->getChildrenCount() < dir2->getChildrenCount();
        }
        else
            return node1->getItem().originalSize < node2->getItem().originalSize;
    case Column::PackedSize:
         return node1->getItem().packedSize < node2->getItem().packedSize;
    case Column::Ratio:
         return node1->getItem().ratio < node2->getItem().ratio;
    case Column::User:
        return col.compare(node1->getItem().user, node2->getItem().user) < 0;
    default:
        Q_ASSERT(false); return false;
    }
}


namespace
{

QString getAlias(Column column)
{
    switch (column)
    {
    case Column::Accessed: return Packuru::Core::Browser::ArchiveContentModel::tr("Accessed");
    case Column::Attributes: return Packuru::Core::Browser::ArchiveContentModel::tr("Attributes");
    case Column::Block: return Packuru::Core::Browser::ArchiveContentModel::tr("Block");
    case Column::Comment: return Packuru::Core::Browser::ArchiveContentModel::tr("Comment");
    case Column::Checksum: return Packuru::Core::Browser::ArchiveContentModel::tr("CRC");
    case Column::Created: return Packuru::Core::Browser::ArchiveContentModel::tr("Created");
    case Column::Encrypted: return Packuru::Core::Browser::ArchiveContentModel::tr("Encrypted");
    case Column::Extension: return Packuru::Core::Browser::ArchiveContentModel::tr("Extension");
    case Column::Group: return Packuru::Core::Browser::ArchiveContentModel::tr("Group");
    case Column::HostOS: return Packuru::Core::Browser::ArchiveContentModel::tr("Host OS");
    case Column::Method: return Packuru::Core::Browser::ArchiveContentModel::tr("Method");
    case Column::Modified: return Packuru::Core::Browser::ArchiveContentModel::tr("Modified");
    case Column::Name: return Packuru::Core::Browser::ArchiveContentModel::tr("Name");
    case Column::NodeType: return Packuru::Core::Browser::ArchiveContentModel::tr("Node type");
    case Column::OriginalSize: return Packuru::Core::Browser::ArchiveContentModel::tr("Size");
    case Column::PackedSize: return Packuru::Core::Browser::ArchiveContentModel::tr("Packed");
    case Column::Ratio: return Packuru::Core::Browser::ArchiveContentModel::tr("Ratio");
    case Column::User: return Packuru::Core::Browser::ArchiveContentModel::tr("User");
    default: Q_ASSERT(false); return "";
    }
}


Column translate(UserRole role)
{
    switch (role)
    {
    case UserRole::Name: return Column::Name;
    case UserRole::Accessed: return Column::Accessed;
    case UserRole::Attributes: return Column::Attributes;
    case UserRole::Block: return Column::Block;
    case UserRole::Comment: return Column::Comment;
    case UserRole::Checksum: return Column::Checksum;
    case UserRole::Created: return Column::Created;
    case UserRole::Encrypted: return Column::Encrypted;
    case UserRole::Extension: return Column::Extension;
    case UserRole::Group: return Column::Group;
    case UserRole::HostOS: return Column::HostOS;
    case UserRole::Method: return Column::Method;
    case UserRole::Modified: return Column::Modified;
    case UserRole::NodeType: return Column::NodeType;
    case UserRole::OriginalSize: return Column::OriginalSize;
    case UserRole::PackedSize: return Column::PackedSize;
    case UserRole::Ratio: return Column::Ratio;
    case UserRole::User: return Column::User;
    default: Q_ASSERT(false); return Column::___INVALID;
    }
}

}


ArchiveContentModel::Private::Private(ArchiveContentModel* publ)
    : publ(publ)
{
    col.setNumericMode(true);
}


ArchiveFileNode* ArchiveContentModel::Private::indexToFileNode(const QModelIndex &index) const
{
    // Index can be invalid when a view shows root directory
    if (!index.isValid())
        return archiveData.getRootNode();
    else
    {
        auto node = static_cast<ArchiveFileNode*>(index.internalPointer());
        Q_ASSERT(node);
        return node;
    }
}

}
