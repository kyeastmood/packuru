// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>
#include <memory>
#include <unordered_set>

#include <QFileInfo>
#include <QString>

#include "core/private/plugin-api/backendarchiveproperties.h"

#include "archivedialogacceptedcallback.h"
#include "archivedialogacceptedresult.h"


namespace Packuru::Core
{

enum class ArchiveType;
class ArchivingParameterHandlerFactory;
struct ArchivingTaskData;
struct DeletionTaskData;
struct ExtractionTaskData;
class ExtractionTask;
class ReadTask;
enum class TaskType;
struct TestTaskData;

}

namespace Packuru::Core::Browser
{

class ArchiveBrowserCoreNotifier;
class PluginConfigChangeNotifier;
struct PreviewRequestData;
struct ReloadRequestData;

struct ArchiveBrowserCoreInitData
{ 
    using GetSupportedWriteTasksFunction =
    std::function<std::unordered_set<TaskType> (ArchiveType, const BackendArchiveProperties&)>;

    using ArchiveBrowserDestroyed = std::function<void(const QFileInfo&)>;

    using CreateReadTaskCallback = std::function<ReadTask*(const ReloadRequestData&)>;

    using CreatePreviewTaskCallback = std::function<ExtractionTask*(const PreviewRequestData&)>;

    using CreateArchivingParameterHandlerFactoryCallback
    = std::function<std::unique_ptr<ArchivingParameterHandlerFactory>(ArchiveType)>;

    QFileInfo archiveInfo;  // Must be valid
    QString tempDir; // Can be empty but preview will not be available

    // Can be invalid but ArchiveBrowserCore will not receive its signals
    ArchiveBrowserCoreNotifier* notifier = nullptr;

    // Can be invalid but ArchiveBrowserCore will not receive a signal
    // when plugin config has changed and available browser actions will not be refreshed.
    PluginConfigChangeNotifier* pluginChangeNotifier = nullptr;

    CreateReadTaskCallback createReadTask; // Must be valid, can return nullptr

    // Following  callbacks can be invalid if not needed by the caller and can return
    // invalid data or nullptr if target object has been destroyed.
    GetSupportedWriteTasksFunction getSupportedWriteTasks;
    ArchiveBrowserDestroyed destroyed;
    CreatePreviewTaskCallback createPreviewTask;
    CreateArchivingParameterHandlerFactoryCallback createArchivingParameterHandlerFactory;
    ArchiveDialogAcceptedCallback<ArchivingTaskData> onArchivingDialogAccepted;
    ArchiveDialogAcceptedCallback<DeletionTaskData> onDeletionDialogAccepted;
    ArchiveDialogAcceptedCallback<ExtractionTaskData> onExtractionDialogAccepted;
    ArchiveDialogAcceptedCallback<TestTaskData> onTestDialogAccepted;
};

}

