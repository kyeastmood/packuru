// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "core/private/privatestrings.h"

#include "viewfiltertextctrl.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core::Browser
{

ViewFilterTextCtrl::ViewFilterTextCtrl(SetFilterTextFunction&& setFilterText)
    : setFilterText_(setFilterText)
{
    changeTextTimer.setSingleShot(true);
    changeTextTimer.setInterval(500);

    QObject::connect(&changeTextTimer, &QTimer::timeout,
                     [publ = this] ()
    {
        const auto text = publ->getPropertyAs<QString>(ControllerProperty::PublicCurrentValue);
        publ->setFilterText_(text);
    });

    properties[ControllerProperty::PlaceholderText]
            = PrivateStrings::getString(PrivateStrings::UserString::FilterPlaceholderText);

    setCurrentValue("");
}


void ViewFilterTextCtrl::onValueSyncedToWidget(const QVariant& value)
{
    Q_UNUSED(value)
    changeTextTimer.start();
}

}



