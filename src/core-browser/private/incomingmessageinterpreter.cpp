// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>
#include <QDebug>

#include "core/private/messagetype.h"

#include "incomingmessageinterpreter.h"
#include "commandlinereaddata.h"


using namespace Packuru::Core;


namespace Packuru::Core::Browser
{

IncomingMessageInterpreter::IncomingMessageInterpreter(QObject* parent)
    : QObject(parent)
{

}


void IncomingMessageInterpreter::interpret(const QByteArray& message) const
{
    QDataStream stream(message);

    MessageType type;
    stream >> type;

    switch (type)
    {
    case MessageType::CommandLineDefaultStartUp:
    {
        emit defaultStartUp();
        break;
    }
    case MessageType::CommandLineError:
    {
        QString info;
        stream >> info;
        emit commandLineError(info);
        break;
    }
    case MessageType::CommandLineHelp:
    {
        QString info;
        stream >> info;
        emit commandLineHelpRequested(info);
        break;
    }
    case MessageType::CommandLineReadArchives:
    {
        CommandLineReadData data;
        stream >> data;
        emit readArchives(data);
        break;
    }
    default:
        Q_ASSERT(false);
        qWarning() << Packuru::Core::Browser::IncomingMessageInterpreter::tr("Unknown message type");
        return;
    }

    emit newMessage();
}

}
