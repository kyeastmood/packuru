// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "viewfilterfilesctrl.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core::Browser
{

ViewFilterFilesCtrl::ViewFilterFilesCtrl(SetFilteringFunction&& setFiltering)
    : setFiltering_(setFiltering)
{
    setValueListsFromPrivate({ItemFilteringMode::Filter,
                              ItemFilteringMode::ShowAll,
                              ItemFilteringMode::HideAll});

    setCurrentValueFromPrivate(ItemFilteringMode::Filter);


    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::Browser::ViewFilterFilesCtrl::tr("Files");
}


void ViewFilterFilesCtrl::onValueSyncedToWidget(const QVariant& value)
{
    Q_UNUSED(value)
    const auto mode = getPropertyAs<ItemFilteringMode>(ControllerProperty::PrivateCurrentValue);
    setFiltering_(mode);
}

}


