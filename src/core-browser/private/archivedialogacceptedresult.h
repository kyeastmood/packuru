// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>


namespace Packuru::Core
{
class Task;
}


namespace Packuru::Core::Browser
{

struct ArchiveDialogAcceptedResult
{
    bool isValid() const { return willRunInQueue != static_cast<bool>(task); } // Only one must be true

    // Whether this object has been processed/received somewhere
    bool messageProcessed = false;
    bool willRunInQueue = false;
    Task* task = nullptr; // Invalid if run in queue or not set
    QString updatedPassword;
};

}
