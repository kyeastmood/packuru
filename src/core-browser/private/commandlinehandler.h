// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include "core/private/commandlinehandlerbase.h"

#include "symbol_export.h"


namespace Packuru::Core::Browser
{

class PACKURU_CORE_BROWSER_EXPORT CommandLineHandler : public Packuru::Core::CommandLineHandlerBase
{
public:
    CommandLineHandler();
    ~CommandLineHandler() override;

    QByteArray createMessageFromArguments() const;

private:
    struct Private;
    std::unique_ptr<struct Private> priv;
};

}
