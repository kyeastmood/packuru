// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "viewfilterscopectrl.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core::Browser
{

ViewFilterScopeCtrl::ViewFilterScopeCtrl(SetFilterRecursiveFunction&& setFilterRecursive)
    : setFilterRecursive_(setFilterRecursive)
{
    const auto lists = createLists({ {Packuru::Core::Browser::ViewFilterScopeCtrl::tr("Current folder"),
                                      ViewFilterScope::CurrentFolder},
                                     {Packuru::Core::Browser::ViewFilterScopeCtrl::tr("Entire archive"),
                                      ViewFilterScope::EntireArchive} });

    setValueLists(lists.first, lists.second);

    setCurrentValueFromPrivate(ViewFilterScope::CurrentFolder);

    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::Browser::ViewFilterScopeCtrl::tr("Scope");
}


void ViewFilterScopeCtrl::onValueSyncedToWidget(const QVariant& value)
{
    Q_UNUSED(value)
    const auto mode = getPropertyAs<ViewFilterScope>(ControllerProperty::PrivateCurrentValue);
    setFilterRecursive_(mode == ViewFilterScope::EntireArchive);
}

}



