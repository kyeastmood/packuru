// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QCoreApplication>
#include <QString>

#include "qcs-core/controller.h"

#include "viewfilterscope.h"


namespace Packuru::Core::Browser
{

class ViewFilterScopeCtrl : public qcs::core::Controller<QString, ViewFilterScope>
{
    Q_DECLARE_TR_FUNCTIONS(ViewFilterScopeCtrl)
public:
    using SetFilterRecursiveFunction = std::function<void(const bool)>;

    ViewFilterScopeCtrl(SetFilterRecursiveFunction&& setFilterRecursive);

private:
    void onValueSyncedToWidget(const QVariant& value) override;

    SetFilterRecursiveFunction setFilterRecursive_;
};

}
