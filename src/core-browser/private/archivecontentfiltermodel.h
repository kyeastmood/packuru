// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_set>

#include <QSortFilterProxyModel>

#include "utils/private/qttypehasher.h"


namespace Packuru::Core::Browser
{

class ArchiveContentModel;
enum class ItemFilteringMode;

class ArchiveContentFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    ArchiveContentFilterModel(QObject* parent = nullptr);

    void setSourceModel(QAbstractItemModel* sourceModel) override;

    /* Checks if sourceIndex has a matching proxy index in this model. If not, sourceIndex' parents
       are checked, up to the root of the model. If a source index with a matching proxy index has
       been found it is returned. This function is useful when recursive filtering was enabled
       and view's root index has been filtered out, because it tries to find the first available
       ancestor.
       Returns invalid QModelIndex if there is no matching proxy index in this model. */
    QModelIndex findClosestRelativeOfSource(const QModelIndex& sourceIndex) const;

    /* Setting filter's root index is only applicable when recursive filtering is disabled,
       in which case the filter will be invalidated. */
    void setFilterRootSourceIndex(const QModelIndex& sourceIndex);

    // Only sets the flag, will not invalidate the filter
    void setFilterEnabled(bool value);

    // If filter is enabled it will be invalidated
    void setFileFiltering(ItemFilteringMode mode);

    // If filter is enabled it will be invalidated
    void setFolderFiltering(ItemFilteringMode mode);

    // Only clears settings, will not invalidate the filter
    void clearFilterSettings();

protected:
    bool lessThan(const QModelIndex& source_left, const QModelIndex& source_right) const override;

private:
    bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const override;

    /* Keeping private members in the header to avoid indirection of pimpl pattern in filterAcceptsRow(),
       although it hasn't been tested performance-wise. */
    std::unordered_set<QModelIndex, Packuru::Utils::QtTypeHasher<QModelIndex>> whitelistedSourceIndexes;
    QModelIndex whitelistedSourceParent;
    // This flag was introduced to quickly return from filterAcceptsRow() when filter string is empty
    bool filterEnabled = false;
    ArchiveContentModel* archiveModel = nullptr;
    ItemFilteringMode fileFiltering;
    ItemFilteringMode folderFiltering;
};

}
