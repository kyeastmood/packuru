// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "utils/qvariant_utils.h"

#include "core/private/globalsettings.h"

#include "settingsconverter.h"


using Packuru::Core::GlobalSettings::Enum;


namespace Packuru::Core::Browser
{

SettingsConverter::SettingsConverter()
{

}


std::unordered_set<int> SettingsConverter::getSupportedKeys() const
{
    return { toInt(Enum::BC_ForceSingleInstance),
                toInt(Enum::BC_ShowSummaryOnTaskCompletion) };
}


QString SettingsConverter::getKeyName(int key) const
{
    const auto keyId = static_cast<GlobalSettings::Enum>(key);
    switch (keyId)
    {
    case GlobalSettings::Enum::BC_ForceSingleInstance:
        return QLatin1String("bc_force_single_instance");
    case GlobalSettings::Enum::BC_ShowSummaryOnTaskCompletion:
        return QLatin1String("bc_show_summary_on_task_completion");
    default:
        Q_ASSERT(false); return QLatin1String();
    }
}


QVariant SettingsConverter::toSettingsType(int key, const QVariant& value) const
{
    const auto keyId = static_cast<GlobalSettings::Enum>(key);
    switch (keyId)
    {
    case Enum::BC_ForceSingleInstance:
    case Enum::BC_ShowSummaryOnTaskCompletion:
        Q_ASSERT(Utils::containsType<bool>(value));
        return value;
    default:
        Q_ASSERT(false);
        return QVariant();
    }
}


QVariant SettingsConverter::fromSettingsType(int key, const QVariant& value) const
{
    const auto keyId = static_cast<GlobalSettings::Enum>(key);
    switch (keyId)
    {
    case Enum::BC_ForceSingleInstance:
        if (value.canConvert<bool>())
            return value.toBool();
        else
            return false;
    case Enum::BC_ShowSummaryOnTaskCompletion:
        if (value.canConvert<bool>())
            return value.toBool();
        else
            return true;
    default:
        Q_ASSERT(false);
        return QVariant();
    }
}

}
