// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QCoreApplication>

#include "qcs-core/controller.h"

#include "itemfilteringmode.h"


namespace Packuru::Core::Browser
{

class ViewFilterFoldersCtrl : public qcs::core::Controller<QString, ItemFilteringMode>
{
    Q_DECLARE_TR_FUNCTIONS(ViewFilterFoldersCtrl)

public:
    using SetFilteringFunction = std::function<void(ItemFilteringMode)>;

    ViewFilterFoldersCtrl(SetFilteringFunction&& setFiltering);

private:
    void onValueSyncedToWidget(const QVariant& value) override;

    SetFilteringFunction setFiltering_;
};

}
