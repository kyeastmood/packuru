// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>


namespace Packuru::Core::Browser
{

class CommandLineReadData;


class IncomingMessageInterpreter : public QObject
{
    Q_OBJECT
public:
    IncomingMessageInterpreter(QObject* parent = nullptr);

    void interpret(const QByteArray& message) const;

signals:
    void newMessage() const;
    void defaultStartUp() const;
    void commandLineError(const QString& info) const;
    void commandLineHelpRequested(const QString& info) const;
    void readArchives(const CommandLineReadData& data) const;
};

}
