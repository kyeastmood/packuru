// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QAbstractTableModel>


namespace Packuru::Core::Browser
{

class AssociativeModel : public QAbstractTableModel
{    
public:
    struct Item
    {
        QString key;
        QVariant value;
    };
    using ItemList = std::vector<Item>;

    AssociativeModel(QObject* parent = nullptr);
    ~AssociativeModel() override;

    int columnCount(const QModelIndex &parent) const override;
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;

    void setModelData(ItemList& items);
    void clear();

private:
    std::vector<Item> items;
};

}
