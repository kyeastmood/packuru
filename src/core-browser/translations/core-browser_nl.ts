<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>ItemFilteringMode</name>
    <message>
        <location filename="../private/itemfilteringmode.cpp" line="17"/>
        <source>Filter</source>
        <translation>Filteren</translation>
    </message>
    <message>
        <location filename="../private/itemfilteringmode.cpp" line="18"/>
        <source>Show all</source>
        <translation>Alles tonen</translation>
    </message>
    <message>
        <location filename="../private/itemfilteringmode.cpp" line="19"/>
        <source>Hide all</source>
        <translation>Alles verbergen</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::ArchiveBrowserCore</name>
    <message>
        <location filename="../archivebrowsercore.cpp" line="657"/>
        <source>Adding files</source>
        <translation>Bezig met toevoegen…</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="659"/>
        <location filename="../archivebrowsercore.cpp" line="663"/>
        <source>Extracting</source>
        <translation>Bezig met uitpakken…</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="661"/>
        <source>Deleting files</source>
        <translation>Bezig met verwijderen…</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="665"/>
        <source>Testing</source>
        <translation>Bezig met testen…</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="667"/>
        <source>Reading</source>
        <translation>Bezig met uitlezen…</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1036"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1044"/>
        <source>Created</source>
        <translation>Aangemaakt op</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1047"/>
        <source>Modified</source>
        <translation>Bewerkt op</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1039"/>
        <source>Location</source>
        <translation>Locatie</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="447"/>
        <source>Add</source>
        <translation>Toevoegen</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="449"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="453"/>
        <source>Filter</source>
        <translation>Filteren</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="455"/>
        <source>Preview</source>
        <translation>Voorvertonen</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="457"/>
        <source>Properties</source>
        <translation>Eigenschappen</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="459"/>
        <source>Reload</source>
        <translation>Herladen</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1059"/>
        <source>Original size</source>
        <translation>Oorspronkelijke grootte</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1071"/>
        <source>Archive size</source>
        <translation>Gearchiveerde grootte</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1078"/>
        <source>Compression ratio</source>
        <translation>Compressieverhouding</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1097"/>
        <source>Files</source>
        <translation>Bestanden</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1098"/>
        <source>Folders</source>
        <translation>Mappen</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1108"/>
        <source>Archiving methods</source>
        <translation>Archiveringsmethodes</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1124"/>
        <source>Recovery data</source>
        <translation>Herstelgegevens</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1147"/>
        <source>Snapshots</source>
        <translation>Reservekopieën</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1391"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1392"/>
        <source>No</source>
        <translation>Nee</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1116"/>
        <source>Solid</source>
        <translation>Vast</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1087"/>
        <source>Multipart</source>
        <translation>Meerdere delen</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1094"/>
        <source>Parts</source>
        <translation>Delen</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1131"/>
        <source>Encryption</source>
        <translation>Versleuteling</translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1139"/>
        <source>Locked</source>
        <translation>Vergrendeld</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::ArchiveContentModel</name>
    <message numerus="yes">
        <location filename="../private/archivecontentmodel.cpp" line="268"/>
        <source>%n items</source>
        <translation>
            <numerusform>%n item</numerusform>
            <numerusform>%n items</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="610"/>
        <source>Accessed</source>
        <translation>Geopend op</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="611"/>
        <source>Attributes</source>
        <translation>Attributen</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="612"/>
        <source>Block</source>
        <translation>Blok</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="613"/>
        <source>Comment</source>
        <translation>Opmerking</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="614"/>
        <source>CRC</source>
        <translation>CRC</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="615"/>
        <source>Created</source>
        <translation>Aangemaakt op</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="616"/>
        <source>Encrypted</source>
        <translation>Versleuteld</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="617"/>
        <source>Extension</source>
        <translation>Extensie</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="618"/>
        <source>Group</source>
        <translation>Groep</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="619"/>
        <source>Host OS</source>
        <translation>Hostsysteem</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="620"/>
        <source>Method</source>
        <translation>Methode</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="621"/>
        <source>Modified</source>
        <translation>Bewerkt op</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="622"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="623"/>
        <source>Node type</source>
        <translation>Soort node</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="624"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="625"/>
        <source>Packed</source>
        <translation>Ingepakt</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="626"/>
        <source>Ratio</source>
        <translation>Verhouding</translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="627"/>
        <source>User</source>
        <translation>Gebruiker</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::BasicControl</name>
    <message>
        <location filename="../basiccontrol.cpp" line="140"/>
        <source>Abort</source>
        <translation>Afbreken</translation>
    </message>
    <message>
        <location filename="../basiccontrol.cpp" line="143"/>
        <source>Browse archive</source>
        <translation>Bladeren door archiefinhoud</translation>
    </message>
    <message>
        <location filename="../basiccontrol.cpp" line="146"/>
        <source>Retry</source>
        <translation>Opnieuw proberen</translation>
    </message>
    <message>
        <location filename="../basiccontrol.cpp" line="149"/>
        <source>View log</source>
        <translation>Logboek bekijken</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::CommandLineHandler</name>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="77"/>
        <source>Open archives</source>
        <translation>Archieven openen</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::DeletionDialogCore</name>
    <message>
        <location filename="../deletiondialogcore.cpp" line="93"/>
        <source>Delete files</source>
        <translation>Bestanden verwijderen</translation>
    </message>
    <message numerus="yes">
        <location filename="../deletiondialogcore.cpp" line="90"/>
        <source>Do you want to delete %n items?</source>
        <translation>
            <numerusform>Weet u zeker dat u %n item wilt verwijderen?</numerusform>
            <numerusform>Weet u zeker dat u %n items wilt verwijderen?</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::IncomingMessageInterpreter</name>
    <message>
        <location filename="../private/incomingmessageinterpreter.cpp" line="64"/>
        <source>Unknown message type</source>
        <translation>Onbekend bericht</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::MainWindowCore</name>
    <message>
        <location filename="../mainwindowcore.cpp" line="155"/>
        <source>Open</source>
        <translation>Openen</translation>
    </message>
    <message>
        <location filename="../mainwindowcore.cpp" line="164"/>
        <source>%1 Browser</source>
        <translation>%1-verkenner</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::QueueMessenger</name>
    <message>
        <location filename="../queuemessenger.cpp" line="88"/>
        <source>Messaging queue</source>
        <translation>Berichtwachtrij</translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="91"/>
        <source>Run in browser</source>
        <translation>Openen in verkenner</translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="122"/>
        <source>Aborted.</source>
        <translation>Afgebroken.</translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="133"/>
        <source>Running in Browser.</source>
        <translation>Geopend in verkenner.</translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="171"/>
        <source>Sending message to Queue...</source>
        <translation>Bezig met versturen naar wachtrij…</translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="178"/>
        <source>Message sent.</source>
        <translation>Het bericht is verstuurd.</translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="220"/>
        <source>Starting up Queue...</source>
        <translation>Bezig met samenstellen van wachtrij…</translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="224"/>
        <source>Queue started.</source>
        <translation>De wachtrij is samengesteld.</translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="230"/>
        <source>Cannot start</source>
        <translation>Het samenstellen is mislukt</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::SettingsDialogCore</name>
    <message>
        <location filename="../settingsdialogcore.cpp" line="39"/>
        <source>Force single instance</source>
        <translation>Slechts één proces toestaan</translation>
    </message>
    <message>
        <location filename="../settingsdialogcore.cpp" line="46"/>
        <source>Show summary on task completion</source>
        <translation>Samenvatting tonen na afronden van taak</translation>
    </message>
    <message>
        <location filename="../settingsdialogcore.cpp" line="71"/>
        <source>Browser</source>
        <translation>Verkenner</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::TabClosingDialogCore</name>
    <message>
        <location filename="../tabclosingdialogcore.cpp" line="29"/>
        <source>Confirm close</source>
        <translation>Afsluiten bevestigen</translation>
    </message>
    <message>
        <location filename="../tabclosingdialogcore.cpp" line="32"/>
        <source>Do you want to close it?</source>
        <translation>Weet u zeker dat u wilt afsluiten?</translation>
    </message>
    <message>
        <location filename="../tabclosingdialogcore.cpp" line="35"/>
        <source>This tab is busy.</source>
        <translation>Dit tabblad is actief.</translation>
    </message>
</context>
<context>
    <name>ViewFilterFilesCtrl</name>
    <message>
        <location filename="../private/viewfilterfilesctrl.cpp" line="25"/>
        <source>Files</source>
        <translation>Bestanden</translation>
    </message>
</context>
<context>
    <name>ViewFilterFoldersCtrl</name>
    <message>
        <location filename="../private/viewfilterfoldersctrl.cpp" line="24"/>
        <source>Folders</source>
        <translation>Mappen</translation>
    </message>
</context>
<context>
    <name>ViewFilterScopeCtrl</name>
    <message>
        <location filename="../private/viewfilterscopectrl.cpp" line="17"/>
        <source>Current folder</source>
        <translation>Huidige map</translation>
    </message>
    <message>
        <location filename="../private/viewfilterscopectrl.cpp" line="19"/>
        <source>Entire archive</source>
        <translation>Gehele archief</translation>
    </message>
    <message>
        <location filename="../private/viewfilterscopectrl.cpp" line="27"/>
        <source>Scope</source>
        <translation>Bereik</translation>
    </message>
</context>
</TS>
