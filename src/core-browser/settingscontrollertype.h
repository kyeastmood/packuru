// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

/// @file

namespace Packuru::Core::Browser
{

/**
 @brief Identifies input widgets in %Browser settings.
 @headerfile "core-browser/settingscontrollertype.h"
 */
class SettingsControllerType : public QObject
{
    Q_OBJECT
public:
    /// @copybrief Packuru::Core::Browser::SettingsControllerType
    enum class Type
    {
        ForceSingleInstance, ///< Check box.
        ShowSummaryOnTaskCompletion ///< Check box.
    };

    Q_ENUM(Type)
};

}
