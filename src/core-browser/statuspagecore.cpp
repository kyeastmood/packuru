// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/commonstrings.h"
#include "../core/private/privatestrings.h"

#include "statuspagecore.h"
#include "private/statuspagecoreaux.h"


using Packuru::Core::CommonStrings;


namespace Packuru::Core::Browser
{

struct StatusPageCore::Private
{
    void setTaskDescription(const QString& value);
    void setTaskState(const QString& value);
    void setTaskProgress(int value);
    void setProgressBarVisible(bool value);

    StatusPageCore* publ = nullptr;
    StatusPageCoreAux::Init init;
    QString taskDescription;
    QString taskState;
    int taskProgress = -1;
    bool progressBarVisible = false;
};


StatusPageCore::StatusPageCore(StatusPageCoreAux::Init& init,
                               StatusPageCoreAux::Backdoor& backdoor,
                               QObject *parent)
    : QObject(parent),
      priv(new Private)
{
    priv->publ = this;

    Q_ASSERT(init.basicControl);
    priv->init = std::move(init);

    backdoor.setProgressBarVisible = [priv = priv.get()] (bool value)
    { priv->setProgressBarVisible(value); };

    backdoor.setTaskDescription = [priv = priv.get()] (const QString& value)
    { priv->setTaskDescription(value); };

    backdoor.setTaskProgress = [priv = priv.get()] (int value)
    { priv->setTaskProgress(value); };

    backdoor.setTaskState = [priv = priv.get()] (const QString& value)
    { priv->setTaskState(value); };
}


StatusPageCore::~StatusPageCore()
{

}


QString StatusPageCore::getTaskDescription() const
{
    return priv->taskDescription;
}


QString StatusPageCore::getTaskState() const
{
    return priv->taskState;
}


int StatusPageCore::getTaskProgress() const
{
    return priv->taskProgress;
}


bool StatusPageCore::getProgressBarVisible() const
{
    return priv->progressBarVisible;
}


BasicControl* StatusPageCore::getBasicControl()
{
    return priv->init.basicControl;
}


void StatusPageCore::Private::setTaskDescription(const QString& value)
{
    if (value == taskDescription)
        return;

    taskDescription = value;
    emit publ->taskDescriptionChanged(taskDescription);
}


void StatusPageCore::Private::setTaskState(const QString& value)
{
    if (value == taskState)
        return;

    taskState = value;
    emit publ->taskStateChanged(taskState);
}


void StatusPageCore::Private::setTaskProgress(int value)
{
    if (value == taskProgress)
        return;

    taskProgress = value;
    emit publ->taskProgressChanged(taskProgress);
}


void StatusPageCore::Private::setProgressBarVisible(bool value)
{
    if (value == progressBarVisible)
        return;

    progressBarVisible = value;
    emit publ->progressBarVisibleChanged(progressBarVisible);
}

}
