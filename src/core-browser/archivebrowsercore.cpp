// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDir>
#include <QTimer>
#include <QUrl>
#include <QDebug>
#include <QItemSelectionModel>

#include "../utils/private/qstringunorderedset.h"
#include "../utils/boolflag.h"
#include "../utils/qvariant_utils.h"
#include "../utils/makeqpointer.h"

#include "../core/passwordpromptcore.h"
#include "../core/extractionfolderoverwritepromptcore.h"
#include "../core/extractionfileoverwritepromptcore.h"
#include "../core/extractiontypemismatchpromptcore.h"
#include "../core/extractiondialogcore.h"
#include "../core/testdialogcore.h"
#include "../core/testdialogcore.h"
#include "../core/archivingdialogcore.h"
#include "../core/globalsettingsmanager.h"
#include "../core/private/globalsettings.h"
#include "../core/private/tasks/taskstate.h"
#include "../core/private/tasks/taskerror.h"
#include "../core/private/tasks/readtask.h"
#include "../core/private/tasks/readtaskdata.h"
#include "../core/private/tasks/tasktype.h"
#include "../core/private/plugin-api/backendarchiveproperty.h"
#include "../core/private/tasks/extractiontaskdata.h"
#include "../core/private/tasks/extractiontask.h"
#include "../core/private/plugin-api/encryptionstate.h"
#include "../core/private/compressionratiomode.h"
#include "../core/private/digitalsize.h"
#include "../core/private/tasks/archivecontentdata.h"
#include "../core/private/dialogcore/extractiondialog/extractiondialogcoreinitsingle.h"
#include "../core/private/dialogcore/testdialog/testdialogcoreinitsingle.h"
#include "../core/private/plugin-api/archivingparameterhandlerfactory.h"
#include "../core/private/dialogcore/archivingdialog/archivingdialogcoreinitadd.h"
#include "../core/private/privatestrings.h"

#include "archivebrowsercore.h"
#include "private/archivebrowsercoreinitdata.h"
#include "archivenavigator.h"
#include "statuspagecore.h"
#include "deletiondialogcore.h"
#include "private/archivebrowsercorenotifier.h"
#include "private/pluginconfigchangenotifier.h"
#include "private/associativemodel.h"
#include "private/archivecontentmodel.h"
#include "private/archivenavigatorinitdata.h"
#include "private/archivenavigatorbackdoordata.h"
#include "private/previewrequestdata.h"
#include "private/reloadrequestdata.h"
#include "private/deletiondialogcoreinitdata.h"
#include "private/archivedialogacceptedcallback.h"
#include "private/archivedialogacceptedresult.h"
#include "private/statuspagecoreaux.h"
#include "basiccontrol.h"
#include "private/basiccontrolaux.h"


namespace Packuru::Core::Browser
{

struct ArchiveBrowserCore::Private
{
    Private(ArchiveBrowserCore* publ, ArchiveBrowserCoreInitData initData_);

    void setTask(Task* task);
    void updateTitle(const QString& newTitle);
    QString getTaskDescription() const;
    UIState getUIState() const;
    void setUIState(UIState newState);
    void reset();
    void onTaskStateChanged();
    void onReadTaskStateChanged(TaskState state);
    void onModifyingTaskStateChanged(TaskState state);
    void onPreviewTaskStateChanged(TaskState state);
    void preview(const std::vector<QModelIndex>& items);
    void updateArchiveProperties(const BackendArchiveProperties& properties);
    AssociativeModel::ItemList createPropertiesModelDataList() const;
    bool isTaskBusy() const;
    EncryptionState getEncryptionState() const;

    void evaluateAvailableActions();
    bool setReloadAvailable(bool value);
    bool setExtractAvailable(bool value);
    bool setAddAvailable(bool value);
    bool setDeleteAvailable(bool value);
    bool setPreviewAvailable(bool value);
    bool setPropertiesAvailable(bool value);
    bool setFilterAvailable(bool value);
    bool isReloadAvailable() const;
    bool isExtractAvailable() const;
    bool isAddAvailable() const;
    bool isDeleteAvailable() const;
    bool isPreviewAvailable() const;
    bool archivePropertiesAvailable() const;
    bool isFilterAvailable() const;

    template <typename TaskData>
    void onArchiveDialogAccepted(const ArchiveDialogAcceptedCallback<TaskData>& dialogAccepted,
                                 const TaskData& data);

    QString boolToString(bool value) const;

    ArchiveBrowserCore::BrowserTaskState translate(TaskState state);
    
    ArchiveBrowserCore* publ = nullptr;
    ArchiveBrowserCoreInitData initData_;
    StatusPageCore* statusPageCore = nullptr;
    StatusPageCoreAux::Backdoor statusPageBackdoor;
    BasicControl* basicControl = nullptr;
    BasicControlAux::Backdoor basicControlBackdoor;
    ArchiveContentModel* contentModel = nullptr;
    ArchiveNavigator* navigator = nullptr;
    ArchiveNavigatorBackdoorData navigatorBackdoorData;
    QString title;
    ArchiveType archiveType = ArchiveType::___NOT_SUPPORTED;
    QTimer* previewTaskShowStatusPageTimer = nullptr;
    BackendArchiveProperties archiveProperties;
    Task* currentTask = nullptr;
    AssociativeModel* propertiesModel = nullptr;
    bool archiveModified = false;
    std::unordered_set<TaskError> lastTasksErrors;
    QString lastCorrectPassword;
    UIState uiState = UIState::Navigation;

private:
    bool reloadAvailable = false;
    bool extractAvailable = false;
    bool addAvailable = false;
    bool deleteAvailable = false;
    bool previewAvailable = false;
    bool propertiesAvailabe = false;
    bool filterAvailable = false;
};


ArchiveBrowserCore::ArchiveBrowserCore(ArchiveBrowserCoreInitData& initData, QObject *parent)
    : QObject(parent),
      priv(new Private(this, std::move(initData)))
{
    ArchiveNavigatorInitData navInitData;
    navInitData.archiveModel = priv->contentModel;

    navInitData.fileClicked = [publ = Utils::makeQPointer(this)] (const QModelIndex& index)
    {
        if (!publ)
            return;
        Q_ASSERT(!publ->isTaskBusy());
        publ->priv->preview({index});
    };

    navInitData.selectionChanged = [publ = Utils::makeQPointer(this)] (int selectedFiles, int selectedFolders)
    {
        Q_UNUSED(selectedFiles)
        Q_UNUSED(selectedFolders)

        if (!publ)
            return;
        publ->priv->evaluateAvailableActions();
    };


    priv->navigator = new ArchiveNavigator(navInitData, priv->navigatorBackdoorData, this);
    Q_ASSERT(priv->navigatorBackdoorData.isValid());
}


ArchiveBrowserCore::~ArchiveBrowserCore()
{
    Q_ASSERT(!priv->initData_.tempDir.isEmpty());
    
    QDir dir(priv->initData_.tempDir);
    
    if (dir.exists())
        dir.removeRecursively();
    
    if (priv->initData_.destroyed)
        priv->initData_.destroyed(priv->initData_.archiveInfo);
}


QString ArchiveBrowserCore::getTitle() const
{
    return priv->title;
}


QString ArchiveBrowserCore::getArchiveName() const
{
    return priv->initData_.archiveInfo.fileName();
}


ArchiveNavigator* ArchiveBrowserCore::getNavigator()
{
    return priv->navigator;
}


StatusPageCore*ArchiveBrowserCore::getStatusPageCore()
{
    return priv->statusPageCore;
}


ArchiveBrowserCore::UIState ArchiveBrowserCore::getUIState() const
{
    return priv->getUIState();
}


ArchiveBrowserCore::BrowserTaskState ArchiveBrowserCore::getTaskState() const
{
    Q_ASSERT(priv->currentTask);

    return priv->translate(priv->currentTask->getState());
}


QAbstractTableModel* ArchiveBrowserCore::getPropertiesModel()
{
    if (!priv->propertiesModel)
    {
        priv->propertiesModel = new AssociativeModel(this);
        auto list = priv->createPropertiesModelDataList();
        priv->propertiesModel->setModelData(list);
    }

    return priv->propertiesModel;
}


QString ArchiveBrowserCore::getArchiveComment() const
{
    const auto it = priv->archiveProperties.find(BackendArchiveProperty::Comment);
    if (it != priv->archiveProperties.end())
        return Utils::getValueAs<QString>(it->second);
    else
        return QString();
}


bool ArchiveBrowserCore::isTaskBusy() const
{
    return priv->isTaskBusy();
}


bool ArchiveBrowserCore::isTaskFinished() const
{
    if (priv->currentTask)
        return priv->currentTask->isFinished();
    else
        return true;
}


bool ArchiveBrowserCore::isAddAvailable() const
{
    return priv->isAddAvailable();
}


bool ArchiveBrowserCore::isDeleteAvailable() const
{
    return priv->isDeleteAvailable();
}


bool ArchiveBrowserCore::isExtractAvailable() const
{
    return priv->isExtractAvailable();
}


bool ArchiveBrowserCore::isFilterAvailable() const
{
    return priv->isFilterAvailable();
}


bool ArchiveBrowserCore::isPreviewAvailable() const
{
    return priv->isPreviewAvailable();
}


bool ArchiveBrowserCore::isReloadAvailable() const
{
    return priv->isReloadAvailable();
}


bool ArchiveBrowserCore::archivePropertiesAvailable() const
{
    return priv->archivePropertiesAvailable();
}


void ArchiveBrowserCore::reloadArchive()
{
    ReloadRequestData requestData;
    requestData.archiveInfo = priv->initData_.archiveInfo;
    requestData.password = priv->lastCorrectPassword;

    auto task = priv->initData_.createReadTask(requestData);
    priv->setTask(task);
}


void ArchiveBrowserCore::previewSelectedFiles()
{
    Q_ASSERT(!isTaskBusy());
    Q_ASSERT(isPreviewAvailable());
    const std::vector<QModelIndex> items = priv->navigatorBackdoorData.getSelectedItems();
    priv->preview(items);
}


ArchivingDialogCore* ArchiveBrowserCore::createArchivingDialogCore()
{
    Q_ASSERT(isAddAvailable());

    if (!isAddAvailable())
        return nullptr;

    ArchivingDialogCoreInitAdd dialogInit;

    dialogInit.archiveInfo = priv->initData_.archiveInfo;
    dialogInit.archiveEncryption = priv->getEncryptionState();
    dialogInit.password = priv->lastCorrectPassword;
    dialogInit.archiveInternalPath = priv->navigator->getCurrentPath();

    if (priv->initData_.createArchivingParameterHandlerFactory)
        dialogInit.factory = priv->initData_.createArchivingParameterHandlerFactory(priv->archiveType);

    dialogInit.onAccepted = [publ = Utils::makeQPointer(this)] (const ArchivingTaskData& data)
    {
        if (!publ)
            return;
        publ->priv->onArchiveDialogAccepted(publ->priv->initData_.onArchivingDialogAccepted, data);
    };

    return new ArchivingDialogCore(std::move(dialogInit),this);
}


DeletionDialogCore* ArchiveBrowserCore::createDeletionDialogCore()
{
    Q_ASSERT(isDeleteAvailable());

    if (!isDeleteAvailable())
        return nullptr;

    DeletionDialogCoreInitData dialogInit;

    dialogInit.archiveAbsFilePath = priv->initData_.archiveInfo.absoluteFilePath();
    dialogInit.archiveType = priv->archiveType;
    dialogInit.archiveEncryption = priv->getEncryptionState();
    dialogInit.password = priv->lastCorrectPassword;

    const std::vector<QModelIndex> items = priv->navigatorBackdoorData.getSelectedItems();
    dialogInit.filesToRemove.reserve(static_cast<std::size_t>(items.size()));

    for (const auto& item : items)
        dialogInit.filesToRemove.push_back(priv->contentModel->
                                           getAbsoluteFilePath(item, ArchiveContentModel::PathFormat::Backend));

    dialogInit.onAccepted = [publ = Utils::makeQPointer(this)] (const DeletionTaskData& data)
    {
        if (!publ)
            return;
        publ->priv->onArchiveDialogAccepted(publ->priv->initData_.onDeletionDialogAccepted, data);
    };

    return new DeletionDialogCore(std::move(dialogInit), this);
}


ExtractionDialogCore* ArchiveBrowserCore::createExtractionDialogCore()
{
    Q_ASSERT(isExtractAvailable());

    if (!isExtractAvailable())
        return nullptr;

    ExtractionDialogCoreInitSingle dialogInit;

    dialogInit.archiveInfo = priv->initData_.archiveInfo;
    dialogInit.archiveType = priv->archiveType;
    dialogInit.archiveEncryption = priv->getEncryptionState();
    dialogInit.password = priv->lastCorrectPassword;

    const std::vector<QModelIndex> items = priv->navigatorBackdoorData.getSelectedItems();

    dialogInit.selectedFiles.reserve(static_cast<std::size_t>(items.size()));

    for (const auto& item : items)
        dialogInit.selectedFiles.push_back(priv->contentModel->
                                           getAbsoluteFilePath(item, ArchiveContentModel::PathFormat::Backend));

    dialogInit.onAccepted = [publ = Utils::makeQPointer(this)] (const ExtractionTaskData& data)
    {
        if (!publ)
            return;
        publ->priv->onArchiveDialogAccepted(publ->priv->initData_.onExtractionDialogAccepted, data);
    };

    return new ExtractionDialogCore(std::move(dialogInit), this);
}


TestDialogCore* ArchiveBrowserCore::createTestDialogCore()
{
    Q_ASSERT(isExtractAvailable());

    if (!isExtractAvailable())
        return nullptr;

    TestDialogCoreInitSingle dialogInit;
    dialogInit.archiveInfo = priv->initData_.archiveInfo;
    dialogInit.archiveType = priv->archiveType;
    dialogInit.archiveEncryption = priv->getEncryptionState();
    dialogInit.password = priv->lastCorrectPassword;
    dialogInit.onAccepted = [publ = Utils::makeQPointer(this)] (const TestTaskData& data)
    {
        if (!publ)
            return;
        publ->priv->onArchiveDialogAccepted(publ->priv->initData_.onTestDialogAccepted, data);
    };

    return new TestDialogCore(std::move(dialogInit), this);
}


QString ArchiveBrowserCore::getString(ArchiveBrowserCore::UserString value)
{
    switch (value)
    {
    case UserString::ActionAdd:
        return Packuru::Core::Browser::ArchiveBrowserCore::tr("Add");
    case UserString::ActionDelete:
        return Packuru::Core::Browser::ArchiveBrowserCore::tr("Delete");
    case UserString::ActionExtract:
        return PrivateStrings::getString(PrivateStrings::UserString::ActionExtract);
    case UserString::ActionFilter:
        return Packuru::Core::Browser::ArchiveBrowserCore::tr("Filter");
    case UserString::ActionPreview:
        return Packuru::Core::Browser::ArchiveBrowserCore::tr("Preview");
    case UserString::ActionProperties:
        return Packuru::Core::Browser::ArchiveBrowserCore::tr("Properties");
    case UserString::ActionReload:
        return Packuru::Core::Browser::ArchiveBrowserCore::tr("Reload");
    case UserString::ActionTest:
        return PrivateStrings::getString(PrivateStrings::UserString::ActionTest);
    default:
        Q_ASSERT(false); return "";
    }
}


ArchiveBrowserCore::Private::Private(ArchiveBrowserCore* publ, ArchiveBrowserCoreInitData initData)
    : publ(publ),
      initData_(std::move(initData)),
      contentModel(new ArchiveContentModel(publ))
{
    title = initData_.archiveInfo.fileName();

    BasicControlAux::Init basicControlInit;
    basicControlInit.abortRequested = [publ = Utils::makeQPointer(publ)] ()
    {
        if (!publ)
            return;

        auto task = publ->priv->currentTask;
        task->stop();
    };

    basicControlInit.browseRequested = [publ = Utils::makeQPointer(publ)] ()
    {
        if (!publ)
            return;

        publ->priv->setUIState(UIState::Navigation);
    };

    basicControlInit.retryRequested = [publ = Utils::makeQPointer(publ)] ()
    {
        if (!publ)
            return;

        auto task = publ->priv->currentTask;

        /* Resetting a read task will not cover the case when archive type changed, e.g. it
           was unknown (e.g. partially downloaded) and now it is some supported type, because
           Task subclasses don't support changing archive type. Emitting reloadArchive() signal
           will cause a new read task construction with a new archive type. */
        if (qobject_cast<ReadTask*>(task))
            publ->reloadArchive();
        else
        {
            task->reset();
            task->start();
        }
    };

    basicControl = new BasicControl(basicControlInit, basicControlBackdoor, publ);
    Q_ASSERT(basicControlBackdoor.setButtonState);
    Q_ASSERT(basicControlBackdoor.setDefaultButton);
    Q_ASSERT(basicControlBackdoor.setErrorsAndWarnings);
    Q_ASSERT(basicControlBackdoor.setLog);

    StatusPageCoreAux::Init statusPageInit;
    statusPageInit.basicControl = basicControl;

    statusPageCore = new StatusPageCore(statusPageInit, statusPageBackdoor, publ);
    Q_ASSERT(statusPageBackdoor.setProgressBarVisible);
    Q_ASSERT(statusPageBackdoor.setTaskDescription);
    Q_ASSERT(statusPageBackdoor.setTaskProgress);
    Q_ASSERT(statusPageBackdoor.setTaskState);

    Q_ASSERT(!initData_.archiveInfo.absoluteFilePath().isEmpty());
    Q_ASSERT(initData_.createReadTask);

    initData_.notifier->setParent(publ);

    connect(initData_.notifier, &ArchiveBrowserCoreNotifier::runInBrowserRequested,
            publ, [publ = publ] (auto task)
    {
        publ->priv->setTask(task);
    });

    connect(initData_.pluginChangeNotifier, &PluginConfigChangeNotifier::pluginConfigChanged,
            publ, [publ = publ] ()
    {
        publ->priv->evaluateAvailableActions();
    });

    previewTaskShowStatusPageTimer = new QTimer(publ);
    previewTaskShowStatusPageTimer->setSingleShot(true);

    connect(previewTaskShowStatusPageTimer, &QTimer::timeout,
            publ, [publ = publ] ()
    {
        auto priv = publ->priv.get();
        if (priv->currentTask->getType() == TaskType::Preview
                && priv->currentTask->getState() == TaskState::InProgress)
            priv->setUIState(UIState::StatusPage);
    });

    ReloadRequestData reqData;
    reqData.archiveInfo = initData_.archiveInfo;
    Task* const task = initData_.createReadTask(reqData);
    setTask(task);
}


void ArchiveBrowserCore::Private::setTask(Task *task)
{
    if (!task)
        return;

    Q_ASSERT(task != currentTask);
    Q_ASSERT(!currentTask || !currentTask->isBusy());
    // Do not allow any tasks except read for modified archives
    Q_ASSERT(task->getType() == TaskType::Read || !archiveModified);

    if (currentTask)
        currentTask->deleteLater();
    
    currentTask = task;
    currentTask->setParent(publ);
    
    statusPageBackdoor.setTaskDescription(getTaskDescription());

    QObject::connect(currentTask, &Task::stateChanged,
                     publ, [publ = publ] ()
    { publ->priv->onTaskStateChanged(); });

    QObject::connect(currentTask, &Task::progressChanged,
                     publ, [publ = publ] (int value)
    {
        if (value >= 0)
        {
            const QString newTitle = QLatin1Char('[') + QString::number(value) + QLatin1String("%] ")
                    + publ->getArchiveName();
            publ->priv->updateTitle(newTitle);
        }
    });

    QObject::connect(currentTask, &Task::progressChanged,
                     publ, [&backdoor = statusPageBackdoor] (int value)
    { backdoor.setTaskProgress(value); });
    
    QObject::connect(currentTask, &Task::passwordNeeded,
                     statusPageCore, &StatusPageCore::showPasswordPrompt);

    QObject::connect(currentTask, &Task::busy,
                     publ, [&backdoor = statusPageBackdoor] (bool value)
    {
        if (value)
            backdoor.setTaskProgress(-1);
    });

    QObject::connect(currentTask, &Task::busy,
                     publ, &ArchiveBrowserCore::taskBusyChanged);
    
    if (auto readTask = qobject_cast<ReadTask*>(currentTask))
    {
        /* Archive type must be reset because any task performed next will be initialised
           with it - archive type is only rechecked when reloading archive. */
        archiveType = readTask->getTaskData().backendData.archiveType;
        reset();
        emit publ->needsActivation();
    }
    else if (auto extractionTask = qobject_cast<ExtractionTask*>(currentTask))
    {
        QObject::connect(extractionTask, &ExtractionTask::folderExists,
                         statusPageCore, &StatusPageCore::showFolderExistsPrompt);

        QObject::connect(extractionTask, &ExtractionTask::fileExists,
                         statusPageCore, &StatusPageCore::showFileExistsPrompt);

        QObject::connect(extractionTask, &ExtractionTask::itemTypeMismatch,
                         statusPageCore, &StatusPageCore::showItemTypeMismatchPrompt);

        QObject::connect(extractionTask, &ExtractionTask::openDestinationPath,
                         publ, [publ = publ] (const QString& path)
        { publ->openExtractionDestinationFolder(QUrl::fromLocalFile(path)); });
    }

    // Give other parts of the program a chance to connect to signals when creating a new ArchiveBrowserCore
    QTimer::singleShot(0, currentTask, &Task::start);
}

void ArchiveBrowserCore::Private::updateTitle(const QString& newTitle)
{
    if (newTitle != title)
    {
        title = newTitle;
        emit publ->titleChanged(title);
    }
}


QString ArchiveBrowserCore::Private::getTaskDescription() const
{
    switch (currentTask->getType())
    {
    case TaskType::Add:
        return Packuru::Core::Browser::ArchiveBrowserCore::tr("Adding files");
    case TaskType::Extract:
        return Packuru::Core::Browser::ArchiveBrowserCore::tr("Extracting");
    case TaskType::Delete:
        return Packuru::Core::Browser::ArchiveBrowserCore::tr("Deleting files");
    case TaskType::Preview:
        return Packuru::Core::Browser::ArchiveBrowserCore::tr("Extracting");
    case TaskType::Test:
        return Packuru::Core::Browser::ArchiveBrowserCore::tr("Testing");
    case TaskType::Read:
        return Packuru::Core::Browser::ArchiveBrowserCore::tr("Reading");
    default:
        Q_ASSERT(false); return QString();
    }
}


ArchiveBrowserCore::UIState ArchiveBrowserCore::Private::getUIState() const
{
    return uiState;
}


void ArchiveBrowserCore::Private::setUIState(UIState newState)
{
    if (newState == uiState)
        return;

    uiState = newState;

    if (uiState == UIState::Navigation)
    {
        if (publ->priv->archiveModified)
            publ->reloadArchive();
        else
        {
            const auto taskState = publ->priv->translate(publ->priv->currentTask->getState());

            // Clean up temporary data of uncompleted tasks
            if (taskState == ArchiveBrowserCore::BrowserTaskState::Aborted
                    || taskState == ArchiveBrowserCore::BrowserTaskState::Errors)
                publ->priv->currentTask->reset();
        }
    }

    emit publ->uiStateChanged(uiState);
}

void ArchiveBrowserCore::Private::reset()
{
    contentModel->clear();
    archiveModified = false;
    lastTasksErrors.clear();
    lastCorrectPassword.clear();
    archiveProperties.clear();
    if (propertiesModel)
        propertiesModel->clear();
    emit publ->propertiesAvailableChanged(false);
    emit publ->browserReset();
}


void ArchiveBrowserCore::Private::onTaskStateChanged()
{
    const TaskState taskState = currentTask->getState();
    const TaskType taskType = currentTask->getType();

    switch (taskType)
    {
    case TaskType::Add:
        onModifyingTaskStateChanged(taskState);
        break;
    case TaskType::Preview:
        onPreviewTaskStateChanged(taskState);
        break;
    case TaskType::Read:
        onReadTaskStateChanged(taskState);
        break;
    case TaskType::Delete:
        onModifyingTaskStateChanged(taskState);
        break;
    default:
        break;
    }

    if (isStateFinished(taskState))
    {
        lastTasksErrors = currentTask->getErrors();

        if (isStateCompleted(taskState))
        {
            bool storePassword = false;

            if (taskType == TaskType::Read)
            {
                // If the archive was previously encrypted but was changed by an external
                // process and now it's not encrypted, it's safer to forget the password.
                const auto it = archiveProperties.find(BackendArchiveProperty::EncryptionState);

                if (it != archiveProperties.end())
                {
                    const auto encryptionState = Utils::getValueAs<EncryptionState>(it->second);
                    if (encryptionState != EncryptionState::Disabled)
                        storePassword = true;
                }
            }
            else
                storePassword = true;

            if (storePassword)
                lastCorrectPassword = currentTask->getPassword();
        }

        evaluateAvailableActions();
    }
    else if (taskState == TaskState::StartingUp)
        evaluateAvailableActions();

    statusPageBackdoor.setTaskState(toString(taskState));
    statusPageBackdoor.setProgressBarVisible(isStateBusy(taskState));

    const auto showSuccessSummary = GlobalSettingsManager::instance()->
            getValueAs<bool>(GlobalSettings::Enum::BC_ShowSummaryOnTaskCompletion);

    using BasicControlAux::ButtonEnabled;
    using BasicControlAux::ButtonVisible;

    switch (taskState)
    {
    case TaskState::ReadyToRun:
        break;

    case TaskState::StartingUp:
        break;

    case TaskState::InProgress:
        basicControlBackdoor.setButtonState(BasicControlControllerType::Type::AbortButton,
                                            ButtonVisible::True,
                                            ButtonEnabled::True);

        basicControlBackdoor.setButtonState(BasicControlControllerType::Type::BrowserButton,
                                            ButtonVisible::False,
                                            ButtonEnabled::False);

        basicControlBackdoor.setButtonState(BasicControlControllerType::Type::RetryButton,
                                            ButtonVisible::False,
                                            ButtonEnabled::False);

        basicControlBackdoor.setButtonState(BasicControlControllerType::Type::ViewLogButton,
                                            ButtonVisible::False,
                                            ButtonEnabled::False);

        basicControlBackdoor.setDefaultButton(BasicControlControllerType::Type::AbortButton);

        basicControlBackdoor.setErrorsAndWarnings("");

        statusPageCore->showBasicControl();
        if (taskType == TaskType::Preview)
            previewTaskShowStatusPageTimer->start(1500);
        else
            setUIState(UIState::StatusPage);
        break;

    case TaskState::Success:
        if (taskType == TaskType::Preview
                || taskType == TaskType::Read
                || !showSuccessSummary)
            setUIState(UIState::Navigation);
        else
        {
            basicControlBackdoor.setButtonState(BasicControlControllerType::Type::AbortButton,
                                                ButtonVisible::False,
                                                ButtonEnabled::False);

            basicControlBackdoor.setButtonState(BasicControlControllerType::Type::BrowserButton,
                                                ButtonVisible::True,
                                                ButtonEnabled::True);

            basicControlBackdoor.setButtonState(BasicControlControllerType::Type::RetryButton,
                                                ButtonVisible::False,
                                                ButtonEnabled::False);

            basicControlBackdoor.setButtonState(BasicControlControllerType::Type::ViewLogButton,
                                                ButtonVisible::False,
                                                ButtonEnabled::False);

            basicControlBackdoor.setDefaultButton(BasicControlControllerType::Type::BrowserButton);

            statusPageCore->showBasicControl();
            setUIState(UIState::StatusPage);
        }
        break;

    case TaskState::Warnings:
    case TaskState::Errors:
    case TaskState::Aborted:
    {
        const auto browseEnabled = contentModel->containsData() ? ButtonEnabled::True: ButtonEnabled::False;

        const QString log = currentTask->getLog();
        basicControlBackdoor.setLog(log);
        const auto logEnabled = log.isEmpty() ? ButtonEnabled::False : ButtonEnabled::True;

        basicControlBackdoor.setButtonState(BasicControlControllerType::Type::AbortButton,
                                            ButtonVisible::False,
                                            ButtonEnabled::False);

        basicControlBackdoor.setButtonState(BasicControlControllerType::Type::BrowserButton,
                                            ButtonVisible::True,
                                            browseEnabled);

        basicControlBackdoor.setButtonState(BasicControlControllerType::Type::RetryButton,
                                            ButtonVisible::True,
                                            ButtonEnabled::True);

        basicControlBackdoor.setButtonState(BasicControlControllerType::Type::ViewLogButton,
                                            ButtonVisible::True,
                                            logEnabled);

        basicControlBackdoor.setDefaultButton(BasicControlControllerType::Type::RetryButton);

        basicControlBackdoor.setErrorsAndWarnings(currentTask->getErrorAndWarningNames().join('\n'));

        statusPageCore->showBasicControl();
        setUIState(UIState::StatusPage);
        break;
    }

    default:
        setUIState(UIState::StatusPage);
        break;
    }

    if (isStateFinished(taskState))
        updateTitle(publ->getArchiveName());

    const auto browserTaskState = translate(taskState);

    emit publ->taskStateChanged(browserTaskState);
}


void ArchiveBrowserCore::Private::onReadTaskStateChanged(TaskState state)
{
    if (!isStateFinished(state))
        return;

    initData_.archiveInfo.refresh(); // QFileInfo caches data, including time/date
    initData_.archiveInfo.lastModified(); // Read and cache modification time

    const auto readTask = qobject_cast<const ReadTask*>(currentTask);
    Q_ASSERT(readTask);

    // contentModel must be updated first because updateArchiveProperties will get some data from it
    contentModel->setArchiveData(readTask->getArchiveContentData());

    const BackendArchiveProperties archiveProperties = readTask->getArchiveProperties();
    updateArchiveProperties(archiveProperties);
}


void ArchiveBrowserCore::Private::onPreviewTaskStateChanged(TaskState state)
{
    if (!isStateFinished(state) || state == TaskState::Aborted)
        return;

    const auto previewTask = qobject_cast<const ExtractionTask*>(currentTask);
    Q_ASSERT(previewTask);
    std::vector<QUrl> readyUrls;
    const std::vector<QString> relativeFilePaths = previewTask->getData().backendData.filesToExtract;
    for (const auto& path : relativeFilePaths)
    {
        const QFileInfo info(initData_.tempDir + path);
        if (info.exists() && (state != TaskState::Errors || info.size() > 0))
            readyUrls.push_back(QUrl::fromLocalFile(info.absoluteFilePath()));
    }
    emit publ->filesReadyForPreview(readyUrls);
}


void ArchiveBrowserCore::Private::onModifyingTaskStateChanged(TaskState state)
{
    if (!isStateFinished(state))
        return;

    const auto mod1 = initData_.archiveInfo.lastModified();
    initData_.archiveInfo.refresh();
    const auto mod2 = initData_.archiveInfo.lastModified();

    if (mod1 != mod2)
        archiveModified = true;
}


void ArchiveBrowserCore::Private::preview(const std::vector<QModelIndex>& items)
{
    Q_ASSERT(!archiveModified);

    if (initData_.tempDir.isEmpty())
    {
        qWarning()<<"File preview is not possible as temporary folder was not set.";
        return;
    }

    std::vector<QString> extractionList;
    std::vector<QUrl> previewUrlsList;

    QFileInfo info;
    for (const auto& item : items)
    {
        Q_ASSERT(contentModel->checkIndex(item, QAbstractItemModel::CheckIndexOption::IndexIsValid));

        if (!contentModel->isDir(item))
        {
            const QString filePath = contentModel->getAbsoluteFilePath(item, ArchiveContentModel::PathFormat::Backend);

            if (filePath.startsWith(QLatin1Char('/')))
                initData_.tempDir.append(QLatin1String("/@UNIX_ROOT_DIR"));
            else
                initData_.tempDir.append(QLatin1Char('/'));

            info.setFile(initData_.tempDir + filePath);

            if (!info.exists())
            {
                extractionList.push_back(filePath);
            }
            else
            {
                const QDateTime archiveLastMod = contentModel->lastModified(item);

                // Dates may differ if a file was first extracted for preview, then updated
                // in the archive. In that case it must be extracted again.
                if (archiveLastMod == info.lastModified())
                    previewUrlsList.push_back(QUrl::fromLocalFile(initData_.tempDir + filePath));
                else
                    extractionList.push_back(filePath);
            }
        }
    }

    if (!previewUrlsList.empty())
        emit publ->filesReadyForPreview(previewUrlsList);

    if (!extractionList.empty())
    {
        PreviewRequestData data;
        data.archiveInfo = initData_.archiveInfo;
        data.archiveType = archiveType;
        data.files = extractionList;
        data.destinationPath = initData_.tempDir;
        data.password = lastCorrectPassword;

        if (initData_.createPreviewTask)
        {
            Task* const task = initData_.createPreviewTask(data);
            setTask(task);
        }
    }
}


void ArchiveBrowserCore::Private::updateArchiveProperties(const BackendArchiveProperties& properties)
{
    archiveProperties = properties;

    if (propertiesModel)
    {
        auto list = createPropertiesModelDataList();
        propertiesModel->setModelData(list);
    }
}


AssociativeModel::ItemList ArchiveBrowserCore::Private::createPropertiesModelDataList() const
{
    AssociativeModel::ItemList list;
    const auto archiveInfo = initData_.archiveInfo;

    list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Type"),
                    toString(archiveType)});

    list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Location"),
                    QUrl::fromLocalFile(archiveInfo.absolutePath())});

    auto birthTime = archiveInfo.birthTime();
    if (birthTime.isValid())
        list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Created"),
                        birthTime.toString("yyyy-MM-dd HH:mm:ss")});

    list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Modified"),
                    archiveInfo.lastModified().toString("yyyy-MM-dd HH:mm:ss")});

    const ArchiveContentData contentData = contentModel->getArchiveData();

    const auto units = GlobalSettingsManager::instance()
            ->getValueAs<SizeUnits>(GlobalSettings::Enum::AC_SizeUnits);

    const qulonglong originalSize = contentData.totalOriginalSize();
    QString info = DigitalSize::toString(originalSize, units);
    if (DigitalSize::exceedsByteTreshold(originalSize, units))
        info += " (" + QString::number(originalSize) + " B)";
    list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Original size"), info});

    auto archiveSize = static_cast<qulonglong>(archiveInfo.size());

    auto it = archiveProperties.find(BackendArchiveProperty::TotalSize);
    if (it != archiveProperties.end())
        archiveSize = it->second.toULongLong();

    info = DigitalSize::toString(archiveSize, units);
    if (DigitalSize::exceedsByteTreshold(archiveSize, units))
        info += " (" + QString::number(archiveSize) + " B)";

    list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Archive size"), info});

    if (originalSize > 0 && archiveSize > 0)
    {
        const auto ratioMode = GlobalSettingsManager::instance()
                ->getValueAs<CompressionRatioMode>(GlobalSettings::Enum::AC_CompressionRatioMode);
        const auto ratio = computeSizeRatio(originalSize, archiveSize, ratioMode);
        list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Compression ratio"),
                        QString::number(ratio, 'f', 2)});
    }

    bool multipart = false;
    it = archiveProperties.find(BackendArchiveProperty::Multipart);
    if (it != archiveProperties.end())
    {
        multipart = it->second.toBool();
        list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Multipart"), boolToString(multipart)});
    }

    if (multipart)
    {
        it = archiveProperties.find(BackendArchiveProperty::PartCount);
        if (it != archiveProperties.end())
            list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Parts"), it->second});
    }

    list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Files"), contentData.fileCount()});
    list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Folders"), contentData.dirCount()});

    it = archiveProperties.find(BackendArchiveProperty::Methods);
    if (it != archiveProperties.end())
    {
        const auto set = Utils::getValueAs<Utils::QStringUnorderedSet>(it->second);
        QString methods;
        for (const auto& method : set)
            methods.push_back(method + QLatin1Char(' '));
        methods.chop(1);
        list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Archiving methods"), methods});
    }

    it = archiveProperties.find(BackendArchiveProperty::Solid);
    if (it != archiveProperties.end())
    {
        const auto value = Utils::getValueAs<bool>(it->second);
        const QString info = boolToString(value);
        list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Solid"), info});
    }

    it = archiveProperties.find(BackendArchiveProperty::RecoveryData);
    if (it != archiveProperties.end())
    {
        const auto value = Utils::getValueAs<bool>(it->second);
        const QString info = boolToString(value);
        list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Recovery data"), info});
    }

    it = archiveProperties.find(BackendArchiveProperty::EncryptionState);
    if (it != archiveProperties.end())
    {
        const auto encryptionState = Utils::getValueAs<EncryptionState>(it->second);
        list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Encryption"), toString(encryptionState)});
    }

    it = archiveProperties.find(BackendArchiveProperty::Locked);
    if (it != archiveProperties.end())
    {
        const auto value = Utils::getValueAs<bool>(it->second);
        const QString info = boolToString(value);
        list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Locked"), info});
    }

    it = archiveProperties.find(BackendArchiveProperty::Snapshots);
    if (it != archiveProperties.end())
    {
        const auto info = Utils::getValueAs<QString>(it->second);
        if (!info.isEmpty())
            list.push_back({Packuru::Core::Browser::ArchiveBrowserCore::tr("Snapshots"), info});
    }

    return list;
}


bool ArchiveBrowserCore::Private::isTaskBusy() const
{
    if (currentTask)
        return currentTask->isBusy();
    else
        return false;
}


EncryptionState ArchiveBrowserCore::Private::getEncryptionState() const
{
    auto encryptionState = EncryptionState::Disabled;
    auto it = archiveProperties.find(BackendArchiveProperty::EncryptionState);

    if (it != archiveProperties.end())
        encryptionState = Utils::getValueAs<EncryptionState>(it->second);

    Q_ASSERT(encryptionState != EncryptionState::___INVALID);

    return encryptionState;
}


void ArchiveBrowserCore::Private::evaluateAvailableActions()
{
    Utils::BoolFlag<false> changed;
    const bool containsData = contentModel->containsData();

    if (currentTask && currentTask->isBusy())
    {
        changed = {setReloadAvailable(false),
                   setExtractAvailable(false),
                   setAddAvailable(false),
                   setDeleteAvailable(false),
                   setPreviewAvailable(false),
                   setPropertiesAvailable(false),
                   setFilterAvailable(false)};
    }
    else if (currentTask
             && currentTask->isFinished()
             && currentTask->getType() == TaskType::Read
             && lastTasksErrors.find(TaskError::InvalidPasswordOrDataError) != lastTasksErrors.end())
    {
        changed = {setReloadAvailable(true),
                   setExtractAvailable(false),
                   setAddAvailable(false),
                   setDeleteAvailable(false),
                   setPreviewAvailable(false),
                   setPropertiesAvailable(true),
                   setFilterAvailable(false)};
    }
    else if (lastTasksErrors.find(TaskError::DataError) != lastTasksErrors.end())
    {
        changed = {setReloadAvailable(true),
                   setExtractAvailable(containsData),
                   setAddAvailable(false),
                   setDeleteAvailable(false),
                   setPreviewAvailable(containsData && navigatorBackdoorData.hasSelectedFiles()),
                   setPropertiesAvailable(true),
                   setFilterAvailable(containsData)};
    }
    else if (archiveModified || archiveType == ArchiveType::___NOT_SUPPORTED)
    {
        /* Only reloading is allowed after modyfying an archive because encryption status might have changed
           by encrypting files/entire archive when adding files or by deleting last encrypted file in
           a partially encrypted archive which would influence the options available in archiving dialog. */
        changed = {setReloadAvailable(true),
                   setExtractAvailable(false),
                   setAddAvailable(false),
                   setDeleteAvailable(false),
                   setPreviewAvailable(false),
                   setPropertiesAvailable(false),
                   setFilterAvailable(false)};
    }
    else
    {
        const auto tasks = initData_.getSupportedWriteTasks(archiveType, archiveProperties);

        changed = {setReloadAvailable(true),
                   setExtractAvailable(containsData),
                   setPreviewAvailable(containsData && navigatorBackdoorData.hasSelectedFiles()),
                   setAddAvailable(tasks.find(TaskType::Add) != tasks.end()),
                   setDeleteAvailable(containsData
                   && tasks.find(TaskType::Delete) != tasks.end()
                   && navigator->getSelectionModel()->hasSelection()),
                   setPropertiesAvailable(true),
                   setFilterAvailable(containsData)};
    }

    if (changed)
        emit publ->availableActionsChanged();
}


bool ArchiveBrowserCore::Private::setReloadAvailable(bool value)
{
    if (value == reloadAvailable)
        return  false;

    reloadAvailable = value;
    emit publ->reloadAvailableChanged(value);
    return true;
}


bool ArchiveBrowserCore::Private::setAddAvailable(bool value)
{
    if (value == addAvailable)
        return false;

    addAvailable = value;
    emit publ->addAvailableChanged(value);
    return true;
}


bool ArchiveBrowserCore::Private::setDeleteAvailable(bool value)
{
    if (value == deleteAvailable)
        return false;

    deleteAvailable = value;
    emit publ->deleteAvailableChanged(value);
    return true;
}

bool ArchiveBrowserCore::Private::setPreviewAvailable(bool value)
{
    if (value == previewAvailable)
        return false;

    previewAvailable = value;
    emit publ->previewAvailableChanged(value);
    return true;
}


bool ArchiveBrowserCore::Private::setExtractAvailable(bool value)
{
    if (value == extractAvailable)
        return false;

    extractAvailable = value;
    emit publ->extractAvailableChanged(value);
    return true;
}


bool ArchiveBrowserCore::Private::setPropertiesAvailable(bool value)
{
    if (value == propertiesAvailabe)
        return false;


    propertiesAvailabe = value;
    emit publ->propertiesAvailableChanged(value);
    return true;
}


bool ArchiveBrowserCore::Private::setFilterAvailable(bool value)
{
    if (value == filterAvailable)
        return false;


    filterAvailable = value;
    emit publ->filterAvailableChanged(value);
    return true;
}


bool ArchiveBrowserCore::Private::isReloadAvailable() const
{
    return reloadAvailable;
}


bool ArchiveBrowserCore::Private::isAddAvailable() const
{
    return addAvailable;
}


bool ArchiveBrowserCore::Private::isDeleteAvailable() const
{
    return deleteAvailable;
}


bool ArchiveBrowserCore::Private::isPreviewAvailable() const
{
    return previewAvailable;
}


bool ArchiveBrowserCore::Private::isExtractAvailable() const
{
    return extractAvailable;
}


bool ArchiveBrowserCore::Private::archivePropertiesAvailable() const
{
    return propertiesAvailabe;
}


bool ArchiveBrowserCore::Private::isFilterAvailable() const
{
    return filterAvailable;
}


template<typename TaskData>
void ArchiveBrowserCore::Private::onArchiveDialogAccepted(const ArchiveDialogAcceptedCallback<TaskData>& dialogAccepted,
                                                          const TaskData& data)
{
    const ArchiveDialogAcceptedResult result = dialogAccepted(data);

    if (!result.messageProcessed)
        return;

    Q_ASSERT(result.isValid());

    if (result.willRunInQueue)
    {
        if (currentTask && !result.updatedPassword.isEmpty())
            currentTask->setPassword(result.updatedPassword);
    }
    else
        setTask(result.task);
}


QString ArchiveBrowserCore::Private::boolToString(bool value) const
{
    return value ? Packuru::Core::Browser::ArchiveBrowserCore::tr("Yes")
                 : Packuru::Core::Browser::ArchiveBrowserCore::tr("No");
}


ArchiveBrowserCore::BrowserTaskState ArchiveBrowserCore::Private::translate(TaskState state)
{
    switch (state)
    {
    case TaskState::Aborted:
        return ArchiveBrowserCore::BrowserTaskState::Aborted;
    case TaskState::Errors:
        return ArchiveBrowserCore::BrowserTaskState::Errors;
    case TaskState::ReadyToRun:
        return ArchiveBrowserCore::BrowserTaskState::ReadyToRun;
    case TaskState::StartingUp:
        return ArchiveBrowserCore::BrowserTaskState::StartingUp;
    case TaskState::Paused:
        return ArchiveBrowserCore::BrowserTaskState::Paused;
    case TaskState::Warnings:
        return ArchiveBrowserCore::BrowserTaskState::Warnings;
    case TaskState::InProgress:
        return ArchiveBrowserCore::BrowserTaskState::InProgress;
    case TaskState::Success:
        return ArchiveBrowserCore::BrowserTaskState::Success;
    case TaskState::WaitingForDecision:
        return ArchiveBrowserCore::BrowserTaskState::WaitingForInput;
    case TaskState::WaitingForPassword:
        return ArchiveBrowserCore::BrowserTaskState::WaitingForPassword;
    default:
        Q_ASSERT(false); return ArchiveBrowserCore::BrowserTaskState::___INVALID;
    }
}

}
