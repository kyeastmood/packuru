// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <vector>

#include <QObject>
#include <QModelIndexList>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{
class ArchivingDialogCore;
class ExtractionDialogCore;
class TestDialogCore;
}


namespace Packuru::Core::Browser
{

struct ArchiveBrowserCoreInitData;
class ArchiveNavigator;
class StatusPageCore;
class DeletionDialogCore;

/**
 * @brief Allows browsing, quering information and manipulation of opened archive.
 *
 * %ArchiveBrowserCore works in two UI states, Navigation and Status Page, described by UIState.
 *
 * A number of actions can be performed on the archive:
 * * @link reloadArchive() Reload@endlink.
 * * @link createArchivingDialogCore() Add files@endlink.
 * * @link createDeletionDialogCore() Delete selected files@endlink.
 * * @link createExtractionDialogCore() Extract (selected) files@endlink.
 * * @link createTestDialogCore() Test archive@endlink.
 * * @link previewSelectedFiles() Preview selected files@endlink.
 * * @link getPropertiesModel() Get archive properties@endlink.
 *
 * @warning Check action availability before invoking it.
 *
 * Objects of this class are emitted from MainWindowCore object for each opened archive.
 *
 * @headerfile "core-browser/archivebrowsercore.h"
 */
class PACKURU_CORE_BROWSER_EXPORT ArchiveBrowserCore : public QObject
{
    Q_OBJECT
    /* When two enum classes have the same members and are registered unscoped
       there will be a name clash in QML.
       https://bugreports.qt.io/browse/QTBUG-73394
    */
    Q_CLASSINFO("RegisterEnumClassesUnscoped", "false")

    /** @brief Holds browser title. Title can contain progress information during task execution.
     * @sa getTitle(), titleChanged()
     */
    Q_PROPERTY(QString title READ getTitle NOTIFY titleChanged)

    /** @brief Holds archive name.
     * @sa getArchiveName()
     */
    Q_PROPERTY(QString archiveName READ getArchiveName CONSTANT)

    /** @brief Holds ArchiveNavigator pointer.
     * @sa getNavigator()
     */
    Q_PROPERTY(Packuru::Core::Browser::ArchiveNavigator* navigator READ getNavigator CONSTANT)

    /** @brief Holds StatusPageCore pointer.
     * @sa getStatusPageCore()
     */
    Q_PROPERTY(Packuru::Core::Browser::StatusPageCore* statusPageCore READ getStatusPageCore CONSTANT)

    /** @brief Holds whether there is a task currently running.
     * @sa isTaskBusy(),taskBusyChanged()
     */
    Q_PROPERTY(bool taskBusy READ isTaskBusy NOTIFY taskBusyChanged)

    /** @brief Holds current UIState.
     * @sa getUIState(), uiStateChanged()
     */
    Q_PROPERTY(UIState uiState READ getUIState NOTIFY uiStateChanged)

    /** @brief Holds current task state.
     * @sa BrowserTaskState, getTaskState(), taskStateChanged()
     */
    Q_PROPERTY(BrowserTaskState taskState READ getTaskState NOTIFY taskStateChanged)

    /** @brief Holds whether Add action is available.
     * @sa isAddAvailable(), addAvailableChanged(), createArchivingDialogCore()
     */
    Q_PROPERTY(bool addAvailable READ isAddAvailable NOTIFY addAvailableChanged)

    /** @brief Holds whether Delete action is available.
     * @sa isDeleteAvailable(), deleteAvailableChanged(), createDeletionDialogCore()
     */
    Q_PROPERTY(bool deleteAvailable READ isDeleteAvailable NOTIFY deleteAvailableChanged)

    /** @brief Holds whether Extract action is available.
     * @sa isExtractAvailable(), extractAvailableChanged(), createExtractionDialogCore(),
     * createTestDialogCore()
     */
    Q_PROPERTY(bool extractAvailable READ isExtractAvailable NOTIFY extractAvailableChanged)

    /** @brief Holds whether Filter action is available.
     * @sa isFilterAvailable(), filterAvailableChanged()
     */
    Q_PROPERTY(bool filterAvailable READ isFilterAvailable NOTIFY filterAvailableChanged)

    /** @brief Holds whether Preview action is available.
     * @sa isPreviewAvailable(), previewAvailableChanged()
     */
    Q_PROPERTY(bool previewAvailable READ isPreviewAvailable NOTIFY previewAvailableChanged)

    /** @brief Holds whether Properties action is available.
     * @sa archivePropertiesAvailable(), propertiesAvailableChanged()
     */
    Q_PROPERTY(bool propertiesAvailable READ archivePropertiesAvailable NOTIFY propertiesAvailableChanged)

    /** @brief Holds whether Reload action is available.
     * @sa isReloadAvailable(), reloadAvailableChanged()
     */
    Q_PROPERTY(bool reloadAvailable READ isReloadAvailable NOTIFY reloadAvailableChanged)

public:
    /** @brief Describes the state of current or recently finished task.
     *
     * It plays a supportive role only and can be used to e.g. set browser's tab icon.
     * @sa getTaskState(), taskStateChanged()
     */
    enum class BrowserTaskState
    {
        ReadyToRun, ///< @brief Ready to run.

        StartingUp, ///< [Busy] Starting up.
        InProgress, ///< [Busy] In progress.
        WaitingForInput, ///< [Busy] Waiting for input (e.g. overwrite prompts).
        WaitingForPassword, ///< [Busy] Waiting for password.
        Paused, ///< [Busy] Paused (currently not used).

        Aborted, ///< [Finished] Aborted.
        Success, ///< [Finished] Success.
        Warnings, ///< [Finished] Warnings.
        Errors, ///< [Finished] Errors.

        ___INVALID
    };
    Q_ENUM(BrowserTaskState)

    /** @brief Describes current UI state.
     *
     * The browser's UI can be in 2 states:
     * * Navigation: related functionality is provided by ArchiveNavigator returned by getNavigator().
     * * Status Page: related functionality is provided by StatusPageCore returned by getStatusPageCore().
     *
     * @sa uiStateChanged(), getUIState(), uiState
     */
    enum class UIState
    {
        Navigation, ///< Navigation/browsing state.
        StatusPage ///< Status page state.
    };
    Q_ENUM(UIState)

    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        ActionAdd, ///< 'Add' action.
        ActionDelete, ///< 'Delete' action.
        ActionExtract, ///< 'Extract' action.
        ActionFilter, ///< 'Filter' action.
        ActionPreview, ///< 'Preview' action.
        ActionProperties, ///< 'Properties' action.
        ActionReload, ///< 'Reload' action.
        ActionTest ///< 'Test' action.
    };
    Q_ENUM(UserString)

    ArchiveBrowserCore(ArchiveBrowserCoreInitData& initData, QObject *parent = nullptr);
    ~ArchiveBrowserCore();

    /// @copydoc ArchivingDialogCore::destroyLater
    Q_INVOKABLE void destroyLater() { deleteLater(); } // For QML

    /// @brief Returns browser title.
    Q_INVOKABLE QString getTitle() const;

    /// @brief Returns archive name.
    Q_INVOKABLE QString getArchiveName() const;

    /**
     * @brief Returns ArchiveNavigator pointer.
     *
     * ArchiveNavigator handles folder navigation, single file opening,
     * selection and filtering of items.
     * @note Object ownership is not transferred to the caller.
     */
    Q_INVOKABLE Packuru::Core::Browser::ArchiveNavigator* getNavigator();

    /**
     * @brief Returns StatusPageCore pointer.
     *
     * StatusPageCore contains data and logic necessary for updating Status Page.
     * @note Object ownership is not transferred to the caller.
     */
    Q_INVOKABLE Packuru::Core::Browser::StatusPageCore* getStatusPageCore();

    /// @brief Returns current UIState.
    Q_INVOKABLE UIState getUIState() const;

    /// @brief Returns current task state.
    Q_INVOKABLE BrowserTaskState getTaskState() const;

    /**
     * @brief Returns item model with archive properties.
     * @note Object ownership is not transferred to the caller.
     */
    Q_INVOKABLE QAbstractTableModel* getPropertiesModel();

    /// @brief Returns archive comment.
    Q_INVOKABLE QString getArchiveComment() const;

    /** @brief Returns whether the browser is currently busy.
     *
     * It is useful to check whether there is a running task before closing the browser's tab.
     * @sa taskBusyChanged(), TabClosingDialogCore
     */
    Q_INVOKABLE bool isTaskBusy() const;

    /// @brief Returns whether recent task has finished.
    Q_INVOKABLE bool isTaskFinished() const;

    ///@{
    /// @name Action state accessors.

    /** @brief Returns whether Add action is currently available.
     * @sa addAvailableChanged(), addAvailable, createArchivingDialogCore()
     */
    Q_INVOKABLE bool isAddAvailable() const;

    /** @brief Returns whether Delete action is currently available.
     * @sa deleteAvailableChanged(), deleteAvailable, createDeletionDialogCore()
     */
    Q_INVOKABLE bool isDeleteAvailable() const;

    /** @brief Returns whether Extract action is currently available.
     * @sa extractAvailableChanged(), extractAvailable, createExtractionDialogCore(),
     * createTestDialogCore()
     */
    Q_INVOKABLE bool isExtractAvailable() const;

    /** @brief Returns whether Filter action is currently available.
     * @sa filterAvailableChanged(), filterAvailable
     */
    Q_INVOKABLE bool isFilterAvailable() const;

    /** @brief Returns whether Preview action is currently available.
     * @sa previewAvailableChanged(), previewAvailable
     */
    Q_INVOKABLE bool isPreviewAvailable() const;

    /** @brief Returns whether Reload action is currently available.
     * @sa reloadAvailableChanged(), reloadAvailable
     */
    Q_INVOKABLE bool isReloadAvailable() const;

    /** @brief Returns whether Properties action is currently available.
     * @sa propertiesAvailableChanged(), propertiesAvailable
     */
    Q_INVOKABLE bool archivePropertiesAvailable() const;

    /** @brief Reloads archive.
     * @warning Reload action action must be available or the browser must not be busy.
     * @sa isReloadAvailable(), isTaskBusy()
     */

    ///@}

    Q_INVOKABLE void reloadArchive();

    /** @brief Extracts selected files to the temporary folder.
     *
     * After successful extraction filesReadyForPreview() signal is emitted.
     * @warning Preview action must be available.
     * @sa isPreviewAvailable()
     */
    Q_INVOKABLE void previewSelectedFiles();

    /** @brief Creates ArchivingDialogCore and returns pointer.
     * @note Object ownership is transferred to the caller.
     * @warning Add action must be available.
     * @sa isAddAvailable()
     */
    Q_INVOKABLE Packuru::Core::ArchivingDialogCore* createArchivingDialogCore();

    /** @brief Creates DeletionDialogCore and returns pointer.
     * @note Object ownership is transferred to the caller.
     * @warning Delete action must be available.
     * @sa isDeleteAvailable()
     */
    Q_INVOKABLE Packuru::Core::Browser::DeletionDialogCore* createDeletionDialogCore();

    /** @brief Creates ExtractionDialogCore and returns pointer.
     * @note Object ownership is transferred to the caller.
     * @warning Extract action must be available.
     * @sa isExtractAvailable()
     */
    Q_INVOKABLE Packuru::Core::ExtractionDialogCore* createExtractionDialogCore();

    /** @brief Creates TestDialogCore and returns pointer.
     * @note Object ownership is transferred to the caller.
     * @warning Extract action must be available.
     * @sa isExtractAvailable()
     */
    Q_INVOKABLE Packuru::Core::TestDialogCore* createTestDialogCore();

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE static QString getString(UserString value);

signals:
    /** @brief Signals browser title change.
     * @sa getTitle(), title
     */
    void titleChanged(const QString& title);

    /** @brief Signals wheter the object is busy or not.
     * @sa isTaskBusy(), taskBusy
     */
    void taskBusyChanged(bool value);

    /** @brief Emitted just before the archive is reloaded
     *
     * Supportive role, not strictly required.
     */
    void browserReset();

    /** @brief Signals UI state change.
     * @sa getUIState(), uiState
     */
    void uiStateChanged(UIState state);

    /**
     * @brief Signals task state change.
     * @sa BrowserTaskState
     */
    void taskStateChanged(BrowserTaskState state);

    ///@{
    /// @name Action availability changes.

    /// @sa isAddAvailable(), addAvailable, createArchivingDialogCore()
    void addAvailableChanged(bool available);

    /// @sa isDeleteAvailable(), deleteAvailable, createDeletionDialogCore()
    void deleteAvailableChanged(bool available);

    /// @sa isExtractAvailable(), extractAvailable, createExtractionDialogCore(),
    /// createTestDialogCore()
    void extractAvailableChanged(bool available);

    /// @sa isFilterAvailable(), filterAvailalable
    void filterAvailableChanged(bool available);

    /// @sa isPreviewAvailable(), previewAvailable, previewSelectedFiles()
    void previewAvailableChanged(bool available);

    /// @sa archivePropertiesAvailable, propertiesAvailable, getPropertiesModel()
    void propertiesAvailableChanged(bool available);

    /// @sa isReloadAvailable(), reloadAvailable, reloadArchive()
    void reloadAvailableChanged(bool available);

    /// @sa Signals any actions' availibility changes.
    void availableActionsChanged();
    ///@}

    /// @brief Emitted just before the archive is reloaded.
    void needsActivation() const;

    /**
     * @brief Emitted when the selected files have been extracted and are ready for preview.
     * @sa previewSelectedFiles()
     */
    void filesReadyForPreview(const std::vector<QUrl>& urls);

    /// @brief Emitted when the archive has been extracted.
    void openExtractionDestinationFolder(const QUrl& url);

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
