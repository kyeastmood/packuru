// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>
#include <QUrl>

#include "symbol_export.h"

/// @file

class QFileInfo;


namespace Packuru::Core
{

class AppSettingsDialogCore;


namespace Browser
{

class ArchiveBrowserCore;
class SettingsDialogCore;
class QueueMessenger;

namespace MainWindowCoreAux
{
struct Init;
struct Backdoor;
}

/**
 * @brief Contains core data and logic for %Browser main window.
 *
 * %MainWindowCore object is provided by Init object.
 *
 * @headerfile "core-browser/mainwindowcore.h"
 */
class PACKURU_CORE_BROWSER_EXPORT MainWindowCore : public QObject
{
    Q_OBJECT
public:
    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        ActionAbout, ///< 'About' action.
        ActionClose, ///< 'Close' action.
        ActionNew, ///< 'New' action.
        ActionOpen, ///< 'Open' action.
        ActionQuit, ///< 'Quit' action.
        ActionSettings, ///< 'Settings' action.
        WindowTitle ///< Window title.
    };
    Q_ENUM(UserString)

    explicit MainWindowCore(MainWindowCoreAux::Init& init,
                            MainWindowCoreAux::Backdoor& backdoor,
                            QObject *parent = nullptr);
    ~MainWindowCore() override;

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE QString getString(UserString value);

    /**
     * @brief Requests Queue to open Archiving Dialog.
     *
     * If Queue is not running it will be started up.
     *
     * The messaging procedure can be managed by QueueMessenger provided by getQueueMessenger().
     */
    Q_INVOKABLE void createArchive();

    /**
     * @brief Opens archives.
     *
     * For each archive newBrowserCoreCreated() signal will be emitted which provides
     * ArchiveBrowserCore object.
     */
    void openArchives(const QStringList& absoluteFilePaths);

    /// @copydoc openArchives(const QStringList&)
    Q_INVOKABLE void openArchives(const QList<QUrl>& urls);

    /// @copydoc openArchives(const QStringList&)
    void openArchives(const QList<QFileInfo>& files);

    /**
     * @brief Returns busy browsers count.
     *
     * This information is useful when trying to close main window. If there are any running
     * tasks when %MainWindowCore is being destroyed, the tasks will be aborted.
     */
    Q_INVOKABLE int getBusyBrowserCount() const;

    /**
     * @brief Returns AppSettingsDialogCore object.
     *
     * This class allows to adjust core application settings.
     * @note The object is initially parented to %MainWindowCore object,
     * however the ownership is transferred to the caller and the object must be manually
     * destroyed when it is no longer neeeded.
     */
    Q_INVOKABLE Packuru::Core::AppSettingsDialogCore* createAppSettingsDialogCore();

    /**
     * @brief Returns Packuru::Core::Browser::SettingsDialogCore object.
     *
     * This class allows to adjust core Browser settings.
     * @note The object is initially parented to %MainWindowCore object,
     * however the ownership is transferred to the caller and the object must be manually
     * destroyed when it is no longer neeeded.
     */
    Q_INVOKABLE Packuru::Core::Browser::SettingsDialogCore* createBrowserSettingsDialogCore();

    /**
     * @brief Returns QueueMessenger object.
     * @note Object ownership is not transferred to the caller.
     */
    Q_INVOKABLE Packuru::Core::Browser::QueueMessenger* getQueueMessenger();

signals:
    /**
     * @brief Provides ArchiveBrowserCore object for each opened archive.
     *
     * @note The object is initially parented to %MainWindowCore object,
     * however the ownership is transferred to the caller and the object must be manually
     * destroyed when it is no longer neeeded (any running task will be aborted).
     */
    void newBrowserCoreCreated(Packuru::Core::Browser::ArchiveBrowserCore* browserCore);

    /** @brief Emitted whenever a new message is received from another instance.
     *
     * For example, when single instance mode is set, merely starting up another instance
     * will cause this signal to be emitted.
     *
     * This information can be used to raise application main window.
     */
    void newMessage();

    /** @brief Emitted when the application has been invoked with invalid parameters.
     *
     * @note If single instance mode is set the signal may concern another app instance.
     */
    void commandLineError(const QString& info) const;

    /**
     * @brief Emitted when help has been requested from command-line.
     *
     * @note If single instance mode is set the signal may concern another app instance.
     */
    void commandLineHelpRequested(const QString& info) const;

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}

}
