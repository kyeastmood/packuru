// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../qcs-core/controllerengine.h"

#include "../core/globalsettingsmanager.h"
#include "../core/private/globalsettings.h"
#include "../core/private/dialogcore/genericcheckboxctrl.h"

#include "settingsdialogcore.h"
#include "settingscontrollertype.h"


using namespace qcs::core;
using namespace Packuru::Core;
using Packuru::Core::GlobalSettings::Enum;


namespace Packuru::Core::Browser
{

using Type = SettingsControllerType::Type;

struct SettingsDialogCore::Private
{
    ControllerEngine* engine = nullptr;
};


SettingsDialogCore::SettingsDialogCore(QObject *parent)
    : QObject (parent),
      priv(new Private)
{
    priv->engine = new ControllerEngine(this);

    const auto forceSingleInstance = GlobalSettingsManager::instance()
            ->getValueAs<bool>(Enum::BC_ForceSingleInstance);
    QString label = Packuru::Core::Browser::SettingsDialogCore::tr("Force single instance");
    priv->engine->createTopController<GenericCheckBoxCtrl>(Type::ForceSingleInstance,
                                                           label,
                                                           forceSingleInstance);

    const auto showSummary = GlobalSettingsManager::instance()
            ->getValueAs<bool>(Enum::BC_ShowSummaryOnTaskCompletion);
    label = Packuru::Core::Browser::SettingsDialogCore::tr("Show summary on task completion");
    priv->engine->createTopController<GenericCheckBoxCtrl>(Type::ShowSummaryOnTaskCompletion,
                                                           label,
                                                           showSummary);

    priv->engine->updateAllControllers();
}


SettingsDialogCore::~SettingsDialogCore()
{
}


void SettingsDialogCore::destroyLater()
{
    deleteLater();
}


QString SettingsDialogCore::getString(UserString value)
{
    switch (value)
    {
    case UserString::PageTitle:
        return Packuru::Core::Browser::SettingsDialogCore::tr("Browser");

    default:
        Q_ASSERT(false); return "";
    }
}


ControllerEngine* SettingsDialogCore::getControllerEngine()
{
    return priv->engine;
}


void SettingsDialogCore::accept()
{
    const auto forceSingleInstance
            = priv->engine->getControllerPropertyAs<bool>(Type::ForceSingleInstance,
                                                          ControllerProperty::PublicCurrentValue);
    GlobalSettingsManager::instance()->setValue(Enum::BC_ForceSingleInstance,
                                                forceSingleInstance);

    const auto showSummary
            = priv->engine->getControllerPropertyAs<bool>(Type::ShowSummaryOnTaskCompletion,
                                                          ControllerProperty::PublicCurrentValue);
    GlobalSettingsManager::instance()->setValue(Enum::BC_ShowSummaryOnTaskCompletion,
                                                showSummary);
}

}
