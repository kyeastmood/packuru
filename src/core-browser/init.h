// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>

#include "symbol_export.h"

/// @file


namespace Packuru::Core::Browser
{

class MainWindowCore;

/**
 * @brief Initializes %Browser.
 *
 * This is the the top-level class in %Browser. It should be instantiated very early,
 * ideally just after QApplication.
 *
 * The class loads translations, instantiates GlobalSettingsManager, messages other instances
 * at start up, creates MainWindowCore and processes command-line arguments.
 *
 * @par Example
 *
 * @code
 * using Packuru::Core::AppInfo;
 * using Packuru::Core::Browser::Init;
 *
 * int main(int argc, char *argv[])
 * {
 *     QApplication app(argc, argv);
 *     app.setApplicationVersion(AppInfo::getAppVersion());
 *     app.setOrganizationName(AppInfo::getAppName());
 *     app.setApplicationName(AppInfo::getAppName() + " Desktop");
 *
 *     Init coreInit(QUEUE_EXEC_NAME, QUEUE_EXEC_NAME);
 *
 *     GlobalSettingsManager::instance()
 *         ->addCustomSettingsConverter(std::make_unique<DesktopBrowserSettingsConverter>());
 *
 *     if (coreInit.messagePrimaryInstance())
 *         return 0;
 *
 *     BrowserMainWindow window(coreInit.getMainWindowCore());
 *     window.show();
 *
 *     coreInit.processArguments();
 *
 *     return app.exec();
 * }
 * @endcode
 *
 * @headerfile "core-browser/init.h"
 * @sa Packuru::Core::Queue::Init
 */
class PACKURU_CORE_BROWSER_EXPORT Init : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Constructs %Init object.
     * @param queueExecName Specifies %Queue executable name.
     * %Queue can be started when the user requests running a task in %Queue.
     * The name is frontend-specific.
     * @param queueServerName Specifies %Queue server name.
     * %Queue can be messaged when the user requests running a task in %Queue.
     * The name is frontend-specific.
     */
    explicit Init(const QString& queueExecName,
                  const QString& queueServerName);
    ~Init() override;

    /**
     * @brief Attempts to send command-line arguments to primary instance.
     *
     * If single instance mode is set and this is a secondary application instance,
     * the function attempts to contact primary instance and regardless of the result returns true
     * (in this case this instance can be finished). Otherwise it returns false.
     */
    bool messagePrimaryInstance() const;

    /**
     * @brief Returns pointer to MainWindowCore.
     *
     * The object is created on first invocation.
     * @note The object ownership is not transferred to the caller.
     */
    MainWindowCore* getMainWindowCore();

    /**
     * @brief Processes command-line arguments of this instance.
     * @note This function does nothing if MainWindowCore has not been created by calling
     * getMainWindowCore().
     */
    void processArguments();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
