// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>

#include "symbol_export.h"

/// @file

class QAbstractItemModel;


namespace qcs::core
{
class ControllerEngine;
}


namespace Packuru::Core::Browser
{

struct DeletionDialogCoreInitData;

/**
 @brief Contains core data and logic necessary for Deletion Dialog.

 Object of this class is provided by ArchiveBrowserCore object.

 @headerfile "core-browser/deletiondialogcore.h"
 */
class PACKURU_CORE_BROWSER_EXPORT DeletionDialogCore : public QObject
{
    Q_OBJECT
public:
    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        Cancel, ///< 'Cancel' button.
        DialogQuestion, ///< 'Do you want do delete...?' question.
        DialogTitle, ///< Dialog title.
    };
    Q_ENUM(UserString)

    explicit DeletionDialogCore(DeletionDialogCoreInitData initData,
                                QObject *parent = nullptr);
    ~DeletionDialogCore();

    /// @copydoc ArchivingDialogCore::destroyLater
    Q_INVOKABLE void destroyLater();

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE static QString getString(UserString value, int count = 0);

    /**
     @brief Returns pointer to ControllerEngine controlling most input widgets in the dialog.

     The widgets must be mapped in WidgetMapper using DeletionDialogControllerType::Type enum as the ID.

     @note Object ownership is not transferred to the caller.
     @sa qcs::qqc::WidgetMapper, qcs::qtw::WidgetMapper
     */
    Q_INVOKABLE qcs::core::ControllerEngine* getControllerEngine();

    /** @brief Returns the model containing files to be deleted.
     @note Object ownership is not transferred to the caller.
     */
    Q_INVOKABLE QAbstractItemModel* getFileModel();

    /**
     @brief Accepts the dialog.

     After the dialog has been accepted the archive is processed internally by the application.
     @note The object must be destroyed manually after it has been accepted.
     */
    Q_INVOKABLE void accept();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
