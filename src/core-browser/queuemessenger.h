// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <functional>

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core::Browser
{

namespace QueueMessengerAux
{
struct Init;
struct Backdoor;
}

/**
 * @brief Sends messages to %Queue.
 *
 * This class allows to control (e.g. abort) the process of messaging %Queue.
 * Example messages include requesting to process archive tasks, requesting to open Archiving Dialog.
 *
 * When %QueueMessenger starts to work it emits activated() signal. When the work is fininshed deactivated()
 * signal is emitted. Following these signals allows to show/hide the Messaging dialog when necessary.
 *
 * In case of communication failure an archive task can be run in %Browser.
 *
 * There is only one %QueueMessenger object for each %Browser instance. It is provided by
 * Packuru::Core::Browser::MainWindowCore object.
 * @headerfile "core-browser/queuemessenger.h"
 */
class PACKURU_CORE_BROWSER_EXPORT QueueMessenger : public QObject
{
    Q_OBJECT

    /** @brief Holds whether %QueueMessenger is currently active.
     * @sa isActive(), activeChanged(), activated(), deactivated()
     */
    Q_PROPERTY(bool active READ isActive NOTIFY activeChanged)

    /** @brief Holds whether QueueMessenger can run a task in %Browser.
     *
     * Only archive task can be run in %Browser. If it's been attempted to request
     * Archiving Dialog from Queue this property holds false. In that case the 'Run in %Browser'
     * button should be hidden.
     * @sa getCanRunInBrowser(), canRunInBrowserChanged()
     */
    Q_PROPERTY(bool canRunInBrowser READ getCanRunInBrowser NOTIFY canRunInBrowserChanged)

    /** @brief Holds current status.
     *
     * Status describes what %QueueMessenger is currently doing.
     * @sa getStatus(), statusChanged()
     */
    Q_PROPERTY(QString status READ getStatus NOTIFY statusChanged)
public:
    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        Abort, ///< 'Abort' button.
        DialogTitle, ///< Dialog title.
        RunInBrowser ///< 'Run in %Browser' button.
    };
    Q_ENUM(UserString)

    explicit QueueMessenger(QueueMessengerAux::Init& init,
                            QueueMessengerAux::Backdoor&backdoor,
                            QObject *parent = nullptr);
    ~QueueMessenger();

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE static QString getString(UserString value);

    /**
     * @brief Returns whether %QueueMessenger is currently active.
     * @sa active, activeChanged(), activated(), deactivated()
     */
    Q_INVOKABLE bool isActive() const;

    /**
     * @brief Returns current status.
     * @sa status, statusChanged()
     */
    Q_INVOKABLE QString getStatus() const;

    /**
     * @brief Returns whether QueueMessenger can run a task in %Browser.
     * @return canRunInBrowser, canRunInBrowserChanged()
     */
    Q_INVOKABLE bool getCanRunInBrowser() const;

    /**
     * @brief Stops %QueueMessenger.
     */
    Q_INVOKABLE void abort();

    /**
     * @brief Runs the task in %Browser instead of sending it to Queue.
     * @note This is only works for archive tasks. If an Archiving Dialog has been
     * requested by calling Packuru::Core::Browser::MainWindowCore::createArchive()
     * it is only possible to abort the request.
     * @sa canRunInBrowser, getCanRunInBrowser(), canRunInBrowserChanged()
     */
    Q_INVOKABLE void runInBrowser();

signals:
    /**
     * @brief Signals that %QueueMessenger has been activated/deactivated.
     * @sa active, isActive(), activated(), deactivated()
     */
    void activeChanged(bool active);

    /**
     * @brief Signals status changes.
     * @sa status, getStatus()
     */
    void statusChanged(const QString& info);

    /**
     * @brief Signals that %QueueMessenger has been activated.
     * @sa active, isActive(), activeChanged(), deactivated()
     */
    void activated();

    /**
     * @brief Signals that %QueueMessenger has been deactivated.
     * @sa active, isActive(), activeChanged(), activated()
     */
    void deactivated();

    /**
     * @brief Signals if running a task in %Browser is possible.
     * @note This signal is only needed if Queue Messaging dialog is reused for each request. In that
     * case it allows to hide the 'Run in Browser' button. Otherwise it is not required.
     */
    void canRunInBrowserChanged(bool value);

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
