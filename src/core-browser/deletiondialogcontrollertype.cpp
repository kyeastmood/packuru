// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../utils/private/enumhash.h"

#include "deletiondialogcontrollertype.h"


namespace Packuru::Core::Browser
{

uint qHash(DeletionDialogControllerType::Type value, uint seed)
{
    return Utils::enumHash(value, seed);
}

}
