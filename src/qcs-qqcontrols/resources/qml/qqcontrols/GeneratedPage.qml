// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.4

import QCS_ControllerType 1.0
import ControllerSystem.QQC 1.0 as CS_QQC



/**
 @copydoc qcs::qtw::GeneratedPage
 @ingroup ControllerSystemQML
 */

FocusScope {
    id: root
    implicitHeight: topFlickable.implicitHeight
    implicitWidth: topFlickable.implicitWidth
    property QtObject rowMapper

    Flickable {
        id: topFlickable
        contentHeight: gridLayout.implicitHeight
        contentWidth: gridLayout.implicitWidth
        implicitHeight: contentHeight
        implicitWidth:  contentWidth
        anchors.fill: parent
        boundsBehavior: Flickable.StopAtBounds
        contentItem.anchors.left: contentItem.parent.left
        contentItem.anchors.right: contentItem.parent.right

        GridLayout {
            anchors.fill: parent
            id: gridLayout
            columns: 2
        }
    }

    Component { id: keyLabelComp; Label { Layout.alignment: Qt.AlignRight } }
    Component { id: checkBoxComp; CheckBox {} }
    Component {
        id: comboBoxComp;
        ComboBox {
            Layout.fillWidth: true;
            implicitWidth: contentItem.contentWidth + contentItem.leftPadding + contentItem.rightPadding
                           + indicator.implicitWidth
        }
    }
    Component { id: mappedLabelComp; Label { } }
    Component { id: sliderComp; Slider { Layout.fillWidth: true; stepSize: 1 } }
    Component { id: spinBoxComp; SpinBox {} }
    Component { id: textFieldComp; TextField {} }
    Component { id: placeholderComp; Item { Layout.preferredHeight: 1} }

    onRowMapperChanged: {
        pageCore.rowMapper = rowMapper

        for (var mapperRow = 0; mapperRow < rowMapper.size(); ++mapperRow)
        {
            var isHeader = rowMapper.isHeader(mapperRow)

            if (isHeader) {
                var label = keyLabelComp.createObject(root)
                label.font.bold = true
                label.text = rowMapper.getLabel(mapperRow)

                var item = placeholderComp.createObject(root)

                gridLayout.children.push(label)
                gridLayout.children.push(item)
            }

            else {
                var label = keyLabelComp.createObject(root)
                var type = rowMapper.getControllerType(mapperRow)

                var item

                switch (type) {
                case QCS_ControllerType.CheckBox:
                    item = checkBoxComp.createObject(root); break
                case QCS_ControllerType.ComboBox:
                    item = comboBoxComp.createObject(root); break
                case QCS_ControllerType.Label:
                    item = mappedLabelComp.createObject(root); break
                case QCS_ControllerType.LineEdit:
                    item = textFieldComp.createObject(root); break
                case QCS_ControllerType.Slider:
                    item = sliderComp.createObject(root); break
                case QCS_ControllerType.IntSpinBox:
                    item = spinBoxComp.createObject(root); break
                case QCS_ControllerType.DoubleSpinBox:
                    item = spinBoxComp.createObject(root); break
                }

                if (item)
                    pageCore.addItem(mapperRow, label, item)
                else
                    item = placeholderComp.createObject(root)

                gridLayout.children.push(label)
                gridLayout.children.push(item)
            }
        }
    }

    CS_QQC.GeneratedPageCore {
        id: pageCore
    }
}
