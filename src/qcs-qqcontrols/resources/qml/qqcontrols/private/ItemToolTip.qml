// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.14
import QtQuick.Controls 2.4


ToolTip {
    id: root
    timeout: 3000

    enum Mode {
        Hover,
        Click,
        Invalid
    }

    delay: mode === ItemToolTip.Mode.Click ? 0 : 1000

    property int mode: {

        if (!parent)
            return ItemToolTip.Mode.Invalid

        else if (typeof parent.qcsToolTipVisible === "boolean")
            return ItemToolTip.Mode.Click

        else if (typeof parent.hovered === "boolean")
            return ItemToolTip.Mode.Hover

        return false
    }

    visible:  {
        if (mode === ItemToolTip.Mode.Invalid)
            return false

        else if (mode === ItemToolTip.Mode.Click)
            return parent.qcsToolTipVisible

        else if (mode === ItemToolTip.Mode.Hover)
            return parent.hovered

        return false
    }
}
