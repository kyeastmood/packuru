// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <unordered_set>

#include <QObject>

#include "qcs-core/controllerpropertymap.h"


namespace qcs::qqc
{

class WidgetHandler : public QObject
{
    Q_OBJECT
public:
    using ItemSet = std::unordered_set<QObject*>;

    struct UpdateData
    {
        UpdateData(const qcs::core::ControllerPropertyMap& properties)
            : properties(properties) {}

        ItemSet* labels = nullptr;
        ItemSet* widgets = nullptr;
        ItemSet* buddies = nullptr;
        const qcs::core::ControllerPropertyMap& properties;
    };

    explicit WidgetHandler(QObject *parent = nullptr);
    ~WidgetHandler() override;

    void connectItem(QObject* item);

    void updateItems(const UpdateData& updateData);

    void changeCurrentValue(QObject* item, const QVariant& value);

signals:
    void sigCurrentValueChanged(QObject* item, const QVariant& value);

private slots:
    void onCheckBoxToggled();
    void onComboBoxIndexActivated();
    void onSliderValueChanged();
    void onSpinBoxValueChanged();
    void onTextFieldChanged();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
