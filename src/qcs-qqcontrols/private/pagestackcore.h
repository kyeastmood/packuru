// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>


namespace qcs::core
{
class ControllerIdRowMapper;
}

namespace qcs::qqc
{

class PageStackCore : public QObject
{
    Q_OBJECT
public:
    explicit PageStackCore(QObject *parent = nullptr);
    ~PageStackCore() override;

    Q_INVOKABLE void addPage(qcs::core::ControllerIdRowMapper* mapper, QObject* page);
    Q_INVOKABLE QObject* getPage(qcs::core::ControllerIdRowMapper* mapper);

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
