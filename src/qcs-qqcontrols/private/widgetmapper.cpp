// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <unordered_map>
#include <unordered_set>

#include "../qcs-core/controllerengine.h"
#include "../qcs-core/controllerproperty.h"

#include "widgetmapper.h"
#include "private/widgethandler.h"


using namespace qcs::core;


namespace qcs::qqc
{

using ItemSet = WidgetHandler::ItemSet;

struct WidgetMapper::Private
{

    void addLabels(InternalControllerIdType controllerId, const QList<QObject*>& labels);
    void addWidgets(InternalControllerIdType controllerId, const QList<QObject*>& widgets);
    void addBuddies(InternalControllerIdType controllerId, const QList<QObject*>& buddies);

    void addLabel(InternalControllerIdType controllerId, ItemSet& labelsForId, QObject* label);
    void addWidget(InternalControllerIdType controllerId, ItemSet& widgetsForId, QObject* widget);
    void addBuddy(InternalControllerIdType controllerId, ItemSet& buddiesForId, QObject* buddy);

    void onUserChangedItemValue(QObject* item, const QVariant& value);
    void syncItemsToControllers(InternalControllerIdType controllerId, const ControllerPropertyMap& properties);
    void updateItems();

    std::unordered_map<InternalControllerIdType, ItemSet> idToWidgets;
    std::unordered_map<QObject*, InternalControllerIdType> widgetToId;
    std::unordered_map<InternalControllerIdType, ItemSet> idToBuddies;
    std::unordered_map<InternalControllerIdType, ItemSet> idToLabels;
    std::unordered_set<InternalControllerIdType> widgetsToUpdate;
    WidgetMapper* publ = nullptr;
    ControllerEngine* engine_ = nullptr;
    WidgetHandler* handler;
};


WidgetMapper::WidgetMapper(QObject *parent)
    : QObject(parent),
      priv(new Private)
{
    priv->publ = this;
    priv->handler = new WidgetHandler(this);

    connect(priv->handler, &WidgetHandler::sigCurrentValueChanged,
            this, [priv = priv.get()] (QObject* item, const QVariant& value)
    { priv->onUserChangedItemValue(item, value); });
}


WidgetMapper::~WidgetMapper()
{

}


void WidgetMapper::addLabelAndWidget(int controllerId, QObject* label, QObject* widget)
{
    ItemSet& labelsForId = priv->idToLabels[controllerId];
    ItemSet& widgetsForId = priv->idToWidgets[controllerId];

    priv->addLabel(controllerId, labelsForId, label);
    priv->addWidget(controllerId, widgetsForId, widget);

    priv->widgetsToUpdate.insert(controllerId);
}


void WidgetMapper::addLabelsAndWidgets(int controllerId,
                                       const QList<QObject*>& labels,
                                       const QList<QObject*>& widgets)
{
    ItemSet& labelsForId = priv->idToLabels[controllerId];
    ItemSet& widgetsForId = priv->idToWidgets[controllerId];

    for (auto label : labels)
        priv->addLabel(controllerId, labelsForId, label);

    for (auto widget : widgets)
        priv->addWidget(controllerId, widgetsForId, widget);

    priv->widgetsToUpdate.insert(controllerId);
}


void WidgetMapper::addWidget(int controllerId, QObject* widget)
{
    ItemSet& widgetsForId = priv->idToWidgets[controllerId];

    priv->addWidget(controllerId, widgetsForId, widget);

    priv->widgetsToUpdate.insert(controllerId);
}


void WidgetMapper::addWidgets(int controllerId, const QList<QObject*>& widgets)
{
    ItemSet& widgetsForId = priv->idToWidgets[controllerId];

    for (auto widget : qAsConst(widgets))
        priv->addWidget(controllerId, widgetsForId, widget);

    priv->widgetsToUpdate.insert(controllerId);
}


void WidgetMapper::addBuddy(int controllerId, QObject* buddy)
{
    ItemSet& buddiesForId = priv->idToBuddies[controllerId];

    priv->addBuddy(controllerId, buddiesForId, buddy);

    priv->widgetsToUpdate.insert(controllerId);
}


void WidgetMapper::addBuddies(int controllerId, const QList<QObject*>& buddies)
{
    ItemSet& buddiesForId = priv->idToBuddies[controllerId];

    for (auto buddy : qAsConst(buddies))
        priv->addBuddy(controllerId, buddiesForId, buddy);

    priv->widgetsToUpdate.insert(controllerId);
}


void WidgetMapper::setEngine(ControllerEngine* engine)
{
    Q_ASSERT(engine);

    if (!priv->engine_)
    {
        priv->engine_ = engine;

        connect(engine, &ControllerEngine::sigControllerUpdated,
                this, [priv = priv.get()] (InternalControllerIdType id, const ControllerPropertyMap& properties)
        { priv->syncItemsToControllers(id, properties); });

        emit engineChanged();
    }

    updateWidgets();
}


ControllerEngine* WidgetMapper::getEngine()
{
    return priv->engine_;
}


void WidgetMapper::updateWidgets()
{
    priv->updateItems();
}


bool WidgetMapper::isSetVisible(int controllerId) const
{
    return priv->engine_->getControllerPropertyAs<bool>(controllerId,
                                                        qcs::core::ControllerProperty::Visible);
}


bool WidgetMapper::isSetEnabled(int controllerId) const
{
    return priv->engine_->getControllerPropertyAs<bool>(controllerId,
                                                        qcs::core::ControllerProperty::Enabled);
}


void WidgetMapper::Private::addWidgets(InternalControllerIdType controllerId, const QList<QObject*>& widgets)
{
    ItemSet& widgetsForId = idToWidgets[controllerId];

    for (auto widget : qAsConst(widgets))
        addWidget(controllerId, widgetsForId, widget);

    widgetsToUpdate.insert(controllerId);
}


void WidgetMapper::Private::addBuddies(InternalControllerIdType controllerId, const QList<QObject*>& buddies)
{
    ItemSet& buddiesForId = idToBuddies[controllerId];

    for (auto buddy : qAsConst(buddies))
        addBuddy(controllerId, buddiesForId, buddy);

    widgetsToUpdate.insert(controllerId);
}


void WidgetMapper::Private::addLabel(InternalControllerIdType controllerId,
                                     ItemSet& labelsForId,
                                     QObject* label)
{
    Q_ASSERT(label);

    if (labelsForId.find(label) == labelsForId.end())
    {
        labelsForId.insert(label);

        QObject::connect(label, &QObject::destroyed,
                         publ, [priv = this, label = label, controllerId = controllerId] ()
        {
            ItemSet& buddiesForId = priv->idToBuddies[controllerId];
            buddiesForId.erase(label);
        });
    }
}


void WidgetMapper::Private::addWidget(InternalControllerIdType controllerId,
                                      ItemSet& widgetsForId,
                                      QObject* widget)
{
    Q_ASSERT(widget);

    if (widgetsForId.find(widget) == widgetsForId.end())
    {
        handler->connectItem(widget);

        widgetsForId.insert(widget);
        widgetToId[widget] = controllerId;

        QObject::connect(widget, &QObject::destroyed,
                         publ, [priv = this, widget = widget] ()
        {
            const InternalControllerIdType controllerId = priv->widgetToId[widget];
            ItemSet& itemsForId = priv->idToWidgets[controllerId];
            itemsForId.erase(widget);
            priv->widgetToId.erase(widget);
        });
    }
}


void WidgetMapper::Private::addBuddy(InternalControllerIdType controllerId,
                                     ItemSet& buddiesForId,
                                     QObject* buddy)
{
    Q_ASSERT(buddy);

    if (buddiesForId.find(buddy) == buddiesForId.end())
    {
        buddiesForId.insert(buddy);

        QObject::connect(buddy, &QObject::destroyed,
                         publ, [priv = this, buddy = buddy, controllerId = controllerId] ()
        {
            ItemSet& buddiesForId = priv->idToBuddies[controllerId];
            buddiesForId.erase(buddy);
        });
    }
}


void WidgetMapper::Private::onUserChangedItemValue(QObject* item, const QVariant& value)
{
    const auto it = widgetToId.find(item);
    Q_ASSERT(it != widgetToId.end());

    const InternalControllerIdType id = it->second;

    const auto it2 = idToWidgets.find(id);
    Q_ASSERT(it2 != idToWidgets.end());

    ItemSet widgets = it2->second; // Copy
    widgets.erase(item);

    for (auto widget : widgets)
        handler->changeCurrentValue(widget, value);

    engine_->syncControllerToWidget(id, value);
}


void WidgetMapper::Private::syncItemsToControllers(InternalControllerIdType controllerId, const ControllerPropertyMap& properties)
{
    WidgetHandler::UpdateData data(properties);

    auto it = idToLabels.find(controllerId);
    if (it != idToLabels.end())
        data.labels = &it->second;

    it = idToWidgets.find(controllerId);
    if (it != idToWidgets.end())
        data.widgets = &it->second;

    it = idToBuddies.find(controllerId);
    if (it != idToBuddies.end())
        data.buddies = &it->second;

    handler->updateItems(data);
}


void WidgetMapper::Private::updateItems()
{
    if (!engine_)
        return;

    for (InternalControllerIdType id : widgetsToUpdate)
        syncItemsToControllers(id, engine_->getControllerProperties(id));

    widgetsToUpdate.clear();
}

}
