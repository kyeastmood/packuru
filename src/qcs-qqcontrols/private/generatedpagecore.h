// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>

#include "qcs-core/controllerpropertymap.h"


namespace qcs::core
{
class ControllerIdRowMapper;
}

namespace qcs::qqc
{

class GeneratedPageCore : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qcs::core::ControllerIdRowMapper* rowMapper READ getMapper WRITE setMapper NOTIFY mapperChanged)

public:
    explicit GeneratedPageCore(QObject *parent = nullptr);
    ~GeneratedPageCore() override;

    Q_INVOKABLE void addItem(int mapperRow, QObject* label, QObject* item);
    Q_INVOKABLE void setMapper(qcs::core::ControllerIdRowMapper* mapper);
    Q_INVOKABLE qcs::core::ControllerIdRowMapper* getMapper();

signals:
    void mapperChanged();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
