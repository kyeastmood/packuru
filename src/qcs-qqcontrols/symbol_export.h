// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QtCore/qglobal.h>

#if defined(QCS_QQCONTROLS_LIBRARY)
#  define QCS_QQCONTROLS_EXPORT Q_DECL_EXPORT
#else
#  define QCS_QQCONTROLS_EXPORT Q_DECL_IMPORT
#endif
