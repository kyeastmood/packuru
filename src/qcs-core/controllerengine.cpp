// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <stack>

#include "controllerengine.h"
#include "controllerbase.h"


namespace qcs::core
{

struct ControllerEngine::Private
{
    using ControllerList = std::list<ControllerBase*>;

    void setUpController(std::unique_ptr<ControllerBase> ctrl);

    void updateAllControllers();

    std::list<ControllerBase*> createUpdateList(const std::unordered_set<ControllerBase*>& controllers,
                                                ControllerBase* excludedController = nullptr) const;

    void processUpdateList(const ControllerList& list);

    QVariant tryGetControllerCurrentValue(InternalControllerIdType id) const;

    void onExternalValueInput(ControllerBase* ctrl, const ControllerPropertyMap& properties);

    ControllerEngine* publ = nullptr;
    std::unordered_map<InternalControllerIdType, std::unique_ptr<ControllerBase>> controllers;
    std::unordered_set<ControllerBase*> topControllers;
};


ControllerEngine::ControllerEngine(QObject *parent)
    : QObject(parent),
      priv(new Private)
{
    priv->publ = this;
}


ControllerEngine::~ControllerEngine()
{

}


void ControllerEngine::updateAllControllers()
{
   priv->updateAllControllers();
}


void ControllerEngine::syncControllerPriv(InternalControllerIdType id, const QVariant& newValue)
{
    const auto it = priv->controllers.find(id);
    Q_ASSERT(it != priv->controllers.end());

    const auto ctrl = it->second.get();

    ctrl->aboutToSyncToWidget(newValue);
    ctrl->syncCurrentValueToWidget(newValue);
    ctrl->onValueSyncedToWidget(newValue);

    emit sigControllerSyncedToWidget(id);

    /* The synced controller will also be updated because it might want to change other
       properties e.g. after current value has been synced, the tooltip might have to be updated. */
    const Private::ControllerList list = priv->createUpdateList(ctrl->getDependants());
    priv->processUpdateList(list);
}


void ControllerEngine::updateControllerPriv(InternalControllerIdType id)
{
    const auto it = priv->controllers.find(id);
    Q_ASSERT(it != priv->controllers.end());

    const auto ctrl = it->second.get();

    const std::list<ControllerBase*> list = priv->createUpdateList({ctrl});
    priv->processUpdateList(list);
}


ControllerBase* ControllerEngine::getControllerPriv(int id)
{
    const auto it = priv->controllers.find(id);
    Q_ASSERT(it != priv->controllers.end());
    return it->second.get();
}


QVariant ControllerEngine::getControllerPropertyPriv(int id, ControllerProperty property) const
{
    const auto it = priv->controllers.find(id);
    Q_ASSERT(it != priv->controllers.end());
    return it->second->getProperty(property);
}


void ControllerEngine::addController(InternalControllerIdType id,
                                     std::unique_ptr<ControllerBase> ctrl,
                                     bool isTopController)
{
    ctrl->id = id;

    if (isTopController)
        priv->topControllers.insert(ctrl.get());

    priv->setUpController(std::move(ctrl));
}


void ControllerEngine::getAvailableCurrentValuesPriv(const ValueVisitor& visitor) const
{
    for (const auto& it : priv->controllers)
    {
        const InternalControllerIdType id = it.first;
        visitor(id, priv->tryGetControllerCurrentValue(id));
    }
}


const ControllerPropertyMap& ControllerEngine::getControllerPropertiesPriv(InternalControllerIdType controllerId) const
{
    const auto it = priv->controllers.find(controllerId);
    Q_ASSERT(it != priv->controllers.end());
    return it->second->properties;
}


void ControllerEngine::Private::setUpController(std::unique_ptr<ControllerBase> ctrl)
{
    ctrl->getControllerProperties_.bind<ControllerEngine, &ControllerEngine::getControllerPropertiesPriv>(publ);

    ctrl->sigExternalUpdate.bind<ControllerEngine::Private,
            &ControllerEngine::Private::onExternalValueInput>(this);

    Q_ASSERT(controllers.find(ctrl->id) == controllers.end());

    controllers[ctrl->id] = std::move(ctrl);
}


void ControllerEngine::Private::updateAllControllers()
{
    const Private::ControllerList list = createUpdateList(topControllers);
    processUpdateList(list);
}


std::list<ControllerBase*> ControllerEngine::Private::createUpdateList(const std::unordered_set<ControllerBase*>& controllers,
                                                              ControllerBase* excludedController) const
{
    ControllerList updateList;

    struct ControllerListItem
    {
        ControllerListItem(ControllerBase* ctrl, std::list<ControllerBase*>::iterator origin)
        : ctrl(ctrl), origin(origin) {}

        ControllerBase* ctrl;
        std::list<ControllerBase*>::iterator origin;
    };

    std::stack<ControllerListItem> workStack;

    for (auto ctrl : controllers)
        workStack.push({ctrl, updateList.end()});

    std::unordered_set<ControllerBase*> inQueue;
    if (excludedController)
        inQueue.insert(excludedController);

    while (!workStack.empty())
    {
        auto item = workStack.top();
        workStack.pop();

        if (inQueue.count(item.ctrl) == 0)
        {
            auto assignedPosition = updateList.begin();

            if (item.origin != updateList.end())
            {
                assignedPosition = item.origin;
                ++assignedPosition;
            }

            auto origin = updateList.insert(assignedPosition, item.ctrl);

            auto dependants = item.ctrl->getDependants();
            for (auto ctrl : dependants)
                workStack.push({ctrl, origin});

            inQueue.insert(item.ctrl);
        }
    }

    return updateList;
}


void ControllerEngine::Private::processUpdateList(const ControllerList& list)
{
    for (auto ctrl : list)
    {
        ctrl->update();
        emit publ->sigControllerUpdated(ctrl->getId(), ctrl->properties);
    }
}


QVariant ControllerEngine::Private::tryGetControllerCurrentValue(InternalControllerIdType id) const
{
    auto it = controllers.find(id);
    Q_ASSERT(it != controllers.end());
    return it->second->tryGetCurrentValue();
}


void ControllerEngine::Private::onExternalValueInput(ControllerBase* ctrl, const ControllerPropertyMap& properties)
{
    emit publ->sigControllerUpdated(ctrl->getId(), properties);

    const ControllerList list = createUpdateList(ctrl->getDependants(), ctrl);
    processUpdateList(list);
}

}
