// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../utils/private/unordered_map_utils.h"

#include "controllerbase.h"


namespace qcs::core
{

void ControllerBase::update()
{

}


QVariant ControllerBase::getProperty(ControllerProperty property) const
{
    return Packuru::Utils::getValue(properties, property);
}

}
