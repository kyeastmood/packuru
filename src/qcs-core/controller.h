// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "controllerbase.h"


namespace qcs::core
{

template <typename PublicValueType, typename PrivateValueType = void>
class Controller : public ControllerBase
{
protected:
    using PublicValueList = QList<PublicValueType>;
    using PrivateValueList = QList<PrivateValueType>;

    void setCurrentValue(const PublicValueType& publicValue, const PrivateValueType& privateValue);
    void setCurrentValueFromPublic(const PublicValueType& publicValue);
    void setCurrentValueFromPrivate(const PrivateValueType& privateValue);
    void setCurrentValueFromIndex(int index);
    void setValueLists(const QList<PublicValueType>& publicList, const QList<PrivateValueType>& privateList);
    void setValueListsFromPrivate(const QList<PrivateValueType>& privateList);
    void clearCurrentValue();
    void clearValueLists();

    std::pair<QList<PublicValueType>, QList<PrivateValueType>>
    createLists(std::initializer_list<std::pair<PublicValueType, PrivateValueType>> list);

    std::pair<QList<QString>, QList<PrivateValueType>>
    createListsFrom(std::initializer_list<PrivateValueType> list);

    QVariant tryGetCurrentValue() const override final;

private:
    void syncCurrentValueToWidget(const QVariant& value) override final;
};


template<typename PublicValueType, typename PrivateValueType>
void Controller<PublicValueType, PrivateValueType>::setCurrentValue(const PublicValueType& publicValue,
                                                                    const PrivateValueType& privateValue)
{
    const bool containsPublicList = properties.find(ControllerProperty::PublicValueList)
            != properties.end();

    const bool containsPrivateList = properties.find(ControllerProperty::PrivateValueList)
            != properties.end();

    if (containsPublicList && containsPrivateList)
    {
        const void* ptr = properties[ControllerProperty::PublicValueList].constData();
        auto publicList = static_cast<const PublicValueList*>(ptr);
        Q_ASSERT(publicList->contains(publicValue));

        ptr = properties[ControllerProperty::PrivateValueList].constData();
        auto privateList = static_cast<const PrivateValueList*>(ptr);
        Q_ASSERT(privateList->contains(privateValue));
    }

    properties[ControllerProperty::PublicCurrentValue].setValue(publicValue);
    properties[ControllerProperty::PrivateCurrentValue].setValue(privateValue);
}


template<typename PublicValueType, typename PrivateValueType>
void Controller<PublicValueType, PrivateValueType>::setCurrentValueFromPublic(const PublicValueType& publicValue)
{
    properties[ControllerProperty::PublicCurrentValue].setValue(publicValue);

    const void* ptr = properties[ControllerProperty::PublicValueList].constData();
    auto publicList = static_cast<const PublicValueList*>(ptr);

    const int searchIndex = publicList->indexOf(publicValue);
    Q_ASSERT(searchIndex >= 0);

    ptr = properties[ControllerProperty::PrivateValueList].constData();
    auto privateList = static_cast<const PrivateValueList*>(ptr);
    Q_ASSERT(publicList->size() == privateList->size());

    properties[ControllerProperty::PrivateCurrentValue].setValue((*privateList)[searchIndex]);
}


template<typename PublicValueType, typename PrivateValueType>
void Controller<PublicValueType, PrivateValueType>::setCurrentValueFromPrivate(const PrivateValueType& privateValue)
{
    properties[ControllerProperty::PrivateCurrentValue].setValue(privateValue);

    const void* ptr = properties[ControllerProperty::PrivateValueList].constData();
    auto privateList = static_cast<const PrivateValueList*>(ptr);

    const int searchIndex = privateList->indexOf(privateValue);
    Q_ASSERT(searchIndex >= 0);

    ptr = properties[ControllerProperty::PublicValueList].constData();
    auto publicList = static_cast<const PublicValueList*>(ptr);
    Q_ASSERT(publicList->size() == privateList->size());

    properties[ControllerProperty::PublicCurrentValue].setValue((*publicList)[searchIndex]);
}


template<typename PublicValueType, typename PrivateValueType>
void Controller<PublicValueType, PrivateValueType>::setCurrentValueFromIndex(int index)
{
    const void* ptr = properties[ControllerProperty::PublicValueList].constData();
    auto publicList = static_cast<const PublicValueList*>(ptr);
    Q_ASSERT(publicList->size() > index);

    ptr = properties[ControllerProperty::PrivateValueList].constData();
    auto privateList = static_cast<const PrivateValueList*>(ptr);
    Q_ASSERT(publicList->size() == privateList->size());

    properties[ControllerProperty::PublicCurrentValue].setValue((*publicList)[index]);
    properties[ControllerProperty::PrivateCurrentValue].setValue((*privateList)[index]);
}


template<typename PublicValueType, typename PrivateValueType>
void Controller<PublicValueType, PrivateValueType>::setValueLists(const QList<PublicValueType>& publicList,
                                                                  const QList<PrivateValueType>& privateList)
{
    Q_ASSERT(publicList.size() == privateList.size());

    properties[ControllerProperty::PublicValueList].setValue(publicList);
    properties[ControllerProperty::PrivateValueList].setValue(privateList);
}


template<typename PublicValueType, typename PrivateValueType>
void Controller<PublicValueType, PrivateValueType>
::setValueListsFromPrivate(const QList<PrivateValueType>& privateList)
{
    QList<QString> publicList;
    publicList.reserve(privateList.size());

    for (const auto& v : privateList)
        publicList.append(toString(v));

    properties[ControllerProperty::PublicValueList].setValue(publicList);
    properties[ControllerProperty::PrivateValueList].setValue(privateList);
}


template<typename PublicValueType, typename PrivateValueType>
void Controller<PublicValueType, PrivateValueType>::clearCurrentValue()
{
    properties[ControllerProperty::PublicCurrentValue].clear();
    properties[ControllerProperty::PrivateCurrentValue].clear();
}


template<typename PublicValueType, typename PrivateValueType>
void Controller<PublicValueType, PrivateValueType>::clearValueLists()
{
    properties[ControllerProperty::PublicValueList].clear();
    properties[ControllerProperty::PrivateValueList].clear();
}


template<typename PublicValueType, typename PrivateValueType>
std::pair<QList<PublicValueType>, QList<PrivateValueType> > Controller<PublicValueType, PrivateValueType>
::createLists(std::initializer_list<std::pair<PublicValueType, PrivateValueType> > list)
{
    QList<PublicValueType> publicList;
    QList<PrivateValueType> privateList;

    publicList.reserve(list.size());
    privateList.reserve(list.size());

    for (const auto& pair : list)
    {
        publicList.push_back(pair.first);
        privateList.push_back(pair.second);
    }

    return {publicList, privateList};
}


template<typename PublicValueType, typename PrivateValueType>
std::pair<QList<QString>, QList<PrivateValueType> > Controller<PublicValueType, PrivateValueType>
::createListsFrom(std::initializer_list<PrivateValueType> list)
{
    QList<PrivateValueType> privateList(list);

    QList<QString> publicList;
    publicList.reserve(list.size());

    for (const auto& v : list)
        publicList.push_back(toString(v));

    return {publicList,privateList};
}


template<typename PublicValueType, typename PrivateValueType>
QVariant Controller<PublicValueType, PrivateValueType>::tryGetCurrentValue() const
{
    auto it = properties.find(ControllerProperty::PrivateCurrentValue);

    if (it == properties.end())
        return QVariant();
    else
        return it->second;
}


template<typename PublicValueType, typename PrivateValueType>
void Controller<PublicValueType, PrivateValueType>::syncCurrentValueToWidget(const QVariant& value)
{
    const auto publicValue = Packuru::Utils::getValueAs<PublicValueType>(value);
    setCurrentValueFromPublic(publicValue);
}





template <typename PublicValueType>
class Controller<PublicValueType> : public ControllerBase
{
protected:
    void setCurrentValue(const PublicValueType& publicValue)
    {
        properties[ControllerProperty::PublicCurrentValue].setValue(publicValue);
    }

    void setValueLists(const QList<PublicValueType>& publicList)
    {
        properties[ControllerProperty::PublicValueList].setValue(publicList);
    }

    void setValueRange(const PublicValueType& min, const PublicValueType& max)
    {
        properties[ControllerProperty::MinValue].setValue(min);
        properties[ControllerProperty::MaxValue].setValue(max);
    }

    void clearCurrentValue()
    {
        properties[ControllerProperty::PublicCurrentValue].clear();
    }

    void clearValueLists()
    {
        properties[ControllerProperty::PublicValueList].clear();
    }

    void clearValueRange()
    {
        properties[ControllerProperty::MinValue].clear();
        properties[ControllerProperty::MaxValue].clear();
    }

    QVariant tryGetCurrentValue() const override final;

private:
    void syncCurrentValueToWidget(const QVariant& value) override final
    {
        Q_ASSERT(Packuru::Utils::containsType<PublicValueType>(value));
        properties[ControllerProperty::PublicCurrentValue].setValue(value);
    }
};


template<typename PublicValueType>
QVariant Controller<PublicValueType>::tryGetCurrentValue() const
{
    auto it = properties.find(ControllerProperty::PublicCurrentValue);

    if (it == properties.end())
        return QVariant();
    else
        return it->second;
}


}
