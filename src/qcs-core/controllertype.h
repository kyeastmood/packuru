// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace qcs::core
{

namespace ControllerType
{

Q_NAMESPACE

enum class Type
{
    CheckBox,
    ComboBox,
    DoubleSpinBox,
    IntSpinBox,
    Label,
    LineEdit,
    Slider,

    ___INVALID
};

Q_ENUM_NS(Type)

}

}

