// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>

#include "controllerpropertymap.h"
#include "internalcontrolleridtype.h"
#include "symbol_export.h"


namespace qcs::core
{

class ControllerEngine;

namespace ControllerType
{
enum class Type;
}


class QCS_CORE_EXPORT ControllerIdRowMapper : public QObject
{
    Q_OBJECT
public:
    struct LabelData
    {
        QString text;
        bool isHeader = false;
        InternalControllerIdType controllerId = InternalControllerIdType();
    };

    ControllerIdRowMapper(QObject* parent = nullptr);
    ~ControllerIdRowMapper() override;

    void setEngine(ControllerEngine* engine);

    void addHeader(const QString& label);

    template <typename ControllerId>
    void addController(const QString& label, ControllerId id);

    Q_INVOKABLE QString getLabel(int row) const;

    Q_INVOKABLE bool isHeader(int row) const;

    Q_INVOKABLE qcs::core::ControllerType::Type getControllerType(int row) const;

    const ControllerPropertyMap& getControllerProperties(int row) const;

    Q_INVOKABLE int size() const;

    void onUserChangedValue(int row, const QVariant& value);

signals:
    void sigUserChangedValue(InternalControllerIdType id, const QVariant& value);
    void sigControllerPropertiesChanged(int row, const ControllerPropertyMap& properties);

private:
    void addControllerImpl(const QString& label, InternalControllerIdType id);

    struct Private;
    std::unique_ptr<Private> priv;
};


template <typename ControllerId>
void ControllerIdRowMapper::addController(const QString& label, ControllerId id)
{
    const auto internalId = static_cast<InternalControllerIdType>(id);
    addControllerImpl(label, internalId);
}

}
