// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once


namespace qcs::core
{

template <typename ReturnType = void, typename ... FunctionArgs>
class Callback
{
private:
    using FreeFunctionPtr = ReturnType (*) (FunctionArgs ...);

    template <typename Class>
    using MemberFunctionPtr = ReturnType (Class::*) (FunctionArgs ...);

    template <typename Class>
    using MemberConstFunctionPtr = ReturnType (Class::*) (FunctionArgs ...) const;

public:
    constexpr Callback()
        : classInstance(nullptr), functionWrapper(nullptr) {}

    template <FreeFunctionPtr function>
    void bind() noexcept
    {
        classInstance = nullptr;
        functionWrapper = &freeFunctionWrapper<function>;
    }

    template <class Class, MemberFunctionPtr<Class> function>
    void bind (Class* instance) noexcept
    {
        classInstance = instance;
        functionWrapper = &memberFunctionWrapper<Class,function>;
    }

    template <class Class, MemberConstFunctionPtr<Class> function>
    void bind (Class* instance) noexcept
    {
        classInstance = instance;
        functionWrapper = &memberConstFunctionWrapper<Class,function>;
    }

    ReturnType operator()(FunctionArgs ... args) const
    {
        return functionWrapper (classInstance, args ...);
    }

    ReturnType call (FunctionArgs ... args) const
    {
        return functionWrapper (classInstance, args ...);
    }

    explicit operator bool() const { return functionWrapper; }
    bool isValid() const { return functionWrapper; }

    void reset() { classInstance = nullptr; functionWrapper = nullptr; }
    void* object() { return classInstance; }

private:
    using InstancePtr = void;
    using FunctionWrapperPtr = ReturnType (*) (InstancePtr*, FunctionArgs ... args);

    template <FreeFunctionPtr function>
    static ReturnType freeFunctionWrapper (void*, FunctionArgs ... args)
    {
        return function (args ...);
    }

    template <class Class, MemberFunctionPtr<Class>function>
    static ReturnType memberFunctionWrapper (InstancePtr* instance, FunctionArgs ... args)
    {
        return (static_cast<Class*>(instance)->*function) (args ...);
    }

    template <class Class, MemberConstFunctionPtr<Class> function>
    static ReturnType memberConstFunctionWrapper (InstancePtr* instance, FunctionArgs ... args)
    {
        return (static_cast<Class*>(instance)->*function) (args ...);
    }

    InstancePtr* classInstance = nullptr;
    FunctionWrapperPtr functionWrapper = nullptr;
};

}
