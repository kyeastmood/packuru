// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <list>
#include <type_traits>
#include <unordered_set>
#include <unordered_map>
#include <memory>

#include <QObject>

#include "../utils/qvariant_utils.h"

#include "controllerpropertymap.h"
#include "internalcontrolleridtype.h"
#include "symbol_export.h"


namespace qcs::core
{

class ControllerBase;
enum class ControllerProperty;


class QCS_CORE_EXPORT ControllerEngine : public QObject
{
    Q_OBJECT
public:
    explicit ControllerEngine(QObject *parent = nullptr);
    ~ControllerEngine();

    template <typename ControllerClass,
              typename ControllerId,
              typename ... ConstructorArgs>
    ControllerClass* createController(ControllerId id, ConstructorArgs&& ... args);

    template <typename ControllerClass,
              typename ControllerId,
              typename ... ConstructorArgs>
    ControllerClass* createTopController(ControllerId id, ConstructorArgs&& ... args);

    void updateAllControllers();

    template <typename ControllerId>
    void syncControllerToWidget(ControllerId id, const QVariant& newValue)
    {
        syncControllerPriv(static_cast<InternalControllerIdType>(id), newValue);
    }

    template <typename ControllerId>
    void updateController(ControllerId id);

    template <typename ControllerId>
    ControllerBase* getController(ControllerId id)
    {
        return getControllerPriv(static_cast<InternalControllerIdType>(id));
    }

    template <typename ControllerId>
    QVariant getControllerProperty(ControllerId id, ControllerProperty property) const
    {
        return getControllerPropertyPriv(static_cast<InternalControllerIdType>(id), property);
    }

    template <typename T, typename ControllerId>
    T getControllerPropertyAs(ControllerId id, ControllerProperty property) const
    {
        return Packuru::Utils::getValueAs<T>(getControllerProperty(id, property));
    }

    template <typename ControllerId>
    const ControllerPropertyMap& getControllerProperties(ControllerId id) const
    {
        return getControllerPropertiesPriv(static_cast<InternalControllerIdType>(id));
    }

    template <typename ControllerId>
    std::unordered_map<ControllerId, QVariant> getAvailableCurrentValues() const;

signals:
    void sigControllerUpdated(InternalControllerIdType id, const ControllerPropertyMap& properties);
    void sigControllerSyncedToWidget(InternalControllerIdType id);

private:
    using ValueVisitor = std::function<void(InternalControllerIdType id, const QVariant& value)>;

    void syncControllerPriv(InternalControllerIdType id, const QVariant& newValue);

    void updateControllerPriv(InternalControllerIdType id);

    const ControllerPropertyMap& getControllerPropertiesPriv(InternalControllerIdType controllerId) const;

    ControllerBase* getControllerPriv(int id);

    QVariant getControllerPropertyPriv(int id, ControllerProperty property) const;

    void addController(InternalControllerIdType id,
                       std::unique_ptr<ControllerBase> ctrl,
                       bool isTopController);

    void getAvailableCurrentValuesPriv(const ValueVisitor& visitor) const;

    struct Private;
    std::unique_ptr<Private> priv;
};


template <typename ControllerClass,
          typename ControllerId,
          typename ... ConstructorArgs>
ControllerClass* ControllerEngine::createController(ControllerId id, ConstructorArgs&& ... args)
{
    static_assert(std::is_base_of<ControllerBase, ControllerClass>::value,
            "ControllerBase class must be the base for all controllers");

    const auto internalId = static_cast<InternalControllerIdType>(id);
    auto ctrl = std::make_unique<ControllerClass>(std::forward<ConstructorArgs>(args)...);
    const auto ptr = ctrl.get();

    addController(internalId, std::move(ctrl), false);

    return ptr;
}


template <typename ControllerClass,
          typename ControllerId,
          typename ... ConstructorArgs>
ControllerClass* ControllerEngine::createTopController(ControllerId id, ConstructorArgs&& ... args)
{
    static_assert(std::is_base_of<ControllerBase, ControllerClass>::value,
            "ControllerBase class must be the base for all controllers");

    const auto internalId = static_cast<InternalControllerIdType>(id);
    auto ctrl = std::make_unique<ControllerClass>(std::forward<ConstructorArgs>(args)...);
    const auto ptr = ctrl.get();

    addController(internalId, std::move(ctrl), true);

    return ptr;
}


template <typename ControllerId>
void ControllerEngine::updateController(ControllerId id)
{
    const auto internalId = static_cast<InternalControllerIdType>(id);

    updateControllerPriv(internalId);
}


template<typename ControllerId>
std::unordered_map<ControllerId, QVariant> ControllerEngine::getAvailableCurrentValues() const
{
    std::unordered_map<ControllerId, QVariant> result;

    const ValueVisitor vst = [&result = result] (InternalControllerIdType id, const QVariant& value)
    { result[static_cast<ControllerId>(id)] = value; };

    getAvailableCurrentValuesPriv(vst);

    return result;
}

}
