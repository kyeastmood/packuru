// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/archivetype.h"

#include "../plugin-gnutar-core/backendhandler.h"
#include "../plugin-gnutar-core/archivingparametershandler.h"
#include "../plugin-gnutar-core/registerstreamoperators.h"

#include "archivehandlergnutar.h"


using namespace Packuru::Core;


namespace Packuru::Plugins::GnuTar
{

ArchiveHandlerGnuTar::ArchiveHandlerGnuTar()
    : supportedArchives { ArchiveType::tar,
                          ArchiveType::tar_brotli,
                          ArchiveType::tar_bzip2,
                          ArchiveType::tar_compress,
                          ArchiveType::tar_gzip,
                          ArchiveType::tar_lrzip,
                          ArchiveType::tar_lz4,
                          ArchiveType::tar_lzip,
                          ArchiveType::tar_lzma,
                          ArchiveType::tar_lzop,
                          ArchiveType::tar_xz,
                          ArchiveType::tar_zstd}
{
    Core::registerStreamOperators();
}


std::unordered_set<ArchiveType> ArchiveHandlerGnuTar::supportedTypesForReading() const
{
    return supportedArchives;
}


std::unordered_set<ArchiveType> ArchiveHandlerGnuTar::supportedTypesForWriting() const
{
    return supportedArchives;
}


bool ArchiveHandlerGnuTar::canAddFiles(ArchiveType archiveType, const BackendArchiveProperties& properties) const
{
    Q_UNUSED(properties)
    // GNU tar supports only uncompressed tars
    return archiveType == ArchiveType::tar;
}


bool ArchiveHandlerGnuTar::canDeleteFiles(ArchiveType archiveType, const BackendArchiveProperties& properties) const
{
    Q_UNUSED(properties)
    // GNU tar supports only uncompressed tars
    return archiveType == ArchiveType::tar;
}


std::unique_ptr<AbstractBackendHandler> ArchiveHandlerGnuTar::createBackendHandler() const
{
    return std::make_unique<Core::BackendHandler>();
}


std::unique_ptr<ArchivingParameterHandlerBase>
ArchiveHandlerGnuTar::createArchivingParameterHandler(const ArchivingParameterHandlerInitData& initData) const
{
    return std::make_unique<Core::ArchivingParametersHandler>(initData);
}

}
