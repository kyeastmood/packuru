// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.5 as Kirigami

import PackuruMobileCore 1.0 as PackuruMobileCore

PackuruMobileCore.AutoResizableDialog {
    onClosed: userSortConfig(sortColumn, sortOrder)

    property QtObject archiveModel
    property var columnList: []
    property int sortColumn: 0
    property int sortOrder: Qt.AscendingOrder

    signal userSortConfig(int sortColumn, int sortOrder)

    onColumnListChanged: {
        listModel.clear()

        for (var i = 0; i < columnList.length; ++i)
            listModel.append({"label" : columnList[i], "category" : priv.columnCategory})

        listModel.append({"label" : "Ascending", "order" : Qt.AscendingOrder, "category" : priv.orderCategory})
        listModel.append({"label" : "Descending", "order" : Qt.DescendingOrder, "category" : priv.orderCategory})
    }


    QtObject {
        id: priv
        readonly property string columnCategory: qsTr("Column")
        readonly property string orderCategory: qsTr("Order")

    }

    ListModel {
        id: listModel
    }

    contentItem: ListView {
        id: view
        model: listModel
        clip: true
        boundsBehavior: Flickable.StopAtBounds

        property int delegateMaxImplicitWidth: 0
        property int delegateMaxImplicitHeight: 0

        property int sectionMaxImplicitWidth: 0
        property int sectionMaxImplicitHeight: 0
        property int sectionCount: 0

        implicitWidth: Math.max(delegateMaxImplicitWidth, sectionMaxImplicitWidth)
        implicitHeight: delegateMaxImplicitHeight * count + sectionMaxImplicitHeight * sectionCount

        section {
            property: "category"
            criteria: ViewSection.FullString
            delegate: Kirigami.Heading {
                text: section
                horizontalAlignment: Qt.AlignHCenter
                width: parent.width
                color: Kirigami.Theme.highlightedTextColor
                background: Rectangle {
                    color:  Kirigami.Theme.highlightColor
                }
                onImplicitWidthChanged: {
                    if (implicitWidth + leftPadding + rightPadding > view.sectionMaxImplicitWidth)
                        view.sectionMaxImplicitWidth = implicitWidth  + leftPadding + rightPadding
                }
                onImplicitHeightChanged: {
                    if (implicitHeight > view.sectionMaxImplicitHeight)
                        view.sectionMaxImplicitHeight = implicitHeight
                }

                Component.onCompleted: ++view.sectionCount
                Component.onDestruction: --view.sectionCount
            }
        }

        delegate: Kirigami.AbstractListItem {
            separatorVisible: false
            onClicked: {
                if (category === priv.columnCategory)
                    sortColumn = index
                else if (category === priv.orderCategory)
                    sortOrder = order
            }
            highlighted: {
                if (category === priv.columnCategory)
                    return sortColumn === index
                else if (category === priv.orderCategory)
                    return sortOrder === order
                else
                    return false
            }
            Column {
                Layout.fillWidth: true
                Layout.fillHeight: true

                Label {
                    text: label
                }
            }

            onImplicitWidthChanged: {
                if (implicitWidth + leftPadding + rightPadding > view.delegateMaxImplicitWidth)
                    view.delegateMaxImplicitWidth = implicitWidth  + leftPadding + rightPadding
            }
            onImplicitHeightChanged: {
                if (implicitHeight > view.delegateMaxImplicitHeight)
                    view.delegateMaxImplicitHeight = implicitHeight
            }
        }
    }
}
