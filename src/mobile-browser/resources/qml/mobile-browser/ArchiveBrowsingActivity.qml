// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.4

import org.kde.kirigami 2.3 as Kirigami

import KirigamiActivities 1.0 as KirigamiActivities


KirigamiActivities.SwitchableActivity {
    id: root

    readonly property alias buttonCreateNew: buttonCreateNew
    readonly property alias buttonOpenArchive: buttonOpenArchive
    readonly property alias buttonOpenSettings: buttonOpenSettings
    readonly property alias buttonAbout: buttonAbout
    readonly property alias buttonQuit: buttonQuit

    function createBrowser(browserCore) {
        var component = Qt.createComponent("BrowserActivity.qml");
        var activity = component.createObject(null, {"browserCore" : browserCore,
                                                     "name" : Qt.binding(function() {return browserCore.title})})
        pushActivity(activity, true)
    }

    initialActivity: KirigamiActivities.BasicActivity {
        name: qsTr("Start")
        canClose: false
        pageRow.globalToolBar.style: Kirigami.ApplicationHeaderStyle.Auto
        pageRow.initialPage: Kirigami.Page {
            title: qsTr("Start")

            ColumnLayout {
                anchors.centerIn: parent
                Button {
                   id: buttonCreateNew
                   Layout.fillWidth: true
                }
                Button {
                    id: buttonOpenArchive
                    Layout.fillWidth: true
                }
                Button {
                    id: buttonOpenSettings
                    Layout.fillWidth: true
                }
                Button {
                    id: buttonAbout
                    Layout.fillWidth: true
                }
                Button {
                    id: buttonQuit
                    Layout.fillWidth: true
                }
            }
        }
    }

    onActivityRemoved: {
        activity.destroy()
    }

}
