// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0

import org.kde.kirigami 2.5 as Kirigami

import KirigamiActivities 1.0 as KirigamiActivities


Kirigami.AbstractListItem {
    id: root
    property KirigamiActivities.AbstractActivity activity

    onClicked: {
        if (activity) activity.activate()
        activated(activity.name)
    }
    checked: activity ? activity.current : false
    height: implicitHeight

    signal activated(string archiveName)
    signal closeRequested(QtObject dialog)

    RowLayout {
        id: layout

        Kirigami.Icon {
            id: statusIcon
            source: root.activity ? root.activity.icon : null
            width:32
            height:32
        }
        Label {
            id: labelItem
            Layout.fillWidth: true
            color: root.checked || root.pressed ? root.activeTextColor : root.textColor
            text: root.activity ? root.activity.name : null
        }

        Kirigami.Icon {
            source: "tab-close"
            width: Kirigami.Units.iconSizes.medium
            height:  Kirigami.Units.iconSizes.medium
            visible: activity ? activity.canClose : true
            MouseArea {
                id: mouseArea
                anchors.fill: parent
                onClicked: {
                   if (activity.isBusy) {
                        if (closingDialogLoader.active)
                            return

                        closingDialogLoader.active = true

                        closingDialogLoader.item.accepted.connect(function () {
                            activity.close()
                            root.destroy()
                        })

                        closingDialogLoader.item.open()
                    }
                    else {
                        activity.close()
                        root.destroy()
                    }

                }
            }
        }

        Loader {
            id: closingDialogLoader
            active: false

            sourceComponent: TabClosingDialog {
                parent:  applicationWindow().contentItem
                onClosed: closingDialogLoader.active = false

            }
        }
    }
}
