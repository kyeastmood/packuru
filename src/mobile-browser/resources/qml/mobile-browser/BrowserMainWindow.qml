// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.3

import org.kde.kirigami 2.5 as Kirigami

import KirigamiActivities 1.0 as KirigamiActivities

import PackuruCore 1.0
import PackuruCoreBrowser 1.0
import PackuruMobileCore 1.0 as PackuruMobileCore


Kirigami.AbstractApplicationWindow {
    id: root
    visible: true
    width: 600
    height: 1000
    title: (rootActivity.topActivity ? rootActivity.topActivity.name + " - " : "")
           + mainWindowCore.getString(MainWindowCore.WindowTitle)

    onClosing: {
        var count = mainWindowCore.getBusyBrowserCount()

        if (count === 0)
            return

        close.accepted = false

        if (closingDialogLoader.active)
            closingDialogLoader.active = false

        closingDialogLoader.active = true
        closingDialogLoader.item.busyBrowsers = count
        closingDialogLoader.item.open()
    }

    Loader {
        id: closingDialogLoader
        active: false

        sourceComponent: BrowserClosingDialog {
            parent: root.contentItem
            onClosed: closingDialogLoader.active = false
        }
    }

    Component {
        id: infoDialogComp
        PackuruMobileCore.InformationDialog {}
    }

    Component {
        id: aboutDialogComp
        PackuruMobileCore.AboutDialog {}
    }

    KirigamiActivities.SwitchableActivity {
        id: rootActivity
        active: true
        initialActivity: ArchiveBrowsingActivity {
            id: browsingActivity

            buttonCreateNew.text: actionCreateNew.text
            buttonCreateNew.icon.name: actionCreateNew.iconName
            buttonCreateNew.onClicked: actionCreateNew.trigger()

            buttonOpenArchive.text: actionOpenArchive.text
            buttonOpenArchive.icon.name: actionOpenArchive.iconName
            buttonOpenArchive.onClicked: actionOpenArchive.trigger()

            buttonOpenSettings.text: actionOpenSettings.text
            buttonOpenSettings.icon.name: actionOpenSettings.iconName
            buttonOpenSettings.onClicked: actionOpenSettings.trigger()

            buttonAbout.text: actionAbout.text
            buttonAbout.icon.name: actionAbout.iconName
            buttonAbout.onClicked: actionAbout.trigger()

            buttonQuit.text: actionQuit.text
            buttonQuit.icon.name: actionQuit.iconName
            buttonQuit.onClicked: actionQuit.trigger()

            onActivityPushed: {
                var comp = Qt.createComponent("BrowserTab.qml")
                var tab = comp.createObject(root, {"activity" : activity})
                globalDrawer.topContent.push(tab)
            }
        }

        onActivityRemoved: {
            activity.destroy()
        }

        property QtObject settingsActivity

        function showSettings() {
            if (settingsActivity)
                return

            var comp = Qt.createComponent("BrowserSettingsActivity.qml")
            settingsActivity = comp.createObject(rootActivity,
                                                 {
                                                     "appSettingsDialogCore" : mainWindowCore.createAppSettingsDialogCore(),
                                                     "browserSettingsDialogCore" : mainWindowCore.createBrowserSettingsDialogCore()
                                                 })
            pushActivity(settingsActivity, true)
        }
    }

    Connections {
        target: mainWindowCore

        function onNewBrowserCoreCreated(browserCore) {
            browsingActivity.createBrowser(browserCore)
        }
        function onCommandLineError(info) {
            openInfoDialog(commonStrings.getString(CommonStrings.CommandLineErrorDialogTitle), info);

        }
        function onCommandLineHelpRequested(info) {
            openInfoDialog(commonStrings.getString(CommonStrings.CommandLineHelpDialogTitle), info);
        }
    }

    Connections {
        target: mainWindowCore.getQueueMessenger()

        function onActivated() {
            var comp = Qt.createComponent("QueueMessengerDialog.qml")
            var dialog = comp.createObject(root, {"queueMessenger" : target})
            dialog.destroyOnClose = true
            dialog.open();
        }
    }

    function openInfoDialog(title, text) {
        var dialog = infoDialogComp.createObject(root)
        dialog.destroyOnClose = true
        dialog.headerText = title
        dialog.text = text
        dialog.open();
    }

    wideScreen: pageStack ? width >= pageStack.defaultColumnWidth * 1.5 : false
    pageStack: rootActivity.pageRow

    globalDrawer: Kirigami.GlobalDrawer {
        id: globalDrawer
        title:  mainWindowCore.getString(MainWindowCore.WindowTitle)
        enabled: rootActivity.globalDrawerAvailable
        contentItem.enabled: rootActivity.globalDrawerAvailable

        actions: [
            Kirigami.Action {
                id: actionCreateNew
                text: mainWindowCore.getString(MainWindowCore.ActionNew)
                iconName: "document-new"
                onTriggered: {
                    mainWindowCore.createArchive()
                }
            },
            Kirigami.Action {
                id: actionOpenArchive
                text: mainWindowCore.getString(MainWindowCore.ActionOpen)
                iconName: "document-open"
                onTriggered: {
                    fileDialogLoader.active = true
                }
            },
            Kirigami.Action {
                id: actionOpenSettings
                text: mainWindowCore.getString(MainWindowCore.ActionSettings)
                iconName: "configure"
                onTriggered: {
                    rootActivity.showSettings()
                }
            },
            Kirigami.Action {
                id: actionAbout
                text: mainWindowCore.getString(MainWindowCore.ActionAbout)
                iconName: "help-about-symbolic"
                onTriggered: {
                    var dialog = aboutDialogComp.createObject(root)
                    dialog.destroyOnClose = true
                    dialog.open()
                }
            },
            Kirigami.Action {
                id: actionQuit
                text: mainWindowCore.getString(MainWindowCore.ActionQuit)
                iconName: "application-exit"
                onTriggered: {
                    root.close()
                }
            }
        ]
    }

    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    Loader {
        id: fileDialogLoader
        active: false
        sourceComponent: FileDialog {
            selectMultiple: true
            Component.onCompleted: open()
            onAccepted: {
                var urls = []
                for (var i = 0; i < fileUrls.length; ++i) {
                    urls.push(fileUrls[i])
                }
                if (urls.length > 0)
                    mainWindowCore.openArchives(urls)
                fileDialogLoader.active = false
            }
            onRejected: {
                fileDialogLoader.active = false
            }
        }
    }
}
