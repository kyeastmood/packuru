// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.5 as Kirigami

import ControllerSystem.QQC 1.0 as CS_QQC

import PackuruCore 1.0
import PackuruCoreBrowser 1.0
import PackuruMobileCore 1.0 as PackuruMobileCore


PackuruMobileCore.SettingsActivity {
    id: root

    property QtObject browserSettingsDialogCore

    onAccepted: browserSettingsDialogCore.accept()

    Component.onCompleted: {
        mapper.setEngine(browserSettingsDialogCore.getControllerEngine())

        var page = browserPageComponent.createObject(root)
        insertCategory(1, "document-open", page)
    }

    Component.onDestruction: {
        browserSettingsDialogCore.destroyLater()
    }

    CS_QQC.WidgetMapper {
        id: mapper
    }

    Component {
        id: browserPageComponent

        Kirigami.Page {
            title: browserSettingsDialogCore.getString(SettingsDialogCore.PageTitle)

            ColumnLayout {
                anchors.fill: parent

                GridLayout {
                    columns: 2
                    Label {
                        id: forceSingleInstanceLabel
                        Layout.alignment: Qt.AlignRight
                    }

                    CheckBox {
                        id: forceSingleInstanceCheck
                    }
                    Label {
                        id: showSummaryLabel
                        Layout.alignment: Qt.AlignRight
                    }
                    CheckBox {
                        id: showSummaryCheck
                    }
                }
                Item {
                    Layout.fillHeight: true
                }

                DialogButtonBox {
                    Layout.fillWidth: true
                    Button {
                        text: commonStrings.getString(CommonStrings.Back)
                        icon.name: "go-previous"
                        onClicked: pageRow.layers.pop()
                    }
                }
            }

            Component.onCompleted: {
                mapper.addLabelAndWidget(SettingsControllerType.ForceSingleInstance,
                                         forceSingleInstanceLabel,
                                         forceSingleInstanceCheck )
                mapper.addLabelAndWidget(SettingsControllerType.ShowSummaryOnTaskCompletion,
                                         showSummaryLabel,
                                         showSummaryCheck )
                mapper.updateWidgets()
            }
        }
    }
}
