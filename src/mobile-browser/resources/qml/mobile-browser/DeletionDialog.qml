// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.5 as Kirigami

import ControllerSystem.QQC 1.0 as CS_QQC

import PackuruMobileCore 1.0 as PackuruMobileCore
import PackuruCoreBrowser 1.0
import PackuruCore 1.0


PackuruMobileCore.ArchiveTaskDialog {
    id: root

    contentItem: ColumnLayout {
        spacing: 20
        Label {
            text: dialogCore.getString(DeletionDialogCore.DialogQuestion, dialogCore.getFileModel().rowCount())
        }

        ListView {
            id: view
            boundsBehavior: Flickable.StopAtBounds

            property int delegateMaxImplicitWidth: 0
            property int delegateImplicitHeight: 0

            Layout.preferredWidth: delegateMaxImplicitWidth
            Layout.preferredHeight: delegateImplicitHeight * count
            Layout.fillHeight:  true
            model: dialogCore ? dialogCore.getFileModel() : null
            Layout.fillWidth: true
            clip: true
            delegate: Kirigami.AbstractListItem {
                Label {
                    text: display
                }
                onImplicitWidthChanged: {
                    if (implicitWidth + leftPadding + rightPadding > view.delegateMaxImplicitWidth)
                        view.delegateMaxImplicitWidth = implicitWidth  + leftPadding + rightPadding
                }
                onImplicitHeightChanged: {
                    if (implicitHeight > view.delegateImplicitHeight)
                        view.delegateImplicitHeight = implicitHeight
                }
            }
        }

        GridLayout {
            columns: 2
            Label {
                id: passwordLabel
                Layout.alignment: Qt.AlignRight
            }
            PackuruMobileCore.PasswordField {
                id: passwordField
                Layout.fillWidth: true
                echoMode: TextInput.Password
                focus: visible
                onAccepted: root.accept()
            }
        }
    }

    onAccepted: dialogCore.accept()

    CS_QQC.WidgetMapper {
        id: mapper
    }

    Component.onCompleted: {
        mapper.addLabelAndWidget(DeletionDialogControllerType.Password,
                                 passwordLabel,
                                 passwordField)
        mapper.addWidget(DeletionDialogControllerType.RunInQueue, root.runInQueueCheck)

        mapper.setEngine(dialogCore.getControllerEngine())

        root.cancelButton.text = dialogCore.getString(DeletionDialogCore.Cancel)

    }
}
