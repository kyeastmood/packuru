// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import PackuruMobileCore 1.0 as PackuruMobileCore
import PackuruCore 1.0


PackuruMobileCore.AppClosingDialog {
    id: root
    property int busyBrowsers: 0

    infoItem:
        Label {
        text: dialogCore.getString(AppClosingDialogCore.BrowserBusyBrowsersInfo, busyBrowsers)
    }
}
