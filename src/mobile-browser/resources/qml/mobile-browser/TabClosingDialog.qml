// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import PackuruMobileCore 1.0 as PackuruMobileCore
import PackuruCoreBrowser 1.0
import PackuruCore 1.0


PackuruMobileCore.AutoResizableDialog {
    id: root
    headerText: core.getString(TabClosingDialogCore.DialogTitle)
    contentItem: ColumnLayout {

        Label {
            text: core.getString(TabClosingDialogCore.ThisTabIsBusy)
        }
        Label {
            text: core.getString(TabClosingDialogCore.DoYouWantToCloseIt)
        }
    }

    footer: DialogButtonBox {
        id: buttonBox
        implicitWidth: contentWidth

        Button {
            text: core.getString(TabClosingDialogCore.Close)
            DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
        }
        Button {
            text: core.getString(TabClosingDialogCore.Cancel)
            DialogButtonBox.buttonRole: DialogButtonBox.RejectRole
        }
    }

    TabClosingDialogCore {
        id: core
    }
}
