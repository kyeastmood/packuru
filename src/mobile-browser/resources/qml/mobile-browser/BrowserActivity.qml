// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0

import org.kde.kirigami 2.5 as Kirigami

import KirigamiActivities 1.0 as KirigamiActivities

import PackuruCoreBrowser 1.0
import PackuruCore 1.0
import PackuruMobileCore 1.0 as PackuruMobileCore


KirigamiActivities.BasicActivity {
    id: root
    property QtObject browserCore

    pageRow.layers.focus: active && priv.statusPage.showPage
    pageRow.focus: active && !priv.statusPage.showPage
    pageRow.globalToolBar.style: browserCore.uiState === ArchiveBrowserCore.UIState.StatusPage
                                 ? Kirigami.ApplicationHeaderStyle.None
                                 : Kirigami.ApplicationHeaderStyle.Auto

    /* In mobile environment global toolbar is hidden when scrolling down, however
     * when changing directory there seems to be no way to make it visible again,
     * except for scrolling up, but there might be not enouch items in the new
     * folder to perform scroll. Therefore toolbar size is currently fixed. */
    pageRow.globalToolBar.minimumHeight: pageRow.globalToolBar.preferredHeight
    pageRow.globalToolBar.maximumHeight: pageRow.globalToolBar.preferredHeight

    pageRow.initialPage: NavigationPage {
        id: navPage
        enabled: !priv.isBusy

        archiveName: browserCore.archiveName
        navigator: browserCore.navigator

        actionAdd.enabled: browserCore.addAvailable
        actionAdd.text: browserCore.getString(ArchiveBrowserCore.UserString.ActionAdd)
        actionAdd.onTriggered: {
            var core = browserCore.createArchivingDialogCore()
            var dialog = archivingDialogComp.createObject(applicationWindow(), {"dialogCore" : core})
            dialog.destroyOnClose = true
            dialog.headerText = core.getString(ArchivingDialogCore.DialogTitleAdd)
            dialog.open()
        }

        actionDelete.enabled: browserCore.deleteAvailable
        actionDelete.text: browserCore.getString(ArchiveBrowserCore.UserString.ActionDelete)
        actionDelete.onTriggered: {
            var core = browserCore.createDeletionDialogCore()
            var dialog = deletionDialogComp.createObject(applicationWindow(), {"dialogCore" : core})
            dialog.destroyOnClose = true
            dialog.headerText = core.getString(DeletionDialogCore.DialogTitle)
            dialog.open()
        }

        actionFilter.enabled: browserCore.filterAvailable
        actionFilter.text: browserCore.getString(ArchiveBrowserCore.UserString.ActionFilter)

        actionExtract.enabled: browserCore.extractAvailable
        actionExtract.text: browserCore.getString(ArchiveBrowserCore.UserString.ActionExtract)
        actionExtract.onTriggered: {
            var core = browserCore.createExtractionDialogCore()
            var dialog = extractionDialogComp.createObject(applicationWindow(), {"dialogCore" : core})
            dialog.destroyOnClose = true
            dialog.headerText = core.getString(ExtractionDialogCore.DialogTitleSingle)
            dialog.open()
        }

        actionPreview.enabled: browserCore.previewAvailable
        actionPreview.text: browserCore.getString(ArchiveBrowserCore.UserString.ActionPreview)
        actionPreview.onTriggered: browserCore.previewSelectedFiles()

        actionReload.enabled: browserCore.reloadAvailable
        actionReload.text: browserCore.getString(ArchiveBrowserCore.UserString.ActionReload)
        actionReload.onTriggered: browserCore.reloadArchive()

        actionTest.enabled: browserCore.extractAvailable
        actionTest.text: browserCore.getString(ArchiveBrowserCore.UserString.ActionTest)
        actionTest.onTriggered: {
            var core = browserCore.createTestDialogCore()
            var dialog = testDialogComp.createObject(applicationWindow(), {"dialogCore" : core})
            dialog.destroyOnClose = true
            dialog.headerText = core.getString(TestDialogCore.DialogTitleSingle)
            dialog.open()
        }

        actionProperties.text: browserCore.getString(ArchiveBrowserCore.UserString.ActionProperties)
        actionProperties.onTriggered: {
            var dialog = propertiesDialogComp.createObject(applicationWindow(), {
                                                               "destroyOnClose" : true,
                                                               "headerText" : commonStrings.getString(CommonStrings.ArchivePropertiesDialogTitle),
                                                               "propertiesModel" : browserCore.getPropertiesModel(),
                                                               "archiveComment" : browserCore.getArchiveComment() })
            dialog.open()
        }
        Component.onDestruction: {
            browserCore.destroyLater()
        }
    }

    Component {
        id: archivingDialogComp
        PackuruMobileCore.ArchivingDialog {}
    }

    Component {
        id: deletionDialogComp
        DeletionDialog {}
    }

    Component {
        id: extractionDialogComp
        PackuruMobileCore.ExtractionDialog {}
    }

    Component {
        id: propertiesDialogComp
        ArchivePropertiesDialog {}
    }

    Component {
        id: testDialogComp
        PackuruMobileCore.TestDialog {}
    }

    Connections {
        target: browserCore
        function onTaskStateChanged(state) {
            switch (state) {
            case ArchiveBrowserCore.BrowserTaskState.StartingUp:
            case ArchiveBrowserCore.BrowserTaskState.InProgress:
                root.icon = "media-playback-start"
                break
            case ArchiveBrowserCore.BrowserTaskState.Success:
                root.icon = "dialog-ok"
                break
            case ArchiveBrowserCore.BrowserTaskState.Aborted:
            case ArchiveBrowserCore.BrowserTaskState.Errors:
                root.icon = "error"
                break
            case ArchiveBrowserCore.BrowserTaskState.Warnings:
                root.icon = "messagebox_warning"
                break;
            case ArchiveBrowserCore.BrowserTaskState.WaitingForInput:
                root.icon = "question"
                break
            case ArchiveBrowserCore.BrowserTaskState.WaitingForPassword:
                root.icon = "document-encrypted"
                break
            }
        }

        function onFilesReadyForPreview(urls) {
            for (var i = 0; i < urls.length; ++i)
                Qt.openUrlExternally(urls[i])
        }

        function onOpenExtractionDestinationFolder(url) {
            Qt.openUrlExternally(url)
        }
    }

    QtObject {
        id: priv

        readonly property bool isBusy: browserCore ? browserCore.taskBusy : false

        property Item statusPage: ArchiveBrowserStatusPage {
            statusPageCore: browserCore ? browserCore.statusPageCore : null
            img: root.icon
            readonly property bool showPage: browserCore.uiState === ArchiveBrowserCore.UIState.StatusPage
            focus: showPage
            enabled: showPage
            onShowPageChanged: {
                if (showPage)
                    root.pageRow.layers.push(this)
                else
                    root.pageRow.layers.pop()
            }
        }
    }
}
