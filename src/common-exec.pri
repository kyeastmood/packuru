# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

isEmpty(PROJECT_TARGET_DIR) {
    error($$_FILE_: Variable PROJECT_TARGET_DIR has not been set. Included from $$_PRO_FILE_)
}

isEmpty(PROJECT_LIB_DIR_NAME) {
    error($$_FILE_: Variable PROJECT_LIB_DIR_NAME has not been set. Included from $$_PRO_FILE_)
}

TEMPLATE = app

DESTDIR = $$PROJECT_TARGET_DIR 

QMAKE_RPATHDIR += $$PROJECT_LIB_DIR_NAME
