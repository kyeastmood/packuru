// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "compressionhandler.h"


namespace Packuru::Plugins::GnuTar::Core
{

class CompressionHandlerPigz: public CompressionHandler
{
    Q_OBJECT
public:
    CompressionHandlerPigz(QObject* parent = nullptr);

    static QString executableName();

    void compress(const CompressionInitData& data) override;
    void decompress(const DecompressionInitData& data) override;

private:
    void processErrorLineHandler(const std::vector<QStringRef>& lines) override;
};

}
