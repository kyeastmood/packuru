# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)
include(../common-lib.pri)

MODULE_BUILD_NAME_SUFFIX = plugin-gnutar-core
MODULE_BUILD_NAME = $${PROJECT_BUILD_NAME}-$${MODULE_BUILD_NAME_SUFFIX}

include(../embed-qm-files.pri)

TARGET = $$qtLibraryTarget($$MODULE_BUILD_NAME)

LIBS += -L$${PROJECT_TARGET_LIB_DIR} -l$$QCS_CORE_BUILD_NAME -l$${CORE_BUILD_NAME}

TRANSLATIONS += \
    translations/$${MODULE_BUILD_NAME_SUFFIX}_en.ts \

HEADERS += \
    compressionhandler.h \
    compressionhandlerbrotli.h \
    compressionhandlerbzip2.h \
    compressionhandlercompress.h \
    compressionhandlergzip.h \
    compressionhandlerlrzip.h \
    compressionhandlerlz4.h \
    compressionhandlerlzip.h \
    compressionhandlerlzop.h \
    compressionhandlerpbzip2.h \
    compressionhandlerpigz.h \
    compressionhandlerpixz.h \
    compressionhandlerplzip.h \
    compressionhandlertype.h \
    compressionhandlerxz.h \
    compressionhandlerzstd.h \
    compressioninitdata.h \
    createcompressionhandler.h \
    decompressioninitdata.h \
    getcompressioninfofortype.h \
    privatearchivingparametermapgnutar.h \
    privatedeletionparameter.h \
    privatedeletionparametermapgnutar.h \
    privateextractionparameter.h \
    privateextractionparametermapgnutar.h \
    privatereadparameter.h \
    privatereadparametermapgnutar.h \
    privatetestparameter.h \
    privatetestparametermapgnutar.h \
    processhandler.h \
    tarformat.h \
    archivingparametershandler.h \
    backendhandler.h \
    privatearchivingparameter.h \
    sparsefilesctrl.h \
    registerstreamoperators.h \
    tarformatctrl.h \
    threadcountctrl.h \
    tocliarg.h

SOURCES += \
    archivingparametershandler.cpp \
    backendhandler.cpp \
    compressionhandler.cpp \
    compressionhandlerbrotli.cpp \
    compressionhandlerbzip2.cpp \
    compressionhandlercompress.cpp \
    compressionhandlergzip.cpp \
    compressionhandlerlrzip.cpp \
    compressionhandlerlz4.cpp \
    compressionhandlerlzip.cpp \
    compressionhandlerlzop.cpp \
    compressionhandlerpbzip2.cpp \
    compressionhandlerpigz.cpp \
    compressionhandlerpixz.cpp \
    compressionhandlerplzip.cpp \
    compressionhandlertype.cpp \
    compressionhandlerxz.cpp \
    compressionhandlerzstd.cpp \
    compressioninitdata.cpp \
    createcompressionhandler.cpp \
    decompressioninitdata.cpp \
    getcompressioninfofortype.cpp \
    processhandler.cpp \
    sparsefilesctrl.cpp \
    registerstreamoperators.cpp \
    tarformatctrl.cpp \
    threadcountctrl.cpp \
    tocliarg.cpp
