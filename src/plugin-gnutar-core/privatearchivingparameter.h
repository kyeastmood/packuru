// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace Packuru::Plugins::GnuTar::Core
{

enum class PrivateArchivingParameter
{
    CompressionHandlerType, // CompressionHandlerType; required when compressing
    CompressionLevel, // int; required when compressing
    SparseFileDetection, // bool; required
    TarFormat, // TarFormat; required
    ThreadCount // int; required when compressing
};

}

Q_DECLARE_METATYPE(Packuru::Plugins::GnuTar::Core::PrivateArchivingParameter);
