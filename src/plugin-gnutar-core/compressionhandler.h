// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QFileInfo>

#include "processhandler.h"


namespace Packuru::Plugins::GnuTar::Core
{

struct CompressionInitData;
struct DecompressionInitData;
enum class CompressionHandlerType;

class CompressionHandler : public ProcessHandler
{
    Q_OBJECT
public:
    explicit CompressionHandler(CompressionHandlerType type, QObject *parent = nullptr);

    virtual void compress(const CompressionInitData& data) = 0;
    virtual void decompress(const DecompressionInitData& data) = 0;

protected:
    CompressionHandlerType handlerType;
};

}
