// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/backendhandlererror.h"

#include "compressionhandlerlz4.h"
#include "compressioninitdata.h"
#include "decompressioninitdata.h"
#include "tocliarg.h"
#include "compressionhandlertype.h"


using namespace Packuru::Core;


namespace
{
const QString execName("lz4");
}


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandlerLz4::CompressionHandlerLz4(QObject* parent)
    : CompressionHandler(CompressionHandlerType::Lz4, parent)
{

}


QString CompressionHandlerLz4::executableName()
{
    return execName;
}


void CompressionHandlerLz4::compress(const CompressionInitData& data)
{
    storageInfo.setPath(data.archiveInfo.absolutePath());

    QStringList args;
    args << QLatin1String("-c") // from stdin to stdout
         << toCLIargCompressionLevel(data.compressionLevel, handlerType);

    process->setStandardOutputFile(data.archiveInfo.absoluteFilePath());
    process->start(execName, args);
}


void CompressionHandlerLz4::decompress(const DecompressionInitData& data)
{
    QStringList args;
    args << QLatin1String("-dc") << data.archiveInfo.absoluteFilePath();

    process->start(execName, args);
}


void CompressionHandlerLz4::processErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line.startsWith(QLatin1String("Error 66 : Decompression error"))
                || line.startsWith(QLatin1String("Error 68 : Unfinished stream")))
            emit error(BackendHandlerError::DataError);
        else if (line.endsWith(QLatin1String("Write error : cannot write compressed block ")))
        {
            storageInfo.refresh();
            if (storageInfo.bytesAvailable() == 0)
                emit error(BackendHandlerError::DiskFull);
            else
                emit error(BackendHandlerError::WriteError);

        }
    }
}

}
