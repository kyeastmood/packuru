// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/backendhandlererror.h"

#include "compressionhandlergzip.h"
#include "compressioninitdata.h"
#include "decompressioninitdata.h"
#include "tocliarg.h"
#include "compressionhandlertype.h"


using namespace Packuru::Core;


namespace
{
const QString execName("gzip");
}


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandlerGzip::CompressionHandlerGzip(QObject* parent)
    : CompressionHandler(CompressionHandlerType::Gzip, parent)
{

}


QString CompressionHandlerGzip::executableName()
{
    return execName;
}


void CompressionHandlerGzip::compress(const CompressionInitData& data)
{
    QStringList args;
    args << toCLIargCompressionLevel(data.compressionLevel, handlerType);

    process->setStandardOutputFile(data.archiveInfo.absoluteFilePath());
    process->start(execName, args);
}


void CompressionHandlerGzip::decompress(const DecompressionInitData& data)
{
    QStringList args;
    args << QLatin1String("-dc") << data.archiveInfo.absoluteFilePath();

    process->start(execName, args);
}


void CompressionHandlerGzip::processErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line.endsWith(QLatin1String("invalid compressed data--crc error"))
                || line.endsWith(QLatin1String("invalid compressed data--length error"))
                || line.endsWith(QLatin1String("invalid compressed data--format violated"))
                || line.endsWith(QLatin1String("unexpected end of file")))
            emit error(BackendHandlerError::DataError);
        else if (line.endsWith(QLatin1String("No space left on device")))
            emit error(BackendHandlerError::DiskFull);
    }
}

}
