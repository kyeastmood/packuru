// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QThread>

#include "core/private/plugin-api/archivetype.h"
#include "core/private/plugin-api/generatethreadcountlist.h"

#include "threadcountctrl.h"
#include "createcompressionhandler.h"
#include "compressionhandlertype.h"


using Packuru::Core::ArchiveType;
using namespace qcs::core;


namespace Packuru::Plugins::GnuTar::Core
{

ThreadCountCtrl::ThreadCountCtrl(Packuru::Core::ArchiveType archiveType)
{
    Q_ASSERT(archiveType != ArchiveType::tar);

    setControllerType(ControllerType::Type::ComboBox);

    const CompressionHandlerType handlerType = getCompressionHandlers(archiveType,
                                                                      CompressionHandlerPickMode::SelectBest).front().handlerType;

    const int minThreadCount = 1;
    int maxThreadCount = 1;

    switch (handlerType)
    {
    case CompressionHandlerType::Lrzip:
    case CompressionHandlerType::Pbzip2:
    case CompressionHandlerType::Pigz:
    case CompressionHandlerType::Pixz:
    case CompressionHandlerType::Plzip:
    case CompressionHandlerType::Zstd:
        maxThreadCount = QThread::idealThreadCount();
        break;
    case CompressionHandlerType::Xz:
        // Compression to lzma format seems to be single-threaded
        if (archiveType == ArchiveType::tar_xz)
            maxThreadCount = QThread::idealThreadCount();
        break;
    default:
        break;
    }

    PrivateValueList privateList = Packuru::Core::generateThreadCountList(maxThreadCount);
    PublicValueList publicList;
    publicList.reserve(privateList.size());

    for (int value : privateList)
        publicList.push_back(QString::number(value));

    setValueLists(publicList, privateList);

    // Don't use all threads by default as the system can become unusable
    int setValue = QThread::idealThreadCount() / 2;

    if (setValue > maxThreadCount)
        setValue = maxThreadCount;

    setCurrentValue(QString::number(setValue), setValue);
    setEnabled(maxThreadCount - minThreadCount > 0);
}

}
