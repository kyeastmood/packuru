// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/backendhandlererror.h"

#include "compressionhandlerpigz.h"
#include "compressioninitdata.h"
#include "decompressioninitdata.h"
#include "tocliarg.h"
#include "compressionhandlertype.h"


using namespace Packuru::Core;


namespace
{
const QString execName("pigz");
}


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandlerPigz::CompressionHandlerPigz(QObject* parent)
    : CompressionHandler(CompressionHandlerType::Pigz, parent)
{

}


QString CompressionHandlerPigz::executableName()
{
    return execName;
}


void CompressionHandlerPigz::compress(const CompressionInitData& data)
{
    QStringList args;
    args << QLatin1String("-c") // from stdin to stdout
         << toCLIargCompressionLevel(data.compressionLevel, handlerType)
         << toCLIargThreadCount(data.requestedThreadCount, handlerType);

    process->setStandardOutputFile(data.archiveInfo.absoluteFilePath());
    process->start(execName, args);
}


void CompressionHandlerPigz::decompress(const DecompressionInitData& data)
{
    QStringList args;
    args << QLatin1String("-dc")
         // Decompression in pigz is only partially parallel, read the man page.
         << toCLIargThreadCount(data.requestedThreadCount, handlerType)
         << data.archiveInfo.absoluteFilePath();

    process->start(execName, args);
}


void CompressionHandlerPigz::processErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line.endsWith(QLatin1String("corrupted -- crc32 mismatch"))
                || line.endsWith(QLatin1String("corrupted -- invalid deflate data (invalid literal/lengths set)"))
                || line.endsWith(QLatin1String("corrupted -- incomplete deflate data")))
            emit error(BackendHandlerError::DataError);
        else if (line.endsWith(QLatin1String("(No space left on device)")))
            emit error(BackendHandlerError::DiskFull);
    }
}

}
