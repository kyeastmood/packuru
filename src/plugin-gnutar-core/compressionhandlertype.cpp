// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "compressionhandlertype.h"


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandlerType fromExecName(const QString& name)
{
    if (name == QLatin1String("brotli"))
        return CompressionHandlerType::Brotli;

    else if (name == QLatin1String("bzip2"))
        return CompressionHandlerType::Bzip2;

    else if (name == QLatin1String("compress"))
        return CompressionHandlerType::Compress;

    else if (name == QLatin1String("gzip"))
        return CompressionHandlerType::Gzip;

    else if (name == QLatin1String("lrzip"))
        return CompressionHandlerType::Lrzip;

    else if (name == QLatin1String("lz4"))
        return CompressionHandlerType::Lz4;

    else if (name == QLatin1String("lzip"))
        return CompressionHandlerType::Lzip;

    else if (name == QLatin1String("lzop"))
        return CompressionHandlerType::Lzop;

    else if (name == QLatin1String("pbzip2"))
        return CompressionHandlerType::Pbzip2;

    else if (name == QLatin1String("pigz"))
        return CompressionHandlerType::Pigz;

    else if (name == QLatin1String("pixz"))
        return CompressionHandlerType::Pixz;

    else if (name == QLatin1String("plzip"))
        return CompressionHandlerType::Plzip;

    else if (name == QLatin1String("xz"))
        return CompressionHandlerType::Xz;

    else if (name == QLatin1String("zstd"))
        return CompressionHandlerType::Zstd;

    return CompressionHandlerType::___INVALID;
}

}
