// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "compressionhandler.h"


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandler::CompressionHandler(CompressionHandlerType type, QObject* parent)
    : ProcessHandler(parent),
      handlerType(type)
{

}

}
