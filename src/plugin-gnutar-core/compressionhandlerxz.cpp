// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/archivetype.h"
#include "../core/private/plugin-api/backendhandlererror.h"

#include "compressionhandlerxz.h"
#include "compressioninitdata.h"
#include "decompressioninitdata.h"
#include "tocliarg.h"
#include "compressionhandlertype.h"


using namespace Packuru::Core;


namespace
{
const QString execName("xz");
}


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandlerXz::CompressionHandlerXz(QObject* parent)
    : CompressionHandler(CompressionHandlerType::Xz, parent)
{
}


QString CompressionHandlerXz::executableName()
{
    return execName;
}


void CompressionHandlerXz::compress(const CompressionInitData& data)
{
    Q_ASSERT(data.archiveType == ArchiveType::tar_lzma || data.archiveType == ArchiveType::tar_xz);

    QStringList args;
    args << toCLIargCompressionLevel(data.compressionLevel, handlerType);

    if (data.archiveType == ArchiveType::tar_lzma)
        args << QLatin1String("--format=lzma");
    // Compression to lzma format seems to be single-threaded so this will have no effect
    args << toCLIargThreadCount(data.requestedThreadCount, handlerType);

    process->setStandardOutputFile(data.archiveInfo.absoluteFilePath());
    process->start(execName, args);
}


void CompressionHandlerXz::decompress(const DecompressionInitData& data)
{
    Q_ASSERT(data.archiveType == ArchiveType::tar_lzma || data.archiveType == ArchiveType::tar_xz);

    QStringList args;
    if (data.archiveType == ArchiveType::tar_lzma)
        args << QLatin1String("--format=lzma");
    // Threaded decompression in xz has not been implemented yet
    args << QLatin1String("-dc") << data.archiveInfo.absoluteFilePath();

    process->start(execName, args);
}


void CompressionHandlerXz::processErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line.endsWith(": Compressed data is corrupt")
                || line.endsWith(": File format not recognized")
                || line.endsWith(": Unexpected end of input"))
            emit error(BackendHandlerError::DataError);
        else if (line.endsWith(QLatin1String("No space left on device")))
            emit error(BackendHandlerError::DiskFull);
    }
}

}
