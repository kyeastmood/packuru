// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDir>
#include <QThread>
#include <QRegularExpression>
#include <QStandardPaths>

#include "../utils/qvariant_utils.h"
#include "../utils/private/unordered_map_utils.h"
#include "../utils/private/qvariantunorderedmap.h"

#include "../core/private/plugin-api/backendreaddata.h"
#include "../core/private/plugin-api/backendextractiondata.h"
#include "../core/private/plugin-api/backendtestdata.h"
#include "../core/private/plugin-api/backendarchivingdata.h"
#include "../core/private/plugin-api/backenddeletiondata.h"
#include "../core/private/plugin-api/backendexitcodeinterpretation.h"
#include "../core/private/plugin-api/textstreampreprocessor.h"
#include "../core/private/plugin-api/backendarchiveproperty.h"
#include "../core/private/plugin-api/multipartarchivehelper.h"

#include "backendhandler.h"
#include "privatearchivingparametermapgnutar.h"
#include "privatedeletionparametermapgnutar.h"
#include "privateextractionparametermapgnutar.h"
#include "privatereadparametermapgnutar.h"
#include "privatetestparametermapgnutar.h"
#include "privatearchivingparameter.h"
#include "privatedeletionparameter.h"
#include "privateextractionparameter.h"
#include "privatereadparameter.h"
#include "privatetestparameter.h"
#include "tarformat.h"
#include "createcompressionhandler.h"
#include "compressionhandler.h"
#include "compressionhandlertype.h"
#include "compressioninitdata.h"
#include "decompressioninitdata.h"
#include "tocliarg.h"


using namespace Packuru::Core;


namespace Packuru::Plugins::GnuTar::Core
{

BackendHandler::BackendHandler(QObject* parent)
    : CLIBackendHandler(parent),
      archiveType(ArchiveType::___NOT_SUPPORTED),
      compressionHandler(nullptr)
{
    connect(errPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::errorLineHandler);
}


bool BackendHandler::isTarExecAvailable()
{
    return !QStandardPaths::findExecutable("tar").isEmpty();
}


QString BackendHandler::createSinglePartArchiveName(const QString& archiveBaseName, ArchiveType archiveType) const
{
    QString extension;

    switch (archiveType)
    {
    case ArchiveType::tar_brotli: extension = QLatin1String("tar.br"); break;
    case ArchiveType::tar_bzip2: extension = QLatin1String("tar.bz2"); break;
    case ArchiveType::tar_gzip: extension = QLatin1String("tar.gz"); break;
    case ArchiveType::tar_lrzip: extension = QLatin1String("tar.lrz"); break;
    case ArchiveType::tar_lz4: extension = QLatin1String("tar.lz4"); break;
    case ArchiveType::tar_lzip: extension = QLatin1String("tar.lz"); break;
    case ArchiveType::tar_lzma: extension = QLatin1String("tar.lzma"); break;
    case ArchiveType::tar: extension = QLatin1String("tar"); break;
    case ArchiveType::tar_compress: extension = QLatin1String("tar.Z"); break;
    case ArchiveType::tar_lzop: extension = QLatin1String("tar.lzo"); break;
    case ArchiveType::tar_xz: extension = QLatin1String("tar.xz"); break;
    case ArchiveType::tar_zstd: extension = QLatin1String("tar.zst"); break;
    default: Q_ASSERT(false);
    }

    return archiveBaseName + QLatin1Char('.') + extension;
}



QString BackendHandler::createMultiPartArchiveName(const QString& archiveBaseName,
                                                   ArchiveType archiveType) const
{
    switch (archiveType)
    {
    case ArchiveType::tar_lzip:
        return archiveBaseName + QLatin1String(".tar.part00001.lz");
    default:
        Q_ASSERT(false); return QString();
    }
}


QString BackendHandler::createMultiPartArchiveRegexNamePattern(const QString& archiveBaseName,
                                                               ArchiveType archiveType) const
{
    Q_ASSERT(archiveType == ArchiveType::tar_lzip);
    const QString name = QRegularExpression::escape(archiveBaseName);
    // Exclude *.tar.lz0.lz, *.tar.lz00.lz, etc.
    const QString pattern = name + QLatin1String("\\.tar\\.part(?!0+\\.lz$)(\\d{1,}\\.lz$)");
    return pattern;
}


QStringList BackendHandler::getCompressionHandlerArguments() const
{
    if (compressionHandler)
    {
        QStringList args {compressionHandler->getProcess()->program()};
        args << compressionHandler->getProcess()->arguments();
        return args;
    }

    return QStringList();
}


void BackendHandler::addToArchiveImpl(const BackendArchivingData& data)
{
    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &BackendHandler::onProcessFinished);

    QStringList args;
    const auto privateData = Utils::getValueAs<PrivateArchivingParameterMap>(data.pluginPrivateData);

    Q_ASSERT(data.destinationPath.length() > 0);
    QString destinationPath = data.destinationPath;
    if (!destinationPath.endsWith(QDir::separator()))
        destinationPath += QDir::separator();

    if (data.createNewArchive)
    {
        args << QLatin1String("-c") // Create archive
             << QLatin1String("-H"); // Set tar format

        const auto format = Utils::getValueAs<TarFormat>(Utils::getValue(privateData,
                                                                         PrivateArchivingParameter::TarFormat));

        args << toCLIargTarFormat(format);

        const auto detectSparseFiles
                = Utils::getValueAs<bool>(Utils::getValue(privateData,
                                                          PrivateArchivingParameter::SparseFileDetection));

        const QString sparseFilesArg = toCLIArgSparseFiles(detectSparseFiles);
        if (!sparseFilesArg.isEmpty())
            args << sparseFilesArg;

        const QString archiveName = createSinglePartArchiveName(data.archiveName, data.archiveType);
        const QString absArchivePath = destinationPath + archiveName;

        // Uncompressed tar archive will be written to a file directly by tar
        if (data.archiveType == ArchiveType::tar)
        {
            args << QLatin1String("-f") // Set file
                 << absArchivePath;
        }
        // Tar will write to standard output for compressor
        else
        {
            auto handlerType = Utils::tryValueAs(privateData,
                                                 PrivateArchivingParameter::CompressionHandlerType,
                                                 CompressionHandlerType::___INVALID);

            if (handlerType == CompressionHandlerType::___INVALID)
            {
                const auto list = getCompressionHandlers(data.archiveType, CompressionHandlerPickMode::SelectBest);
                Q_ASSERT(list.size() == 1);
                handlerType = list.front().handlerType;
            }

            compressionHandler = createConnectCompressionHandler(handlerType);
            if (!compressionHandler)
                return;

            backendProcess->setStandardOutputProcess(compressionHandler->getProcess());

            CompressionInitData initData;
            initData.archiveInfo.setFile(absArchivePath);
            initData.archiveType = data.archiveType;
            initData.partSize = data.partSize;
            initData.compressionLevel
                    = Utils::getValueAs<int>(Utils::getValue(privateData,
                                                             PrivateArchivingParameter::CompressionLevel));
            initData.requestedThreadCount
                    = Utils::getValueAs<int>(Utils::getValue(privateData,
                                                             PrivateArchivingParameter::ThreadCount));
            initData.password = data.password;

            // Will wait for input from tar
            compressionHandler->compress(initData);
        }
    }
    // Only uncompressed tar archive can be modified
    else
    {
        Q_ASSERT(data.archiveType == ArchiveType::tar);
        args << QLatin1String("-u") // Update/append
             << QLatin1String("-f") // Set file
             << destinationPath + data.archiveName;
    }

    switch (data.filePaths)
    {
    case ArchivingFilePaths::AbsolutePaths:
    case ArchivingFilePaths::FullPaths:
    {
        if (data.filePaths == ArchivingFilePaths::AbsolutePaths)
            args << QLatin1String("-P");
        for (const auto& item : data.inputItems)
            args << QLatin1String("--add-file=") + item.absoluteFilePath();
        break;
    }
    case ArchivingFilePaths::RelativePaths:
    {
        for (const auto& item : data.inputItems)
            args << QLatin1String("-C") << item.absolutePath() // Change dir
                 << QLatin1String("--add-file=") + item.fileName();
        break;
    }
    default:
        Q_ASSERT(false);
    }

    startBackend(args);
}


void BackendHandler::deleteFilesImpl(const BackendDeletionData& data)
{
    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &BackendHandler::onProcessFinished);

    QStringList args;

    args << QLatin1String("-f") << data.archiveInfo.absoluteFilePath()
         << QLatin1String("--delete");

    for (const auto& item : data.absoluteFilePaths)
        args << item;

    startBackend(args);
}


void BackendHandler::extractArchiveImpl(const BackendExtractionData& data)
{
    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &BackendHandler::onProcessFinished);

    // Currently private data is not required (only set by unit tests)
    const auto privateData = data.pluginPrivateData.value<PrivateExtractionParameterMap>();

    QStringList args{QLatin1String("-xC"), data.tempDestinationPath}; // Change dir

    if (data.archiveType == ArchiveType::tar)
    {
        // Tar will process archive directly
        args << QLatin1String("-f") << data.archiveInfo.absoluteFilePath();
    }
    else
    {
        auto handlerType = Utils::tryValueAs(privateData,
                                             PrivateExtractionParameter::CompressionHandlerType,
                                             CompressionHandlerType::___INVALID);

        if (handlerType == CompressionHandlerType::___INVALID)
        {
            const auto list = getCompressionHandlers(data.archiveType, CompressionHandlerPickMode::SelectBest);
            Q_ASSERT(list.size() == 1);
            handlerType = list.front().handlerType;
        }

        compressionHandler = createConnectCompressionHandler(handlerType);
        if (!compressionHandler)
            return;

        compressionHandler->setStandardOutputProcess(backendProcess);
    }

    for (const auto& name : data.filesToExtract)
        args << name;

    startBackend(args);

    if (compressionHandler)
    {
        DecompressionInitData initData;
        initData.archiveInfo = data.archiveInfo;
        initData.archiveType = data.archiveType;
        initData.password = data.password;
        compressionHandler->decompress(initData);
    }

}


void BackendHandler::readArchiveImpl(const BackendReadData& data)
{
    archiveFilePath = data.archiveInfo.absoluteFilePath();
    archiveType = data.archiveType;

    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &BackendHandler::onListProcessFinished);

    connect(outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::listHandler);

    archiveProperties[BackendArchiveProperty::TotalSize] = data.archiveInfo.size();

    // Currently private data is not required (only set by unit tests)
    const auto privateData = data.pluginPrivateData.value<PrivateReadParameterMap>();

    if (data.archiveType == ArchiveType::tar)
    {
        startBackend({QLatin1String("-tvf"), data.archiveInfo.absoluteFilePath()});
    }
    else
    {
        auto handlerType = Utils::tryValueAs(privateData,
                                             PrivateReadParameter::CompressionHandlerType,
                                             CompressionHandlerType::___INVALID);

        if (handlerType == CompressionHandlerType::___INVALID)
        {
            const auto list = getCompressionHandlers(data.archiveType, CompressionHandlerPickMode::SelectBest);
            Q_ASSERT(list.size() == 1);
            handlerType = list.front().handlerType;
        }

        compressionHandler = createConnectCompressionHandler(handlerType);
        if (!compressionHandler)
            return;

        compressionHandler->setStandardOutputProcess(backendProcess);
        startBackend({QLatin1String("-tv")});

        DecompressionInitData initData;
        initData.archiveInfo = data.archiveInfo;
        initData.archiveType = data.archiveType;
        initData.password = data.password;
        compressionHandler->decompress(initData);
    }
}


void BackendHandler::testArchiveImpl(const BackendTestData& data)
{
    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &BackendHandler::onProcessFinished);

    // Currently private data is not required (only set by unit tests)
    const auto privateData = data.pluginPrivateData.value<PrivateTestParameterMap>();

    backendProcess->setStandardOutputFile(QProcess::nullDevice());

    if (data.archiveType == ArchiveType::tar)
    {
        startBackend({QLatin1String("-xf"),
                      data.archiveInfo.absoluteFilePath(),
                      QLatin1String("-O")}); // Write to standard output (/dev/null)
    }
    else
    {
        auto handlerType = Utils::tryValueAs(privateData,
                                             PrivateTestParameter::CompressionHandlerType,
                                             CompressionHandlerType::___INVALID);

        if (handlerType == CompressionHandlerType::___INVALID)
        {
            const auto list = getCompressionHandlers(data.archiveType, CompressionHandlerPickMode::SelectBest);
            Q_ASSERT(list.size() == 1);
            handlerType = list.front().handlerType;
        }

        compressionHandler = createConnectCompressionHandler(handlerType);
        if (!compressionHandler)
            return;

        compressionHandler->setStandardOutputProcess(backendProcess);

        startBackend({QLatin1String("-x"),
                      QLatin1String("-O")}); // Write to standard output (/dev/null)

        DecompressionInitData initData;
        initData.archiveInfo = data.archiveInfo;
        initData.archiveType = data.archiveType;
        initData.password = data.password;
        compressionHandler->decompress(initData);
    }
}


void BackendHandler::stopImpl()
{
    CLIBackendHandler::stopImpl();

    if (compressionHandler)
        compressionHandler->getProcess()->close();
}


void BackendHandler::onNewError(BackendHandlerError error)
{
    if (error == BackendHandlerError::DiskFull)
    {
        Q_ASSERT(isWritingTask(getTaskType()));

        if (compressionHandler)
        {
            auto process = compressionHandler->getProcess();
            if  (process->state() != QProcess::NotRunning)
            {
                addLogLine("Disk full, aborting process " + process->program());
                process->kill();
            }
        }
    }

    CLIBackendHandler::onNewError(error);
}


CompressionHandler* BackendHandler::createConnectCompressionHandler(CompressionHandlerType handlerType)
{
    const CompressionHandlerCreationResult result = createCompressionHandler(handlerType, this);

    if (!result)
    {
        addError(BackendHandlerError::SubmoduleNotFound, result.errorInfo);
        changeState(BackendHandlerState::Errors);
        return nullptr;
    }

    auto handler = result.handler;

    // If encryption is ever supported make sure that password is not logged
    connect(handler, &ProcessHandler::processStarted,
            this, &BackendHandler::logCommand);

    connect(handler, &ProcessHandler::processFinished,
            this, &BackendHandler::onCompressionHandlerFinished);

    using AddErrorType = void(AbstractBackendHandler::*)(BackendHandlerError);

    connect(handler, &ProcessHandler::error,
            this, static_cast<AddErrorType>(&BackendHandler::addError));

    using AddLogLineType = void(AbstractBackendHandler::*)(const QString&);

    connect(handler, &ProcessHandler::logLine,
            this, static_cast<AddLogLineType>(&BackendHandler::addLogLine));

    using AddLogLinesType = void(AbstractBackendHandler::*)(const std::vector<QStringRef>&);

    connect(handler, &ProcessHandler::logLines,
            this, static_cast<AddLogLinesType>(&BackendHandler::addLogLines));

    return handler;
}


void BackendHandler::startBackend(const QStringList& gnuTarParameters)
{
    changeState(BackendHandlerState::InProgress);

    QStringList gnuTarGlobalSwitches;

    auto args = QStringList() << gnuTarParameters << gnuTarGlobalSwitches;

    logCommand("tar", args);

    backendProcess->start("tar", args);
}


void BackendHandler::onListProcessFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    if (archiveType == ArchiveType::tar_lzip)
    {
        if (isMultiPartArchiveName(archiveFilePath, archiveType))
        {
            archiveProperties[BackendArchiveProperty::Multipart] = true;
            archiveProperties[BackendArchiveProperty::PartCount] = getArchiveExistingPartCount(archiveFilePath,
                                                                                               archiveType);
            archiveProperties[BackendArchiveProperty::TotalSize] = computeArchiveSize(archiveFilePath,
                                                                                      archiveType);
        }
        else
            archiveProperties[BackendArchiveProperty::Multipart] = false;
    }

    onProcessFinished(exitCode, exitStatus);
}


void BackendHandler::onProcessFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    Q_UNUSED(exitStatus)

    /* When creating archive and compressing in multi-threaded mode (e.g. xz, zstd)
     * tar exits much earlier while the compressor is still running. So we have to wait for
     * it to finish before we signal an end state.
     * Also sometimes compression process is still in the Starting state when this function is executed.
     * This occurs when compressor finishes quickly, so it's already done but QProcess has not
     * been updated yet. So we have to check explicitly for NotRunning state.*/
    if (compressionHandler && compressionHandler->getProcess()->state() != QProcess::NotRunning)
        return;

    const auto intepretation = exitCode == 0 ? BackendExitCodeInterpretation::Success
                                             : BackendExitCodeInterpretation::WarningsOrErrors;

    const BackendHandlerState state = handleFinishedProcess(intepretation);

    changeState(state);
}


void BackendHandler::onCompressionHandlerFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    Q_UNUSED(exitCode)
    Q_UNUSED(exitStatus)

    addLogLine(compressionHandler->getProcess()->program()
               + " exit code = "+ QString::number(backendProcess->exitCode()));

    if (backendProcess->state() == QProcess::NotRunning && isStateBusy(getState()))
        onProcessFinished(backendProcess->exitCode(), backendProcess->exitStatus());
}


void BackendHandler::errorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        auto error = BackendHandlerError::___INVALID;

        if (line.endsWith(QLatin1String("tar: This does not look like a tar archive"))
                || line.endsWith(QLatin1String("tar: Skipping to next header"))
                || line.endsWith(QLatin1String("tar: Unexpected EOF in archive")))
            error = BackendHandlerError::DataError;
        // When adding files to archive
        else if (line.endsWith(QLatin1String("Cannot stat: No such file or directory")))
            error = BackendHandlerError::FileNotFound;
        else if (line.endsWith(QLatin1String(": Cannot open: File exists")))
            error = BackendHandlerError::ItemTypeMismatch;
        else if (line.endsWith(QLatin1String(": Cannot open: Permission denied")))
            error = BackendHandlerError::AccessDenied;
        else if ((line.endsWith(QLatin1String("bytes")) && line.contains(QLatin1String(": Wrote only")))
                 || line.endsWith(QLatin1String("No space left on device")))
            error = BackendHandlerError::DiskFull;

        if (error != BackendHandlerError::___INVALID)
            addError(error, line);
        else
            addLogLine(line);
    }
}


void BackendHandler::listHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        tempItem.attributes = line.left(10).toString();

        if (tempItem.attributes.startsWith(QLatin1Char('d')))
            tempItem.nodeType = NodeType::Folder;
        else if (tempItem.attributes.startsWith(QLatin1Char('l')))
            tempItem.nodeType = NodeType::Link;

        const int firstSpace = 10;

        if (line.size() < firstSpace + 1 || !(line[firstSpace] == QLatin1Char(' ')))
            continue;

        const int userGroupDelimiter = line.indexOf(QLatin1Char('/'), firstSpace);

        if (userGroupDelimiter < 0)
            continue;

        tempItem.user = line.mid(firstSpace + 1, userGroupDelimiter - firstSpace - 1).toString();

        const int secondSpace = line.indexOf(QLatin1Char(' '), userGroupDelimiter);
        if (secondSpace < 0)
            continue;

        tempItem.group = line.mid(userGroupDelimiter + 1, secondSpace - userGroupDelimiter - 1).toString();

        int sizeBegin = secondSpace;

        while (++sizeBegin < line.size())
        {
            if (line[sizeBegin].isDigit())
                break;
        }

        const int spaceAfterSize = line.indexOf(QLatin1Char(' '), sizeBegin);
        if (spaceAfterSize < 0)
            continue;

        tempItem.originalSize = line.mid(sizeBegin, spaceAfterSize - sizeBegin).toULongLong();

        const int timeBegin = spaceAfterSize + 1;
        const int timeLength = 16;

        const QString time = line.mid(timeBegin, timeLength).toString();
        if (time.length() < timeLength)
            continue;

        tempItem.modified = QDateTime::fromString(time, Qt::ISODate);

        const int pathBegin = timeBegin + timeLength + 1;
        const int pathLength = line.size() - pathBegin;
        if (pathLength <= 0)
            continue;

        tempItem.name = line.right(pathLength).toString();
        if (tempItem.name.endsWith("/"))
            tempItem.name.chop(1);

        if (tempItem.nodeType == NodeType::Link)
        {
            /* This is not a completely reliable method of removing the link's destination path
               as the link's path might contain the " -> " string; in such case the link's path
               will be incorrectly truncated. The control string could also contain the root directory
               character (" -> /") to limit false positives but it might not work if the archive
               was created on platform other then Unix. */
            const auto pos = tempItem.name.indexOf(QLatin1String(" -> "));
            if (pos > 0)
                tempItem.name.truncate(pos);
        }

        items.emplace_back(std::move(tempItem));
    }

    if (!items.empty())
        emit newItems(items);
}

}
