// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/backendhandlererror.h"

#include "compressionhandlercompress.h"
#include "compressioninitdata.h"
#include "decompressioninitdata.h"
#include "compressionhandlertype.h"


using namespace Packuru::Core;


namespace
{
const QString execName("compress");
}


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandlerCompress::CompressionHandlerCompress(QObject* parent)
    : CompressionHandler(CompressionHandlerType::Compress, parent)
{

}


QString CompressionHandlerCompress::executableName()
{
    return execName;
}


void CompressionHandlerCompress::compress(const CompressionInitData& data)
{
    storageInfo.setPath(data.archiveInfo.absolutePath());

    QStringList args;

    process->setStandardOutputFile(data.archiveInfo.absoluteFilePath());
    process->start(execName, args);
}


void CompressionHandlerCompress::decompress(const DecompressionInitData& data)
{
    QStringList args;
    args << QLatin1String("-dc") << data.archiveInfo.absoluteFilePath();

    process->start(execName, args);
}


void CompressionHandlerCompress::processErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line.endsWith("corrupt input"))
            emit error(BackendHandlerError::DataError);
        else if (line.endsWith(QLatin1String("No space left on device")))
            emit error(BackendHandlerError::DiskFull);
        // Sometimes the error message says "write error onstdout: Success"
        else if (line.startsWith(QLatin1String("write error")))
        {
            storageInfo.refresh();
            if (storageInfo.bytesAvailable() == 0)
                emit error(BackendHandlerError::DiskFull);
        }
    }
}

}
