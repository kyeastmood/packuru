// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once


namespace Packuru::Core
{
enum class ArchiveType;
struct ArchivingCompressionInfo;
}

namespace Packuru::Plugins::GnuTar::Core
{

Packuru::Core::ArchivingCompressionInfo getCompressionInfoForType(Packuru::Core::ArchiveType archiveType);

}
