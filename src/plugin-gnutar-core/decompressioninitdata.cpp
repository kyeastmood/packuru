// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QThread>

#include "core/private/plugin-api/archivetype.h"

#include "decompressioninitdata.h"


namespace Packuru::Plugins::GnuTar::Core
{

DecompressionInitData::DecompressionInitData()
    : archiveType(Packuru::Core::ArchiveType::___NOT_SUPPORTED),
      requestedThreadCount(QThread::idealThreadCount())
{

}

}
