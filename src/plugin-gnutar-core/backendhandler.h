// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QProcess>

#include "../core/private/plugin-api/clibackendhandler.h"


namespace Packuru::Plugins::GnuTar::Core
{

class CompressionHandler;
enum class CompressionHandlerType;
struct CompressionHandlerExecInfo;


class BackendHandler : public Packuru::Core::CLIBackendHandler
{
public:
    BackendHandler(QObject* parent = nullptr);

    static bool isTarExecAvailable();

    QString createSinglePartArchiveName(const QString& archiveBaseName,
                                        Packuru::Core::ArchiveType archiveType) const override;
    QString createMultiPartArchiveName(const QString& archiveBaseName,
                                       Packuru::Core::ArchiveType archiveType) const override;
    QString createMultiPartArchiveRegexNamePattern(const QString& archiveBaseName,
                                                   Packuru::Core::ArchiveType archiveType) const override;

    // Program name at index 0; empty list if compression handler has not been used
    QStringList getCompressionHandlerArguments() const;

private:
    void addToArchiveImpl(const Packuru::Core::BackendArchivingData& data) override;
    void deleteFilesImpl(const Packuru::Core::BackendDeletionData& data) override;
    void extractArchiveImpl(const Packuru::Core::BackendExtractionData& data) override;
    void readArchiveImpl(const Packuru::Core::BackendReadData& data) override;
    void testArchiveImpl(const Packuru::Core::BackendTestData& data) override;
    void stopImpl() override;
    void onNewError(Packuru::Core::BackendHandlerError error) override;

    CompressionHandler* createConnectCompressionHandler(CompressionHandlerType handlerType);

    void startBackend(const QStringList& gnuTarParameters);

    void onListProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onCompressionHandlerFinished(int exitCode, QProcess::ExitStatus exitStatus);

    void errorLineHandler(const std::vector<QStringRef>& lines);
    void listHandler(const std::vector<QStringRef>& lines);

    QString archiveFilePath;
    Packuru::Core::ArchiveType archiveType;
    CompressionHandler* compressionHandler;
    Packuru::Core::ArchiveItem tempItem;
    qulonglong dataSize = 0;
};

}
