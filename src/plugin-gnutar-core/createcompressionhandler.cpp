// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <array>
#include <QStandardPaths>

#include "../core/private/plugin-api/archivetype.h"

#include "createcompressionhandler.h"
#include "compressionhandlertype.h"
#include "compressionhandlerbrotli.h"
#include "compressionhandlerbzip2.h"
#include "compressionhandlercompress.h"
#include "compressionhandlergzip.h"
#include "compressionhandlerlrzip.h"
#include "compressionhandlerlz4.h"
#include "compressionhandlerlzip.h"
#include "compressionhandlerlzop.h"
#include "compressionhandlerpbzip2.h"
#include "compressionhandlerpigz.h"
#include "compressionhandlerpixz.h"
#include "compressionhandlerplzip.h"
#include "compressionhandlerxz.h"
#include "compressionhandlerzstd.h"


using namespace Packuru::Core;


namespace Packuru::Plugins::GnuTar::Core
{

std::vector<CompressionHandlerExecInfo> getCompressionHandlers(Packuru::Core::ArchiveType archiveType,
                                                               CompressionHandlerPickMode pickMode)
{
    Q_ASSERT(isTar(archiveType));

    // Types should be ordered from best to worst
    std::vector<CompressionHandlerType> temp;

    switch (archiveType)
    {
    case ArchiveType::tar:
        return {};
    case ArchiveType::tar_brotli:
        temp.push_back(CompressionHandlerType::Brotli);
        break;
    case ArchiveType::tar_bzip2:
        temp.push_back(CompressionHandlerType::Pbzip2);
        temp.push_back(CompressionHandlerType::Bzip2);
        break;
    case ArchiveType::tar_compress:
        temp.push_back(CompressionHandlerType::Compress);
        break;
    case ArchiveType::tar_gzip:
        temp.push_back(CompressionHandlerType::Pigz);
        temp.push_back(CompressionHandlerType::Gzip);
        break;
    case ArchiveType::tar_lrzip:
        temp.push_back(CompressionHandlerType::Lrzip);
        break;
    case ArchiveType::tar_lz4:
        temp.push_back(CompressionHandlerType::Lz4);
        break;
    case ArchiveType::tar_lzip:
        temp.push_back(CompressionHandlerType::Plzip);
        temp.push_back(CompressionHandlerType::Lzip);
        break;
    case ArchiveType::tar_lzma:
        temp.push_back(CompressionHandlerType::Xz);
        break;
    case ArchiveType::tar_lzop:
        temp.push_back(CompressionHandlerType::Lzop);
        break;
    case ArchiveType::tar_xz:
        temp.push_back(CompressionHandlerType::Pixz);
        temp.push_back(CompressionHandlerType::Xz);
        break;
    case ArchiveType::tar_zstd:
        temp.push_back(CompressionHandlerType::Zstd);
        break;
    default:
        Q_ASSERT(false);
    }

    Q_ASSERT(!temp.empty());

    std::vector<CompressionHandlerExecInfo> result;

    if (pickMode == CompressionHandlerPickMode::SelectBest)
    {
        while (temp.size() > 1)
        {
            const auto type = temp.front();
            if (getCompressionHandlerExecInfo(type).available)
                break;
            else
                temp.erase(temp.begin());
        }

        result.push_back(getCompressionHandlerExecInfo(temp.front()));
        return result;
    }

    // Can remove all elements
    if (pickMode == CompressionHandlerPickMode::OnlyAvailable)
    {
        const auto newEnd = std::remove_if(temp.begin(), temp.end(),
                                           [] (auto type) { return !getCompressionHandlerExecInfo(type).available; });
        temp.erase(newEnd, temp.end());
    }

    for (const auto handlerType : temp)
        result.push_back(getCompressionHandlerExecInfo(handlerType));

    return result;
}


CompressionHandlerExecInfo getCompressionHandlerExecInfo(CompressionHandlerType handlerType)
{
    CompressionHandlerExecInfo result;
    result.handlerType = handlerType;

    switch (handlerType)
    {
    case CompressionHandlerType::Brotli:
        result.execName = CompressionHandlerBrotli::executableName(); break;
    case CompressionHandlerType::Bzip2:
        result.execName = CompressionHandlerBzip2::executableName(); break;
    case CompressionHandlerType::Compress:
        result.execName = CompressionHandlerCompress::executableName(); break;
    case CompressionHandlerType::Gzip:
        result.execName = CompressionHandlerGzip::executableName(); break;
    case CompressionHandlerType::Lrzip:
        result.execName = CompressionHandlerLrzip::executableName(); break;
    case CompressionHandlerType::Lz4:
        result.execName = CompressionHandlerLz4::executableName(); break;
    case CompressionHandlerType::Lzip:
        result.execName = CompressionHandlerLzip::executableName(); break;
    case CompressionHandlerType::Lzop:
        result.execName = CompressionHandlerLzop::executableName(); break;
    case CompressionHandlerType::Pbzip2:
        result.execName = CompressionHandlerPBzip2::executableName(); break;
    case CompressionHandlerType::Pigz:
        result.execName = CompressionHandlerPigz::executableName(); break;
    case CompressionHandlerType::Pixz:
        result.execName = CompressionHandlerPixz::executableName(); break;
    case CompressionHandlerType::Plzip:
        result.execName = CompressionHandlerPlzip::executableName(); break;
    case CompressionHandlerType::Xz:
        result.execName = CompressionHandlerXz::executableName(); break;
    case CompressionHandlerType::Zstd:
        result.execName = CompressionHandlerZstd::executableName(); break;
    default:
        Q_ASSERT(false);
    }

    Q_ASSERT(!result.execName.isEmpty());

    result.available = !QStandardPaths::findExecutable(result.execName).isEmpty();

    return result;
}


CompressionHandlerCreationResult createCompressionHandler(CompressionHandlerType handlerType, QObject* parent)
{
    CompressionHandlerCreationResult result;

    const CompressionHandlerExecInfo info = getCompressionHandlerExecInfo(handlerType);

    if (!info.available)
    {
        result.errorInfo = "Executable not found: " + info.execName;
        return result;
    }

    switch (handlerType)
    {
    case CompressionHandlerType::Brotli:
        result.handler = new CompressionHandlerBrotli(parent); break;
    case CompressionHandlerType::Bzip2:
        result.handler = new CompressionHandlerBzip2(parent); break;
    case CompressionHandlerType::Compress:
        result.handler = new CompressionHandlerCompress(parent); break;
    case CompressionHandlerType::Gzip:
        result.handler = new CompressionHandlerGzip(parent); break;
    case CompressionHandlerType::Lrzip:
        result.handler = new CompressionHandlerLrzip(parent); break;
    case CompressionHandlerType::Lz4:
        result.handler = new CompressionHandlerLz4(parent); break;
    case CompressionHandlerType::Lzip:
        result.handler = new CompressionHandlerLzip(parent); break;
    case CompressionHandlerType::Lzop:
        result.handler = new CompressionHandlerLzop(parent); break;
    case CompressionHandlerType::Pbzip2:
        result.handler = new CompressionHandlerPBzip2(parent); break;
    case CompressionHandlerType::Pigz:
        result.handler = new CompressionHandlerPigz(parent); break;
    case CompressionHandlerType::Pixz:
        result.handler = new CompressionHandlerPixz(parent); break;
    case CompressionHandlerType::Plzip:
        result.handler = new CompressionHandlerPlzip(parent); break;
    case CompressionHandlerType::Xz:
        result.handler = new CompressionHandlerXz(parent); break;
    case CompressionHandlerType::Zstd:
        result.handler = new CompressionHandlerZstd(parent); break;
    default:
        Q_ASSERT(false); break;
    }

    return result;
}

}
