// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../qcs-core/controller.h"


namespace Packuru::Core
{
enum class ArchiveType;
}


namespace Packuru::Plugins::GnuTar::Core
{

class ThreadCountCtrl : public qcs::core::Controller<QString, int>
{
public:
    ThreadCountCtrl(Packuru::Core::ArchiveType archiveType);
};

}
