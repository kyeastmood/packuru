// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/backendhandlererror.h"

#include "compressionhandlerzstd.h"
#include "compressioninitdata.h"
#include "decompressioninitdata.h"
#include "tocliarg.h"
#include "compressionhandlertype.h"


using namespace Packuru::Core;


namespace
{
const QString execName("zstd");
}


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandlerZstd::CompressionHandlerZstd(QObject* parent)
    : CompressionHandler(CompressionHandlerType::Zstd, parent)
{

}


QString CompressionHandlerZstd::executableName()
{
    return execName;
}


void CompressionHandlerZstd::compress(const CompressionInitData& data)
{
    QStringList args;

    const QStringList compressionArgs = toCLIargCompressionLevel(data.compressionLevel, handlerType);

    for (const auto& arg : compressionArgs)
        args << arg;

    args << toCLIargThreadCount(data.requestedThreadCount, handlerType);

    process->setStandardOutputFile(data.archiveInfo.absoluteFilePath());
    process->start(execName, args);
}


void CompressionHandlerZstd::decompress(const DecompressionInitData& data)
{
    QStringList args;
    args << QLatin1String("-dc")
         << toCLIargThreadCount(data.requestedThreadCount, handlerType)
         << data.archiveInfo.absoluteFilePath();

    process->start(execName, args);
}


void CompressionHandlerZstd::processErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line.contains(QLatin1String(" : Read error (39) : "))
                || line.contains(QLatin1String(" : Decoding error (36) : ")))
            emit error(BackendHandlerError::DataError);
        else if (line.endsWith(QLatin1String("No space left on device (cannot write compressed block) ")))
            emit error(BackendHandlerError::DiskFull);
    }
}

}
