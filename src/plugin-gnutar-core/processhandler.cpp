// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/backendhandlererror.h"
#include "../core/private/plugin-api/textstreampreprocessor.h"

#include "processhandler.h"


using namespace Packuru::Core;


namespace Packuru::Plugins::GnuTar::Core
{

ProcessHandler::ProcessHandler(QObject *parent)
    : QObject(parent),
      process(new QProcess(this)),
      stdOutProcess(nullptr),
      processStdErrPreprocessor(new TextStreamPreprocessor(this))
{
    connect(process, &QProcess::started,
            this, [comp = this, process = process] ()
    { comp->onProcessStarted(process);});

    connect(process, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &ProcessHandler::onProcessFinished);

    process->setProcessChannelMode(QProcess::SeparateChannels);

    connect(process, &QProcess::readyReadStandardError,
            this, [preprocessor = processStdErrPreprocessor, process = process] ()
    { preprocessor->processBytes(process->readAllStandardError()); });

    connect(processStdErrPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &ProcessHandler::processErrorLineHandlerPriv);
}


QProcess* ProcessHandler::getProcess()
{
    return process;
}


void ProcessHandler::setStandardOutputProcess(QProcess* outputProcess)
{
    if (stdOutProcess)
        return;

    stdOutProcess = outputProcess;
    process->setStandardOutputProcess(stdOutProcess);
}


void ProcessHandler::processErrorLineHandler(const std::vector<QStringRef>& lines)
{
    Q_UNUSED(lines)
}


void ProcessHandler::onProcessFinished()
{
    emit processFinished(process->exitCode(), process->exitStatus());
}


void ProcessHandler::onProcessStarted(QProcess* process)
{
    processStarted(process->program(), process->arguments());
}


void ProcessHandler::processErrorLineHandlerPriv(const std::vector<QStringRef>& lines)
{
    processErrorLineHandler(lines);
    emit logLines(lines);
}

}
