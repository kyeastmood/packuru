// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/backendhandlererror.h"

#include "compressionhandlerlzop.h"
#include "compressioninitdata.h"
#include "decompressioninitdata.h"
#include "tocliarg.h"
#include "compressionhandlertype.h"


using namespace Packuru::Core;


namespace
{
const QString execName("lzop");
}


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandlerLzop::CompressionHandlerLzop(QObject* parent)
    : CompressionHandler(CompressionHandlerType::Lzop, parent)
{

}


QString CompressionHandlerLzop::executableName()
{
    return execName;
}



void CompressionHandlerLzop::compress(const CompressionInitData& data)
{
    QStringList args;
    args << toCLIargCompressionLevel(data.compressionLevel, handlerType);

    process->setStandardOutputFile(data.archiveInfo.absoluteFilePath());
    process->start(execName, args);
}


void CompressionHandlerLzop::decompress(const DecompressionInitData& data)
{
    QStringList args;
    args << QLatin1String("-dc") << data.archiveInfo.absoluteFilePath();

    process->start(execName, args);
}


void CompressionHandlerLzop::processErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line.endsWith(": Checksum error")
                || line.endsWith(": not a lzop file")
                || line.endsWith(": Compressed data violation")
                || line.endsWith(": header corrupted (checksum error)")
                || line.endsWith(": header corrupted (transmitted in text mode ?)")
                || line.startsWith("lzop: unexpected end of file:"))
            emit error(BackendHandlerError::DataError);
        else if (line.startsWith(QLatin1String("lzop: No space left on device:")))
            emit error(BackendHandlerError::DiskFull);
    }
}

}
