// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../qcs-core/controller.h"

#include "tarformat.h"


namespace Packuru::Plugins::GnuTar::Core
{

class TarFormatCtrl : public qcs::core::Controller<QString, TarFormat>
{
public:
    TarFormatCtrl();
};

}
