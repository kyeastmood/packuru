// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>
#include <QProcess>


namespace Packuru::Core
{
enum class BackendHandlerError;
class TextStreamPreprocessor;
}

namespace Packuru::Plugins::GnuTar::Core
{

class ProcessHandler : public QObject
{
    Q_OBJECT
public:
    explicit ProcessHandler(QObject *parent = nullptr);

    QProcess* getProcess();
    void setStandardOutputProcess(QProcess* outputProcess);

signals:
    void processStarted(const QString& executable, const QStringList& args);
    void processFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void error(Packuru::Core::BackendHandlerError error);
    void logLine(const QString& line);
    void logLines(const std::vector<QStringRef>& lines);

protected:
    virtual void processErrorLineHandler(const std::vector<QStringRef>& lines);
    virtual void onProcessFinished();

    QProcess* process;
    QProcess* stdOutProcess;

private:
    void onProcessStarted(QProcess* process);
    void processErrorLineHandlerPriv(const std::vector<QStringRef>& lines);

    Packuru::Core::TextStreamPreprocessor* processStdErrPreprocessor;
};

}
