// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../qcs-core/controller.h"


namespace Packuru::Plugins::GnuTar::Core
{

class SparseFilesCtrl : public qcs::core::Controller<bool>
{
public:
    SparseFilesCtrl();

    void update() override;

private:
    void onValueSyncedToWidget(const QVariant& value) override;

    bool userValue;
};

}
