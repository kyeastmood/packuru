// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>
#include <QFileInfo>


namespace Packuru::Core
{
enum class ArchiveType;
}


namespace Packuru::Plugins::GnuTar::Core
{
struct DecompressionInitData
{
    DecompressionInitData();

    QFileInfo archiveInfo;
    Packuru::Core::ArchiveType archiveType;
    int requestedThreadCount;
    QString password;
};
}
