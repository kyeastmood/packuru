# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

isEmpty(PROJECT_TARGET_LIB_DIR) {
    error($$_FILE_: Variable PROJECT_TARGET_LIB_DIR has not been set. Included from $$_PRO_FILE_)
}

TEMPLATE = lib

DESTDIR = $$PROJECT_TARGET_LIB_DIR

QMAKE_RPATHDIR += $ORIGIN

unix {
    target.path = /usr/lib
    INSTALLS += target
}
