// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>
#include <QtPlugin>

#include "../core/private/plugin-api/archivehandlerinterface.h"


namespace Packuru::Plugins::Unarchiver
{

class ArchiveHandlerUnarchiver : public QObject, public Packuru::Core::ArchiveHandlerInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ArchiveHandlerUnarchiver" FILE "pluginmetadata.json")
    Q_INTERFACES(Packuru::Core::ArchiveHandlerInterface)

public:
    ArchiveHandlerUnarchiver();

    std::unordered_set<Packuru::Core::ArchiveType> supportedTypesForReading() const override;
    std::unique_ptr<Packuru::Core::AbstractBackendHandler> createBackendHandler() const override;
};

}
