# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)
include(../common-qcs.pri)

QT += widgets

MODULE_BUILD_NAME_SUFFIX = $$QCS_QTWIDGETS_BUILD_NAME_SUFFIX
MODULE_BUILD_NAME = $$QCS_QTWIDGETS_BUILD_NAME

TARGET = $$qtLibraryTarget($$MODULE_BUILD_NAME)

LIBS += -L$${PROJECT_TARGET_LIB_DIR} -l$$QCS_CORE_BUILD_NAME

DEFINES += $${QCS_MACRO_NAME}_QTWIDGETS_LIBRARY

public_headers.files = $$files(*.h)
public_headers.path = $${PROJECT_API_TARGET_DIR}/$${MODULE_BUILD_NAME}

HEADERS += \
    generatedpage.h \
    pagestack.h \
    private/widgethandler.h \
    symbol_export.h \
    widgetmapper.h


SOURCES += \
    generatedpage.cpp \
    pagestack.cpp \
    private/widgethandler.cpp \
    widgetmapper.cpp



