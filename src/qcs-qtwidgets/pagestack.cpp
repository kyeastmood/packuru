// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <unordered_map>

#include <QScrollArea>

#include "pagestack.h"
#include "generatedpage.h"


using namespace qcs::core;


namespace qcs::qtw
{

struct PageStack::Private
{
    std::unordered_map<ControllerIdRowMapper*, QWidget*> pages;
};


PageStack::PageStack(QWidget* parent)
    : QStackedWidget(parent),
      priv(new Private)
{
    setFrameStyle(QFrame::StyledPanel);
    setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Expanding);
}


PageStack::~PageStack()
{

}


void PageStack::setCurrentPage(ControllerIdRowMapper* mapper)
{
    auto it = priv->pages.find(mapper);

    QWidget* page = nullptr;

    if (it == priv->pages.end())
    {
        auto container = new QScrollArea;
        container->setFrameStyle(QFrame::NoFrame);
        container->setAlignment(Qt::AlignHCenter | Qt::AlignTop);
        container->setWidget(new GeneratedPage(mapper));
        page = container;
        priv->pages[mapper] = page;
    }
    else
        page = it->second;

    Q_ASSERT(page);

    addWidget(page);

    setCurrentWidget(page);
}

}
