// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QtCore/qglobal.h>

#if defined(QCS_QTWIDGETS_LIBRARY)
#  define QCS_QTWIDGETS_EXPORT Q_DECL_EXPORT
#else
#  define QCS_QTWIDGETS_EXPORT Q_DECL_IMPORT
#endif
