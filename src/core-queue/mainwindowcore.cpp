// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/appinfo.h"
#include "../core/commonstrings.h"
#include "../core/passwordpromptcore.h"
#include "../core/creationnameerrorpromptcore.h"
#include "../core/extractionfileoverwritepromptcore.h"
#include "../core/extractionfolderoverwritepromptcore.h"
#include "../core/extractiontypemismatchpromptcore.h"
#include "../core/private/privatestrings.h"
#include "../core/globalsettingsmanager.h"
#include "../core/private/globalsettings.h"

#include "mainwindowcore.h"
#include "taskqueuemodel.h"
#include "private/mainwindowcoreaux.h"
#include "private/taskqueuemodelaux.h"


using namespace Packuru::Core;


namespace Packuru::Core::Queue
{

struct MainWindowCore::Private
{
    TaskQueueModel* queueModel = nullptr;
    TaskQueueModelAux::Backdoor queueModelBackdoor;
};


MainWindowCore::MainWindowCore(const MainWindowCoreAux::Init& initData,
                               MainWindowCoreAux::Backdoor& backdoorData,
                               QObject *parent) :
    QObject(parent),
    priv(new Private)
{
    Q_UNUSED(initData)

    TaskQueueModelAux::Init init;
    auto queueModel = new TaskQueueModel(init, priv->queueModelBackdoor, this);
    Q_ASSERT(priv->queueModelBackdoor.addTasks);
    backdoorData.addTasks = priv->queueModelBackdoor.addTasks;

    connect(queueModel, &TaskQueueModel::allTasksCompleted,
            this, [publ = this] ()
    {
        const auto autoClose = GlobalSettingsManager::instance()
                        ->getValueAs<bool>(GlobalSettings::Enum::QC_AutoCloseOnAllTasksCompletion);
        if (autoClose)
            emit publ->allTasksCompletedReadyToClose();
    });

    priv->queueModel = queueModel;
}


MainWindowCore::~MainWindowCore()
{
}


QString MainWindowCore::getString(MainWindowCore::UserString value)
{
    switch (value)
    {
    case UserString::ActionAbout:
        return PrivateStrings::getString(PrivateStrings::UserString::ActionAbout);

    case UserString::ActionExtract:
        return PrivateStrings::getString(PrivateStrings::UserString::ActionExtract);

    case UserString::ActionNew:
        return PrivateStrings::getString(PrivateStrings::UserString::ActionNew);

    case UserString::ActionQuit:
        return PrivateStrings::getString(PrivateStrings::UserString::Quit);

    case UserString::ActionSettings:
        return PrivateStrings::getString(PrivateStrings::UserString::Settings);

    case UserString::ActionTest:
        return PrivateStrings::getString(PrivateStrings::UserString::ActionTest);

    case UserString::WindowTitle:
        return Packuru::Core::Queue::MainWindowCore::tr("%1 Queue").arg(AppInfo::getAppName());

    default:
        Q_ASSERT(false); return "";
    }
}


TaskQueueModel* MainWindowCore::getTaskQueueModel()
{
    return priv->queueModel;
}

}
