// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

/// @file

namespace Packuru::Core::Queue
{

/**
 @brief Identifies input widgets in %Queue settings.
 @headerfile "core-queue/settingscontrollertype.h"
 */
class SettingsControllerType : public QObject
{
    Q_OBJECT
public:
    /// @copybrief Packuru::Core::Queue::SettingsControllerType
    enum class Type
    {
        AbortTasksRequiringPassword, ///< Checkbox.
        AutoCloseOnAllTasksCompletion, ///< Checkbox.
        AutoRemoveCompletedTasks, ///< Checkbox.
        MaxErrorCount ///< Spinbox.
    };

    Q_ENUM(Type)

};

}
