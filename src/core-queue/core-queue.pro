# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)
include(../common-core.pri)

MODULE_BUILD_NAME_SUFFIX = $$CORE_QUEUE_BUILD_NAME_SUFFIX
MODULE_BUILD_NAME = $$CORE_QUEUE_BUILD_NAME

include(../embed-qm-files.pri)

QT += network

TARGET = $$qtLibraryTarget($$MODULE_BUILD_NAME)

DEFINES += $${PROJECT_MACRO_NAME}_CORE_QUEUE_LIBRARY

LIBS += -L$$PROJECT_TARGET_LIB_DIR -l$$QCS_CORE_BUILD_NAME -l$${CORE_BUILD_NAME}

public_headers.files = $$files(*.h)
public_headers.path = $${PROJECT_API_TARGET_DIR}/$${MODULE_BUILD_NAME_SUFFIX}

TRANSLATIONS += \
    translations/$${MODULE_BUILD_NAME_SUFFIX}_en.ts \
    translations/$${MODULE_BUILD_NAME_SUFFIX}_nl.ts \

HEADERS += \
    init.h \
    mainwindowcore.h \
    private/commandlinehandler.h \
    private/incomingmessageinterpreter.h \
    private/mainwindowcoreaux.h \
    private/queuecoreaux.h \
    private/taskqueuemodelaux.h \
    queuecore.h \
    settingscontrollertype.h \
    settingsdialogcore.h \
    symbol_export.h \
    taskqueuemodel.h \
    private/commandlineextractiondata.h \
    private/commandlinecreationdata.h \
    private/commandlinetestdata.h \
    private/settingsconverter.h \
    private/taskbatch.h \
    private/queuesettings/maxerrorcountctrl.h \
    private/queueserver.h \
    private/incomingconnectionhandler.h \

SOURCES += \
    init.cpp \
    mainwindowcore.cpp \
    private/commandlinehandler.cpp \
    private/incomingmessageinterpreter.cpp \
    queuecore.cpp \
    settingsdialogcore.cpp \
    taskqueuemodel.cpp \
    private/commandlineextractiondata.cpp \
    private/commandlinecreationdata.cpp \
    private/commandlinetestdata.cpp \
    private/settingsconverter.cpp \
    private/taskbatch.cpp \
    private/queuesettings/maxerrorcountctrl.cpp \
    private/queueserver.cpp \
    private/incomingconnectionhandler.cpp \
