<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MaxErrorCountCtrl</name>
    <message>
        <location filename="../private/queuesettings/maxerrorcountctrl.cpp" line="16"/>
        <source>Maximum error count to stop queue (0 = never stop)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Queue::CommandLineHandler</name>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="108"/>
        <source>Create archives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="110"/>
        <source>Extract archives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="112"/>
        <source>Test archives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="114"/>
        <source>Show dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="117"/>
        <source>Set destination.
If not set and a dialog is not shown, each archive is extracted to its parent folder.</source>
        <comment>path</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="122"/>
        <source>Set type of archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="123"/>
        <source>type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="125"/>
        <source>Creates single archive if set, otherwise archive per item will be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="129"/>
        <source>Create top folder. Valid values:
 yes, no, auto
Used when extracting archives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="132"/>
        <source>value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Queue::IncomingMessageInterpreter</name>
    <message>
        <location filename="../private/incomingmessageinterpreter.cpp" line="113"/>
        <source>Unknown message type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Queue::MainWindowCore</name>
    <message>
        <location filename="../mainwindowcore.cpp" line="89"/>
        <source>%1 Queue</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Queue::QueueCore</name>
    <message>
        <location filename="../queuecore.cpp" line="143"/>
        <source>Error while starting queue server:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuecore.cpp" line="252"/>
        <source>No plugins to create archives.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuecore.cpp" line="259"/>
        <source>No input urls specified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuecore.cpp" line="261"/>
        <source>Unsupported archive type specified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuecore.cpp" line="274"/>
        <source>No plugin to create this type of archive.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuecore.cpp" line="313"/>
        <location filename="../queuecore.cpp" line="325"/>
        <source>archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuecore.cpp" line="353"/>
        <source>No archives to extract.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuecore.cpp" line="392"/>
        <source>No archives to test.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Queue::SettingsDialogCore</name>
    <message>
        <location filename="../settingsdialogcore.cpp" line="39"/>
        <source>Abort tasks that require password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialogcore.cpp" line="46"/>
        <source>Quit if all tasks completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialogcore.cpp" line="54"/>
        <source>Automatically remove completed tasks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialogcore.cpp" line="85"/>
        <source>Queue</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Queue::TaskQueueModel</name>
    <message>
        <location filename="../taskqueuemodel.cpp" line="892"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="897"/>
        <source>Add files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="899"/>
        <source>Create archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="901"/>
        <source>Extract archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="903"/>
        <source>Delete files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="905"/>
        <source>Test archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="253"/>
        <source>Task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="255"/>
        <source>Archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="257"/>
        <source>State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="259"/>
        <source>Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="261"/>
        <source>Error info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="524"/>
        <source>Remove completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="527"/>
        <source>Remove task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="530"/>
        <source>Reset task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="533"/>
        <source>Start queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="536"/>
        <source>Stop here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="539"/>
        <source>Stop queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="871"/>
        <source>Stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../taskqueuemodel.cpp" line="878"/>
        <source>%n left</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
</TS>
