// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QFileInfo>

#include "core/private/plugin-api/archivetype.h"


namespace Packuru::Core::Queue
{

struct CommandLineCreationData
{
    Packuru::Core::ArchiveType archiveType = Packuru::Core::ArchiveType::___NOT_SUPPORTED;
    std::vector<QFileInfo> inputItems;
    bool archivePerItem = false;
    QString destination;
    bool openDialog = false;
    QString appStartPath;
};

}


QDataStream& operator<<(QDataStream& out, const Packuru::Core::Queue::CommandLineCreationData& data);
QDataStream& operator>>(QDataStream& in, Packuru::Core::Queue::CommandLineCreationData& data);
