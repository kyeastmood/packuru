// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>


namespace Packuru::Core
{
class ArchivingTaskData;
class ExtractionTaskData;
class DeletionTaskData;
class TestTaskData;
}


namespace Packuru::Core::Queue
{

class CommandLineExtractionData;
class CommandLineCreationData;
class CommandLineTestData;

class IncomingMessageInterpreter : public QObject
{
    Q_OBJECT
public:
    IncomingMessageInterpreter(QObject* parent = nullptr);

    void interpret(const QByteArray& message) const;

signals:
    void defaultStartUp() const;

    void commandLineHelpRequested(const QString& info) const;
    void commandLineError(const QString& info) const;
    void commandLineCreateArchives(const CommandLineCreationData& inputFiles) const;
    void commandLineExtractArchives(const CommandLineExtractionData& data) const;
    void commandLineTestArchives(const CommandLineTestData& data) const;

    void archivingTaskData(const Packuru::Core::ArchivingTaskData& data) const;
    void extractionTaskData(const Packuru::Core::ExtractionTaskData& data) const;
    void deletionTaskData(const Packuru::Core::DeletionTaskData& data) const;
    void testTaskData(const Packuru::Core::TestTaskData& data) const;

};

}
