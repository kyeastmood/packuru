// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QLocalSocket>
#include <QDataStream>

#include "utils/makeqpointer.h"

#include "incomingconnectionhandler.h"


namespace Packuru::Core::Queue
{

IncomingConnectionHandler::IncomingConnectionHandler(qintptr socketDescriptor, QObject *parent) :
    QObject(parent),
    socket(nullptr),
    socketDescriptor(socketDescriptor),
    messageSize(0),
    isBusy(false)
{

}


IncomingConnectionHandler::~IncomingConnectionHandler()
{
}


bool IncomingConnectionHandler::init()
{
    socket = new QLocalSocket(this);

    if (!socket->setSocketDescriptor(socketDescriptor))
        return false;

    connect(socket, &QLocalSocket::readyRead,
            this, &IncomingConnectionHandler::slotSocketReadyRead);

    connect(socket, &QLocalSocket::errorOccurred,
            this, [publ = this] (auto errorCode)
    {
        emit publ->error(errorCode);
        if (!publ->isBusy)
            emit publ->finished();
    });

    return true;
}


void IncomingConnectionHandler::slotSocketReadyRead()
{
    QDataStream in(socket);

    if (messageSize == 0)
    {
        if (socket->bytesAvailable() < static_cast<qint64>(sizeof(quint64)))
            return;

        in >> messageSize;
    }

    if (socket->bytesAvailable() < messageSize)
        return;

    isBusy = true;

    QByteArray data;

    data = socket->readAll();

    emit sigNewMessageReceived(data);

    isBusy = false;

    emit finished();
}

}
