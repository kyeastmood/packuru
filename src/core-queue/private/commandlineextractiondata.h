// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QFileInfo>
#include <QString>

#include "core/private/extractiontopfoldermode.h"


namespace Packuru::Core::Queue
{

struct CommandLineExtractionData
{
    std::vector<QFileInfo> archives;
    QString destinationPath;
    Packuru::Core::ExtractionTopFolderMode topFolderMode = Packuru::Core::ExtractionTopFolderMode::DoNotCreateFolder;
    bool openDialog = false;
};

}


QDataStream& operator<<(QDataStream& out, const Packuru::Core::Queue::CommandLineExtractionData& data);
QDataStream& operator>>(QDataStream& in, Packuru::Core::Queue::CommandLineExtractionData& data);
