// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QLocalServer>


namespace Packuru::Core::Queue
{

class QueueServer : public QLocalServer
{
    Q_OBJECT
public:
    explicit QueueServer(QObject *parent = nullptr);

signals:
    void newMessageReceived(const QByteArray& data);

private:
    void incomingConnection(quintptr socketDescriptor) override;
};

}
