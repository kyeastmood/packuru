// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QLocalSocket>

#include "queueserver.h"
#include "incomingconnectionhandler.h"


namespace Packuru::Core::Queue
{

QueueServer::QueueServer(QObject *parent)
    : QLocalServer(parent)
{
}


void QueueServer::incomingConnection(quintptr socketDescriptor)
{
    auto handler = new IncomingConnectionHandler(socketDescriptor, this);

    const bool init = handler->init();

    if (init)
    {
        connect(handler, &IncomingConnectionHandler::sigNewMessageReceived,
                this, &QueueServer::newMessageReceived);

        connect(handler, &IncomingConnectionHandler::finished,
                handler, &QObject::deleteLater);
    }
    else
        handler->deleteLater();
}

}
