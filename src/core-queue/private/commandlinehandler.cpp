// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>

#include "core/private/messagetype.h"
#include "core/private/createmessage.h"

#include "commandlinehandler.h"
#include "private/commandlinecreationdata.h"
#include "private/commandlineextractiondata.h"
#include "private/commandlinetestdata.h"


using namespace Packuru::Core;


namespace Packuru::Core::Queue
{

struct CommandLineHandler::Private
{
    Private(CommandLineHandler* publ);

    CommandLineCreationData createCommandLineCreationData();
    CommandLineExtractionData createCommandLineExtractionData();
    CommandLineTestData createCommandLineTestData();

    CommandLineHandler* publ;

    QCommandLineOption create;
    QCommandLineOption extract;
    QCommandLineOption test;
    QCommandLineOption showDialog;
    QCommandLineOption destination;
    QCommandLineOption archiveType;
    QCommandLineOption singleArchive;
    QCommandLineOption createTopFolder;
};


CommandLineHandler::CommandLineHandler()
    : priv(new Private(this))
{
    parser.addHelpOption();
    parser.addVersionOption();

    parser.addOption(priv->create);
    parser.addOption(priv->extract);
    parser.addOption(priv->test);
    parser.addOption(priv->showDialog);
    parser.addOption(priv->createTopFolder);
    parser.addOption(priv->archiveType);
    parser.addOption(priv->destination);
    parser.addOption(priv->singleArchive);

    parser.parse(QCoreApplication::instance()->arguments());
}


CommandLineHandler::~CommandLineHandler()
{

}


QByteArray CommandLineHandler::createMessageFromArguments() const
{
    QByteArray message;
    QString errorInfo = parser.errorText();

    if (errorInfo.length() > 0)
    {
        message = createMessage(MessageType::CommandLineError, errorInfo, true);
    }
    else if (parser.isSet("help"))
    {
        message = createMessage(MessageType::CommandLineHelp, parser.helpText(), true);
    }
    else if (parser.isSet(priv->create))
    {
        const auto data = priv->createCommandLineCreationData();
        message = createMessage(MessageType::CommandLineCreateArchives, data, true);
    }
    else if (parser.isSet(priv->extract))
    {
        const auto data = priv->createCommandLineExtractionData();
        message = createMessage(MessageType::CommandLineExtractArchives, data, true);
    }
    else if (parser.isSet(priv->test))
    {
        const auto data = priv->createCommandLineTestData();
        message = createMessage(MessageType::CommandLineTestArchives, data, true);
    }
    else
    {
        message = createMessage(MessageType::CommandLineDefaultStartUp, QByteArray(), true);
    }

    return message;
}


CommandLineHandler::Private::Private(CommandLineHandler* handler)
    : publ(handler),
      create(QCommandLineOption({"c", "create"},
                                Packuru::Core::Queue::CommandLineHandler::tr("Create archives"))),
      extract(QCommandLineOption({"e", "extract"},
                                 Packuru::Core::Queue::CommandLineHandler::tr("Extract archives"))),
      test(QCommandLineOption({"t", "test"},
                              Packuru::Core::Queue::CommandLineHandler::tr("Test archives"))),
      showDialog(QCommandLineOption({"d", "dialog"},
                                    Packuru::Core::Queue::CommandLineHandler::tr("Show dialog"))),
      destination(QCommandLineOption ({"o", "destination"},
                                      Packuru::Core::Queue::CommandLineHandler::
                                      tr("Set destination.\n"
                                         "If not set and a dialog is not shown, "
                                         "each archive is extracted to its parent folder.",
                                         "path"))),
      archiveType({"y", "archivetype"},
                  CommandLineHandler::tr("Set type of archive"),
                  CommandLineHandler::tr("type")),
      singleArchive({"s", "singlearchive"},
                    Packuru::Core::Queue::CommandLineHandler::tr("Creates single archive if set, "
                                                                      "otherwise archive per item will be created")),
      createTopFolder(QCommandLineOption({"f", "topfolder"},
                                         Packuru::Core::Queue::CommandLineHandler::
                                         tr("Create top folder. Valid values:\n yes, no, auto\n"
                                            "Used when extracting archives"),
                                         Packuru::Core::Queue::CommandLineHandler::
                                         tr("value"),
                                         "no"))
{

}


CommandLineCreationData CommandLineHandler::Private::createCommandLineCreationData()
{
    CommandLineCreationData data;
    data.inputItems = publ->createUrlList();
    data.destination = publ->parser.value(destination);
    data.openDialog = publ->parser.isSet(showDialog);
    fromString(publ->parser.value(archiveType), data.archiveType);
    data.archivePerItem = !publ->parser.isSet(singleArchive);
    data.appStartPath = publ->appDir;
    return data;
}


CommandLineExtractionData CommandLineHandler::Private::createCommandLineExtractionData()
{
    CommandLineExtractionData data;
    data.archives = publ->createUrlList();
    data.destinationPath = publ->parser.value(destination);
    const QString topFolderMode = publ->parser.value(createTopFolder);
    if (topFolderMode == QLatin1String("yes"))
        data.topFolderMode = ExtractionTopFolderMode::CreateFolder;
    else if (topFolderMode == QLatin1String("no"))
        data.topFolderMode = ExtractionTopFolderMode::DoNotCreateFolder;
    else if (topFolderMode == QLatin1String("auto"))
        data.topFolderMode = ExtractionTopFolderMode::CreateIfAbsent;
    data.openDialog = publ->parser.isSet(showDialog);
    return data;
}


CommandLineTestData CommandLineHandler::Private::createCommandLineTestData()
{
    CommandLineTestData data;
    data.inputItems = publ->createUrlList();
    data.openDialog = publ->parser.isSet(showDialog);
    return data;
}

}
