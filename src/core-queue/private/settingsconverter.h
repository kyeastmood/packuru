// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "core/globalsettingsconverter.h"


namespace Packuru::Core::Queue
{

class SettingsConverter : public Packuru::Core::GlobalSettingsConverter
{
public:
    SettingsConverter();

    std::unordered_set<int> getSupportedKeys() const override;
    QString getKeyName(int key) const override;
    QVariant toSettingsType(int key, const QVariant& value) const override;
    QVariant fromSettingsType(int key, const QVariant& value) const override;
};

}
