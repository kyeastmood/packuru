// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "core/private/tasks/archivingtask.h"
#include "core/private/tasks/extractiontask.h"

#include "taskbatch.h"


using namespace Packuru::Core;


namespace Packuru::Core::Queue
{

TaskBatch::TaskBatch(QObject *parent)
    : QObject(parent)
{

}


void TaskBatch::addTask(Task* task)
{
    Q_ASSERT(tasks.find(task) == tasks.end());

    if (auto t = qobject_cast<ArchivingTask*>(task))
    {
        connect(t, &ArchivingTask::openDestinationPath,
                this, &TaskBatch::onOpenDestinationRequested);
    }
    else if (auto t = qobject_cast<ExtractionTask*>(task))
    {
        connect(t, &ExtractionTask::openDestinationPath,
                this, &TaskBatch::onOpenDestinationRequested);
    }
    else
        return;

    connect(task, &QObject::destroyed,
            this, &TaskBatch::onTaskDestroyed);

    tasks.insert(task);
}


void TaskBatch::onTaskDestroyed()
{
    // qobject_cast with Q_ASSERT(task) will not work here because the task
    // has been destroyed and qobject_cast returns nullptr
    auto task = static_cast<Task*>(sender());
    tasks.erase(task);

    if (tasks.empty())
        deleteLater();
}


void TaskBatch::onOpenDestinationRequested(const QString& path)
{
    if (openedPaths.find(path) != openedPaths.end())
        return;

    openedPaths.insert(path);
    emit openDestinationPath(path);
}

}
