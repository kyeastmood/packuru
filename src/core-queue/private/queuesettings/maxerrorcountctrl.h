// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"


namespace Packuru::Core::Queue
{

class MaxErrorCountCtrl : public qcs::core::Controller<int>
{
    Q_DECLARE_TR_FUNCTIONS(MaxErrorCountCtrl)
public:
    MaxErrorCountCtrl(int initValue);
};

}
