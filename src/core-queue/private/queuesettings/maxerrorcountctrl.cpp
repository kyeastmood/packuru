// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "maxerrorcountctrl.h"


namespace Packuru::Core::Queue
{

MaxErrorCountCtrl::MaxErrorCountCtrl(int initValue)
{
    setCurrentValue(initValue);
    setValueRange(0, 1000);
    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::Queue::MaxErrorCountCtrl::tr("Maximum error count to stop queue (0 = never stop)");
}

}
