// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include "core/private/commandlinehandlerbase.h"

#include "symbol_export.h"


namespace Packuru::Core::Queue
{

class PACKURU_CORE_QUEUE_EXPORT CommandLineHandler : public Packuru::Core::CommandLineHandlerBase
{
public:
    explicit CommandLineHandler();
    ~CommandLineHandler();

    QByteArray createMessageFromArguments() const;

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
