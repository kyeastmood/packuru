// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>
#include <QTimer>

#include "../utils/makeqpointer.h"

#include "../core/plugindatamodel.h"
#include "../core/archivingdialogcore.h"
#include "../core/extractiondialogcore.h"
#include "../core/testdialogcore.h"
#include "../core/private/pluginmanager.h"
#include "../core/private/archivetypedetector.h"
#include "../core/private/archiveservices.h"
#include "../core/private/dialogcore/archivingdialog/archivingdialogcoreinitcreate.h"
#include "../core/private/tasks/archivingtask.h"
#include "../core/private/tasks/archivingtaskdata.h"
#include "../core/private/tasks/extractiontask.h"
#include "../core/private/tasks/extractiontaskdata.h"
#include "../core/private/tasks/deletiontask.h"
#include "../core/private/tasks/testtask.h"
#include "../core/private/tasks/testtaskdata.h"
#include "../core/private/dialogcore/extractiondialog/extractiondialogcoreinitmulti.h"
#include "../core/private/dialogcore/testdialog/testdialogcoreinitmulti.h"
#include "../core/private/dialogcore/appsettingsdialog/appsettingsdialogcoreaux.h"
#include "../core/private/plugin-api/archivingparameterhandlerinitdata.h"
#include "../core/private/plugin-api/archivingparameterhandlerfactory.h"
#include "../core/private/plugin-api/archivingparameterhandlerbase.h"
#include "../core/private/findcommonparentpath.h"
#include "../core/private/plugindatamodelaux.h"

#include "queuecore.h"
#include "mainwindowcore.h"
#include "settingsdialogcore.h"
#include "private/incomingmessageinterpreter.h"
#include "private/incomingmessageinterpreter.h"
#include "private/commandlinecreationdata.h"
#include "private/commandlineextractiondata.h"
#include "private/commandlinetestdata.h"
#include "private/queueserver.h"
#include "private/mainwindowcoreaux.h"
#include "private/queuecoreaux.h"


using Packuru::Utils::makeQPointer;


namespace Packuru::Core::Queue
{

using namespace QueueCoreAux;


struct QueueCore::Private
{
    Private(QueueCore* publ);

    template <typename TaskData>
    void processTaskDataList(const std::vector<TaskData>& dataList);

    template <typename TaskData>
    void processTaskDataFromBrowser(const TaskData& data);

    void onCommandLineCreate(const CommandLineCreationData& data);
    void onCommandLineExtract(const CommandLineExtractionData& data);
    void onCommandLineTest(const CommandLineTestData& data);

    ArchivingDialogCore* createArchivingDialogCore(const std::vector<QFileInfo>& filesToArchive);
    ExtractionDialogCore* createExtractionDialogCore(const std::vector<QFileInfo>& archives);
    TestDialogCore* createTestDialogCore(const std::vector<QFileInfo>& archives);

    void addNewTasks(QList<Task*>& tasks);
    void addNewTaskFromBrowser(Task* task);

    QueueCore* publ;
    ArchiveTypeDetector typeDetector;
    ArchiveServices* archiveServices;
    MainWindowCore* mainWindowCore = nullptr;
    MainWindowCoreAux::Backdoor mainWindowCoreBackdoor;
    PluginManager pm;
    IncomingMessageInterpreter* messageInterpreter = nullptr;
    QueueServer* server = nullptr;
};


QueueCore::QueueCore(QueueCoreAux::Init& init, QueueCoreAux::Backdoor& backdoor, QObject* parent)
    : QObject(parent),
      priv(new Private(this))
{
    priv->messageInterpreter = new IncomingMessageInterpreter(this);

    auto interpreter = priv->messageInterpreter;

    connect(interpreter, &IncomingMessageInterpreter::defaultStartUp,
            this, &QueueCore::defaultStartUp);

    connect(interpreter, &IncomingMessageInterpreter::commandLineError,
            this, &QueueCore::commandLineError);

    connect(interpreter, &IncomingMessageInterpreter::commandLineHelpRequested,
            this, &QueueCore::commandLineHelpRequested);

    connect(interpreter, &IncomingMessageInterpreter::commandLineCreateArchives,
            this, [publ = this] (const auto& data)
    {
        publ->priv->onCommandLineCreate(data);
    });

    connect(interpreter, &IncomingMessageInterpreter::commandLineExtractArchives,
            this, [publ = this] (const auto& data)
    {
        publ->priv->onCommandLineExtract(data);
    });

    connect(interpreter, &IncomingMessageInterpreter::commandLineTestArchives,
            this, [publ = this] (const auto& data)
    {
        publ->priv->onCommandLineTest(data);
    });

    const auto processTaskData =  [publ = this] (const auto& data)
    { publ->priv->processTaskDataFromBrowser(data); };

    connect(interpreter, &IncomingMessageInterpreter::archivingTaskData,
            this, processTaskData);

    connect(interpreter, &IncomingMessageInterpreter::extractionTaskData,
            this, processTaskData);

    connect(interpreter, &IncomingMessageInterpreter::deletionTaskData,
            this, processTaskData);

    connect(interpreter, &IncomingMessageInterpreter::testTaskData,
            this, processTaskData);

    priv->server = new QueueServer(this);

    QLocalServer::removeServer(init.queueServerName);

    if (!priv->server->listen(init.queueServerName))
        qWarning() << Packuru::Core::Queue::QueueCore::tr("Error while starting queue server:")
                   << priv->server->errorString();

    connect(priv->server, &QueueServer::newMessageReceived,
            interpreter, &IncomingMessageInterpreter::interpret);

    backdoor.accessor = new Accessor(this);

    connect(backdoor.accessor, &Accessor::processMessage,
            priv->messageInterpreter, [interpreter = priv->messageInterpreter] (const QByteArray& message)
    { interpreter->interpret(message); });
}


QueueCore::~QueueCore()
{
}


void QueueCore::createArchivingDialogCore()
{
    ArchivingDialogCore* dialogCore = priv->createArchivingDialogCore({});
    if (!dialogCore)
        return;
    emit archivingDialogCoreCreated(dialogCore);
}


void QueueCore::createExtractionDialogCore()
{
    auto dialogCore = priv->createExtractionDialogCore({});
    emit extractionDialogCoreCreated(dialogCore);
}


void QueueCore::createTestDialogCore()
{
    auto dialogCore = priv->createTestDialogCore({});
    emit testDialogCoreCreated(dialogCore);
}


AppSettingsDialogCore* QueueCore::createAppSettingsDialogCore()
{
    PluginDataModelAux::Init init;
    init.dataList = priv->pm.getAllPlugins();
    auto pluginModel = new PluginDataModel(init, this);
    const ArchiveTypeModelAccessor accessor = priv->archiveServices->getArchiveTypeModelAccessor();
    auto settings = new AppSettingsDialogCore({pluginModel, accessor}, this);
    return settings;
}


SettingsDialogCore* QueueCore::createQueueSettingsDialogCore()
{
    auto settings = new SettingsDialogCore(this);
    return settings;
}


MainWindowCore* QueueCore::getMainWindowCore()
{
    return priv->mainWindowCore;
}


QueueCore::Private::Private(QueueCore* publ)
    : publ(publ),
      pm(QCoreApplication::applicationDirPath() + "/" + PROJECT_PLUGIN_DIR_NAME)
{
    MainWindowCoreAux::Init init;
    mainWindowCore = new MainWindowCore(init, mainWindowCoreBackdoor, publ);
    Q_ASSERT(mainWindowCoreBackdoor.addTasks);

    archiveServices = new ArchiveServices(pm.getAllPlugins(), publ);
}


template <typename TaskData>
void QueueCore::Private::processTaskDataList(const std::vector<TaskData>& dataList)
{
    QList<Task*> tasks;

    for (const auto& data : dataList)
    {
        auto task = archiveServices->createTask(data);
        if (task)
            tasks.push_back(task);
    }

    addNewTasks(tasks);
}


template<typename TaskData>
void QueueCore::Private::processTaskDataFromBrowser(const TaskData& data)
{
    auto task = archiveServices->createTask(data);
    if (task)
        addNewTaskFromBrowser(task);
}


void QueueCore::Private::onCommandLineCreate(const CommandLineCreationData& data)
{
    if (data.openDialog)
    {
        ArchivingDialogCore* dialogCore = createArchivingDialogCore(data.inputItems);
        if (!dialogCore)
            return publ->commandLineError(Packuru::Core::Queue::QueueCore::tr("No plugins to create archives."));

        emit publ->archivingDialogCoreCreated(dialogCore);
    }
    else
    {
        if (data.inputItems.empty())
            return publ->commandLineError(Packuru::Core::Queue::QueueCore::tr("No input urls specified."));
        else if (data.archiveType == ArchiveType::___NOT_SUPPORTED)
            return publ->commandLineError(Packuru::Core::Queue::QueueCore::tr("Unsupported archive type specified."));

        std::vector<ArchivingTaskData> dataList;
        ArchivingTaskData taskData;
        BackendArchivingData& backendData = taskData.backendData;

        backendData.archiveType = data.archiveType;
        backendData.createNewArchive = true;

        auto factory = archiveServices->createArchivingParameterHandlerFactory(data.archiveType);

        if (!factory)
            return publ->commandLineError(Packuru::Core::Queue::QueueCore::
                                          tr("No plugin to create this type of archive."));

        taskData.pluginId = factory->getPluginId();

        ArchivingParameterHandlerInitData initData{data.archiveType, true, EncryptionState::___INVALID};
        auto handler = factory->createHandler(initData);

        Q_ASSERT(handler);

        backendData.pluginPrivateData = handler->getPrivateArchivingData();

        if (data.archivePerItem || data.inputItems.size() == 1)
        {
            for (const auto& item : data.inputItems)
            {
                backendData.archiveName = item.fileName();

                if (!data.destination.isEmpty())
                    backendData.destinationPath = data.destination;
                else
                    backendData.destinationPath = item.absolutePath();

                backendData.inputItems.clear();
                backendData.inputItems.push_back(item);

                dataList.push_back(taskData);
            }
        }
        else // Create single archive from multiple items
        {
            const QFileInfo parentFolder(findCommonParentPath(data.inputItems));
            QString archiveBaseName;

            if (!data.destination.isEmpty())
            {
                backendData.destinationPath = data.destination;
                if (!parentFolder.absoluteFilePath().isEmpty())
                    archiveBaseName = parentFolder.fileName();
                else
                    archiveBaseName = Packuru::Core::Queue::QueueCore::tr("archive");
            }
            else
            {
                if (!parentFolder.absoluteFilePath().isEmpty())
                {
                    backendData.destinationPath = parentFolder.absoluteFilePath();
                    archiveBaseName = parentFolder.fileName();
                }
                else
                {
                    backendData.destinationPath = data.appStartPath;
                    archiveBaseName = Packuru::Core::Queue::QueueCore::tr("archive");
                }
            }

            if (!backendData.destinationPath.endsWith('/'))
                backendData.destinationPath += QLatin1Char('/');

            backendData.archiveName = archiveBaseName;
            backendData.inputItems = data.inputItems;
            dataList.push_back(taskData);
        }

        processTaskDataList(dataList);
    }
}


void QueueCore::Private::onCommandLineExtract(const CommandLineExtractionData& data)
{
    if (data.openDialog)
    {
        auto dialogCore = createExtractionDialogCore(data.archives);
        emit publ->extractionDialogCoreCreated(dialogCore);
    }
    else
    {
        if (data.archives.empty())
        {
            emit publ->commandLineError(Packuru::Core::Queue::QueueCore::tr("No archives to extract."));
            return;
        }

        std::vector<ExtractionTaskData> dataList;
        ExtractionTaskData taskData;
        taskData.topFolderMode = data.topFolderMode;

        for (const auto& archive : data.archives)
        {
            taskData.backendData.archiveInfo = archive;

            if (!data.destinationPath.isEmpty())
                taskData.destinationPath = data.destinationPath;
            else
                taskData.destinationPath = taskData.backendData.archiveInfo.absolutePath();

            taskData.backendData.archiveType = typeDetector.detectType(archive.absoluteFilePath());

            dataList.push_back(taskData);
        }

        processTaskDataList(dataList);

    }
}


void QueueCore::Private::onCommandLineTest(const CommandLineTestData& data)
{
    if (data.openDialog)
    {
        auto dialogCore = createTestDialogCore(data.inputItems);
        emit publ->testDialogCoreCreated(dialogCore);
    }
    else
    {
        if (data.inputItems.empty())
        {
            emit publ->commandLineError(Packuru::Core::Queue::QueueCore::tr("No archives to test."));
            return;
        }

        TestTaskData taskData;
        std::vector<TestTaskData> dataList;
        for (const auto& item : data.inputItems)
        {
            taskData.backendData.archiveInfo = item;
            taskData.backendData.archiveType = typeDetector.detectType(item.absoluteFilePath());
            dataList.push_back(taskData);
        }
        processTaskDataList(dataList);
    }
}


ArchivingDialogCore* QueueCore::Private::createArchivingDialogCore(const std::vector<QFileInfo> &filesToArchive)
{
    ArchivingDialogCoreInitCreate dialogInit;

    dialogInit.factoryDataList = archiveServices->createArchivingParameterHandlerFactoryList();

    if (dialogInit.factoryDataList.empty())
        return nullptr;

    dialogInit.filesToArchive = filesToArchive;

    dialogInit.onAccepted = [publ = makeQPointer(publ)] (const std::vector<ArchivingTaskData>& dataList)
    {
        if (!publ)
            return;
        publ->priv->processTaskDataList(dataList);
    };

    return new ArchivingDialogCore(std::move(dialogInit), publ);
}


ExtractionDialogCore* QueueCore::Private::createExtractionDialogCore(const std::vector<QFileInfo> &archives)
{
    ExtractionDialogCoreInitMulti dialogInit;

    dialogInit.getBackend = [archiveServices = makeQPointer(archiveServices)] (ArchiveType type)
    {
        if (!archiveServices)
            return QString();
        return archiveServices->getExtractionBackendNameForType(type);
    };

    dialogInit.guessArchiveType = [publ = makeQPointer(publ)] (const QFileInfo& info)
    {
        if (!publ)
            return ArchiveType::___NOT_SUPPORTED;
        return publ->priv->typeDetector.detectType(info.absoluteFilePath());
    };

    dialogInit.archives = archives;

    dialogInit.onAccepted = [publ = makeQPointer(publ)] (const std::vector<ExtractionTaskData>& dataList)
    {
        if (!publ)
            return;
        publ->priv->processTaskDataList(dataList);
    };

    return new ExtractionDialogCore(std::move(dialogInit), publ);
}


TestDialogCore* QueueCore::Private::createTestDialogCore(const std::vector<QFileInfo>& archives)
{
    TestDialogCoreInitMulti dialogInit;

    dialogInit.getBackend = [archiveServices = makeQPointer(archiveServices)] (ArchiveType type)
    {
        if (!archiveServices)
            return QString();
        return archiveServices->getExtractionBackendNameForType(type);
    };

    dialogInit.guessArchiveType = [publ = makeQPointer(publ)] (const QFileInfo& info)
    {
        if (!publ)
            return ArchiveType::___NOT_SUPPORTED;
        return publ->priv->typeDetector.detectType(info.absoluteFilePath());
    };

    dialogInit.archives = archives;

    dialogInit.onAccepted = [publ = makeQPointer(publ)] (const std::vector<TestTaskData>& dataList)
    {
        if (!publ)
            return;
        publ->priv->processTaskDataList(dataList);
    };

    return new TestDialogCore(std::move(dialogInit), publ);
}


void QueueCore::Private::addNewTasks(QList<Task*>& tasks)
{
    /* Adding tasks to queue is delayed because they might throw an error which requires showing
       a dialog which must centered on top of main window. But even if the
       gui code invoked QDialog::show() on dialog window etc., the main window might not appeared yet,
       so we wait a moment. */
    emit publ->newTasks();

    QTimer::singleShot(100, publ, [priv = this, tasks = std::move(tasks)] () mutable
    { priv->mainWindowCoreBackdoor.addTasks(tasks); });
}


void QueueCore::Private::addNewTaskFromBrowser(Task* task)
{
    QList<Task*> tasks;
    tasks.push_back(task);
    addNewTasks(tasks);
}

}
