// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../qcs-core/controllerengine.h"

#include "../core/globalsettingsmanager.h"
#include "../core/private/globalsettings.h"
#include "../core/private/dialogcore/genericcheckboxctrl.h"

#include "settingsdialogcore.h"
#include "settingscontrollertype.h"
#include "private/queuesettings/maxerrorcountctrl.h"


using namespace qcs::core;
using namespace Packuru::Core;
using Packuru::Core::GlobalSettings::Enum;

namespace Packuru::Core::Queue
{

using Type = SettingsControllerType::Type;

struct SettingsDialogCore::Private
{
    ControllerEngine* engine = nullptr;
};


SettingsDialogCore::SettingsDialogCore(QObject *parent)
    : QObject(parent),
      priv(new Private)
{
    priv->engine = new ControllerEngine(this);

    const auto abortEncrypted = GlobalSettingsManager::instance()
            ->getValueAs<bool>(Enum::QC_AbortTasksRequiringPassword);
    QString label = Packuru::Core::Queue::SettingsDialogCore::tr("Abort tasks that require password");
    priv->engine->createTopController<GenericCheckBoxCtrl>(Type::AbortTasksRequiringPassword,
                                                           label,
                                                           abortEncrypted);

    const auto autoClose = GlobalSettingsManager::instance()
            ->getValueAs<bool>(Enum::QC_AutoCloseOnAllTasksCompletion);
    label = Packuru::Core::Queue::SettingsDialogCore::tr("Quit if all tasks completed");
    priv->engine->createTopController<GenericCheckBoxCtrl>(Type::AutoCloseOnAllTasksCompletion,
                                                           label,
                                                           autoClose);


    const auto autoRemove = GlobalSettingsManager::instance()
            ->getValueAs<bool>(Enum::QC_AutoRemoveCompletedTasks);
    label = Packuru::Core::Queue::SettingsDialogCore::tr("Automatically remove completed tasks");
    priv->engine->createTopController<GenericCheckBoxCtrl>(Type::AutoRemoveCompletedTasks,
                                                           label,
                                                           autoRemove);


    const auto maxErrorCount = GlobalSettingsManager::instance()
            ->getValueAs<int>(Enum::QC_MaxErrorCountToStop);
    priv->engine->createTopController<MaxErrorCountCtrl>(Type::MaxErrorCount,
                                                         maxErrorCount);

    priv->engine->updateAllControllers();
}


SettingsDialogCore::~SettingsDialogCore()
{

}


void SettingsDialogCore::destroyLater()
{
    deleteLater();
}

QString SettingsDialogCore::getString(SettingsDialogCore::UserString value)
{
    switch (value)
    {
    case UserString::PageTitle:
        return Packuru::Core::Queue::SettingsDialogCore::tr("Queue");

    default:
        Q_ASSERT(false); return "";
    }
}


ControllerEngine* SettingsDialogCore::getControllerEngine()
{
    return priv->engine;
}


void SettingsDialogCore::accept()
{
    const auto abortEncrypted = priv->engine->getControllerPropertyAs<bool>(Type::AbortTasksRequiringPassword,
                                                                            ControllerProperty::PublicCurrentValue);
    GlobalSettingsManager::instance()->setValue(Enum::QC_AbortTasksRequiringPassword, abortEncrypted);


    const auto autoClose = priv->engine->getControllerPropertyAs<bool>(Type::AutoCloseOnAllTasksCompletion,
                                                                       ControllerProperty::PublicCurrentValue);
    GlobalSettingsManager::instance()->setValue(Enum::QC_AutoCloseOnAllTasksCompletion, autoClose);


    const auto autoRemove = priv->engine->getControllerPropertyAs<bool>(Type::AutoRemoveCompletedTasks,
                                                                        ControllerProperty::PublicCurrentValue);
    GlobalSettingsManager::instance()->setValue(Enum::QC_AutoRemoveCompletedTasks, autoRemove);


    const auto maxErrorCount = priv->engine->getControllerPropertyAs<int>(Type::MaxErrorCount,
                                                                          ControllerProperty::PublicCurrentValue);
    GlobalSettingsManager::instance()->setValue(Enum::QC_MaxErrorCountToStop, maxErrorCount);
}

}
