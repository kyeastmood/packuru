// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QAbstractListModel>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{

namespace PluginDataModelAux
{
struct Init;
}

/**
 * @brief Stores information about loaded plugins.
 *
 * Object of this class is provided by AppSettingsDialogCore object.
 * @warning The object must not be destroyed manually.
 * @headerfile "core/plugindatamodel.h"
 */
class PACKURU_CORE_EXPORT PluginDataModel : public QAbstractListModel
{
    Q_OBJECT

public:
    PluginDataModel(PluginDataModelAux::Init& initData, QObject* parent = nullptr);
    ~PluginDataModel() override;

    ///@{
    /// @name Qt Model-View API
    int rowCount(const QModelIndex& parent) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    ///@}

    /// @brief Returns plugin description.
    Q_INVOKABLE QString getDescription(int row) const;

    /// @brief Returns plugin homepage.
    Q_INVOKABLE QString getHomepage(int row) const;

    /// @brief Returns plugin file path.
    Q_INVOKABLE QString getPath(int row) const;

    /// @brief Returns a list of supported types for reading (one type per line).
    Q_INVOKABLE QString supportedReadArchives(int row) const;

    /// @brief Returns a list of supported types for writing (one type per line).
    Q_INVOKABLE QString supportedWriteArchives(int row) const;

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
