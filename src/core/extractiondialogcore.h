// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>
#include <QModelIndexList>
#include <QUrl>

#include "symbol_export.h"

/// @file

class QAbstractItemModel;

namespace qcs::core
{
class ControllerEngine;
}


namespace Packuru::Core
{

struct ExtractionDialogCoreInitMulti;
struct ExtractionDialogCoreInitSingle;

/**
 @brief Contains data and logic necessary for Extraction Dialog.

 Object of this class is provided by ArchiveBrowserCore and QueueCore objects.

 @headerfile "core/extractiondialogcore.h"
 */
class PACKURU_CORE_EXPORT ExtractionDialogCore : public QObject
{
    Q_OBJECT
public:
    /// @copydoc ArchivingDialogCore::DialogMode
    enum class DialogMode
    {
        SingleArchive, ///< 'Single archive' mode (in Browser)
        MultiArchive ///< 'Multi archive' mode (in Queue)
    };
    Q_ENUM(DialogMode)

    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        Cancel, ///< 'Cancel' button.
        DialogTitleMulti, ///< Dialog title in 'Multi archive' mode.
        DialogTitleSingle, ///< Dialog title in 'Single archive' mode.
        TabArchives, ///< 'Archives' tab.
        TabOptions, ///< 'Options' tab.
    };
    Q_ENUM(UserString)

    // 'single archive' constructor
    ExtractionDialogCore(ExtractionDialogCoreInitSingle initData, QObject* parent = nullptr);

    // 'multi archive' constructor
    ExtractionDialogCore(ExtractionDialogCoreInitMulti initData, QObject* parent = nullptr);

    ~ExtractionDialogCore() override;
    
    /// @copydoc ArchivingDialogCore::destroyLater
    Q_INVOKABLE void destroyLater();

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE static QString getString(UserString value);

    /// @copydoc ArchivingDialogCore::getMode
    Q_INVOKABLE DialogMode getMode() const;

    /// @brief Adds archives to input archives model.
    Q_INVOKABLE void addArchives(const QList<QUrl>& archives);

    /// @brief Removes archives corresponding to specified indexes from input archives model.
    Q_INVOKABLE void removeArchives(const QModelIndexList& indexes);

    /** @brief Returns the input archives model.
     @note Object ownership is not transferred to the caller.
     */
    Q_INVOKABLE QAbstractItemModel* getArchivesModel() const;

    /**
     @brief Returns pointer to ControllerEngine controlling most input widgets in the dialog.

     The widgets must be mapped in WidgetMapper using ExtractionDialogControllerType::Type enum as the ID.

     @note Object ownership is not transferred to the caller.
     @sa qcs::qqc::WidgetMapper, qcs::qtw::WidgetMapper
     */
    Q_INVOKABLE qcs::core::ControllerEngine* getControllerEngine();

    /// @copydoc ArchivingDialogCore::accept
    Q_INVOKABLE void accept();
    
private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
