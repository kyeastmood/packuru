// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "utils/makeqpointer.h"

#include "archivetypemodel.h"

#include "private/archivetypemodelaux.h"


namespace Packuru::Core
{

struct ArchiveTypeModel::Private
{
    void saveChanges();

    ArchiveTypeModelAux::InitData initData;
};


ArchiveTypeModel::ArchiveTypeModel(ArchiveTypeModelAux::InitData& initData,
                                   ArchiveTypeModelAux::BackdoorData& backdoorData,
                                   QObject *parent)
    : QAbstractTableModel(parent),
      priv(new Private)
{
    priv->initData = std::move(initData);
    auto& entries = priv->initData.entries;

    std::sort(entries.begin(), entries.end(), [] (const auto& e1, const auto& e2)
    { return e1.archiveType < e2.archiveType; });

    for (auto& entry : entries)
    {
        std::array<QStringList*, 2> lists { &entry.read.availablePlugins, &entry.write.availablePlugins };

        for (auto& list : lists)
            std::sort(list->begin(), list->end(), [] (const auto& s1, const auto& s2) { return s1 < s2; });
    }

    backdoorData.saveChanges = [model = Utils::makeQPointer(this)] ()
    {
        if (!model)
            return;

        model->priv->saveChanges();
    };
}

ArchiveTypeModel::~ArchiveTypeModel()
{
}


QVariant ArchiveTypeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
    {
        switch (section)
        {
        case 0: return Packuru::Core::ArchiveTypeModel::tr("Archive type");
        case 1: return Packuru::Core::ArchiveTypeModel::tr("Read plugin");
        case 2: return Packuru::Core::ArchiveTypeModel::tr("Write plugin");
        }
    }

    return QVariant();
}


int ArchiveTypeModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return static_cast<int>(priv->initData.entries.size());
}


int ArchiveTypeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 3;
}


QVariant ArchiveTypeModel::data(const QModelIndex &index, int role) const
{
    Q_ASSERT(index.isValid());

    const auto row = static_cast<std::size_t>(index.row());
    const auto& entries = priv->initData.entries;

    if (role == Qt::DisplayRole)
    {
        switch (index.column())
        {
        case 0: return entries[row].archiveType;
        case 1: return entries[row].read.currentPlugin;
        case 2: return entries[row].write.currentPlugin;
        }
    }

    return QVariant();
}

bool ArchiveTypeModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    Q_ASSERT(index.isValid());

    if (role != Qt::EditRole)
        return false;

    const auto row = static_cast<std::size_t>(index.row());
    auto& entries = priv->initData.entries;

    if (index.column() == 1)
    {
        entries[row].read.currentPlugin = value.toString();
        entries[row].read.changed = true;
        return true;
    }
    else if (index.column() == 2)
    {
        entries[row].write.currentPlugin = value.toString();
        entries[row].write.changed = true;
        return true;
    }

    return false;
}

Qt::ItemFlags ArchiveTypeModel::flags(const QModelIndex& index) const
{
    Q_ASSERT(index.isValid());

    const auto flg = QAbstractItemModel::flags(index);

    switch (index.column())
    {
    case 0:
        return Qt::ItemIsEnabled;
    case 1:
        return flg | Qt::ItemIsEditable;
    case 2:
        return flg | Qt::ItemIsEditable;
    default:
        Q_ASSERT(false); return flg;
    }

}


QStringList ArchiveTypeModel::getReadPlugins(int row) const
{
    Q_ASSERT(row >= 0);
    const auto vectorRow = static_cast<std::size_t>(row);
    const auto& entries = priv->initData.entries;
    Q_ASSERT(vectorRow < entries.size());
    return entries[vectorRow].read.availablePlugins;
}


QString ArchiveTypeModel::getCurrentReadPlugin(int row) const
{
    Q_ASSERT(row >= 0);
    const auto vectorRow = static_cast<std::size_t>(row);
    const auto& entries = priv->initData.entries;
    Q_ASSERT(vectorRow < entries.size());
    return entries[vectorRow].read.currentPlugin;
}


QStringList ArchiveTypeModel::getWritePlugins(int row) const
{
    Q_ASSERT(row >= 0);
    const auto vectorRow = static_cast<std::size_t>(row);
    const auto& entries = priv->initData.entries;
    Q_ASSERT(vectorRow < entries.size());
    return entries[vectorRow].write.availablePlugins;
}


QString ArchiveTypeModel::getCurrentWritePlugin(int row) const
{
    Q_ASSERT(row >= 0);
    const auto vectorRow = static_cast<std::size_t>(row);
    const auto& entries = priv->initData.entries;
    Q_ASSERT(vectorRow < entries.size());
    return entries[vectorRow].write.currentPlugin;
}


void ArchiveTypeModel::setReadPlugin(int row, const QString& value)
{
    Q_ASSERT(row >= 0);
    const auto vectorRow = static_cast<std::size_t>(row);
    auto& entries = priv->initData.entries;
    Q_ASSERT(vectorRow < entries.size());
    entries[vectorRow].read.currentPlugin = value;
    entries[vectorRow].read.changed = true;
    emit dataChanged(index(row, 0), index(row, columnCount()));
}


void ArchiveTypeModel::setWritePlugin(int row, const QString& value)
{
    Q_ASSERT(row >= 0);
    const auto vectorRow = static_cast<std::size_t>(row);
    auto& entries = priv->initData.entries;
    Q_ASSERT(vectorRow < entries.size());
    entries[vectorRow].write.currentPlugin = value;
    entries[vectorRow].write.changed = true;
    emit dataChanged(index(row, 0), index(row, columnCount()));
}


QStringList ArchiveTypeModel::getValueSet(const QModelIndex& index) const
{
    Q_ASSERT(index.isValid());

    if (index.column() == 1)
        return getReadPlugins(index.row());
    else if (index.column() == 2)
        return getWritePlugins(index.row());

    Q_ASSERT(false);
    return QStringList();
}


void ArchiveTypeModel::Private::saveChanges()
{
    ArchiveTypeModelAux::ChangedValuesMap map;

    for (const auto& entry : initData.entries)
    {
        if (entry.read.changed)
            map[entry.archiveType].newReadPlugin = entry.read.currentPlugin;

        if (entry.write.changed)
            map[entry.archiveType].newWritePlugin = entry.write.currentPlugin;
    }

    if (!map.empty())
    {
        Q_ASSERT(initData.onValuesChanged);
        initData.onValuesChanged(map);
    }
}

}
