// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

/// @file

namespace Packuru::Core
{

/**
 @brief Identifies input widgets in core application settings.
 @headerfile "core/appsettingscontrollertype.h"
 */
class AppSettingsControllerType : public QObject
{
    Q_OBJECT
public:
    /// @copybrief Packuru::Core::AppSettingsControllerType
    enum class Type
    {
        CompressionRatioDisplayMode, ///< Combo box
        SizeUnits, ///< Combo box
        SizeUnitsExample ///< Line edit
    };

    Q_ENUM(Type)

};

}
