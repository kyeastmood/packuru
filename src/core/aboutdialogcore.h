// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{

/**
 @brief Contains all data necessary for About Dialog.

 The object must be manually created.

 @headerfile "core/aboutdialogcore.h"
 */
class PACKURU_CORE_EXPORT AboutDialogCore : public QObject
{
    Q_OBJECT
public:
    /**
     @brief Identifies strings to display in the dialog.
     */
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        AppInfo, ///< Full application info, including 3rd-party code (Markdown format).
        DialogTitle, ///< Dialog title.
    };
    Q_ENUM(UserString)

    explicit AboutDialogCore(QObject *parent = nullptr);

    /// @brief Returns a translated string for specified ID.
    Q_INVOKABLE static QString getString(UserString value);
};

}
