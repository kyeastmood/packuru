// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>
#include <QString>
#include <QModelIndexList>
#include <QUrl>

#include "symbol_export.h"

/// @file

class QAbstractItemModel;


namespace qcs::core
{
class ControllerEngine;
}


namespace Packuru::Core
{

struct TestDialogCoreInitSingle;
struct TestDialogCoreInitMulti;

/**
 @brief Contains data and logic necessary for Test Dialog.

 Object of this class is provided by ArchiveBrowserCore and QueueCore objects.

 @headerfile "core/testdialogcore.h"
 */
class PACKURU_CORE_EXPORT TestDialogCore : public QObject
{
    Q_OBJECT

public:
    /// @copydoc ArchivingDialogCore::DialogMode
    enum class DialogMode
    {
        SingleArchive, ///< @copydoc ExtractionDialogCore::DialogMode::SingleArchive
        MultiArchive ///< @copydoc ExtractionDialogCore::DialogMode::MultiArchive
    };
    Q_ENUM(DialogMode)

    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        Cancel, ///< 'Cancel' button.
        DialogTitleMulti, ///< Dialog title in 'Multi archive' mode.
        DialogTitleSingle, ///< Dialog title in 'Single archive' mode.
        TabArchives, ///< 'Archives' tab.
        TabOptions, ///< 'Options' tab.
    };
    Q_ENUM(UserString)

    // 'Single archive' constructor
    explicit TestDialogCore(TestDialogCoreInitSingle initData, QObject *parent = nullptr);

    // 'Multi archive' constructor
    explicit TestDialogCore(TestDialogCoreInitMulti initData, QObject *parent = nullptr);

    ~TestDialogCore();

    /// @copydoc ArchivingDialogCore::destroyLater
    Q_INVOKABLE void destroyLater();

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE static QString getString(UserString value);

    /// @copydoc ArchivingDialogCore::getMode
    Q_INVOKABLE DialogMode getMode() const;

    /// @copydoc ExtractionDialogCore::getArchivesModel
    Q_INVOKABLE QAbstractItemModel* getArchivesModel() const;

    /// @copydoc ExtractionDialogCore::addArchives
    Q_INVOKABLE void addArchives(const QList<QUrl>& archives);

    /// @copydoc ExtractionDialogCore::removeArchives
    Q_INVOKABLE void removeArchives(const QModelIndexList& indexes);

    /**
     @brief Returns pointer to ControllerEngine controlling most input widgets in the dialog.

     The widgets must be mapped in WidgetMapper using TestDialogControllerType::Type enum as the ID.

     @note Object ownership is not transferred to the caller.
     @sa qcs::qqc::WidgetMapper, qcs::qtw::WidgetMapper
     */
    Q_INVOKABLE qcs::core::ControllerEngine* getControllerEngine();

    /// @copydoc ArchivingDialogCore::accept
    Q_INVOKABLE void accept();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
