// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "commonstrings.h"


namespace Packuru::Core
{

CommonStrings::CommonStrings(QObject *parent)
    : QObject(parent)
{

}


QString CommonStrings::getString(CommonStrings::UserString value, int count)
{
    Q_UNUSED(count)

    switch (value)
    {
    case UserString::Add:
        return Packuru::Core::CommonStrings::tr("Add...", "Add items");

    case UserString::ArchivePropertiesDialogTitle:
        return Packuru::Core::CommonStrings::tr("Properties");

    case UserString::Back:
        return Packuru::Core::CommonStrings::tr("Back");

    case UserString::Cancel:
        return Packuru::Core::CommonStrings::tr("Cancel");

    case UserString::Close:
        return Packuru::Core::CommonStrings::tr("Close", "e.g. a dialog");

    case UserString::CommandLineErrorDialogTitle:
        return Packuru::Core::CommonStrings::tr("Command line error");

    case UserString::CommandLineHelpDialogTitle:
        return Packuru::Core::CommonStrings::tr("Command line help");

    case UserString::Comment:
        return Packuru::Core::CommonStrings::tr("Comment");

    case UserString::ErrorLogDialogTitle:
        return Packuru::Core::CommonStrings::tr("Error log");

    case UserString::Remove:
        return Packuru::Core::CommonStrings::tr("Remove", "Remove items");

    default:
        Q_ASSERT(false); return "";
    }
}

}
