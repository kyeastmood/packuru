// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <map>
#include <unordered_set>

#include <QStandardPaths>
#include <QUrl>

#include "../utils/private/tounderlyingtype.h"
#include "../utils/private/enumcast.h"

#include "../qcs-core/controllerengine.h"
#include "../qcs-core/controllerproperty.h"

#include "archivingdialogcontrollertype.h"
#include "archivingdialogcore.h"
#include "commonstrings.h"
#include "private/plugin-api/archivingparameterhandlerinitdata.h"
#include "private/dialogcore/archivingdialog/archivingdialogcoreinitadd.h"
#include "private/dialogcore/archivingdialog/archivingdialogcoreinitcreate.h"
#include "private/plugin-api/archivingparameterhandlerfactory.h"
#include "private/dialogcore/archivingdialog/inputfilesmodel.h"
#include "private/dialogcore/archivingdialog/archivenamectrl.h"
#include "private/dialogcore/archivingdialog/destinationpathctrl.h"
#include "private/dialogcore/archivingdialog/archivingmodectrl.h"
#include "private/dialogcore/archivingdialog/destinationmodectrl.h"
#include "private/dialogcore/archivingdialog/archivetypectrl.h"
#include "private/dialogcore/archivingdialog/splitpresetctrl.h"
#include "private/dialogcore/archivingdialog/splitsizectrl.h"
#include "private/dialogcore/archivingdialog/generalcompressionlevelctrl.h"
#include "private/dialogcore/archivingdialog/compressionstatectrl.h"
#include "private/plugin-api/archivingcompressioninfo.h"
#include "private/dialogcore/archivingdialog/passwordctrl.h"
#include "private/dialogcore/archivingdialog/encryptionstatectrl.h"
#include "private/dialogcore/archivingdialog/encryptionmodectrl.h"
#include "private/dialogcore/archivingdialog/internalpathctrl.h"
#include "private/dialogcore/archivingdialog/opennewarchivectrl.h"
#include "private/dialogcore/archivingdialog/archivingopendestinationctrl.h"
#include "private/dialogcore/archivingdialog/archivenameerrorctrl.h"
#include "private/dialogcore/archivingdialog/directoryerrorctrl.h"
#include "private/dialogcore/archivingdialog/archivingfilepathsctrl.h"
#include "private/dialogcore/archivingdialog/passwordmatchctrl.h"
#include "private/dialogcore/runinqueuectrl.h"
#include "private/dialogcore/acceptbuttonctrl.h"
#include "private/dialogcore/genericdialogerrorsctrl.h"
#include "private/tasks/archivingtaskdata.h"
#include "private/privatestrings.h"


using namespace qcs::core;


namespace Packuru::Core
{

struct ArchivingDialogCore::Private
{
    Private(ArchivingDialogCore* publ, ArchivingDialogCore::DialogMode dialogMode);
    ~Private();

    void setup(const QString& absArchivePath,
               const std::vector<QFileInfo>& filesToArchive,
               const QString& archiveInternalPath,
               const QString& password);

    void onControllerSyncedToWidget(ArchivingDialogControllerType::Type type);
    void setCurrentHandler(int index);

    ArchivingDialogCore* publ;
    ArchivingDialogCore::DialogMode dialogMode;
    ArchivingDialogCoreInitAdd::OnAcceptedCallback onAcceptedAdd;
    ArchivingDialogCoreInitCreate::OnAcceptedCallback onAcceptedCreate;

    std::vector<std::unique_ptr<ArchivingParameterHandlerFactory>> factoryDataList;
    InputFilesModel* fileTableModel;
    ArchivingParameterHandlerBase* currentHandler;
    ArchivingParameterHandlerFactory* currentFactory;
    ControllerEngine* engine;
    GeneralCompressionLevelCtrl* compressionLevelCtrl;
    EncryptionStateCtrl* encryptionStateCtrl;
    EncryptionState currentArchiveEncryption;
};


ArchivingDialogCore::ArchivingDialogCore(ArchivingDialogCoreInitAdd initData, QObject *parent)
    : QObject(parent),
      priv(new Private(this, DialogMode::AddToArchive))
{
    Q_ASSERT(initData.isValid());

    priv->currentArchiveEncryption = initData.archiveEncryption;
    priv->onAcceptedAdd = std::move(initData.onAccepted);
    priv->factoryDataList.push_back(std::move(initData.factory));

    priv->setup(initData.archiveInfo.absoluteFilePath(),
                initData.filesToArchive,
                initData.archiveInternalPath,
                initData.password);
}


ArchivingDialogCore::ArchivingDialogCore(ArchivingDialogCoreInitCreate initData, QObject *parent)
    : QObject(parent),
      priv(new Private(this, DialogMode::CreateArchive))
{
    Q_ASSERT(initData.isValid());

    priv->onAcceptedCreate = std::move(initData.onAccepted);
    priv->factoryDataList = std::move(initData.factoryDataList);

    priv->setup(QString(),
                initData.filesToArchive,
                QString(),
                QString());
}


ArchivingDialogCore::~ArchivingDialogCore()
{

}


void ArchivingDialogCore::destroyLater()
{
    deleteLater();
}


QString ArchivingDialogCore::getString(UserString value)
{
    switch (value)
    {
    case UserString::AddFiles:
        return Packuru::Core::ArchivingDialogCore::tr("Add files...", "Button");

    case UserString::AddFolder:
        return Packuru::Core::ArchivingDialogCore::tr("Add folder");

    case UserString::Cancel:
        return CommonStrings::getString(CommonStrings::UserString::Cancel);

    case UserString::Compression:
        return Packuru::Core::ArchivingDialogCore::tr("Compression");

    case UserString::DialogTitleAdd:
        return Packuru::Core::ArchivingDialogCore::tr("Add files");

    case UserString::DialogTitleCreate:
        return Packuru::Core::ArchivingDialogCore::tr("Create new archive");

    case UserString::Encryption:
        return Packuru::Core::ArchivingDialogCore::tr("Encryption");

    case UserString::FilePaths:
        return Packuru::Core::ArchivingDialogCore::tr("File paths");

    case UserString::OnCompletion:
        return Packuru::Core::ArchivingDialogCore::tr("On completion");

    case UserString::PluginSettings:
        return Packuru::Core::ArchivingDialogCore::tr("Plugin settings");

    case UserString::RemoveItems:
        return Packuru::Core::ArchivingDialogCore::tr("Remove");

    case UserString::TabAdvanced:
        return Packuru::Core::ArchivingDialogCore::tr("Advanced");

    case UserString::TabArchive:
        return Packuru::Core::ArchivingDialogCore::tr("Archive");

    case UserString::TabFiles:
        return Packuru::Core::ArchivingDialogCore::tr("Files");

    case UserString::TabOptions:
        return Packuru::Core::ArchivingDialogCore::tr("Options");

    default:
        Q_ASSERT(false); return "";
    }
}


ArchivingDialogCore::DialogMode ArchivingDialogCore::getMode() const
{
    return priv->dialogMode;
}


ControllerEngine* ArchivingDialogCore::getControllerEngine() const
{
    return priv->engine;
}


QAbstractItemModel* ArchivingDialogCore::getFileModel() const
{
    return priv->fileTableModel;
}


ControllerIdRowMapper* ArchivingDialogCore::getPluginRowMapper() const
{
    if (!priv->currentHandler)
        return nullptr;

    return priv->currentHandler->getMapper();
}


void ArchivingDialogCore::accept()
{
    const auto hasErrors
            = priv->engine->getControllerPropertyAs<bool>(ArchivingDialogControllerType::Type::DialogErrors,
                                                          ControllerProperty::PrivateCurrentValue);

    Q_ASSERT(!hasErrors);

    auto values = priv->engine->getAvailableCurrentValues<ArchivingDialogControllerType::Type>();

    ArchivingTaskData data;
    data.pluginId = priv->currentFactory->getPluginId();
    data.runInQueue = Utils::getValueAs<bool>(values, ArchivingDialogControllerType::Type::RunInQueue);
    BackendArchivingData& backendData = data.backendData;
    backendData.archiveType = priv->currentHandler->getArchiveType();
    backendData.createNewArchive = priv->dialogMode == DialogMode::CreateArchive;
    backendData.pluginPrivateData = priv->currentHandler->getPrivateArchivingData();
    // QUrl will remove "file://" from path which is passed by qml
    const QUrl destUrl
            = Utils::getValueAs<QString>(values,
                                         ArchivingDialogControllerType::Type::DestinationPath);
    backendData.destinationPath = destUrl.path();

    if (priv->dialogMode == DialogMode::CreateArchive)
    {
        data.openArchiveOnCompletion
                = Utils::getValueAs<bool>(values,
                                          ArchivingDialogControllerType::Type::OpenNewArchive);
        data.openDestinationOnCompletion
                = Utils::getValueAs<bool>(values,
                                          ArchivingDialogControllerType::Type::OpenDestinationPath);
    }

    backendData.currentArchiveEncryption = priv->currentArchiveEncryption;

    if (priv->currentArchiveEncryption == EncryptionState::EntireArchive)
    {
        Q_ASSERT(priv->dialogMode == DialogMode::AddToArchive);
        backendData.newArchiveEncryption = data.backendData.currentArchiveEncryption;
    }
    else
    {
        backendData.newArchiveEncryption = priv->encryptionStateCtrl->getEncryptionState();
        Q_ASSERT(backendData.newArchiveEncryption != EncryptionState::___INVALID);
        // It is currently not allowed to fully encrypt an archive with file content encryption only
        Q_ASSERT(!(backendData.currentArchiveEncryption == EncryptionState::FilesContentOnly
                   && backendData.newArchiveEncryption == EncryptionState::EntireArchive));
    }

    if (backendData.newArchiveEncryption == EncryptionState::EntireArchive
            || backendData.newArchiveEncryption == EncryptionState::FilesContentOnly)
    {
        backendData.password =
                Utils::getValueAs<QString>(values, ArchivingDialogControllerType::Type::Password);
        Q_ASSERT(backendData.password.length() > 0);
    }

    data.backendData.filePaths
            = Utils::getValueAs<ArchivingFilePaths>(values,
                                                    ArchivingDialogControllerType::Type::FilePaths);

    if (priv->dialogMode == DialogMode::CreateArchive)
    {
        backendData.partSize = Utils::getValueAs<qulonglong>
                (values[ArchivingDialogControllerType::Type::SplitPreset]);
    }

    backendData.internalPath
            = Utils::getValueAs<QString>(values,
                                         ArchivingDialogControllerType::Type::InternalPath);
    // Adding / to empty path would create an absolute path
    if (backendData.internalPath.length() > 0 && !backendData.internalPath.endsWith(QLatin1Char('/')))
        backendData.internalPath.append(QLatin1Char('/'));

    std::vector<ArchivingTaskData> dataList;

    const auto archivingMode
            = Utils::getValueAs<ArchivingMode>(values,
                                               ArchivingDialogControllerType::Type::ArchivingMode);
    switch (archivingMode)
    {
    case ArchivingMode::SingleArchive:
    {
        backendData.archiveName
                = Utils::getValueAs<QString>(values,
                                             ArchivingDialogControllerType::Type::ArchiveName);
        backendData.inputItems = priv->fileTableModel->getItems();
        dataList.push_back(data);
        break;
    }
    case ArchivingMode::ArchivePerItem:
    {
        const auto destinationMode
                = Utils::getValueAs<ArchivingDestinationMode>(values,
                                                              ArchivingDialogControllerType::Type::DestinationMode);

        const auto items = priv->fileTableModel->getItems();
        int index = 0;
        for (const auto& item : items)
        {
            backendData.archiveName = item.fileName();
            if (destinationMode == ArchivingDestinationMode::ItemParentFolder)
                backendData.destinationPath = item.absolutePath();
            backendData.inputItems.clear();
            backendData.inputItems.push_back(item);
            dataList.push_back(data);
            ++index;
        }
        break;
    }
    default:
        Q_ASSERT(false);
        break;
    }

    switch (priv->dialogMode)
    {
    case DialogMode::AddToArchive:
        Q_ASSERT(dataList.size() == 1);
        priv->onAcceptedAdd(dataList[0]);
        break;
    case DialogMode::CreateArchive:
        priv->onAcceptedCreate(dataList);
        break;
    default:
        Q_ASSERT(false);
        break;
    }
}


void ArchivingDialogCore::addItems(const QList<QUrl>& newItems)
{
    priv->fileTableModel->addItems(newItems);
}


void ArchivingDialogCore::removeItems(const QModelIndexList& indexes)
{   
    priv->fileTableModel->removeItems(indexes);
}


ArchivingDialogCore::Private::Private(ArchivingDialogCore* publ, ArchivingDialogCore::DialogMode mode)
    : publ(publ),
      dialogMode(mode),
      fileTableModel(new InputFilesModel(publ)),
      currentHandler(nullptr),
      currentFactory(nullptr),
      engine(new ControllerEngine(publ)),
      compressionLevelCtrl(nullptr),
      encryptionStateCtrl(nullptr),
      currentArchiveEncryption(EncryptionState::Disabled)
{

}


ArchivingDialogCore::Private::~Private()
{
}


void ArchivingDialogCore::Private::setup(const QString& absArchivePath,
                                         const std::vector<QFileInfo>& filesToArchive,
                                         const QString& archiveInternalPath,
                                         const QString& password)
{
    fileTableModel->addItems(filesToArchive);

    QString archiveName;
    QString destinationPath;

    if (dialogMode == ArchivingDialogCore::DialogMode::AddToArchive)
    {
        Q_ASSERT(!absArchivePath.isEmpty());
        QFileInfo info(absArchivePath);
        Q_ASSERT(info.isFile());
        archiveName = info.fileName();
        destinationPath = info.path();
    }
    else if (dialogMode == ArchivingDialogCore::DialogMode::CreateArchive)
    {
        if (absArchivePath.isEmpty())
        {
            const auto homeLocations = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);

            const QString defaultArchiveName = Packuru::Core::ArchivingDialogCore::tr("archive");
            if (filesToArchive.size() == 0)
            {
                archiveName = defaultArchiveName;
                if (!homeLocations.empty())
                    destinationPath = homeLocations[0];
            }
            else
            {
                const QString parentFolder = fileTableModel->getCommonParentFolder();
                const QFileInfo parentFolderInfo(parentFolder);
                const bool parentFolderValid = !parentFolder.isEmpty()
                        && parentFolderInfo.exists()
                        && parentFolderInfo.isDir();

                const int itemCount = fileTableModel->rowCount();
                Q_ASSERT(itemCount >= 0);

                if (itemCount == 0)
                    archiveName = defaultArchiveName;
                else if (itemCount == 1)
                    archiveName = filesToArchive.front().fileName();
                else
                {
                    if (parentFolderValid)
                        archiveName = parentFolderInfo.fileName();
                    else
                        archiveName = defaultArchiveName;
                }

                if (parentFolderValid)
                    destinationPath = parentFolderInfo.absoluteFilePath();
                else if (!homeLocations.empty())
                    destinationPath = homeLocations[0];
            }
        }
        else
        {
            QFileInfo info(absArchivePath);
            archiveName = info.fileName();
            destinationPath = info.absolutePath();
        }
    }
    else
        Q_ASSERT(false);

    auto archiveNameCtrl = engine->createController<ArchiveNameCtrl>
            (ArchivingDialogControllerType::Type::ArchiveName,
             archiveName);

    auto destinationPathCtrl = engine->createController<DestinationPathCtrl>
            (ArchivingDialogControllerType::Type::DestinationPath,
             destinationPath);

    auto archivingModeCtrl = engine->createTopController<ArchivingModeCtrl>
            (ArchivingDialogControllerType::Type::ArchivingMode);

    auto destinationModeCtrl = engine->createController<DestinationModeCtrl>
            (ArchivingDialogControllerType::Type::DestinationMode);

    Q_ASSERT(factoryDataList.size() > 0);

    std::sort(factoryDataList.begin(), factoryDataList.end(),
              [] (const auto& data1, const auto& data2)
    { return data1->getArchiveTypeAlias() < data2->getArchiveTypeAlias(); });

    QList<QString> aliases;
    for (const auto& d : factoryDataList)
        aliases.push_back(d->getArchiveTypeAlias());

    int initialHandlerIndex = 0;

    auto archiveTypeCtrl = engine->createTopController<ArchiveTypeCtrl>
            (ArchivingDialogControllerType::Type::ArchiveType, aliases, initialHandlerIndex);

    const auto archiveSplittingSupport = [handler = &currentHandler] ()
    { return (*handler)->archiveSplittingSupport(); };

    auto splitPresetCtrl = engine->createController<SplitPresetCtrl>
            (ArchivingDialogControllerType::Type::SplitPreset, archiveSplittingSupport);

    auto splitSizeCtrl = engine->createController<SplitSizeCtrl>
            (ArchivingDialogControllerType::Type::SplitSize, archiveSplittingSupport);

    auto dirErrorCtrl = engine->createController<DirectoryErrorCtrl>
            (ArchivingDialogControllerType::Type::DirectoryError);

    auto archiveNameErrorCtrl = engine->createController<ArchiveNameErrorCtrl>
            (ArchivingDialogControllerType::Type::ArchiveNameError);

    auto internalPathCtrl = engine->createController<InternalPathCtrl>
            (ArchivingDialogControllerType::Type::InternalPath,
             archiveInternalPath,
             [handler = &currentHandler] () { return (*handler)->customInternalPathSupport(); });

    const auto getCompressionInfo = [handler = &currentHandler] ()
    { return (*handler)->getCompressionInfo(); };

    const auto setCompressionLevel = [handler = &currentHandler] (auto level)
    { return (*handler)->setCompressionLevel(level); };

    compressionLevelCtrl = engine->createController<GeneralCompressionLevelCtrl>
            (ArchivingDialogControllerType::Type::CompressionLevel, getCompressionInfo, setCompressionLevel);

    auto compressionStateCtrl = engine->createController<CompressionStateCtrl>
            (ArchivingDialogControllerType::Type::CompressionState,
             getCompressionInfo);

    const auto getEncryptionSupport = [handler = &currentHandler] ()
    { return (*handler)->getEncryptionSupport(); };

    auto passwordCtrl = engine->createController<PasswordCtrl>
            (ArchivingDialogControllerType::Type::Password,
             PrivateStrings::getString(PrivateStrings::UserString::Password),
             password,
             getEncryptionSupport);

    auto passwordRepeatCtrl = engine->createController<PasswordCtrl>
            (ArchivingDialogControllerType::Type::PasswordRepeat,
             Packuru::Core::ArchivingDialogCore::tr("Repeat"),
             password,
             getEncryptionSupport);

    auto passwordMatchCtrl = engine->createController<PasswordMatchCtrl>
            (ArchivingDialogControllerType::Type::PasswordMatch);

    auto encryptionModeCtrl = engine->createController<EncryptionModeCtrl>
            (ArchivingDialogControllerType::Type::EncryptionMode,
             currentArchiveEncryption,
             getEncryptionSupport);

    const auto getEncryptionMethod = [handler = &currentHandler] ()
    { return (*handler)->getEncryptionMethod(); };

    encryptionStateCtrl = engine->createController<EncryptionStateCtrl>
            (ArchivingDialogControllerType::Type::EncryptionState,
             getEncryptionSupport,
             getEncryptionMethod);

    const auto getSupportedFilePaths = [handler = &currentHandler] ()
    { return (*handler)->getSupportedFilePaths(); };

    auto filePathsCtrl = engine->createController<ArchivingFilePathsCtrl>
            (ArchivingDialogControllerType::Type::FilePaths, getSupportedFilePaths);

    engine->createTopController<OpenNewArchiveCtrl>
            (ArchivingDialogControllerType::Type::OpenNewArchive);

    engine->createTopController<ArchivingOpenDestinationCtrl>
            (ArchivingDialogControllerType::Type::OpenDestinationPath);

    std::vector<ArchivingDialogControllerType::Type> errorIds
            = {ArchivingDialogControllerType::Type::ArchiveNameError,
               ArchivingDialogControllerType::Type::DirectoryError,
               ArchivingDialogControllerType::Type::PasswordMatch};

    using DialogErrorsCtrl = GenericDialogErrorsCtrl<ArchivingDialogControllerType::Type>;

    auto dialogErrorsCtrl = engine->createController<DialogErrorsCtrl>
            (ArchivingDialogControllerType::Type::DialogErrors,
             fileTableModel->rowCount() == 0,
             errorIds);

    QObject::connect(fileTableModel, &InputFilesModel::modelEmpty,
                     publ, [ctrl = dialogErrorsCtrl] (bool empty)
    { ctrl->setFileListEmpty(empty); });

    using AcceptButtonCtrlType = AcceptButtonCtrl<ArchivingDialogControllerType::Type>;

    auto acceptButtonCtrl = engine->createController<AcceptButtonCtrlType>
            (ArchivingDialogControllerType::Type::AcceptButton,
             ArchivingDialogControllerType::Type::DialogErrors);

    const auto runInQueueMode =
            dialogMode == ArchivingDialogCore::DialogMode::CreateArchive
            ? RunInQueueCtrl::Mode::ForcedQueue
            : RunInQueueCtrl::Mode::Switchable;

    engine->createTopController<RunInQueueCtrl>
            (ArchivingDialogControllerType::Type::RunInQueue, runInQueueMode);

    archivingModeCtrl->addDependant(destinationModeCtrl);
    archivingModeCtrl->addDependant(archiveNameCtrl);
    archiveNameCtrl->addDependant(archiveNameErrorCtrl);
    archiveNameErrorCtrl->addDependant(dialogErrorsCtrl);
    destinationModeCtrl->addDependant(destinationPathCtrl);
    destinationPathCtrl->addDependant(dirErrorCtrl);
    dirErrorCtrl->addDependant(dialogErrorsCtrl);
    archiveTypeCtrl->addDependant(splitPresetCtrl);
    splitPresetCtrl->addDependant(splitSizeCtrl);
    splitSizeCtrl->addDependant(splitPresetCtrl);
    archiveTypeCtrl->addDependant(compressionLevelCtrl);
    compressionLevelCtrl->addDependant(compressionStateCtrl);
    archiveTypeCtrl->addDependant(passwordCtrl);
    archiveTypeCtrl->addDependant(passwordRepeatCtrl);
    archiveTypeCtrl->addDependant(encryptionModeCtrl);
    archiveTypeCtrl->addDependant(filePathsCtrl);
    filePathsCtrl->addDependant(internalPathCtrl);
    passwordCtrl->addDependant(passwordMatchCtrl);
    passwordRepeatCtrl->addDependant(passwordMatchCtrl);
    passwordMatchCtrl->addDependant(encryptionStateCtrl);
    passwordMatchCtrl->addDependant(dialogErrorsCtrl);
    encryptionModeCtrl->addDependant(encryptionStateCtrl);
    dialogErrorsCtrl->addDependant(acceptButtonCtrl);

    QObject::connect(engine, &ControllerEngine::sigControllerSyncedToWidget,
                     publ, [publ = publ] (auto id)
    {
        publ->priv->onControllerSyncedToWidget(static_cast<ArchivingDialogControllerType::Type>(id));
    });

    setCurrentHandler(initialHandlerIndex);

    engine->updateAllControllers();
}


void ArchivingDialogCore::Private::onControllerSyncedToWidget(ArchivingDialogControllerType::Type type)
{
    switch (type)
    {
    case ArchivingDialogControllerType::Type::ArchiveType:
    {
        const auto index = engine->getControllerPropertyAs<int>(ArchivingDialogControllerType::Type::ArchiveType,
                                                                ControllerProperty::PrivateCurrentValue);
        setCurrentHandler(index);
        break;
    }
    default:
        break;
    }
}


void ArchivingDialogCore::Private::setCurrentHandler(int index)
{

    if (currentHandler)
        currentHandler->disconnect();

    currentFactory = factoryDataList[static_cast<std::size_t>(index)].get();
    currentHandler = currentFactory->getHandler();

    if (!currentHandler)
    {
        const bool creatingNewArchive = dialogMode == ArchivingDialogCore::DialogMode::CreateArchive;

        const ArchivingParameterHandlerInitData initData{currentFactory->getArchiveType(),
                    creatingNewArchive,
                    currentArchiveEncryption};

        currentHandler = currentFactory->createHandler(initData);
    }

    QObject::connect(currentHandler, &ArchivingParameterHandlerBase::sigCompressionInfoChanged,
                     publ, [publ = publ] ()
    {
        publ->priv->compressionLevelCtrl->onExternalCompressionInfoChange();
    });

    QObject::connect(currentHandler, &ArchivingParameterHandlerBase::sigEncryptionMethodChanged,
                     publ, [publ = publ] (auto method)
    {
        publ->priv->encryptionStateCtrl->onEncryptionMethodChanged(method);
    });

    publ->pluginRowMapperChanged();
}

}
