// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_set>

#include <QVariant>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{

/** @brief Converts values between serialized type and run-time type.
 *
 * GlobalSettingsManager uses %GlobalSettingsConverter subclasses to save and load values
 * stored in QSettings.
 *
 * This is an interface class and subclasses have to override all functions.
 *
 * @warning All overridden functions must be thread-safe. The easiest way to achieve this
 * is not accessing global state and not writing anything (the functions are const-qualified
 * to discourage it).
 * @sa GlobalSettingsManager
 * @headerfile "core/globalsettingsconverter.h"
 */

class PACKURU_CORE_EXPORT GlobalSettingsConverter
{
public:
    GlobalSettingsConverter();
    virtual ~GlobalSettingsConverter();

    /**
     * @brief Returns a set of supported keys.
     *
     * This allows GlobalSettingsManager to choose converter for a given key.
     * @warning The keys must not be smaller than globalSettingsUserValue.
     */
    virtual std::unordered_set<int> getSupportedKeys() const = 0;

    /**
     * @brief Returns string-based key for specified int key.
     *
     * This allows GlobalSettingsManager to load and save values in QSettings using string-based key.
     */
    virtual QString getKeyName(int key) const = 0;

    /**
     * @brief Converts run-time type to a type that can be serialized and stored in QSettings.
     */
    virtual QVariant toSettingsType(int key, const QVariant& value) const = 0;

    /**
     * @brief Converts serialized type stored in QSettings to run-time type.
     */
    virtual QVariant fromSettingsType(int key, const QVariant& value) const = 0;
};

}
