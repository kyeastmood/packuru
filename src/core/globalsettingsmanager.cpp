// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <shared_mutex>
#include <unordered_map>

#include <QSettings>
#include <QDebug>

#include "globalsettingsmanager.h"
#include "globalsettingsconverter.h"
#include "private/3rdparty/cs_libguarded/cs_shared_guarded.h"
#include "private/globalsettingsmanageraux.h"


namespace Packuru::Core
{

namespace
{
std::shared_mutex instancePtrMutex;
}


struct GlobalSettingsManager::Private
{

    static GlobalSettingsManager*& getInstance()
    {
        static GlobalSettingsManager* instance = nullptr;
        return instance;
    }

    using InternalKeyType = int;

    using DataMap = std::unordered_map<InternalKeyType, QVariant>;
    libguarded::shared_guarded<DataMap> data;

    using ConverterMap = std::unordered_map<InternalKeyType, std::shared_ptr<GlobalSettingsConverter>>;
    libguarded::shared_guarded<ConverterMap> converters;

    using DataMapHandleShared = decltype(data)::shared_handle;
    using DataMapHandleUnique = decltype(data)::handle;

    using ConverterMapHandleShared = decltype(converters)::shared_handle;
    using ConverterMapHandleUnique = decltype(converters)::handle;

    void addConverter(ConverterMapHandleUnique& converterMap,
                      std::unique_ptr<GlobalSettingsConverter> converter);
};


GlobalSettingsManager::GlobalSettingsManager(QObject *parent)
    : QObject(parent),
      priv(new Private)
{

}


GlobalSettingsManager::~GlobalSettingsManager()
{
    std::unique_lock<std::shared_mutex> lock(instancePtrMutex);
    Private::getInstance() = nullptr;
}


GlobalSettingsManager*
GlobalSettingsManager::createSingleInstance(GlobalSettingsManagerAux::Init& initData,
                                            QObject* parent)
{
    Q_ASSERT(parent);

    std::unique_lock<std::shared_mutex> lock(instancePtrMutex);

    auto*& instance = GlobalSettingsManager::Private::getInstance();
    Q_ASSERT(!instance);

    instance = new GlobalSettingsManager(parent);

    Private::ConverterMapHandleUnique converterMap = instance->priv->converters.lock();

    for (auto& converter : initData.converters)
        instance->priv->addConverter(converterMap, std::move(converter));

    return instance;
}


GlobalSettingsManager* GlobalSettingsManager::instance()
{
    std::shared_lock<std::shared_mutex> lock(instancePtrMutex);
    return GlobalSettingsManager::Private::getInstance();
}


void GlobalSettingsManager::addCustomSettingsConverter(std::unique_ptr<GlobalSettingsConverter> converter)
{
    Private::ConverterMapHandleUnique converterMap = instance()->priv->converters.lock();

    priv->addConverter(converterMap, std::move(converter));
}


QVariant GlobalSettingsManager::getValue(int key) const
{
    Private::DataMapHandleShared dataMapShared = priv->data.lock_shared();

    // First attempt to read the value for this key
    auto dataIt = dataMapShared->find(key);
    if (dataIt != dataMapShared->end())
        return dataIt->second;

    // Releases read lock so write lock can be acquired
    dataMapShared.reset();

    const Private::ConverterMapHandleShared converterMap = priv->converters.lock_shared();

    // If not data is present for this key, try to use the converter to read it from settings
    const auto converterIt = converterMap->find(key);
    Q_ASSERT(converterIt != converterMap->end());

    if (converterIt == converterMap->end())
    {
        qWarning()<<"Settings converter not found for key" << key;
        return QVariant();
    }

    // Get unique/write lock for data map
    const Private::DataMapHandleUnique dataMapUnique = priv->data.lock();

    /* Got the write lock. But try to read again, maybe another thread has just written value
       for this key while we were waiting to get unique lock. */
    dataIt = dataMapUnique->find(key);
    if (dataIt != dataMapUnique->end())
        return dataIt->second;

    QSettings s;

    // Read data using QSettings and convert it to desired type using converter
    const std::shared_ptr<GlobalSettingsConverter>& converter = converterIt->second;
    const QString keyName = converter->getKeyName(key);
    const QVariant valueRaw = s.value(keyName);
    const QVariant valueConverted = converter->fromSettingsType(key, valueRaw);
    (*dataMapUnique)[key] = valueConverted;

    return valueConverted;
}


void GlobalSettingsManager::setValue(int key, const QVariant& value)
{
    const Private::DataMapHandleUnique dataMap = priv->data.lock();
    const auto dataIt = dataMap->find(key);

    // If data has been already set or read from disk
    if (dataIt != dataMap->end())
    {
        QVariant& currentValue = dataIt->second;
        if (value == currentValue)
            return;

        currentValue = value;
    }
    else
        dataMap->insert({key, value});

    const Private::ConverterMapHandleShared converterMap = priv->converters.lock_shared();

    const auto converterIt = converterMap->find(key);
    Q_ASSERT(converterIt != converterMap->end());

    if (converterIt == converterMap->end())
    {
        qWarning()<<"Settings converter not found for key" << key;
        return;
    }

    const std::shared_ptr<GlobalSettingsConverter>& converter = converterIt->second;

    const QString keyName = converter->getKeyName(key);
    const QVariant valueRaw = converter->toSettingsType(key, value);

    QSettings s;
    s.setValue(keyName, valueRaw);

    emit valueChanged(key, value);
}


void GlobalSettingsManager::Private::addConverter(ConverterMapHandleUnique& converterMap,
                                                  std::unique_ptr<GlobalSettingsConverter> converter)
{
    const std::shared_ptr<GlobalSettingsConverter> shared = std::move(converter);

    for (auto key : shared->getSupportedKeys())
    {
        const bool keyAlreadyAdded = converterMap->find(key) != converterMap->end();

        Q_ASSERT(!keyAlreadyAdded);

        if (keyAlreadyAdded)
        {
            qWarning()<<"Cannot add settings converter for already provided key" << key;
            continue;
        }

        (*converterMap)[key] = shared;
    }
}

}
