// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../utils/boolflag.h"

#include "passwordpromptcore.h"
#include "commonstrings.h"
#include "private/privatestrings.h"
#include "private/promptcore/passwordpromptcoreaux.h"


namespace Packuru::Core
{

using namespace PasswordPromptCoreAux;


struct PasswordPromptCore::Private
{
    Init initData;
    Utils::BoolFlag<false> finished;
    Emitter* emitter = nullptr;
};


PasswordPromptCore::PasswordPromptCore(const PasswordPromptCoreAux::Init& init,
                                       PasswordPromptCoreAux::Backdoor& backdoor,
                                       QObject* parent)
    : QObject(parent),
      priv(new Private)
{
    priv->initData = init;
    priv->emitter = new Emitter(this);
    backdoor.emitter = priv->emitter;
}


PasswordPromptCore::~PasswordPromptCore()
{
    emit deleted();
}


QString PasswordPromptCore::getString(UserString value)
{
    switch (value)
    {
    case UserString::Abort:
        return PrivateStrings::getString(PrivateStrings::UserString::Abort);
    case UserString::DialogTitle:
        return Packuru::Core::PasswordPromptCore::tr("Password required");
    default:
        Q_ASSERT(false); return "";
    }
}


void PasswordPromptCore::enterPassword(const QString& value)
{
    Q_ASSERT(!value.isEmpty());

    if (value.isEmpty() || !priv->finished.set())
        return;

    emit priv->emitter->passwordEntered(value);
}


void PasswordPromptCore::abort()
{
    if (!priv->finished.set())
        return;

    emit priv->emitter->abortRequested();
}


QString PasswordPromptCore::getArchiveName() const
{
    return priv->initData.archiveName;
}

}
