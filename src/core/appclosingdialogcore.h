// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{

/**
 @brief Contains all data necessary for %Browser and %Queue Closing dialog.

 The object must be manually created. It only contains data and does not close the application itself.

 @headerfile "core/appclosingdialogcore.h"
 */
class PACKURU_CORE_EXPORT AppClosingDialogCore : public QObject
{
    Q_OBJECT
public:
    /**
     @brief Identifies strings to display in the dialog.
     */
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        BrowserBusyBrowsersInfo, ///< Information about number of opened tabs in Browser.
        Cancel, ///< Cancel button.
        DialogTitle, ///< Dialog title.
        QueueOpenedDialogsInfo, ///< Information about number of opened archive dialogs in Queue.
        QueueUnfinishedTasksInfo, ///< Information about number of unfinished tasks in Queue.
        Quit, ///< Label for the Quit button.
        QuitQuestion ///< "Do you want to quit?"
    };
    Q_ENUM(UserString)

    explicit AppClosingDialogCore(QObject *parent = nullptr);

    /**
     @brief Returns a translated string for specified ID.
     @param value ID of the string.
     @param count Required for strings that contain numbers.
     */
    Q_INVOKABLE static QString getString(UserString value, int count = 0);
};

}
