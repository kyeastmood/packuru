// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{

/**
 @brief Identifies input widgets in Extraction Dialog.
 @headerfile "core/extractiondialogcontrollertype.h"
 */
class ExtractionDialogControllerType : public QObject
{
    Q_OBJECT
public:
    /// @copybrief Packuru::Core::ExtractionDialogControllerType
    enum class Type
    {
        AcceptButton, ///< Push button.
        DestinationError, ///< Label.
        DestinationMode, ///< Combo box.
        DestinationPath, ///< Line edit.
        DialogErrors, ///< Label.
        FileExtractionMode, ///< Combo box.
        FileOverwriteMode, ///< Combo box.
        FolderOverwriteMode, ///< Combo box.
        OpenDestinationOnCompletion, ///< Check box.
        Password, ///< Line edit.
        RunInQueue, ///< Check box.
        TopFolderMode, ///< Combo box.
    };
    Q_ENUM(Type)
};

PACKURU_CORE_EXPORT uint qHash(ExtractionDialogControllerType::Type value, uint seed = 0);

}
