// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>

#include "symbol_export.h"


namespace qcs::core
{
class ControllerEngine;
}


namespace Packuru::Core
{

namespace AppSettingsDialogCoreAux
{
struct InitData;
}


class PluginDataModel;
class ArchiveTypeModel;


/**
 @brief Allows to adjust core application settings.

 It is used in the settings dialog.

 Object of this class is provided by Packuru::Core::Browser::MainWindowCore and QueueCore objects.

 For Browser and Queue additional settings you also need Packuru::Core::Browser::SettingsDialogCore
 and Packuru::Core::Queue::SettingsDialogCore respectively.

 @headerfile "core/appsettingsdialogcore.h"
 */
class PACKURU_CORE_EXPORT AppSettingsDialogCore : public QObject
{
    Q_OBJECT
public:
    /**
     @brief Identifies strings to display in the dialog.
     */
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        CategoryArchiveTypes, ///< Archive types page name.
        CategoryGeneral, ///< General settings page name.
        CategoryPlugins, ///< Plugins page name.
        DialogTitle, ///< Dialog title.
        FilterPlaceholderText, ///< Filter placeholder text.
        PluginBackendHomepage, ///< Plugin "Backend homepage" label.
        PluginConfigChangeWarning, ///< Warning about using different plugins for read and write.
        PluginDescription, ///< Plugin "Description" label.
        PluginFilePath, ///< Plugin "File path" label.
        PluginReads, ///< Plugin "Reads" label.
        PluginWrites, ///< Plugin "Writes" label.
        ReadWarning, ///< "Read warning" label for a button to display warning.
    };
    Q_ENUM(UserString)

    explicit AppSettingsDialogCore(const AppSettingsDialogCoreAux::InitData& initData,
                                   QObject *parent = nullptr);
    ~AppSettingsDialogCore() override;

    /// @brief Allows destruction from QML.
    Q_INVOKABLE void destroyLater();

    /// @brief Returns a translated string for specified ID.
    Q_INVOKABLE static QString getString(UserString value);

    /**
     @brief Returns pointer to ControllerEngine controlling input widgets of core settings.

     The widgets must be mapped in WidgetMapper using AppSettingsControllerType::Type enum as the ID.

     @note Object ownership is not transferred to the caller.
     @sa qcs::qqc::WidgetMapper, qcs::qtw::WidgetMapper
     */
    Q_INVOKABLE qcs::core::ControllerEngine* getControllerEngine();

    /**
     @brief Returns pointer to PluginDataModel that contains information about loaded plugins.

     The ownership is not transferred to the caller.
     */
    Q_INVOKABLE Packuru::Core::PluginDataModel* getPluginModel();

    /**
     @brief Returns pointer to ArchiveTypeModel that contains information about supported
     archive types.

     The ownership is not transferred to the caller.
     */
    Q_INVOKABLE Packuru::Core::ArchiveTypeModel* getArchiveTypeModel();

    /** @brief Saves settings.
    @note The object must be destroyed manually after it has been accepted.
    */
    Q_INVOKABLE void accept();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
