// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>
#include <memory>

#include <QAbstractTableModel>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{

namespace ArchiveTypeModelAux
{
struct InitData;
struct BackdoorData;
}

/**
 @brief Contains information about supported archive types.

 Object of this class is provided by AppSettingsDialogCore object.

 @sa ArchiveTypeFilterModel
 @headerfile "core/archivetypemodel.h"
 */
class PACKURU_CORE_EXPORT ArchiveTypeModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit ArchiveTypeModel(ArchiveTypeModelAux::InitData& initData,
                              ArchiveTypeModelAux::BackdoorData& backdoorData,
                              QObject *parent = nullptr);
    ~ArchiveTypeModel() override;

    ///@{
    /// @name Qt Model-View API
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role) override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    ///@}

    /** @brief Returns a list of plugins that can read the type at specified row.
     @param row Must be a valid row.
     */
    Q_INVOKABLE QStringList getReadPlugins(int row) const;

    /** @brief Returns a current plugin selected for reading of the type at specified row.
     @param row Must be a valid row.
     */
    Q_INVOKABLE QString getCurrentReadPlugin(int row) const;

    /** @brief Returns a list of plugins that can write the type at specified row.
     @param row Must be a valid row.
     */
    Q_INVOKABLE QStringList getWritePlugins(int row) const;

    /** @brief Returns a current plugin selected for writing of the type at specified row.
     @param row Must be a valid row.
     */
    Q_INVOKABLE QString getCurrentWritePlugin(int row) const;

    /**
     @brief Sets read plugin for the type at specified row.
     @param row Must be a valid row.
     @param value Must be in a list of read plugins for specified row.
     */
    Q_INVOKABLE void setReadPlugin(int row, const QString& value);

    /**
     @brief Sets write plugin for the type at specified row.
     @param row Must be a valid row.
     @param value Must be in a list of write plugins for specified row.
     */
    Q_INVOKABLE void setWritePlugin(int row, const QString& value);

    /** @brief Returns a list of plugins for specified index.
     @param index Must be a valid index.
     */
    QStringList getValueSet(const QModelIndex& index) const;

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
