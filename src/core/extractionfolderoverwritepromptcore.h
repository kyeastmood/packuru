// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>

#include "symbol_export.h"

/// @file
///
class QDir;


namespace Packuru::Core
{

namespace ExtractionFolderOverwritePromptCoreAux
{
struct Init;
struct Backdoor;
}

/**
 @brief Contains data and logic necessary for Extraction Folder Overwrite dialog.

 Object of this class is emitted by StatusPageCore and TaskQueueModel objects
 and will be automatically destroyed after user action (overwrite(), skip(), retry() or abort())
 has been received.

 @warning The object must not be destroyed manually.

 @headerfile "core/extractionfolderoverwritepromptcore.h"
 */
class PACKURU_CORE_EXPORT ExtractionFolderOverwritePromptCore : public QObject
{
    Q_OBJECT

public:
    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        Abort, ///< 'Abort' button.
        ApplyToAll, ///< 'Apply to all' checkbox.
        Content, ///< 'Content' label.
        Destination, ///< 'Destination' header label.
        Folder, ///< 'Folder' label.
        FolderAlreadyExists, ///< 'Folder already exists' label.
        Modified, ///< 'Modified' label.
        Retry, ///< 'Retry' button.
        Skip, ///< 'Skip' button.
        Source, ///< 'Source' header label.
        WriteInto ///< 'Write into' button.
    };
    Q_ENUM(UserString)

    explicit ExtractionFolderOverwritePromptCore(const ExtractionFolderOverwritePromptCoreAux::Init& init,
                                                 ExtractionFolderOverwritePromptCoreAux::Backdoor& backdoor,
                                                 QObject* parent = nullptr);

    ~ExtractionFolderOverwritePromptCore() override;

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE static QString getString(UserString value);

    /// @brief Returns source folder path.
    Q_INVOKABLE QString sourcePath() const;

    /// @brief Returns destination folder path.
    Q_INVOKABLE QString destinationPath() const;

    /// @brief Returns source folder item count.
    Q_INVOKABLE QString sourceContent() const;

    /// @brief Returns destination folder item count.
    Q_INVOKABLE QString destinationContent() const;

    /// @brief Returns source folder modification time.
    Q_INVOKABLE QString sourceModifiedDate() const;

    /// @brief Returns destination folder modification time.
    Q_INVOKABLE QString destinationModifiedDate() const;

    ///@{
    /// @name Actions
    /// @warning Calling Actions will also automatically destroy the object.

    /**
     * @brief Writes source folder files into destination folder.
     * @param applyToAll Specifies whether all subsequent folders should be automatically written into.
     */
    Q_INVOKABLE void overwrite(bool applyToAll);

    /**
     * @brief Skips the folder.
     *
     * No source files will be moved to the destination folder.
     * @param applyToAll Specifies whether all subsequent folders should be automatically skipped.
     */
    Q_INVOKABLE void skip(bool applyToAll);

    /**
     * @brief Retries to move the folder to its destination.
     */
    Q_INVOKABLE void retry();

    /// @copydoc CreationNameErrorPromptCore::abort()
    Q_INVOKABLE void abort();

    ///@}

signals:
    /// @brief Required for QML.
    void deleted();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
