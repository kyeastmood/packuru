// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../qcs-core/controllerengine.h"

#include "testdialogcore.h"
#include "testdialogcontrollertype.h"
#include "private/dialogcore/testdialog/testdialogcoreinitsingle.h"
#include "private/dialogcore/testdialog/testdialogcoreinitmulti.h"
#include "private/tasks/testtaskdata.h"
#include "private/plugin-api/encryptionstate.h"
#include "private/plugin-api/archivetype.h"
#include "private/dialogcore/inputarchivesmodel.h"
#include "private/dialogcore/runinqueuectrl.h"
#include "private/dialogcore/genericpasswordctrl.h"
#include "private/dialogcore/acceptbuttonctrl.h"
#include "private/dialogcore/genericdialogerrorsctrl.h"
#include "commonstrings.h"


using namespace qcs::core;


namespace Packuru::Core
{

struct TestDialogCore::Private
{
    Private(TestDialogCore* publ,
            DialogMode mode,
            bool acceptsPassword);

    void initControllers(const QString& password);

    TestDialogCore* publ;
    TestDialogCore::DialogMode dialogMode;
    bool accepted = false;
    ControllerEngine* engine = nullptr;
    TestDialogCoreInitSingle initSingle;
    TestDialogCoreInitMulti initMulti;
    InputArchivesModel* archivesModel = nullptr;
    const bool acceptsPassword;
};


TestDialogCore::TestDialogCore(TestDialogCoreInitSingle initData, QObject *parent)
    : QObject(parent),
      priv(new Private(this,
                       DialogMode::SingleArchive,
                       initData.archiveEncryption == EncryptionState::FilesContentOnly
                       && initData.password.isEmpty()))
{
    Q_ASSERT(initData.isValid());
    priv->initSingle = std::move(initData);

    priv->initControllers(priv->initSingle.password);
}


TestDialogCore::TestDialogCore(TestDialogCoreInitMulti initData, QObject* parent)
    : QObject(parent),
      priv(new Private(this, DialogMode::MultiArchive, true))
{
    Q_ASSERT(initData.isValid());

    priv->archivesModel = new InputArchivesModel(initData.guessArchiveType, initData.getBackend, this);
    priv->archivesModel->addArchives(std::move(initData.archives));

    priv->initMulti = std::move(initData);

    priv->initControllers(QString());
}


TestDialogCore::~TestDialogCore()
{
}


void TestDialogCore::destroyLater()
{
    deleteLater();
}


QString TestDialogCore::getString(TestDialogCore::UserString value)
{
    switch (value)
    {
    case UserString::Cancel:
        return CommonStrings::getString(CommonStrings::UserString::Cancel);

    case UserString::DialogTitleMulti:
        return Packuru::Core::TestDialogCore::tr("Test archives");

    case UserString::DialogTitleSingle:
        return Packuru::Core::TestDialogCore::tr("Test");

    case UserString::TabArchives:
        return Packuru::Core::TestDialogCore::tr("Archives");

    case UserString::TabOptions:
        return Packuru::Core::TestDialogCore::tr("Options");

    default:
        Q_ASSERT(false); return "";
    }
}


TestDialogCore::DialogMode TestDialogCore::getMode() const
{
    return priv->dialogMode;
}


QAbstractItemModel* TestDialogCore::getArchivesModel() const
{
    return priv->archivesModel;
}


void TestDialogCore::addArchives(const QList<QUrl> &archives)
{
    priv->archivesModel->addArchives(archives);
}


void TestDialogCore::removeArchives(const QModelIndexList& indexes)
{
    priv->archivesModel->removeArchives(indexes);
}


qcs::core::ControllerEngine* TestDialogCore::getControllerEngine()
{
    return priv->engine;
}


void TestDialogCore::accept()
{
    Q_ASSERT(!priv->accepted);
    priv->accepted = true;

    auto values = priv->engine->getAvailableCurrentValues<TestDialogControllerType::Type>();

    TestTaskData data;
    data.backendData.password
            = Utils::getValueAs<QString>(values,
                                         TestDialogControllerType::Type::Password);

    data.runInQueue = Utils::getValueAs<bool>(values, TestDialogControllerType::Type::RunInQueue);

    if (priv->dialogMode == DialogMode::SingleArchive)
    {
        data.backendData.archiveType =  priv->initSingle.archiveType;
        data.backendData.archiveInfo = priv->initSingle.archiveInfo;
        data.backendData.encryptionState = priv->initSingle.archiveEncryption;
        priv->initSingle.onAccepted(data);
    }
    else if (priv->dialogMode == DialogMode::MultiArchive)
    {
        const auto archives = priv->archivesModel->getArchives();
        std::vector<TestTaskData> dataList;

        for (const auto& archive : archives)
        {
            data.backendData.archiveInfo = archive.info;
            data.backendData.archiveType = archive.type;
            dataList.push_back(data);
        }

        priv->initMulti.onAccepted(dataList);
    }
    else
        Q_ASSERT(false);
}


TestDialogCore::Private::Private(TestDialogCore* publ,
                                 TestDialogCore::DialogMode mode,
                                 bool acceptsPassword)
    : publ(publ),
      dialogMode(mode),
      engine(new ControllerEngine(publ)),
      acceptsPassword(acceptsPassword)
{

}


void TestDialogCore::Private::initControllers(const QString& password)
{
    engine->createTopController<GenericPasswordCtrl>(TestDialogControllerType::Type::Password,
                                                     password, acceptsPassword);

    // In Single Archive mode the model is not created
    const bool archiveListEmpty = archivesModel ? archivesModel->rowCount() == 0
                                                : false;

    using DialogErrorsCtrl = GenericDialogErrorsCtrl<TestDialogControllerType::Type>;
    auto dialogErrorsCtrl = engine->createTopController<DialogErrorsCtrl>
            (TestDialogControllerType::Type::DialogErrors,
             archiveListEmpty,
             std::vector<TestDialogControllerType::Type>{});

    if (archivesModel)
    {
        QObject::connect(archivesModel, &InputArchivesModel::modelEmpty,
                         publ, [ctrl = dialogErrorsCtrl] (bool empty)
        { ctrl->setFileListEmpty(empty); });
    }

    using AcceptButtonCtrlType = AcceptButtonCtrl<TestDialogControllerType::Type>;

    auto acceptButtonCtrl = engine->createController<AcceptButtonCtrlType>
            (TestDialogControllerType::Type::AcceptButton,
             TestDialogControllerType::Type::DialogErrors);

    const auto runInQueueMode =
            dialogMode == TestDialogCore::DialogMode::MultiArchive
            ? RunInQueueCtrl::Mode::ForcedQueue
            : RunInQueueCtrl::Mode::Switchable;

    engine->createTopController<RunInQueueCtrl>
            (TestDialogControllerType::Type::RunInQueue, runInQueueMode);

    dialogErrorsCtrl->addDependant(acceptButtonCtrl);

    engine->updateAllControllers();
}

}
