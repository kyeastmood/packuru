// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QString>

#include "htmllink.h"


namespace Packuru::Core
{

QString htmlLink(const QString& address, const QString& label)
{
    if(label.isEmpty())
        return "<a href=\"" + address + QString("\">") + address + "</a>";
    else
        return "<a href=\"" + address + QString("\">") + label + "</a>";
}

}
