// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QFileInfo>

#include "../utils/boolflag.h"

#include "extractiontypemismatchpromptcore.h"
#include "commonstrings.h"
#include "private/privatestrings.h"
#include "private/promptcore/extractiontypemismatchpromptcoreaux.h"


namespace Packuru::Core
{

using ExtractionTypeMismatchPromptCoreAux::Init;
using ExtractionTypeMismatchPromptCoreAux::Backdoor;
using ExtractionTypeMismatchPromptCoreAux::Emitter;


struct ExtractionTypeMismatchPromptCore::Private
{
    ExtractionTypeMismatchPromptCore::FileType getFileType(const QFileInfo& info) const;

    Init initData;
    Utils::BoolFlag<false> finished;
    Emitter* emitter;
};


ExtractionTypeMismatchPromptCore::ExtractionTypeMismatchPromptCore(const Init& init,
                                                                   Backdoor& backdoor,
                                                                   QObject* parent)
    : QObject(parent),
      priv(new Private)
{
    priv->initData = init;

    Q_ASSERT(priv->initData.source.fileName() == priv->initData.destination.fileName());

    priv->emitter = new ExtractionTypeMismatchPromptCoreAux::Emitter(this);

    backdoor.emitter = priv->emitter;
}


ExtractionTypeMismatchPromptCore::~ExtractionTypeMismatchPromptCore()
{
    emit deleted();
}


void ExtractionTypeMismatchPromptCore::skip(bool applyToAll)
{
    if (!priv->finished.set())
        return;

    emit priv->emitter->skipRequested(applyToAll);
}


void ExtractionTypeMismatchPromptCore::retry()
{
    if (!priv->finished.set())
        return;

    emit priv->emitter->retryRequested();
}


void ExtractionTypeMismatchPromptCore::abort()
{
    if (!priv->finished.set())
        return;

    emit priv->emitter->abortRequested();
}


QString ExtractionTypeMismatchPromptCore::getString(ExtractionTypeMismatchPromptCore::UserString value)
{
    switch (value)
    {
    case UserString::Abort:
        return PrivateStrings::getString(PrivateStrings::UserString::Abort);
    case UserString::ApplyToAll:
        return PrivateStrings::getString(PrivateStrings::UserString::ApplyToAll);
    case UserString::Destination:
        return PrivateStrings::getString(PrivateStrings::UserString::Destination);
    case UserString::File:
        return Packuru::Core::ExtractionTypeMismatchPromptCore::tr("File");
    case UserString::Folder:
        return Packuru::Core::ExtractionTypeMismatchPromptCore::tr("Folder");
    case UserString::ItemsTypeMismatch:
        return Packuru::Core::ExtractionTypeMismatchPromptCore::tr("Items' type mismatch");
    case UserString::Link:
        return Packuru::Core::ExtractionTypeMismatchPromptCore::tr("Link");
    case UserString::Location:
        return Packuru::Core::ExtractionTypeMismatchPromptCore::tr("Location");
    case UserString::Retry:
        return PrivateStrings::getString(PrivateStrings::UserString::Retry);
    case UserString::Skip:
        return PrivateStrings::getString(PrivateStrings::UserString::Skip);
    case UserString::Source:
        return PrivateStrings::getString(PrivateStrings::UserString::Source);
    default:
        Q_ASSERT(false); return "";
    }
}


ExtractionTypeMismatchPromptCore::FileType ExtractionTypeMismatchPromptCore::sourceFileType() const
{
    return priv->getFileType(priv->initData.source);
}


ExtractionTypeMismatchPromptCore::FileType ExtractionTypeMismatchPromptCore::destinationFileType() const
{
    return priv->getFileType(priv->initData.destination);
}


QString ExtractionTypeMismatchPromptCore::itemFileName() const
{
    return priv->initData.source.fileName();
}


QString ExtractionTypeMismatchPromptCore::sourcePath() const
{
    return priv->initData.source.absolutePath();
}


QString ExtractionTypeMismatchPromptCore::destinationPath() const
{
    return priv->initData.destination.absolutePath();
}


QString ExtractionTypeMismatchPromptCore::sourceFilePath() const
{
    return priv->initData.source.absoluteFilePath();
}


QString ExtractionTypeMismatchPromptCore::destinationFilePath() const
{
    return priv->initData.destination.absoluteFilePath();
}


ExtractionTypeMismatchPromptCore::FileType ExtractionTypeMismatchPromptCore::Private::getFileType(const QFileInfo& info) const
{
    if (info.isSymLink())
        return ExtractionTypeMismatchPromptCore::FileType::Link;
    else if (info.isDir())
        return ExtractionTypeMismatchPromptCore::FileType::Dir;
    else if (info.isFile())
        return ExtractionTypeMismatchPromptCore::FileType::File;

    Q_ASSERT(false);
    return ExtractionTypeMismatchPromptCore::FileType::___INVALID;
}

}
