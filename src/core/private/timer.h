// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <chrono>

#include <QString>

#include "core/symbol_export.h"


namespace Packuru::Core
{

class PACKURU_CORE_EXPORT Timer
{
public:
    Timer();

    void start();
    void stop();
    void reset();

    bool isStarted() const { return started; }
    bool isPaused() const { return paused; }

    std::chrono::system_clock::duration getDuration() const;
    std::chrono::system_clock::rep getMilliseconds() const;
    std::chrono::system_clock::rep getSeconds() const;

private:
    bool started = false;
    bool paused = false;

    std::chrono::system_clock::time_point startTime;
    std::chrono::system_clock::time_point pauseTime;
};

}
