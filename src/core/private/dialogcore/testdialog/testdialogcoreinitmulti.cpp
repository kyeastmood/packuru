// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "testdialogcoreinitmulti.h"


namespace Packuru::Core
{

bool TestDialogCoreInitMulti::isValid() const
{
    return guessArchiveType
            && getBackend
            && onAccepted;
}

}
