// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "runinqueuectrl.h"


namespace Packuru::Core
{

RunInQueueCtrl::RunInQueueCtrl(Mode mode)
{
    switch (mode)
    {
    case Mode::ForcedQueue:
        setCurrentValue(true);
        setEnabled(false);
        break;
    case Mode::Switchable:
        setCurrentValue(false);
        setEnabled(true);
        break;
    default:
        Q_ASSERT(false);
    }

    properties[qcs::core::ControllerProperty::Text] = Packuru::Core::RunInQueueCtrl::tr("Run in queue");
}

}
