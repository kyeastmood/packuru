// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "qcs-core/controller.h"


namespace Packuru::Core
{

class GenericPasswordCtrl : public qcs::core::Controller<QString>
{
public:
    GenericPasswordCtrl(const QString& password, bool acceptsPassword);
};

}
