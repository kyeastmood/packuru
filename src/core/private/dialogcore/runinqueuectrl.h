// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"


namespace Packuru::Core
{

class RunInQueueCtrl : public qcs::core::Controller<bool>
{
    Q_DECLARE_TR_FUNCTIONS(RunInQueueCtrl)
public:
    enum class Mode { ForcedQueue, Switchable };

    RunInQueueCtrl(Mode mode);
};

}
