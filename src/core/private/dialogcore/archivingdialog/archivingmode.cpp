// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QString>
#include <QCoreApplication>

#include "archivingmode.h"


namespace Packuru::Core
{

QString toString(ArchivingMode value)
{
    switch (value)
    {
    case ArchivingMode::ArchivePerItem:
        return QCoreApplication::translate("ArchivingMode", "Separate archive for each item");
    case ArchivingMode::SingleArchive:
        return QCoreApplication::translate("ArchivingMode", "Single archive");
    default:
        Q_ASSERT(false); return QString();
    }

}

}
