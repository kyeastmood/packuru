// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"

#include "archivingdestinationmode.h"


namespace Packuru::Core
{

class DestinationModeCtrl : public qcs::core::Controller<QString, ArchivingDestinationMode>
{
    Q_DECLARE_TR_FUNCTIONS(DestinationModeCtrl)

public:
    DestinationModeCtrl();

    void update() override;

private:
    void onValueSyncedToWidget(const QVariant&) override;

    ArchivingDestinationMode userChoice;
};


}
