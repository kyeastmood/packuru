// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QString>

#include "qcs-core/controller.h"

#include "core/private/plugin-api/encryptionsupport.h"


namespace Packuru::Core
{

class PasswordCtrl : public qcs::core::Controller<QString>
{
    using GetEncryptionSupport = std::function<EncryptionSupport()>;
public:
    PasswordCtrl(const QString& label,
                 const QString& initPassword,
                 const GetEncryptionSupport& getEncryptionSupport);

    void update() override;

private:
    GetEncryptionSupport getEncryptionSupport;
    QString temp;
};

}
