// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "utils/qvariant_utils.h"

#include "generalcompressionlevelctrl.h"
#include "core/private/plugin-api/archivingcompressioninfo.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

GeneralCompressionLevelCtrl::GeneralCompressionLevelCtrl(std::function<ArchivingCompressionInfo()>&& getCompressionInfo,
                                                         std::function<void(int)>&& setCompressionLevel)
    : getCompressionInfo(getCompressionInfo),
      setCompressionLevel(setCompressionLevel)
{
    setCurrentValue(0);
    properties[ControllerProperty::MinValue] = 0;
    properties[ControllerProperty::MaxValue] = 0;
    setEnabled(false);

    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::GeneralCompressionLevelCtrl::tr("Level");
}


void GeneralCompressionLevelCtrl::update()
{
    updateCompressionInfo();
}


void GeneralCompressionLevelCtrl::onValueSyncedToWidget(const QVariant &value)
{
    const auto level = Utils::getValueAs<int>(value);
    setCompressionLevel(level);
}


void GeneralCompressionLevelCtrl::onExternalCompressionInfoChange()
{
    updateCompressionInfo();
    externalValueInput();
}


void GeneralCompressionLevelCtrl::updateCompressionInfo()
{
    const ArchivingCompressionInfo info = getCompressionInfo();

    setCurrentValue(info.getCurrent());
    properties[ControllerProperty::MinValue] = info.min;
    properties[ControllerProperty::MaxValue] = info.max;
    setEnabled(info.min != info.max);
}

}
