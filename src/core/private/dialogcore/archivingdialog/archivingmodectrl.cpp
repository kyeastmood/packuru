// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivingmodectrl.h"
#include "core/private/privatestrings.h"


namespace Packuru::Core
{

ArchivingModeCtrl::ArchivingModeCtrl()
{
    setValueListsFromPrivate({ArchivingMode::SingleArchive,
                              ArchivingMode::ArchivePerItem});

    setCurrentValueFromPrivate(ArchivingMode::SingleArchive);

    properties[qcs::core::ControllerProperty::LabelText]
            = PrivateStrings::getString(PrivateStrings::UserString::Mode);
}

}
