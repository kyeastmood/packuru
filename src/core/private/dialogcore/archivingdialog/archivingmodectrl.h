// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "qcs-core/controller.h"

#include "archivingmode.h"


namespace Packuru::Core
{

class ArchivingModeCtrl : public qcs::core::Controller<QString, ArchivingMode>
{
public:
    ArchivingModeCtrl();
};

}
