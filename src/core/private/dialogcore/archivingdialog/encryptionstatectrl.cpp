// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "encryptionstatectrl.h"
#include "core/private/plugin-api/encryptionstate.h"
#include "core/private/plugin-api/encryptionsupport.h"
#include "archivingdialogcontrollertype.h"
#include "passwordmatch.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

EncryptionStateCtrl::EncryptionStateCtrl(const GetEncryptionSupportFunction& getEncryptionSupport,
                                         const GetEncryptionMethodFunction& getEncryptionMethod)
    : getEncryptionSupport(getEncryptionSupport),
      getEncryptionMethod(getEncryptionMethod)
{
    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::EncryptionStateCtrl::tr("State");
}


void EncryptionStateCtrl::update()
{    
    const auto passwordMatch = getControllerPropertyAs<PasswordMatch>
            (ArchivingDialogControllerType::Type::PasswordMatch,
             ControllerProperty::PrivateCurrentValue);

    const auto encryptionMode = getControllerPropertyAs<EncryptionState>
            (ArchivingDialogControllerType::Type::EncryptionMode,
             ControllerProperty::PrivateCurrentValue);

    auto state = EncryptionState::___INVALID;

    if (passwordMatch != PasswordMatch::Match)
        state = EncryptionState::Disabled;
    else
        state = encryptionMode;

    Q_ASSERT(state != EncryptionState::___INVALID);
    currentEncryptionState = state;

    setEnabled(getEncryptionSupport() != EncryptionSupport::NoSupport);

    const QString method = getEncryptionMethod();

    QString text;

    switch (state) {
    case EncryptionState::Disabled:
        text = Packuru::Core::EncryptionStateCtrl::tr("Disabled");
        break;
    case EncryptionState::EntireArchive:
    case EncryptionState::FilesContentOnly:
        text = setEnabledLabel(method);
        break;
    default:
        Q_ASSERT(false);
    }

    setCurrentValue(text);
}


void EncryptionStateCtrl::onEncryptionMethodChanged(const QString &method)
{
    if (currentEncryptionState == EncryptionState::EntireArchive
            || currentEncryptionState == EncryptionState::FilesContentOnly)
    {
        setCurrentValue(setEnabledLabel(method));
        externalValueInput();
    }
}


QString EncryptionStateCtrl::setEnabledLabel(const QString &encryptionMethod) const
{
    QString text(Packuru::Core::EncryptionStateCtrl::tr("Enabled"));
    if (encryptionMethod.length() > 0)
        text += " (" + encryptionMethod + ")";
    return text;
}

}
