// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QCoreApplication>

#include "qcs-core/controller.h"

#include "core/private/plugin-api/encryptionsupport.h"
#include "core/private/plugin-api/encryptionstate.h"


namespace Packuru::Core
{

class EncryptionModeCtrl : public qcs::core::Controller<QString, EncryptionState>
{
    Q_DECLARE_TR_FUNCTIONS(EncryptionModeCtrl)
public:
    EncryptionModeCtrl(EncryptionState currentArchiveEncryption,
                       std::function<EncryptionSupport()>&& getEncryptionSupport);

    void update() override;
    void onValueSyncedToWidget(const QVariant& value) override;

private:
    EncryptionState currentArchiveEncryption;
    std::function<EncryptionSupport()> getEncryptionSupport;
    bool userChoice;
    QString userChoicePubl;
    EncryptionState userChoicePriv;
    QString entireArchiveDescription;
    QString filesContentDescription;
};

}
