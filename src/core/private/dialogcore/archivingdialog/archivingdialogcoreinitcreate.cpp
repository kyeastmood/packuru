// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivingdialogcoreinitcreate.h"


namespace Packuru::Core
{

bool ArchivingDialogCoreInitCreate::isValid() const
{
    return static_cast<bool>(onAccepted);
}

}
