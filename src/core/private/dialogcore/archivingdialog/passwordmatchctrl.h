// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"

#include "passwordmatch.h"


namespace Packuru::Core
{

class PasswordMatchCtrl : public qcs::core::Controller<QString, PasswordMatch>
{
    Q_DECLARE_TR_FUNCTIONS(PasswordMatchCtrl)
public:
    PasswordMatchCtrl();

private:
    void update() override;
};

}
