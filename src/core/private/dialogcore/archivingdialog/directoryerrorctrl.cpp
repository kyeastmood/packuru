// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QUrl>

#include "directoryerrorctrl.h"
#include "archivingdestinationmode.h"
#include "archivingdialogcontrollertype.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

DirectoryErrorCtrl::DirectoryErrorCtrl()
    : pathErrorText(Packuru::Core::DirectoryErrorCtrl::tr("Destination path cannot be empty"))
{

}


void DirectoryErrorCtrl::update()
{
    const auto destinationMode = getControllerPropertyAs<ArchivingDestinationMode>
            (ArchivingDialogControllerType::Type::DestinationMode,
             ControllerProperty::PrivateCurrentValue);

    if (destinationMode != ArchivingDestinationMode::CustomFolder)
    {
        setVisible(false);
        return;
    }

    const auto path = getControllerPropertyAs<QString>
            (ArchivingDialogControllerType::Type::DestinationPath,
             ControllerProperty::PublicCurrentValue);

    QString error;

    if (path.isEmpty())
        error = pathErrorText;

    setCurrentValue(error);
    setVisible(!error.isEmpty());
}

}
