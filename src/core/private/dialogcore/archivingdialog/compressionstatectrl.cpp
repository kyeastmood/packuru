// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "compressionstatectrl.h"
#include "core/private/plugin-api/archivingcompressioninfo.h"


namespace Packuru::Core
{

CompressionStateCtrl::CompressionStateCtrl(std::function<ArchivingCompressionInfo()>&& getCompressionInfo)
    : getCompressionInfo(getCompressionInfo)
{
    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::CompressionStateCtrl::tr("State");
}


void CompressionStateCtrl::update()
{
    const auto info = getCompressionInfo();

    setEnabled(info.min != info.max);

    if (info.getCurrent() == 0)
        setCurrentValue(Packuru::Core::CompressionStateCtrl::tr("Disabled"));
    else
    {
        QString text = QCoreApplication::translate("CompressionStateCtrl", "Enabled");

        if (info.max > 1)
        {
            const int current = info.getCurrent();
            text += QString(" (")
                    + Packuru::Core::CompressionStateCtrl::tr("level %n", "", current)
                    + QLatin1String(")");

        }

        setCurrentValue(text);
    }
}

}
