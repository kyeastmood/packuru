// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "splitsizectrl.h"
#include "core/private/sizeunits.h"
#include "globalsettingsmanager.h"
#include "private/globalsettings.h"
#include "archivingdialogcontrollertype.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

SplitSizeCtrl::SplitSizeCtrl(const std::function<bool()>& archiveSplittingSupport)
    : archiveSplittingSupport(archiveSplittingSupport)
{
    setCurrentValue(0);

    units = GlobalSettingsManager::instance()->getValueAs<SizeUnits>(GlobalSettings::Enum::AC_SizeUnits);
    const QString suffix = units == SizeUnits::Binary ? " MiB" : " MB";
    properties[ControllerProperty::SpinBoxSuffix] = suffix;

    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::SplitSizeCtrl::tr("Part size");
}


void SplitSizeCtrl::update()
{
    if (archiveSplittingSupport())
    {
        setEnabled(true);

        const auto currentSize = getPropertyAs<int>(ControllerProperty::PublicCurrentValue);
        const auto presetSizeBytes = getControllerPropertyAs<qulonglong>(ArchivingDialogControllerType::Type::SplitPreset,
                                         ControllerProperty::PrivateCurrentValue);

        const qulonglong baseUnit = units == SizeUnits::Binary ? 1024 : 1000;
        const int presetSize = static_cast<int>(presetSizeBytes / baseUnit / baseUnit);

        if (presetSize != currentSize)
            setCurrentValue(presetSize);

        properties[ControllerProperty::ToolTip] = QString::number(presetSizeBytes) + QLatin1String(" B");

    }
    else
    {
        setEnabled(false);
        setCurrentValue(0);
        properties[ControllerProperty::ToolTip] = QString();
    }
}

}
