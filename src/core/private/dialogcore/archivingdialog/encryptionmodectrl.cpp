// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "encryptionmodectrl.h"
#include "core/private/plugin-api/encryptionsupport.h"
#include "core/private/privatestrings.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

EncryptionModeCtrl::EncryptionModeCtrl(EncryptionState currentArchiveEncryption,
                                       std::function<EncryptionSupport()>&& getEncryptionSupport)
    : currentArchiveEncryption(currentArchiveEncryption),
      getEncryptionSupport(getEncryptionSupport),
      userChoice(true)
{
    entireArchiveDescription = Packuru::Core::EncryptionModeCtrl::tr("Encrypt entire archive");
    filesContentDescription = Packuru::Core::EncryptionModeCtrl::tr("Encrypt files content only");

    setValueLists({entireArchiveDescription, filesContentDescription},
    {EncryptionState::EntireArchive, EncryptionState::FilesContentOnly  });

    userChoicePubl = entireArchiveDescription;
    userChoicePriv = EncryptionState::EntireArchive;

    setCurrentValue(userChoicePubl, userChoicePriv);

    properties[qcs::core::ControllerProperty::LabelText]
            = PrivateStrings::getString(PrivateStrings::UserString::Mode);
}


void EncryptionModeCtrl::update()
{
    if (currentArchiveEncryption == EncryptionState::EntireArchive)
    {
        setEnabled(false);
        return;
    }

    switch (getEncryptionSupport())
    {
    case EncryptionSupport::NoSupport:
        setEnabled(false);
        break;
    case EncryptionSupport::FilesContentOnly:
        setCurrentValue(filesContentDescription, EncryptionState::FilesContentOnly);
        setEnabled(false);
        break;
    case EncryptionSupport::EntireArchiveOnly:
        setCurrentValue(entireArchiveDescription, EncryptionState::EntireArchive);
        setEnabled(false);
        break;
    case EncryptionSupport::FilesContentAndEntireArchive:
        // Do not allow a partially encrypted archive (like 7z) to be fully encrypted
        // because it would mean data is encrypted with two (possibly) different passwords.
        if (currentArchiveEncryption == EncryptionState::FilesContentOnly)
        {
            setCurrentValue(filesContentDescription, EncryptionState::FilesContentOnly);
            setEnabled(false);
        }
        else
        {
            setCurrentValue(userChoicePubl, userChoicePriv);
            setEnabled(true);
        }
        break;
    default:
        Q_ASSERT(false);
    }
}


void EncryptionModeCtrl::onValueSyncedToWidget(const QVariant& value)
{
    Q_UNUSED(value)
    userChoicePubl = getPropertyAs<QString>(ControllerProperty::PublicCurrentValue);
    userChoicePriv = getPropertyAs<EncryptionState>(ControllerProperty::PrivateCurrentValue);
}

}
