// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDir>
#include <QUrl>
#include <QIcon>
#include <QMimeData>

#include "inputfilesmodel.h"
#include "core/private/findcommonparentpath.h"


namespace Packuru::Core
{

InputFilesModel::InputFilesModel(QObject *parent) :
    QAbstractTableModel(parent),
    duplicatesAllowed(false)
{
    folderType = mimeDB.mimeTypeForUrl(QDir::rootPath());
}


int InputFilesModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return static_cast<int>(infoList.size());
}


int InputFilesModel::columnCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return 2;
}


QVariant InputFilesModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant{};

    const auto row = static_cast<std::size_t>(index.row());

    if (row < infoList.size())
    {
        const int column = index.column();

        switch (column)
        {
        case 0:
            if (role == Qt::DisplayRole)
                return infoList[row].fileName();
            else if (role == Qt::DecorationRole)
            {
                if (infoList[row].isDir())
                    return QIcon::fromTheme(folderType.iconName());
                else if (infoList[row].isFile())
                {
                    const auto type = mimeDB.mimeTypeForFile(infoList[row].absoluteFilePath(), QMimeDatabase::MatchExtension);
                    auto icon = QIcon::fromTheme(type.iconName());

                    if (icon.isNull())
                        icon = QIcon::fromTheme("application-octet-stream");

                    return icon;
                }
            }
            break;

        case 1:
            if (role == Qt::DisplayRole)
                return infoList[row].absolutePath();
        }
    }

    return QVariant();
}


QVariant InputFilesModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role!=Qt::DisplayRole || orientation == Qt::Vertical)
        return QVariant();

    if (section == 0)
        return Packuru::Core::InputFilesModel::tr("Name");
    else if (section == 1)
        return Packuru::Core::InputFilesModel::tr("Path");

    return QVariant();
}


bool InputFilesModel::removeRows(int row, int count, const QModelIndex& parent)
{
    Q_UNUSED(parent)
    const auto stdRow = static_cast<std::size_t>(row);
    const auto stdCount = static_cast<std::size_t>(count);

    if (stdRow < infoList.size() and stdRow + stdCount > stdRow and stdRow + stdCount <= infoList.size())
    {
        const auto rangeBegin = infoList.begin() + row;
        const auto rangeEnd = infoList.begin() + row + count;

        beginRemoveRows(QModelIndex(), row, row + count - 1);

        for (auto it = rangeBegin; it != rangeEnd; ++it)
        {
            const auto& info = *it;

            const auto pathHash = qHash(info.absoluteFilePath());

            bool removalSuccess = false;

            if (info.isDir())
                removalSuccess = dirPaths.erase(pathHash);
            else
                removalSuccess = filePaths.erase(pathHash);

            Q_ASSERT(removalSuccess);
        }

        infoList.erase(rangeBegin,rangeEnd);

        endRemoveRows();

        if (infoList.empty())
            emit modelEmpty(true);

        // Needed for column auto-resizing in Qt Widgets ???
        const auto indexTop = createIndex(row, 0);
        const auto indexBottom = createIndex(row + count - 1, columnCount());
        emit dataChanged(indexTop, indexBottom);

        return true;
    }

    return false;
}


Qt::ItemFlags InputFilesModel::flags(const QModelIndex& index) const
{
    return QAbstractItemModel::flags(index) | Qt::ItemIsDropEnabled;
}


bool InputFilesModel::canDropMimeData(const QMimeData* data,
                                      Qt::DropAction action,
                                      int row,
                                      int column,
                                      const QModelIndex& parent) const
{
    Q_UNUSED(action)
    Q_UNUSED(row)
    Q_UNUSED(column)
    Q_UNUSED(parent)

    return data->hasUrls();
}


bool InputFilesModel::dropMimeData(const QMimeData* data,
                                   Qt::DropAction action,
                                   int row,
                                   int column,
                                   const QModelIndex& parent)
{
    Q_UNUSED(action)
    Q_UNUSED(row)
    Q_UNUSED(column)
    Q_UNUSED(parent)

    if (data->hasUrls())
    {
        addItems(data->urls());
        return true;
    }

    return false;
}


void InputFilesModel::addItems(const std::vector<QFileInfo>& newItems)
{
    if (newItems.empty())
        return;

    const bool wasEmptyList = infoList.empty();

    std::vector<QFileInfo> newInfoList;
    std::vector<QVariant> newCustomDataList;

    for (const auto& info : qAsConst(newItems))
    {
        if (info.fileName().isEmpty())
            continue;

        const uint pathHash = qHash(info.absoluteFilePath());

        const bool fileAdded = filePaths.find(pathHash) != filePaths.end();
        const bool dirAdded = dirPaths.find(pathHash) != dirPaths.end();

        if (!duplicatesAllowed)
        {
            if(fileAdded || dirAdded)
                continue;
        }
        else
        {
            if ((fileAdded && info.isFile()) || (dirAdded && info.isDir()))
                continue;
        }

        newInfoList.push_back(info);

        if (info.isFile())
            filePaths.insert(pathHash);
        else
            dirPaths.insert(pathHash);
    }

    if (!newInfoList.empty())
    {

        const auto oldSize = static_cast<int>(infoList.size());
        const auto firstRow = oldSize;
        const int lastRow = static_cast<int>(infoList.size() + newInfoList.size() - 1);

        beginInsertRows(QModelIndex{}, firstRow, lastRow);

        infoList.insert(infoList.end(), newInfoList.begin(), newInfoList.end());

        endInsertRows();

        // Needed for column auto-resizing in Qt Widgets ???
        const auto indexTop = createIndex(firstRow, 0);
        const auto indexBottom = createIndex(lastRow, columnCount());
        emit dataChanged(indexTop,indexBottom);
    }

    if (wasEmptyList && !infoList.empty())
        emit modelEmpty(false);
}


void InputFilesModel::addItems(const QList<QUrl>& newItems)
{
    std::vector<QFileInfo> list;
    list.reserve(static_cast<std::size_t>(newItems.size()));
    for (const auto& url : qAsConst(newItems))
    {
        QFileInfo info(url.path());

        if (info.exists())
            list.push_back(info);
    }

    addItems(list);
}


void InputFilesModel::removeItems(const QModelIndexList& indexes)
{
    auto sortedIndexes = indexes;
    // Remove rows starting from the end to avoid row shifting
    std::sort(sortedIndexes.begin(), sortedIndexes.end(),
              [](const auto& index1, const auto& index2) { return index1.row() > index2.row(); });

    for (const auto& i: qAsConst(sortedIndexes))
        removeRows(i.row(), 1, QModelIndex());
}


QString InputFilesModel::getCommonParentFolder() const
{
    return findCommonParentPath(infoList);
}

}
