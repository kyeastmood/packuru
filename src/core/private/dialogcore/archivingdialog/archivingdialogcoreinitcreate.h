// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>
#include <memory>
#include <vector>

#include <QFileInfo>


namespace Packuru::Core
{

struct ArchivingTaskData;
class ArchivingParameterHandlerFactory;

struct ArchivingDialogCoreInitCreate
{
    using OnAcceptedCallback = std::function<void(const std::vector<ArchivingTaskData>&)>;

    bool isValid() const;

    std::vector<QFileInfo> filesToArchive;
    std::vector<std::unique_ptr<ArchivingParameterHandlerFactory>> factoryDataList;
    OnAcceptedCallback onAccepted;
};

}
