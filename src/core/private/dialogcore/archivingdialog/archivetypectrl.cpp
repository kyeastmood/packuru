// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivetypectrl.h"


namespace Packuru::Core
{

ArchiveTypeCtrl::ArchiveTypeCtrl(const QList<QString>& archiveTypes, int initialHandlerIndex)
{
    QList<int> indexes;
    for (int i = 0; i < archiveTypes.size(); )
        indexes.push_back(i++);

    setValueLists(archiveTypes, indexes);
    setCurrentValue(archiveTypes[initialHandlerIndex],initialHandlerIndex);

    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::ArchiveTypeCtrl::tr("Type");
}

}
