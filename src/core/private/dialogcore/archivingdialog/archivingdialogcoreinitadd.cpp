// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivingdialogcoreinitadd.h"


namespace Packuru::Core
{

bool ArchivingDialogCoreInitAdd::isValid() const
{
    return !archiveInfo.absoluteFilePath().isEmpty()
            && archiveEncryption != EncryptionState::___INVALID
            && !(archiveEncryption == EncryptionState::EntireArchive && password.isEmpty())
            && onAccepted;

}

}
