// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"


namespace Packuru::Core
{

class DestinationPathCtrl : public qcs::core::Controller<QString>
{
    Q_DECLARE_TR_FUNCTIONS(DestinationPathCtrl)
public:
    DestinationPathCtrl(const QString& initialPath);

    void update() override;
    void onValueSyncedToWidget(const QVariant &value) override;

private:
    QString automaticPathText;
    QString userSetValue;
};

}
