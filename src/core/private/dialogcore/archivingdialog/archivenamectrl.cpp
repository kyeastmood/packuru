// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivenamectrl.h"
#include "archivingmode.h"
#include "archivingdialogcontrollertype.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

ArchiveNameCtrl::ArchiveNameCtrl(const QString& initialName)
    : automaticNameText(Packuru::Core::ArchiveNameCtrl::tr("Automatic")),
      userSetValue(initialName)
{
    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::ArchiveNameCtrl::tr("Name");

}


void ArchiveNameCtrl::update()
{
    const auto archivingMode = getControllerPropertyAs<ArchivingMode>
            (ArchivingDialogControllerType::Type::ArchivingMode,
             ControllerProperty::PrivateCurrentValue);

    switch (archivingMode)
    {
    case ArchivingMode::ArchivePerItem:
        setCurrentValue(automaticNameText);
        setEnabled(false);
        break;
    case ArchivingMode::SingleArchive:
    {
        setCurrentValue(userSetValue);
        setEnabled(true);
        break;
    }
    default:
        Q_ASSERT(false);
    }
}


void ArchiveNameCtrl::onValueSyncedToWidget(const QVariant &value)
{
    userSetValue = Utils::getValueAs<QString>(value);
}

}
