// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"


namespace Packuru::Core
{

class ArchiveTypeCtrl : public qcs::core::Controller<QString, int>
{
    Q_DECLARE_TR_FUNCTIONS(ArchiveTypeCtrl)
public:
    ArchiveTypeCtrl(const QList<QString>& archiveTypes, int initialHandlerIndex);
};

}
