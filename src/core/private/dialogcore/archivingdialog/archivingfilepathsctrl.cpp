// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivingfilepathsctrl.h"
#include "core/private/privatestrings.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

ArchivingFilePathsCtrl::ArchivingFilePathsCtrl(const GetFilePathsSupport& getFilePathsSupport)
    : getFilePathsSupport(getFilePathsSupport),
      userChoice(ArchivingFilePaths::RelativePaths)
{
    properties[qcs::core::ControllerProperty::LabelText]
            = PrivateStrings::getString(PrivateStrings::UserString::Mode);
}


void ArchivingFilePathsCtrl::update()
{
    const auto pathsSet = getFilePathsSupport();

    Q_ASSERT(!pathsSet.empty());

    QList<ArchivingFilePaths> pathsList;

    auto mode = ArchivingFilePaths::RelativePaths;
    if (pathsSet.find(mode) != pathsSet.end())
        pathsList.push_back(mode);

    mode = ArchivingFilePaths::FullPaths;
    if (pathsSet.find(mode) != pathsSet.end())
        pathsList.push_back(mode);

    mode = ArchivingFilePaths::AbsolutePaths;
    if (pathsSet.find(mode) != pathsSet.end())
        pathsList.push_back(mode);

    Q_ASSERT(!pathsList.empty());

    if (pathsSet.find(userChoice) != pathsSet.end())
        mode = userChoice;
    else
        mode = pathsList[0]; // Modes are ordered from the most 'desired' to least

    setValueListsFromPrivate(pathsList);
    setCurrentValueFromPrivate(mode);
    setEnabled(pathsList.size() > 1);
}


void ArchivingFilePathsCtrl::onValueSyncedToWidget(const QVariant&)
{
    userChoice = getPropertyAs<ArchivingFilePaths>(ControllerProperty::PrivateCurrentValue);
}

}
