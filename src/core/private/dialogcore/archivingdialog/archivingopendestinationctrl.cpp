// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivingopendestinationctrl.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

ArchivingOpenDestinationCtrl::ArchivingOpenDestinationCtrl()
{
    setCurrentValue(false);
    properties[ControllerProperty::Text] =
            Packuru::Core::ArchivingOpenDestinationCtrl::tr("Open destination folder");
}

}
