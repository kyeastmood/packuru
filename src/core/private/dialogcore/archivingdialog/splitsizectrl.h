// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QCoreApplication>

#include "qcs-core/controller.h"


namespace Packuru::Core
{

class ArchivingParameterHandlerBase;
enum class SizeUnits;


class SplitSizeCtrl : public qcs::core::Controller<int>
{
    Q_DECLARE_TR_FUNCTIONS(SplitSizeCtrl)
public:
    SplitSizeCtrl(const std::function<bool()>& archiveSplittingSupport);

    void update() override;

private:
    const std::function<bool()> archiveSplittingSupport;
    SizeUnits units;
};

}
