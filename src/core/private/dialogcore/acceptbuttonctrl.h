// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "qcs-core/controllerbase.h"
#include "qcs-core/controllerproperty.h"


namespace Packuru::Core
{

template <typename ControllerId>
class AcceptButtonCtrl : public qcs::core::ControllerBase
{
public:
    AcceptButtonCtrl(ControllerId errorsCtrlId);

private:
    void update() override;

    ControllerId errorsId;
};

template<typename ControllerId>
AcceptButtonCtrl<ControllerId>::AcceptButtonCtrl(ControllerId errorsCtrlId)
    : errorsId(errorsCtrlId)
{

}

template<typename ControllerId>
void AcceptButtonCtrl<ControllerId>::update()
{
    const auto hasErrors
            = getControllerPropertyAs<bool>(errorsId, qcs::core::ControllerProperty::PrivateCurrentValue);

    setEnabled(!hasErrors);
}


}
