// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"

#include "core/private/compressionratiomode.h"


namespace Packuru::Core
{

class CompressionRatioCtrl : public qcs::core::Controller<QString, CompressionRatioMode>
{
    Q_DECLARE_TR_FUNCTIONS(CompressionRatioCtrl)
public:
    CompressionRatioCtrl(CompressionRatioMode ratioMode);
};

}
