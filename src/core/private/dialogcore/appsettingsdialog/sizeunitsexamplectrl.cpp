// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "sizeunitsexamplectrl.h"
#include "core/private/sizeunits.h"
#include "appsettingscontrollertype.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

using Type = AppSettingsControllerType::Type;


SizeUnitsExampleCtrl::SizeUnitsExampleCtrl()
{

}


void SizeUnitsExampleCtrl::update()
{
    const auto units = getControllerPropertyAs<SizeUnits>(Type::SizeUnits,
                                                          ControllerProperty::PrivateCurrentValue);

    switch (units)
    {
    case SizeUnits::Binary: setCurrentValue("1 KiB = 1024 "
                                            + Packuru::Core::SizeUnitsExampleCtrl::tr( "bytes", "1024"));
        break;
    case SizeUnits::Decimal: setCurrentValue("1 KB = 1000 "
                                             + Packuru::Core::SizeUnitsExampleCtrl::tr("bytes", "1000"));
        break;
    default: Q_ASSERT(false);
    }
}

}
