// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "core/private/archivetypemodelaccessor.h"


namespace Packuru::Core
{

class PluginDataModel;


namespace AppSettingsDialogCoreAux
{

struct InitData
{
    PluginDataModel* pluginModel = nullptr;
    ArchiveTypeModelAccessor archiveTypeModelAccessor;
};

}

}
