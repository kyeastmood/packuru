// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "core/private/privatestrings.h"
#include "genericpasswordctrl.h"


namespace Packuru::Core
{

GenericPasswordCtrl::GenericPasswordCtrl(const QString& password, bool acceptsPassword)
{
    setCurrentValue(password);
    setVisible(acceptsPassword);


    properties[qcs::core::ControllerProperty::LabelText]
            = PrivateStrings::getString(PrivateStrings::UserString::Password);
}

}
