// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"

#include "extractiondestinationmode.h"


namespace Packuru::Core
{

class ExtractionDestinationModeCtrl : public qcs::core::Controller<QString, ExtractionDestinationMode>
{
    Q_DECLARE_TR_FUNCTIONS(ExtractionDestinationModeCtrl)
public:
    ExtractionDestinationModeCtrl(bool userDestinationOnly);
};

}
