// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"


namespace Packuru::Core
{

class ExtractionDestinationPathCtrl : public qcs::core::Controller<QString>
{
    Q_DECLARE_TR_FUNCTIONS(ExtractionDestinationPathCtrl)
public:
    ExtractionDestinationPathCtrl(const QString& initialPath);

    void update() override;

private:
    void onValueSyncedToWidget(const QVariant& value) override;

    const QString automaticPathText;
    QString tempPath;
};

}
