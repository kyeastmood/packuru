// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "extractionfileoverwritectrl.h"


namespace Packuru::Core
{

ExtractionFileOverwriteCtrl::ExtractionFileOverwriteCtrl()
{
    setValueListsFromPrivate({ ExtractionFileOverwriteMode::AskBeforeOverwrite,
                        ExtractionFileOverwriteMode::SkipExisting,
                        ExtractionFileOverwriteMode::UpdateExisting,
                        ExtractionFileOverwriteMode::OverwriteExisting });

    setCurrentValueFromPrivate(ExtractionFileOverwriteMode::AskBeforeOverwrite);

    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::ExtractionFileOverwriteCtrl::tr("File overwriting");
}

}
