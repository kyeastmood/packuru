// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"

#include "core/private/extractiontopfoldermode.h"


namespace Packuru::Core
{

class TopFolderModeCtrl : public qcs::core::Controller<QString, ExtractionTopFolderMode>
{
    Q_DECLARE_TR_FUNCTIONS(TopFolderModeCtrl)
public:
    TopFolderModeCtrl();
};

}
