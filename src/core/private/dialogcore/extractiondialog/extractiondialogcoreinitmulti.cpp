// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "extractiondialogcoreinitmulti.h"


namespace Packuru::Core
{

bool ExtractionDialogCoreInitMulti::isValid() const
{
    return guessArchiveType
            && getBackend
            && onAccepted;
}

}
