// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "extractionfolderoverwritectrl.h"


namespace Packuru::Core
{

ExtractionFolderOverwriteCtrl::ExtractionFolderOverwriteCtrl()
{
    setValueListsFromPrivate({ ExtractionFolderOverwriteMode::AskBeforeOverwrite,
                        ExtractionFolderOverwriteMode::SkipExisting,
                        ExtractionFolderOverwriteMode::WriteIntoExisting });

    setCurrentValueFromPrivate(ExtractionFolderOverwriteMode::AskBeforeOverwrite);

    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::ExtractionFolderOverwriteCtrl::tr("Folder overwriting");
}

}
