// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"


namespace Packuru::Core
{

class ExtractionDestinationErrorCtrl : public qcs::core::Controller<QString>
{
    Q_DECLARE_TR_FUNCTIONS(ExtractionDestinationErrorCtrl)
public:
    ExtractionDestinationErrorCtrl();

private:
    void update() override;
};

}
