// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "extractiondestinationmodectrl.h"


namespace Packuru::Core
{

ExtractionDestinationModeCtrl::ExtractionDestinationModeCtrl(bool userDestinationOnly)
{
    if (userDestinationOnly)
    {
        setValueListsFromPrivate({ ExtractionDestinationMode::CustomFolder });
        setVisible(false);
    }
    else
        setValueListsFromPrivate({ ExtractionDestinationMode::CustomFolder,
                            ExtractionDestinationMode::ArchiveParentFolder });

    setCurrentValueFromPrivate(ExtractionDestinationMode::CustomFolder);

    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::ExtractionDestinationModeCtrl::tr("Extract to");
}

}
