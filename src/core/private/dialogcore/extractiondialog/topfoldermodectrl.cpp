// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "topfoldermodectrl.h"


namespace Packuru::Core
{

TopFolderModeCtrl::TopFolderModeCtrl()
{
    setValueListsFromPrivate({ ExtractionTopFolderMode::CreateIfAbsent,
                               ExtractionTopFolderMode::CreateFolder,
                               ExtractionTopFolderMode::DoNotCreateFolder });

    setCurrentValueFromPrivate(ExtractionTopFolderMode::CreateIfAbsent);


    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::TopFolderModeCtrl::tr("Top folder");
}

}
