// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"

#include "core/private/extractionfileoverwritemode.h"


namespace Packuru::Core
{

class ExtractionFileOverwriteCtrl : public qcs::core::Controller<QString, ExtractionFileOverwriteMode>
{
    Q_DECLARE_TR_FUNCTIONS(ExtractionFileOverwriteCtrl)
public:
    ExtractionFileOverwriteCtrl();
};

}
