// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>
#include <functional>

#include <QFileInfo>

#include "core/private/dialogcore/getbackendfortypecallback.h"
#include "core/private/dialogcore/guessarchivetypecallback.h"


namespace Packuru::Core
{

struct ExtractionTaskData;

struct ExtractionDialogCoreInitMulti
{
    using OnAcceptedCallback = std::function<void(const std::vector<ExtractionTaskData>&)>;

    bool isValid() const;

    GuessArchiveTypeCallback guessArchiveType;
    GetBackendForTypeCallback getBackend;
    std::vector<QFileInfo> archives;
    OnAcceptedCallback onAccepted;
};

}
