// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"

#include "core/private/extractionfolderoverwritemode.h"


namespace Packuru::Core
{

class ExtractionFolderOverwriteCtrl : public qcs::core::Controller<QString, ExtractionFolderOverwriteMode>
{
    Q_DECLARE_TR_FUNCTIONS(ExtractionFolderOverwriteCtrl)
public:
    ExtractionFolderOverwriteCtrl();

};

}
