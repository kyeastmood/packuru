// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "extractiondialogcoreinitsingle.h"


namespace Packuru::Core
{

bool ExtractionDialogCoreInitSingle::isValid() const
{
    return !archiveInfo.absoluteFilePath().isEmpty()
            && Packuru::Core::isValid(archiveType)
            && archiveEncryption != EncryptionState::___INVALID
            && !(archiveEncryption == EncryptionState::EntireArchive && password.isEmpty())
            && onAccepted;
}

}
