// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>

#include "sizeunits.h"


namespace Packuru::Core
{

QString toString(SizeUnits value)
{
    switch (value)
    {
    case SizeUnits::Binary: return QCoreApplication::translate("SizeUnits", "Binary");
    case SizeUnits::Decimal: return QCoreApplication::translate("SizeUnits", "Decimal");
    default: Q_ASSERT(false); return QString();
    }
}


QString toSettingsValue(SizeUnits value)
{
    switch (value)
    {
    case SizeUnits::Binary: return "binary";
    case SizeUnits::Decimal: return "decimal";
    default: Q_ASSERT(false); return QString();
    }
}


void fromString(const QString& key, SizeUnits& out)
{
    if (key == QLatin1String("binary"))
        out = SizeUnits::Binary;
    else if (key == QLatin1String("decimal"))
        out = SizeUnits::Decimal;
    else
        out = SizeUnits::___INVALID;
}

}
