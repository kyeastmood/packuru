// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivecontentdata.h"
#include "archivedirnode.h"
#include "pathlookuptable.h"


namespace Packuru::Core
{

ArchiveContentData::ArchiveContentData()
    : rootNode(new ArchiveDirNode),
      pathLookupTable(new PathLookupTable)
{
}


ArchiveContentData::~ArchiveContentData()
{

}


bool ArchiveContentData::containsData() const
{
    return static_cast<bool>(rootNode->getChildrenCount());
}

}
