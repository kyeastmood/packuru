// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDir>
#include <QTemporaryFile>
#include <QThread>
#include <QStandardPaths>
#include <QDebug>

#include "taskprivate.h"
#include "task.h"
#include "taskworker.h"
#include "passwordpromptcore.h"


namespace Packuru::Core
{

TaskPrivate::TaskPrivate(QObject* parent)
    : QObject(parent)
{
    const auto paths = QStandardPaths::standardLocations(QStandardPaths::CacheLocation);

    if (!paths.empty())
        cacheDir = paths[0];
}


TaskPrivate::~TaskPrivate()
{
}


void TaskPrivate::setWorker(std::unique_ptr<TaskWorker> newWorker)
{
    Q_ASSERT(newWorker != worker);

    worker = std::move(newWorker); // Deletes previous worker
    worker->moveToThread(workerThread);

    connect(worker.get(), &TaskWorker::stateChanged,
            this, &TaskPrivate::changeState);

    connect(worker.get(), &TaskWorker::progressChanged,
            this, &TaskPrivate::changeProgress);

    connect(worker.get(), &TaskWorker::finished,
            this, &TaskPrivate::onWorkerFinished);

    connect(worker.get(), &TaskWorker::errors,
            this, &TaskPrivate::setErrors);

    connect(worker.get(), &TaskWorker::warnings,
            this, &TaskPrivate::setWarnings);

    connect(worker.get(), &TaskWorker::log,
            this, &TaskPrivate::setLog);

    Q_ASSERT(publ);
    connect(worker.get(), &TaskWorker::passwordNeeded,
            publ, &Task::passwordNeeded);
}


void TaskPrivate::changeState(TaskState newState)
{
    if (state != newState)
    {
        const bool wasBusy = isStateBusy(state);
        state = newState;
        const bool isBusyNow = isStateBusy(state);

        if (isStateCompleted(state))
            changeProgress(100);

        if (state == TaskState::InProgress)
            timer.start();
        else if (state == TaskState::ReadyToRun)
            timer.reset();
        else
            timer.stop();

        emit publ->stateChanged(state);

        if (wasBusy != isBusyNow)
            emit publ->busy(isBusyNow);
        else if (state == TaskState::ReadyToRun)
            emit publ->readyToRun();
    }
}

void TaskPrivate::onWorkerFinished()
{
    // qDebug() << "task run time=" << timer.getMilliseconds() << "ms (" << timer.getSeconds() << "s )";
    workerThread->quit();
    workerThread->wait();
    emit publ->finished();
}



void TaskPrivate::changeProgress(int value)
{
    if (value != progress)
    {
        progress = value;
        emit publ->progressChanged(progress);
    }
}


bool TaskPrivate::setUpPathForWriting(const QString& path) const
{
    Q_ASSERT(path.endsWith('/'));

    QDir dir;

    bool ready = dir.mkpath(path);

    if (ready)
    {
        QTemporaryFile f(path + QLatin1String("tmp"));
        ready = f.open();
    }

    return ready;
}


void TaskPrivate::setErrors(const std::unordered_set<TaskError>& errors)
{
    this->errors = errors;
}


void TaskPrivate::setWarnings(const std::unordered_set<TaskWarning>& warnings)
{
    this->warnings = warnings;
}


void TaskPrivate::setLog(const QString& log)
{
    this->log = log;
}

}
