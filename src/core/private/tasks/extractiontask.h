// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "task.h"
#include "backendhandlerfactory.h"


namespace Packuru::Core
{

class ExtractionTaskData;
class ExtractionFileOverwritePromptCore;
class ExtractionFolderOverwritePromptCore;
class ExtractionTypeMismatchPromptCore;


class ExtractionTask : public Task
{
    Q_OBJECT
public:
    ExtractionTask(const ExtractionTaskData& taskData, const BackendHandlerFactory& backendFactory, QObject *parent = nullptr);
    ~ExtractionTask() override;

    ExtractionTaskData getData() const;

signals:
    void fileExists(ExtractionFileOverwritePromptCore* promptCore);
    void folderExists(ExtractionFolderOverwritePromptCore* promptCore);
    void itemTypeMismatch(ExtractionTypeMismatchPromptCore* promptCore);
    void openDestinationPath(const QString& path);

private:
    std::unique_ptr<TaskWorker> createWorker() override;

    struct Private;
    std::unique_ptr<Private> priv;
};

}
