// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>
#include <QMetaType>

#include "core/symbol_export.h"


namespace Packuru::Core
{

enum class TaskWarning
{
    // Invoked executable has a very old version and might not work as expected
    BackendVersionDeprecated,
    // Invoked executable has a newer version that the one used during development
    BackendVersionNotTested,
    LowDiskSpace,
    Unknown
};

PACKURU_CORE_EXPORT QString toString(TaskWarning value);

}

Q_DECLARE_METATYPE(Packuru::Core::TaskWarning);
