// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QString>

#include "archivefilenode.h"


namespace Packuru::Core
{

ArchiveFileNode::ArchiveFileNode(ArchiveFileNode *parent, int row, ArchiveItem& item)
    : parent(parent),
      row(row),
      _item(std::move(item))
{

}


ArchiveFileNode::ArchiveFileNode(ArchiveFileNode* parent, int row, const QString& name)
    : parent(parent),
      row(row)
{
    _item.name = name;
}


ArchiveFileNode::~ArchiveFileNode() = default;


ArchiveFileNode* ArchiveFileNode::createFileNode(ArchiveFileNode* parent, int row, ArchiveItem& item)
{
    return new ArchiveFileNode(parent, row, item);
}

void ArchiveFileNode::setItem(ArchiveItem& item)
{
    this->_item = std::move(item);
}

void ArchiveFileNode::computeCompressionRatio(CompressionRatioMode mode)
{
    _item.computeRatio(mode);
}

}
