// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <unordered_set>

#include <QObject>


namespace Packuru::Core
{

enum class TaskState;
class PasswordPromptCore;
class AbstractBackendHandler;
enum class BackendHandlerState;
enum class TaskError;
enum class TaskWarning;


class TaskWorker : public QObject
{
    Q_OBJECT
public:
    // No parent for TaskWorker, it will be moved to a worker thread
    TaskWorker(QObject* promptOwner,
               bool archiveTypeSupported,
               std::unique_ptr<AbstractBackendHandler> backendHandler);
    ~TaskWorker() override;

    // NOT thread-safe, invoke indirectly
    Q_INVOKABLE void start();

    // NOT thread-safe, invoke indirectly
    Q_INVOKABLE void stop();

    // Must be thread-safe
    virtual QString getArchiveName() const = 0;

    // Must be thread-safe
    virtual QString getPassword() const = 0;

    // NOT thread-safe - backend handler is used, invoke indirectly
    Q_INVOKABLE void setPassword(const QString& password);

signals:
    void stateChanged(Packuru::Core::TaskState state);
    void progressChanged(int value);
    void passwordNeeded(PasswordPromptCore* promptCore);
    void errors(const std::unordered_set<Packuru::Core::TaskError>& errors);
    void warnings(const std::unordered_set<Packuru::Core::TaskWarning>& warnings);
    void log(const QString& log);
    void finished();

protected:
    // Return false to halt the start procedure; do not change task state; backendHandler can be nullptr
    virtual bool preStartInit();
    virtual void startImpl() = 0;
    virtual void stopImpl();
    virtual void onBackendStateChanged(BackendHandlerState state);
    virtual void onBackendFinished(TaskState concludedState);
    virtual void updatePassword(const QString& password) = 0;

    struct TaskWorkerPrivate* priv;
    friend struct TaskWorkerPrivate;
};

}
