// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "testtask.h"
#include "tasktype.h"
#include "testtaskworker.h"
#include "taskprivate.h"
#include "testtaskdata.h"
#include "core/private/plugin-api/abstractbackendhandler.h"


namespace Packuru::Core
{

TestTask::TestTask(const TestTaskData& taskData, const BackendHandlerFactory& backendFactory, QObject* parent)
    : Task(TaskType::Test,
           std::make_unique<TestTaskWorker>(this, taskData, backendFactory(taskData.backendData.archiveType)),
           backendFactory,
           parent)
{
    priv->backendFactory = backendFactory;
}


TestTask::~TestTask()
{

}


std::unique_ptr<TaskWorker> TestTask::createWorker()
{
    auto worker = qobject_cast<TestTaskWorker*>(Task::priv->worker.get());
    Q_ASSERT(worker);
    const TestTaskData data = worker->getData();
    auto newWorker = std::make_unique<TestTaskWorker>(this, data, Task::priv->backendFactory(data.backendData.archiveType));
    return newWorker;
}

}
