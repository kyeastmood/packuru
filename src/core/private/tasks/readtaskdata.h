// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "core/private/plugin-api/backendreaddata.h"


namespace Packuru::Core
{

struct ReadTaskData
{
    BackendReadData backendData;
};

}
