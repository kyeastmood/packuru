// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <shared_mutex>

#include "core/private/3rdparty/cs_libguarded/cs_shared_guarded.h"
#include "deletiontaskworker.h"
#include "deletiontaskdata.h"
#include "taskworkerprivate.h"
#include "core/private/plugin-api/abstractbackendhandler.h"
#include "taskerror.h"


namespace Packuru::Core
{

struct DeletionTaskWorker::Private
{
    libguarded::shared_guarded<DeletionTaskData> taskData;
};


DeletionTaskWorker::DeletionTaskWorker(QObject* owner,
                                       const DeletionTaskData& taskData,
                                       std::unique_ptr<AbstractBackendHandler> backendHandler)
    : TaskWorker(owner,
                 taskData.backendData.archiveType != ArchiveType::___NOT_SUPPORTED,
                 std::move(backendHandler)),
      priv(new Private{std::move(taskData)})

{

}


DeletionTaskWorker::~DeletionTaskWorker()
{

}


DeletionTaskData DeletionTaskWorker::getData() const
{
    return *priv->taskData.lock_shared();
}


QString DeletionTaskWorker::getArchiveName() const
{
    return priv->taskData.lock_shared()->backendData.archiveInfo.fileName();
}


QString DeletionTaskWorker::getPassword() const
{
    return priv->taskData.lock_shared()->backendData.password;
}


bool DeletionTaskWorker::preStartInit()
{
    const auto data = priv->taskData.lock_shared();
    return TaskWorker::priv->confirmArchiveExistence(data->backendData.archiveInfo.absoluteFilePath());
}


void DeletionTaskWorker::startImpl()
{
    const auto data = priv->taskData.lock_shared();

    QString path = data->backendData.archiveInfo.absolutePath();
    if (!path.endsWith('/'))
        path += QLatin1Char('/');

    if (!TaskWorker::priv->tryWriteToFolder(path))
        return TaskWorker::priv->changeState(TaskState::Errors);

    TaskWorker::priv->backendHandler->deleteFiles(data->backendData);
}


void DeletionTaskWorker::updatePassword(const QString& password)
{
    priv->taskData.lock()->backendData.password = password;
}

}
