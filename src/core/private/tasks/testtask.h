// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "task.h"
#include "backendhandlerfactory.h"


namespace Packuru::Core
{

class TestTaskData;


class TestTask : public Task
{
    Q_OBJECT
public:
    TestTask(const TestTaskData& taskData, const BackendHandlerFactory& backendFactory, QObject *parent = nullptr);
    ~TestTask() override;

private:
    std::unique_ptr<TaskWorker> createWorker() override;
};

}
