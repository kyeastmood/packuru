// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <shared_mutex>

#include "core/private/3rdparty/cs_libguarded/cs_shared_guarded.h"
#include "testtaskworker.h"
#include "testtaskdata.h"
#include "taskworkerprivate.h"
#include "core/private/plugin-api/abstractbackendhandler.h"


namespace Packuru::Core
{

struct TestTaskWorker::Private
{
    libguarded::shared_guarded<TestTaskData> taskData;
};


TestTaskWorker::TestTaskWorker(QObject* owner, const TestTaskData& taskData, std::unique_ptr<AbstractBackendHandler> backendHandler)
    : TaskWorker(owner,
                 taskData.backendData.archiveType != ArchiveType::___NOT_SUPPORTED,
                 std::move(backendHandler)),
      priv(new Private{std::move(taskData)})
{

}


TestTaskWorker::~TestTaskWorker()
{

}


TestTaskData TestTaskWorker::getData() const
{
    return *priv->taskData.lock_shared();
}


QString TestTaskWorker::getArchiveName() const
{
    return priv->taskData.lock_shared()->backendData.archiveInfo.fileName();
}


QString TestTaskWorker::getPassword() const
{
    return priv->taskData.lock_shared()->backendData.password;
}


bool TestTaskWorker::preStartInit()
{
    const auto data = priv->taskData.lock_shared();
    return TaskWorker::priv->confirmArchiveExistence(data->backendData.archiveInfo.absoluteFilePath());
}


void TestTaskWorker::startImpl()
{
    const auto data = priv->taskData.lock_shared();
    auto handler = TaskWorker::priv->backendHandler;
    handler->testArchive(data->backendData);
}


void TestTaskWorker::updatePassword(const QString& password)
{
    priv->taskData.lock()->backendData.password = password;
}

}
