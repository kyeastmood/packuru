// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "taskworker.h"


namespace Packuru::Core
{

class DeletionTaskData;
class AbstractBackendHandler;


class DeletionTaskWorker : public TaskWorker
{
    Q_OBJECT
public:
    DeletionTaskWorker(QObject* owner, const DeletionTaskData& taskData, std::unique_ptr<AbstractBackendHandler> backendHandler);
    ~DeletionTaskWorker();

    // Thread-safe
    DeletionTaskData getData() const;

    // Thread-safe
    QString getArchiveName() const override;
    QString getPassword() const override;

private:
    bool preStartInit() override;
    void startImpl() override;
    void updatePassword(const QString& password) override;

    struct Private;
    std::unique_ptr<Private> priv;
};

}
