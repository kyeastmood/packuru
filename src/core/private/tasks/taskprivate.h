// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <unordered_set>

#include <QObject>
#include <QString>

#include "tasktype.h"
#include "taskstate.h"
#include "backendhandlerfactory.h"
#include "core/private/timer.h"


namespace Packuru::Core
{

class Task;
class TaskWorker;
enum class TaskError;
enum class TaskWarning;


struct TaskPrivate : public QObject
{
    Q_OBJECT
public:
    TaskPrivate(QObject* parent = nullptr);
    ~TaskPrivate();

    void setWorker(std::unique_ptr<TaskWorker> newWorker);
    void onWorkerFinished();
    void changeState(TaskState newState);
    void changeProgress(int value);
    bool setUpPathForWriting(const QString& path) const;
    void setErrors(const std::unordered_set<TaskError>& errors);
    void setWarnings(const std::unordered_set<TaskWarning>& warnings);
    void setLog(const QString& log);

    Task* publ = nullptr;
    TaskType type = TaskType::___INVALID;
    std::unique_ptr<TaskWorker> worker;
    QThread* workerThread = nullptr;
    TaskState state = TaskState::ReadyToRun;
    Timer timer;
    int progress = -1;
    BackendHandlerFactory backendFactory;
    QString cacheDir;
    std::unordered_set<TaskError> errors;
    std::unordered_set<TaskWarning> warnings;
    QString log;
};

}
