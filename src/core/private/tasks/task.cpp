// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QThread>
#include <QDir>

#include "task.h"
#include "taskstate.h"
#include "taskprivate.h"
#include "taskworker.h"
#include "taskerror.h"
#include "taskwarning.h"


namespace Packuru::Core
{

/*
 * Task object runs in the main thread. TaskWorker object runs in a worker thread.
 *
 * Task destructor stops worker thread so the event queue is not running
 * and it's safe to directly delete TaskWorker object.
 *
 * All *PromptCore objects must be moved to main thread from *TaskWorker objects
 * and Task object must be set as their parent. Users of *PromptCore must not
 * re-parent it to guarantee its destruction.
 *
 * *PromptCore objects must be 'one action' objects, that is after they receive a command
 * via a public function call from *PromptWidget they should block themselves from subsequent
 * commands (e.g. by setting bool finished to true), then invoke some internal action.
 *
 * TaskWorker must change its state in reaction to any *PromptCore command (e.g. Waiting* -> Running).
 * *PromptCore must react to this state change by destroying itself. This is because Task can also be stopped
 * outside of a *PromptWidget (e.g. in Queue) and * *PromptCore must react to this state change.
 *
 * *PromptWidget objects must destroy/hide themselves when they receive destroyed()
 * signal from *PromptCore object.
 */


Task::Task(TaskType type,
           std::unique_ptr<TaskWorker> worker,
           const BackendHandlerFactory& backendFactory,
           QObject *parent)
    : QObject(parent),
      priv(new TaskPrivate(this))
{
    Q_ASSERT(type != TaskType::___INVALID);
    priv->publ = this;
    priv->type = type;

    priv->workerThread = new QThread(this);

    priv->setWorker(std::move(worker));

    priv->backendFactory = backendFactory;
}


Task::~Task()
{
    stop();
    priv->workerThread->quit();
    if (!priv->workerThread->wait(3000))
        priv->workerThread->terminate(); // shut down a misbehaving/blocking worker
}


void Task::start()
{
    if (!isReadyToRun())
        return;

    priv->changeState(TaskState::StartingUp);

    // Not necessary but safer
    if (!priv->cacheDir.isEmpty())
        QDir::setCurrent(priv->cacheDir);

    priv->workerThread->start();

    const bool success = QMetaObject::invokeMethod(priv->worker.get(), &TaskWorker::start);
    Q_ASSERT(success);
}


void Task::stop()
{
    if (isBusy())
    {
        const bool success = QMetaObject::invokeMethod(priv->worker.get(), &TaskWorker::stop);
        Q_ASSERT(success);
    }
}


bool Task::reset()
{
    if (!isFinished())
        return false;

    auto newWorker = createWorker();
    Q_ASSERT(newWorker);
    // Direct destruction should be safe: the event queue in the worker thread is not running
    // and worker object is also idle
    priv->setWorker(std::move(newWorker));

    priv->timer.reset();
    priv->errors.clear();
    priv->warnings.clear();
    priv->log.clear();

    priv->changeState(TaskState::ReadyToRun);
    priv->changeProgress(-1);

    return true;
}


bool Task::isReadyToRun() const
{
    return priv->state == TaskState::ReadyToRun;
}


bool Task::isBusy() const
{
    return isStateBusy(priv->state);
}


bool Task::isFinished() const
{
    return isStateFinished(priv->state);
}


bool Task::isCompleted() const
{
    return isStateCompleted(priv->state);
}


TaskType Task::getType() const
{
    return priv->type;
}


TaskState Task::getState() const
{
    return priv->state;
}


int Task::getProgress() const
{
    return priv->progress;
}


std::unordered_set<TaskError> Task::getErrors() const
{
    return priv->errors;
}


std::unordered_set<TaskWarning> Task::getWarnings() const
{
    return priv->warnings;
}


QStringList Task::getErrorAndWarningNames() const
{
    QStringList result;

    for (const auto e : priv->errors)
        result.push_back(toString(e));

    for (const auto w : priv->warnings)
        result.push_back(toString(w));

    return result;
}


bool Task::hasLog() const
{
    return !priv->log.isEmpty();
}


QString Task::getLog() const
{
    return priv->log;
}


QString Task::getArchiveName() const
{
    return priv->worker->getArchiveName();
}


QString Task::getPassword() const
{
    return priv->worker->getPassword();
}

void Task::setPassword(const QString& password)
{
    const bool success = QMetaObject::invokeMethod(priv->worker.get(),
                              "setPassword",
                              Qt::AutoConnection,
                              Q_ARG(QString, password));
    Q_ASSERT(success);
}

}
