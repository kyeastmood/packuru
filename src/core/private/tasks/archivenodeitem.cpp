// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivenodeitem.h"
#include "core/private/compressionratiomode.h"


namespace Packuru::Core
{

void ArchiveNodeItem::computeRatio(CompressionRatioMode ratioMode)
{
    if (originalSize > 0 && packedSize > 0)
        ratio = packedSize / static_cast<float>(originalSize);

    ratio = static_cast<float>(computeSizeRatio(originalSize, packedSize, ratioMode));
}

}
