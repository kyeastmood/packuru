// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <unordered_set>

#include <QObject>

#include "backendhandlerfactory.h"


namespace Packuru::Core
{

enum class TaskState;
enum class TaskType;
enum class TaskError;
enum class TaskWarning;
class PasswordPromptCore;
class TaskPrivate;
class TaskWorker;


class Task : public QObject
{
    Q_OBJECT
public:
    virtual ~Task();

    void start();
    void stop();
    bool reset();

    bool isReadyToRun() const;
    bool isBusy() const;
    bool isFinished() const;
    bool isCompleted() const;

    TaskType getType() const;
    TaskState getState() const;
    int getProgress() const;
    std::unordered_set<TaskError> getErrors() const;
    std::unordered_set<TaskWarning> getWarnings() const;
    QStringList getErrorAndWarningNames() const;
    bool hasLog() const;
    QString getLog() const;

    QString getArchiveName() const;
    QString getPassword() const;
    void setPassword(const QString& password);

signals:
    bool busy(bool value);
    void stateChanged(TaskState state);
    void progressChanged(int value);
    // Task is the parent of promptCore, do not re-parent
    void passwordNeeded(PasswordPromptCore* promptCore);
    void finished();
    void readyToRun();

protected:
    explicit Task(TaskType type,
                  std::unique_ptr<TaskWorker> worker,
                  const BackendHandlerFactory& backendFactory,
                  QObject *parent = nullptr);

    virtual std::unique_ptr<TaskWorker> createWorker() = 0;

    TaskPrivate* priv;
};

}
