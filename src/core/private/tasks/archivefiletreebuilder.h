// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>

#include <QObject>
#include <QMimeDatabase>

#include "utils/private/qttypehasher.h"

#include "core/private/plugin-api/archiveitem.h"
#include "archivecontentdata.h"



namespace Packuru::Core
{

class ArchiveContentData;
class ArchiveFileNode;
enum class CompressionRatioMode;;


class ArchiveFileTreeBuilder : public QObject
{
    Q_OBJECT
public:
    ArchiveFileTreeBuilder(CompressionRatioMode ratioMode, QObject* parent = nullptr);
    ~ArchiveFileTreeBuilder();

    void addItems(std::vector<ArchiveItem>& items);
    void reset();
    ArchiveContentData getArchiveContentData() const { return data; }

private:
    ArchiveDirNode* getDirNode(const QString& path);
    ArchiveDirNode* createPath(const QString& path);

    ArchiveContentData data;

    std::unordered_map<QString, ArchiveFileNode*, Packuru::Utils::QtTypeHasher<QString>> fileNodes;
    CompressionRatioMode ratioMode;
    QMimeDatabase mimeDb;
};

}
