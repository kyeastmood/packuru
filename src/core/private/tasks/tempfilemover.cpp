// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDateTime>
#include <QDir>

#include "tempfilemover.h"
#include "extractiontaskdata.h"
#include "extractionfileoverwritepromptcore.h"
#include "extractionfolderoverwritepromptcore.h"
#include "extractiontypemismatchpromptcore.h"
#include "core/private/promptcore/extractionfileoverwritepromptcoreaux.h"
#include "core/private/promptcore/extractionfolderoverwritepromptcoreaux.h"
#include "core/private/promptcore/extractiontypemismatchpromptcoreaux.h"


namespace Packuru::Core
{

TempFileMover::TempFileMover(ExtractionFileOverwriteMode fileOverwriteMode,
                             ExtractionFolderOverwriteMode folderOverwriteMode,
                             QObject *parent)
    : QObject(parent),
      fileOverwriteMode(fileOverwriteMode),
      folderOverwriteMode(folderOverwriteMode)
{

}


TempFileMover::~TempFileMover()
{
}


void TempFileMover::moveFolderContent(const QDir &source, const QDir &destination)
{
    if (running)
        return;

    running = true;

    queue.push({source, destination});

    processQueue();
}


void TempFileMover::processQueue()
{
    emit waitingForInput(false);

    while (!queue.empty())
    {
        auto& moveTask = queue.front();

        if (currentItem == -1)
        {
            Q_ASSERT(sourceItems.isEmpty());
            currentItem = 0;
            // This doesn't scale. QDirIterator is more efficient, however its usage would require
            // redesigning of entire class. Unless it seriously affects performance it might not be
            // worth the effort since the code runs in a worker thread anyway.
            sourceItems = moveTask.sourceDir.entryInfoList();
        }

        for (; currentItem < sourceItems.size(); ++currentItem)
        {
            const auto& sourceItem = sourceItems[currentItem];
            destinationItem = (moveTask.destinationDir.absolutePath()
                               + QLatin1Char('/')
                               + sourceItem.fileName());

            const auto success = moveTask.destinationDir.rename(sourceItem.absoluteFilePath(),
                                                                destinationItem.absoluteFilePath());

            const bool bothExist = sourceItem.exists() && destinationItem.exists();
            const bool neitherIsLink = !sourceItem.isSymLink() && !destinationItem.isSymLink();

            if (!success)
            {
                if ((bothExist && neitherIsLink && sourceItem.isFile() && destinationItem.isFile())
                    || (sourceItem.isSymLink() && destinationItem.isSymLink()))
                {
                    if (!processFile())
                    {
                        ExtractionFileOverwritePromptCoreAux::Init init;
                        init.source = sourceItem;
                        init.destination = destinationItem;

                        ExtractionFileOverwritePromptCoreAux::Backdoor backdoor;

                        auto promptCore = new ExtractionFileOverwritePromptCore(init, backdoor);
                        Q_ASSERT(backdoor.emitter);

                        connect(backdoor.emitter,
                                &ExtractionFileOverwritePromptCoreAux::Emitter::overwriteRequested,
                                this, &TempFileMover::promptOverwriteFile);

                        connect(backdoor.emitter,
                                &ExtractionFileOverwritePromptCoreAux::Emitter::skipRequested,
                                this, &TempFileMover::promptSkipFile);

                        connect(backdoor.emitter,
                                &ExtractionFileOverwritePromptCoreAux::Emitter::retryRequested,
                                this, &TempFileMover::processQueue);

                        emit waitingForInput(true);
                        return fileExists(promptCore, backdoor);
                    }
                }
                else if (bothExist && neitherIsLink && sourceItem.isDir() && destinationItem.isDir())
                {
                    if (!processFolder())
                    {
                        ExtractionFolderOverwritePromptCoreAux::Init init;
                        init.source.setPath(sourceItem.absoluteFilePath());
                        init.destination.setPath(destinationItem.absoluteFilePath());

                        ExtractionFolderOverwritePromptCoreAux::Backdoor backdoor;

                        auto promptCore = new ExtractionFolderOverwritePromptCore(init, backdoor);
                        Q_ASSERT(backdoor.emitter);

                        connect(backdoor.emitter, &ExtractionFolderOverwritePromptCoreAux::Emitter::writeIntoRequested,
                                this, &TempFileMover::promptWriteIntoFolder);

                        connect(backdoor.emitter, &ExtractionFolderOverwritePromptCoreAux::Emitter::skipRequested,
                                this, &TempFileMover::promptSkipFolder);

                        connect(backdoor.emitter, &ExtractionFolderOverwritePromptCoreAux::Emitter::retryRequested,
                                this, &TempFileMover::processQueue);

                        emit waitingForInput(true);
                        return folderExists(promptCore, backdoor);
                    }
                }
                else if (!skipItemTypeMismatch)
                {
                    ExtractionTypeMismatchPromptCoreAux::Init init;
                    init.source = sourceItem;
                    init.destination = destinationItem;

                    ExtractionTypeMismatchPromptCoreAux::Backdoor backdoor;

                    auto promptCore = new ExtractionTypeMismatchPromptCore(init, backdoor);
                    Q_ASSERT(backdoor.emitter);

                    connect(backdoor.emitter, &ExtractionTypeMismatchPromptCoreAux::Emitter::skipRequested,
                            this, &TempFileMover::promptSkipTypeMismatch);

                    connect(backdoor.emitter, &ExtractionTypeMismatchPromptCoreAux::Emitter::retryRequested,
                            this, &TempFileMover::processQueue);

                    emit waitingForInput(true);
                    return itemTypeMismatch(promptCore, backdoor);
                }
                else
                {
                    // unknown error
                    failedItems.push_back(sourceItem.absoluteFilePath());
                }
            }
        }

        currentItem = -1;
        sourceItems.clear();
        queue.pop();
    }

    running = false;

    if (failedItems.empty())
        emit success();
    else
        emit errors(std::vector<QString>(std::move(failedItems)));

}


bool TempFileMover::processFile()
{
    switch (fileOverwriteMode)
    {
    case ExtractionFileOverwriteMode::AskBeforeOverwrite:
        return false;
    case ExtractionFileOverwriteMode::OverwriteExisting:
    {
        overwriteFile();
        return true;

    }
    case ExtractionFileOverwriteMode::SkipExisting:
        return true;

    case ExtractionFileOverwriteMode::UpdateExisting:
    {
        if (sourceItems[currentItem].lastModified() > destinationItem.lastModified())
            overwriteFile();
        return true;

    }
    default:
        Q_ASSERT(false); return true;
    }

}


bool TempFileMover::processFolder()
{
    switch (folderOverwriteMode)
    {
    case ExtractionFolderOverwriteMode::AskBeforeOverwrite:
        return false;
    case ExtractionFolderOverwriteMode::WriteIntoExisting:
    {
        // Get all entries from source,
        // add to queue and move to destination;
        queue.push({sourceItems[currentItem].absoluteFilePath(), destinationItem.absoluteFilePath()});
        return true;

    }
    case ExtractionFolderOverwriteMode::SkipExisting:
        return true;

    default:
        Q_ASSERT(false); return true;
    }
}


void TempFileMover::overwriteFile()
{
    Q_ASSERT(running);

    const auto& sourceItem = sourceItems[currentItem];

    QDir dir;
    const bool r1 = dir.remove(destinationItem.absoluteFilePath());
    const bool r2 = dir.rename(sourceItem.absoluteFilePath(),
                         destinationItem.absoluteFilePath());

    if (!r1 || !r2)
        failedItems.push_back(sourceItem.absoluteFilePath());
}


void TempFileMover::promptOverwriteFile(bool applyToAll)
{
    overwriteFile();
    if (applyToAll)
        fileOverwriteMode = ExtractionFileOverwriteMode::OverwriteExisting;
    ++currentItem;
    processQueue();
}


void TempFileMover::promptWriteIntoFolder(bool applyToAll)
{
    queue.push({sourceItems[currentItem].absoluteFilePath(), destinationItem.absoluteFilePath()});
    if (applyToAll)
        folderOverwriteMode = ExtractionFolderOverwriteMode::WriteIntoExisting;
    ++currentItem;
    processQueue();
}


void TempFileMover::promptSkipFile(bool applyToAll)
{
    if (applyToAll)
        fileOverwriteMode = ExtractionFileOverwriteMode::SkipExisting;
    ++currentItem;
    processQueue();
}


void TempFileMover::promptSkipFolder(bool applyToAll)
{
    if (applyToAll)
        folderOverwriteMode = ExtractionFolderOverwriteMode::SkipExisting;
    ++currentItem;
    processQueue();
}


void TempFileMover::promptSkipTypeMismatch(bool applyToAll)
{
    if (applyToAll)
        skipItemTypeMismatch = true;
    ++currentItem;
    processQueue();
}

}
