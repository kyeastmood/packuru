// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>

#include "core/symbol_export.h"


namespace Packuru::Core
{

enum class TaskState
{
    ReadyToRun,

    ___BUSY_STATES_BEGIN,
    StartingUp,
    InProgress,
    WaitingForDecision,
    WaitingForPassword,
    Paused,
    ___BUSY_STATES_END,

    ___FINISHED_STATES_BEGIN,
    // No warnings and no errors must be present in Success state
    Success,
    // No errors must be present in Warnings state
    Warnings,
    Errors,
    Aborted,
    ___FINISHED_STATES_END,

    ___INVALID
};

PACKURU_CORE_EXPORT QString toString(TaskState state);
PACKURU_CORE_EXPORT bool isStateBusy(TaskState state);
PACKURU_CORE_EXPORT bool isStateFinished(TaskState state);
PACKURU_CORE_EXPORT bool isStateCompleted(TaskState state);
PACKURU_CORE_EXPORT bool isStateValid(TaskState state);

}

Q_DECLARE_METATYPE(Packuru::Core::TaskState);
