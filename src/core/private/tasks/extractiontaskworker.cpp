// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <shared_mutex>

#include <QDir>

#include "core/private/3rdparty/cs_libguarded/cs_shared_guarded.h"
#include "extractiontaskworker.h"
#include "extractiontaskdata.h"
#include "taskworkerprivate.h"
#include "core/private/plugin-api/abstractbackendhandler.h"
#include "taskstate.h"
#include "tempfilemover.h"
#include "extractionfileoverwritepromptcore.h"
#include "extractionfolderoverwritepromptcore.h"
#include "extractiontypemismatchpromptcore.h"
#include "core/private/plugin-api/abstractbackendhandler.h"
#include "taskerror.h"
#include "core/private/promptcore/extractionfileoverwritepromptcoreaux.h"
#include "core/private/promptcore/extractionfolderoverwritepromptcoreaux.h"
#include "core/private/promptcore/extractiontypemismatchpromptcoreaux.h"


namespace Packuru::Core
{

template <typename PromptCore, typename PromptCoreBackdoor>
void preparePromptCore(ExtractionTaskWorker* worker,
                       QObject* promptOwner,
                       PromptCore* promptCore,
                       const PromptCoreBackdoor& backdoor)
{
    using EmitterType = std::remove_pointer_t<decltype(PromptCoreBackdoor::emitter)>;

    QObject::connect(backdoor.emitter, &EmitterType::abortRequested,
                     worker, &TaskWorker::stop);

    QObject::connect(worker, &TaskWorker::stateChanged,
                     promptCore, &QObject::deleteLater);

    promptCore->moveToThread(promptOwner->thread());
    promptCore->setParent(promptOwner);
};


struct ExtractionTaskWorker::Private
{
    Private(ExtractionTaskWorker* publ_,
            const ExtractionTaskData& taskData_);

    TempFileMover* createTempFileMover(const ExtractionTaskData& data);

    void onTempFileMoverWaitingForInput(bool value);
    void onTempFileMoverSuccess();
    void onTempFileMoverErrors(const std::vector<QString>& failedItems);
    void onTempFileMoverFileExists(ExtractionFileOverwritePromptCore* promptCore,
                                   const ExtractionFileOverwritePromptCoreAux::Backdoor& backdoor);
    void onTempFileMoverFolderExists(ExtractionFolderOverwritePromptCore* promptCore,
                                     const ExtractionFolderOverwritePromptCoreAux::Backdoor& backdoor);
    void onTempFileMoverItemTypeMismatch(ExtractionTypeMismatchPromptCore* promptCore,
                                         const ExtractionTypeMismatchPromptCoreAux::Backdoor& backdoor);

    void removeTempFolder();
    bool renameTempDir(const ExtractionTaskData& data);
    void finishExtraction();

    ExtractionTaskWorker* publ;
    libguarded::shared_guarded<ExtractionTaskData> taskData;
};


ExtractionTaskWorker::ExtractionTaskWorker(QObject* owner,
                                           const ExtractionTaskData& taskData,
                                           std::unique_ptr<AbstractBackendHandler> backendHandler)
    : TaskWorker(owner,
                 taskData.backendData.archiveType != ArchiveType::___NOT_SUPPORTED,
                 std::move(backendHandler)),
      priv(new Private(this, taskData))
{
    auto data = priv->taskData.lock();
    // required by TaskWorkerPrivate::preparePathForWriting
    if (!data->destinationPath.endsWith('/'))
        data->destinationPath += QLatin1Char('/');
}


ExtractionTaskWorker::~ExtractionTaskWorker()
{
    priv->removeTempFolder();
}


ExtractionTaskData ExtractionTaskWorker::getData() const
{
    return *priv->taskData.lock_shared();
}


QString ExtractionTaskWorker::getArchiveName() const
{
    return priv->taskData.lock_shared()->backendData.archiveInfo.fileName();
}


QString ExtractionTaskWorker::getPassword() const
{
    return priv->taskData.lock_shared()->backendData.password;
}


bool ExtractionTaskWorker::preStartInit()
{
    const auto data = priv->taskData.lock_shared();
    return TaskWorker::priv->confirmArchiveExistence(data->backendData.archiveInfo.absoluteFilePath());
}


void ExtractionTaskWorker::startImpl()
{
    ExtractionTaskData dataCopy = getData();

    if (dataCopy.backendData.tempDestinationPath.isEmpty())
    {
        Q_ASSERT(!dataCopy.destinationPath.isEmpty());
        Q_ASSERT(!dataCopy.backendData.archiveInfo.absoluteFilePath().isEmpty());
        const QString archivePath = dataCopy.backendData.archiveInfo.absoluteFilePath();
        const uint hash = qHash(archivePath);
        const qint64 epochTime = QDateTime::currentSecsSinceEpoch();

        QString path = dataCopy.destinationPath
                + QLatin1String("!" PROJECT_BUILD_NAME "-temp-")
                + QString::number(hash)
                + QLatin1Char('-')
                + QString::number(epochTime)
                + QLatin1Char('/');

        dataCopy.backendData.tempDestinationPath = path;
        priv->taskData.lock()->backendData.tempDestinationPath = path;
    }

    if (!TaskWorker::priv->tryWriteToFolder(dataCopy.destinationPath)
            || !TaskWorker::priv->tryWriteToFolder(dataCopy.backendData.tempDestinationPath))
        return TaskWorker::priv->changeState(TaskState::Errors);

    TaskWorker::priv->backendHandler->extractArchive(dataCopy.backendData);
}


void ExtractionTaskWorker::onBackendFinished(TaskState concludedState)
{
    Q_ASSERT(isStateFinished(concludedState));

    if (concludedState == TaskState::Aborted)
        return TaskWorker::priv->changeState(concludedState);

    const ExtractionTaskData dataCopy = getData();

    const bool isCompleted = isStateCompleted(concludedState);

    // If error occurred continue only when extracted for preview
    if (!isCompleted && !dataCopy.backendData.extractionForPreview)
        return TaskWorker::priv->changeState(concludedState);

    const QDir destinationDir(dataCopy.destinationPath);
    QDir tempDir(dataCopy.backendData.tempDestinationPath);
    tempDir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::Hidden);

    /* NoFilesExtracted is only reported when there has been no other error that could cause extraction
       failure (invalid password or corrupted data).
       Also, QDir::count() is used instead of isEmpty() because the latter returns true if there
       are only file names starting with dot.
     */
    if (tempDir.count() == 0 && TaskWorker::priv->errors.empty())
    {
        TaskWorker::priv->errors.insert(TaskError::NoFilesExtracted);
        return TaskWorker::priv->changeState(TaskState::Errors);
    }

    if (isCompleted)
    {
        /* Progress might not reach 100% yet because of problems with processing backend output stream
           which may cause that some progress info is missed or it wasn't printed at all.
           When showing extraction override prompts it looks better when the progress is 100%.
           TaskPrivate would change the progress to 100% when task state changes to completed (not
           when backend state changed) but we don't inform the task about task worker completion yet. */
        emit progressChanged(100);
    }

    switch (dataCopy.topFolderMode)
    {

    case ExtractionTopFolderMode::CreateFolder:
    {
        if (priv->renameTempDir(dataCopy))
        {
            priv->finishExtraction();
        }
        else
        {
            TaskWorker::priv->errors.insert(TaskError::TemporaryFolderError);
            TaskWorker::priv->changeState(TaskState::Errors);
        }
        break;
    }

    case ExtractionTopFolderMode::DoNotCreateFolder:
    {
        auto tfm = priv->createTempFileMover(dataCopy);
        tfm->moveFolderContent(tempDir, destinationDir);
        break;
    }

    case ExtractionTopFolderMode::CreateIfAbsent:
    {
        // empty archive?
        if (tempDir.count() == 0)
        {
            priv->removeTempFolder();
            priv->finishExtraction();
        }
        else if (tempDir.count() == 1)
        {
            auto tfm = priv->createTempFileMover(dataCopy);
            tfm->moveFolderContent(tempDir, destinationDir);
        }
        else
        {
            if (priv->renameTempDir(dataCopy))
            {
                priv->finishExtraction();
            }
            else
            {
                TaskWorker::priv->errors.insert(TaskError::TemporaryFolderError);
                TaskWorker::priv->changeState(TaskState::Errors);
            }
        }
        break;
    }

    default:
        Q_ASSERT(false);
        return;
    }
}


void ExtractionTaskWorker::updatePassword(const QString& password)
{
    priv->taskData.lock()->backendData.password = password;
}


ExtractionTaskWorker::Private::Private(ExtractionTaskWorker* publ_,
                                       const ExtractionTaskData& taskData_)
    : publ(publ_),
      taskData(taskData_)
{

}


TempFileMover* ExtractionTaskWorker::Private::createTempFileMover(const ExtractionTaskData& data)
{
    auto tfm = new TempFileMover(data.fileOverwriteMode, data.folderOverwriteMode, publ);

    // task can be aborted while waiting for input
    connect(publ, &ExtractionTaskWorker::finished,
            tfm, &QObject::deleteLater);

    connect(tfm, &TempFileMover::waitingForInput,
            publ, [priv = this] (bool value)
    { priv->onTempFileMoverWaitingForInput(value); });

    connect(tfm, &TempFileMover::success,
            publ, [priv = this] ()
    { priv->onTempFileMoverSuccess(); });

    connect(tfm, &TempFileMover::errors,
            publ, [priv = this] (const std::vector<QString>& failedItems)
    { priv->onTempFileMoverErrors(failedItems); });

    connect(tfm, &TempFileMover::fileExists,
            publ, [priv = this]
            (ExtractionFileOverwritePromptCore* promptCore,
            const ExtractionFileOverwritePromptCoreAux::Backdoor& backdoorData)
    { priv->onTempFileMoverFileExists(promptCore, backdoorData); });

    connect(tfm, &TempFileMover::folderExists,
            publ, [priv = this]
            (ExtractionFolderOverwritePromptCore* promptCore,
            const ExtractionFolderOverwritePromptCoreAux::Backdoor& backdoorData)
    { priv->onTempFileMoverFolderExists(promptCore, backdoorData); });

    connect(tfm, &TempFileMover::itemTypeMismatch,
            publ, [priv = this]
            (ExtractionTypeMismatchPromptCore* promptCore,
            const ExtractionTypeMismatchPromptCoreAux::Backdoor& backdoorData)
    { priv->onTempFileMoverItemTypeMismatch(promptCore, backdoorData); });

    return tfm;
}


void ExtractionTaskWorker::Private::onTempFileMoverWaitingForInput(bool value)
{
    if (value)
        publ->TaskWorker::priv->changeState(TaskState::WaitingForDecision);
    else
        publ->TaskWorker::priv->changeState(TaskState::InProgress);
}


void ExtractionTaskWorker::Private::onTempFileMoverSuccess()
{
    removeTempFolder();
    finishExtraction();
}


void ExtractionTaskWorker::Private::onTempFileMoverErrors(const std::vector<QString>& failedItems)
{
    Q_UNUSED(failedItems)
    publ->TaskWorker::priv->errors.insert(TaskError::TemporaryFolderError);
    publ->TaskWorker::priv->changeState(TaskState::Errors);
}


void ExtractionTaskWorker::Private::onTempFileMoverFileExists(ExtractionFileOverwritePromptCore* promptCore,
                                                              const ExtractionFileOverwritePromptCoreAux::Backdoor& backdoor)
{
    preparePromptCore(publ, publ->TaskWorker::priv->promptOwner, promptCore, backdoor);
    emit publ->fileExists(promptCore);
}


void ExtractionTaskWorker::Private::onTempFileMoverFolderExists(ExtractionFolderOverwritePromptCore* promptCore,
                                                                const ExtractionFolderOverwritePromptCoreAux::Backdoor& backdoor)
{
    preparePromptCore(publ, publ->TaskWorker::priv->promptOwner, promptCore, backdoor);
    emit publ->folderExists(promptCore);
}


void ExtractionTaskWorker::Private::onTempFileMoverItemTypeMismatch(ExtractionTypeMismatchPromptCore* promptCore,
                                                                    const ExtractionTypeMismatchPromptCoreAux::Backdoor& backdoor)
{
    preparePromptCore(publ, publ->TaskWorker::priv->promptOwner, promptCore, backdoor);
    emit publ->itemTypeMismatch(promptCore);
}


void ExtractionTaskWorker::Private::removeTempFolder()
{
    QString tempFolder;

    {
        tempFolder = taskData.lock_shared()->backendData.tempDestinationPath;
    }

    if (!tempFolder.isEmpty())
    {
        QDir dir(tempFolder);
        if (dir.exists())
            dir.removeRecursively();
    }
}


bool ExtractionTaskWorker::Private::renameTempDir(const ExtractionTaskData& data)
{
    QDir destinationDir(data.destinationPath);
    const QDir tempDir(data.backendData.tempDestinationPath);

    const QString newDirName = data.backendData.archiveInfo.fileName() + QLatin1Char('_');
    int counter = 0;

    while (destinationDir.exists(newDirName + QString::number(counter)))
        ++counter;

    return destinationDir.rename(tempDir.dirName(),
                                 newDirName + QString::number(counter));
}


void ExtractionTaskWorker::Private::finishExtraction()
{
    const TaskState concludedState = publ->TaskWorker::priv->getConcludedState();

    publ->TaskWorker::priv->changeState(concludedState);

    auto data = taskData.lock_shared();
    if (data->openDestinationOnCompletion)
    {
        const QString folder = data->destinationPath;
        data.release();
        emit publ->openDestinationPath(folder);
    }
}

}
