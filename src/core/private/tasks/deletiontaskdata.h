// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "core/private/plugin-api/backenddeletiondata.h"
#include "core/symbol_export.h"


namespace Packuru::Core
{

struct DeletionTaskData
{
    bool runInQueue = false;
    BackendDeletionData backendData;
};

}


PACKURU_CORE_EXPORT QDataStream& operator<<(QDataStream& out, const Packuru::Core::DeletionTaskData& data);
PACKURU_CORE_EXPORT QDataStream& operator>>(QDataStream& in, Packuru::Core::DeletionTaskData& data);
