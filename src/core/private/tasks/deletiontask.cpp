// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "deletiontask.h"
#include "tasktype.h"
#include "deletiontaskworker.h"
#include "taskprivate.h"
#include "deletiontaskdata.h"
#include "core/private/plugin-api/abstractbackendhandler.h"


namespace Packuru::Core
{

DeletionTask::DeletionTask(const DeletionTaskData& taskData, const BackendHandlerFactory& backendFactory, QObject* parent)
    : Task(TaskType::Delete,
           std::make_unique<DeletionTaskWorker>(this, taskData, backendFactory(taskData.backendData.archiveType)),
           backendFactory,
           parent)
{

}


DeletionTask::~DeletionTask()
{

}


std::unique_ptr<TaskWorker> DeletionTask::createWorker()
{
    auto worker = qobject_cast<DeletionTaskWorker*>(Task::priv->worker.get());
    Q_ASSERT(worker);
    const DeletionTaskData data = worker->getData();
    auto newWorker = std::make_unique<DeletionTaskWorker>(this, data, Task::priv->backendFactory(data.backendData.archiveType));
    return newWorker;
}

}
