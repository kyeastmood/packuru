// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once


namespace Packuru::Core
{

enum class BackendArchiveProperty
{
    Comment, // QString
    DirCount, // int
    EncryptionState, // EncryptionState
    FileCount, // int
    Locked, // bool
    Method, // QString
    OriginalSize, // qulonglong
    PackedSize, // qulonglong
    Solid, // bool
    PartCount // int
};

}
