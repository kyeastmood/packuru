// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <queue>

#include <QObject>
#include <QFileInfoList>
#include <QDir>


namespace Packuru::Core
{

enum class ExtractionFileOverwriteMode;
enum class ExtractionFolderOverwriteMode;
class ExtractionFileOverwritePromptCore;
class ExtractionFolderOverwritePromptCore;
class ExtractionTypeMismatchPromptCore;

namespace ExtractionFileOverwritePromptCoreAux
{
struct Backdoor;
}

namespace ExtractionFolderOverwritePromptCoreAux
{
struct Backdoor;
}

namespace ExtractionTypeMismatchPromptCoreAux
{
struct Backdoor;
}


class TempFileMover : public QObject
{
    Q_OBJECT

public:
    explicit TempFileMover(ExtractionFileOverwriteMode fileOverwriteMode,
                           ExtractionFolderOverwriteMode folderOverwriteMode,
                           QObject *parent = nullptr);
    ~TempFileMover();

    void moveFolderContent(const QDir& source, const QDir& destination);

signals:
    void success();
    void errors(const std::vector<QString>& failedItems);
    void waitingForInput(bool);
    void fileExists(ExtractionFileOverwritePromptCore* promptCore,
                    const ExtractionFileOverwritePromptCoreAux::Backdoor& backdoorData);
    void folderExists(ExtractionFolderOverwritePromptCore* promptCore,
                      const ExtractionFolderOverwritePromptCoreAux::Backdoor& backdoorData);
    void itemTypeMismatch(ExtractionTypeMismatchPromptCore* promptCore,
                          const ExtractionTypeMismatchPromptCoreAux::Backdoor& backdoorData);

private:
    void processQueue();
    bool processFile();
    bool processFolder();
    void overwriteFile();
    void promptOverwriteFile(bool applyToAll);
    void promptWriteIntoFolder(bool applyToAll);
    void promptSkipFile(bool applyToAll);
    void promptSkipFolder(bool applyToAll);
    void promptSkipTypeMismatch(bool applyToAll);

    struct MoveFileTask
    {
        MoveFileTask(const QDir& source, const QDir& destination)
            : sourceDir(source),
              destinationDir(destination)
        {
            sourceDir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::Hidden);
            destinationDir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::Hidden);
        }

        QDir sourceDir;
        QDir destinationDir;
    };

    ExtractionFileOverwriteMode fileOverwriteMode;
    ExtractionFolderOverwriteMode folderOverwriteMode;

    std::queue<MoveFileTask> queue;
    bool running = false;
    bool skipItemTypeMismatch = false;
    QFileInfoList sourceItems;
    int currentItem = -1;
    QFileInfo destinationItem;
    std::vector<QString> failedItems;
};

}
