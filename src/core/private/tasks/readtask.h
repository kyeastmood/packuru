// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "task.h"
#include "backendhandlerfactory.h"
#include "core/private/plugin-api/backendarchiveproperties.h"


namespace Packuru::Core
{

class ReadTaskData;
class ArchiveContentData;


class ReadTask : public Task
{
    Q_OBJECT
public:
    ReadTask(const ReadTaskData& taskData, const BackendHandlerFactory& backendFactory, QObject* parent = nullptr);
    ~ReadTask() override;

    ReadTaskData getTaskData() const;
    ArchiveContentData getArchiveContentData() const;
    BackendArchiveProperties getArchiveProperties() const;

private:
    std::unique_ptr<TaskWorker> createWorker() override;
};

}
