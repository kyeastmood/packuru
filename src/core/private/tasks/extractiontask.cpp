// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "extractiontask.h"
#include "tasktype.h"
#include "extractiontaskworker.h"
#include "taskprivate.h"
#include "extractiontaskdata.h"
#include "core/private/plugin-api/abstractbackendhandler.h"
#include "extractionfileoverwritepromptcore.h"
#include "extractionfolderoverwritepromptcore.h"
#include "extractiontypemismatchpromptcore.h"


namespace Packuru::Core
{

struct ExtractionTask::Private
{
    void connectToSignals(ExtractionTask* task, ExtractionTaskWorker* worker) const;
};


ExtractionTask::ExtractionTask(const ExtractionTaskData& taskData, const BackendHandlerFactory& backendFactory, QObject* parent)
    : Task(taskData.backendData.extractionForPreview ? TaskType::Preview : TaskType::Extract,
           std::make_unique<ExtractionTaskWorker>(this, taskData, backendFactory(taskData.backendData.archiveType)),
           backendFactory,
           parent),
      priv(new Private)
{
    auto worker = qobject_cast<ExtractionTaskWorker*>(Task::priv->worker.get());
    Q_ASSERT(worker);
    priv->connectToSignals(this, worker);
}


ExtractionTask::~ExtractionTask()
{

}


ExtractionTaskData ExtractionTask::getData() const
{
    auto worker = qobject_cast<ExtractionTaskWorker*>(Task::priv->worker.get());
    return worker->getData();
}


std::unique_ptr<TaskWorker> ExtractionTask::createWorker()
{
    auto worker = qobject_cast<ExtractionTaskWorker*>(Task::priv->worker.get());
    Q_ASSERT(worker);
    const ExtractionTaskData data = worker->getData();
    auto newWorker = std::make_unique<ExtractionTaskWorker>(this, data, Task::priv->backendFactory(data.backendData.archiveType));
    priv->connectToSignals(this, newWorker.get());

    return newWorker;
}


void ExtractionTask::Private::connectToSignals(ExtractionTask* task, ExtractionTaskWorker* worker) const
{
    QObject::connect(worker, &ExtractionTaskWorker::fileExists,
                     task, &ExtractionTask::fileExists);

    QObject::connect(worker, &ExtractionTaskWorker::folderExists,
                     task, &ExtractionTask::folderExists);

    QObject::connect(worker, &ExtractionTaskWorker::itemTypeMismatch,
                     task, &ExtractionTask::itemTypeMismatch);

    QObject::connect(worker, &ExtractionTaskWorker::openDestinationPath,
                     task, &ExtractionTask::openDestinationPath);
}

}
