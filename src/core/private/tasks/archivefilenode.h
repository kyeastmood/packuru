// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "core/private/plugin-api/archiveitem.h"
#include "archivenodeitem.h"


class QString;


namespace Packuru::Core
{

class ArchiveFileNode
{
    friend class ArchiveFileTreeBuilder;
public:
    virtual ~ArchiveFileNode();

    const ArchiveNodeItem& getItem() const { return _item; }
    ArchiveNodeItem& getItem() { return _item; }
    bool isDir() const { return _item.nodeType == NodeType::Folder; }
    ArchiveFileNode* getParent() const { return parent; }

    void setRow(int row) { this->row = row; }
    int getRow() const { return row; }

protected:
    ArchiveFileNode(ArchiveFileNode* parent, int row, ArchiveItem& item);
     ArchiveFileNode(ArchiveFileNode* parent, int row, const QString& name);

    ArchiveFileNode* createFileNode(ArchiveFileNode* parent, int row, ArchiveItem& item);

    void setItem(ArchiveItem& item);

    void computeCompressionRatio(CompressionRatioMode mode);

    ArchiveFileNode* parent;
    int row;
    ArchiveNodeItem _item;
};

}
