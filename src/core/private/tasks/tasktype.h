// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QHash>


namespace Packuru::Core
{

enum class TaskType
{
    Add,
    Create,
    Extract,
    Preview,
    Read,
    Delete,
    Test,

    ___INVALID
};

}
