// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QHash>
#include <QString>
#include <QCoreApplication>

#include "fileextractionmode.h"


namespace Packuru::Core
{

QString toString(FileExtractionMode value)
{
    switch (value)
    {
    case FileExtractionMode::AllFiles:
        return QCoreApplication::translate("FileExtractionMode", "All");
    case FileExtractionMode::SelectedFilesOnly:
        return QCoreApplication::translate("FileExtractionMode", "Selected only");
    default:
        Q_ASSERT(false); return QString();
    };
}

}
