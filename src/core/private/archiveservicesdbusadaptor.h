// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QDBusAbstractAdaptor>


namespace Packuru::Core
{

class ArchiveServicesDBusAdaptor : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org." PROJECT_BUILD_NAME ".archiveservices")
public:
    ArchiveServicesDBusAdaptor(QObject *parent);

Q_SIGNALS:
    void pluginConfigChangedBroadcast(qint64 processId);
};

}
