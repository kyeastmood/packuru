// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>


namespace Packuru::Core
{

struct ArchiveTypeModelAccessor
{
    class ArchiveTypeModel* model = nullptr;
    std::function<void()> saveModelChanges;
};

}
