// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QString>
#include <QCoreApplication>

#include "extractiontopfoldermode.h"


namespace Packuru::Core
{

QString toString(ExtractionTopFolderMode value)
{
    switch(value)
    {
    case ExtractionTopFolderMode::CreateFolder:
        return QCoreApplication::translate("ExtractionTopFolderMode", "Create top folder");
    case ExtractionTopFolderMode::CreateIfAbsent:
        return QCoreApplication::translate("ExtractionTopFolderMode", "Create if absent in the archive");
    case ExtractionTopFolderMode::DoNotCreateFolder:
        return QCoreApplication::translate("ExtractionTopFolderMode", "Do not create top folder");
    default:
        Q_ASSERT(false); return QString();
    };
}

}
