// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QFileInfo>
#include <QCommandLineParser>

#include "core/symbol_export.h"


namespace Packuru::Core
{

class PACKURU_CORE_EXPORT CommandLineHandlerBase
{
    Q_DECLARE_TR_FUNCTIONS(CommandLineHandlerBase)
public:
    explicit CommandLineHandlerBase();
    virtual ~CommandLineHandlerBase();

protected:
    std::vector<QFileInfo> createUrlList() const;

    QCommandLineParser parser;
    const QString appDir;
};

}
