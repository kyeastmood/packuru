// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QLatin1String>
#include <QMetaType>

#include "../globalsettingsuservalue.h"
#include "../symbol_export.h"


namespace Packuru::Core
{

namespace GlobalSettings
{

Q_NAMESPACE

enum class Enum
{
    ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

    AC_CompressionRatioMode,
    AC_SizeUnits,

    BC_ForceSingleInstance, // bool
    BC_ShowSummaryOnTaskCompletion, // bool

    QC_AbortTasksRequiringPassword, // bool
    QC_AutoCloseOnAllTasksCompletion, // bool
    QC_AutoRemoveCompletedTasks, // bool
    QC_MaxErrorCountToStop, // int

    // Do not exceed this value, custom settings can use it as the first value
    ___END = globalSettingsUserValue
};

Q_ENUM_NS(Enum)

}

PACKURU_CORE_EXPORT int toInt(GlobalSettings::Enum key);

}
