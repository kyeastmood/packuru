// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "globalsettingsconverterlist.h"


namespace Packuru::Core::GlobalSettingsManagerAux
{

struct Init
{
    GlobalSettingsConverterList converters;
};

}
