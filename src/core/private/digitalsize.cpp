// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <cmath>

#include "digitalsize.h"
#include "sizeunits.h"


namespace Packuru::Core
{

namespace {

// Binary
static const qulonglong kibibyte = 1024;
static const qulonglong mebibyte = static_cast<qulonglong>(std::pow(1024, 2));
static const qulonglong gibibyte = static_cast<qulonglong>(std::pow(1024, 3));
static const qulonglong tebibyte = static_cast<qulonglong>(std::pow(1024, 4));
static const qulonglong pebibyte = static_cast<qulonglong>(std::pow(1024, 5));
static const qulonglong exbibyte = static_cast<qulonglong>(std::pow(1024, 6));

// Decimal
static const qulonglong kilobyte = 1000;
static const qulonglong megabyte = static_cast<qulonglong>(std::pow(kilobyte, 2));
static const qulonglong gigabyte = static_cast<qulonglong>(std::pow(1000, 3));
static const qulonglong terabyte = static_cast<qulonglong>(std::pow(1000, 4));
static const qulonglong petabyte = static_cast<qulonglong>(std::pow(1000, 5));
static const qulonglong exabyte = static_cast<qulonglong>(std::pow(1000, 6));
}


QString DigitalSize::toString(qulonglong byteCount, SizeUnits units)
{
    switch (units)
    {
    case SizeUnits::Binary:
        if (byteCount < kibibyte)
            return QString::number(byteCount) + " B";
        else if (byteCount < mebibyte)
            return QString::number(static_cast<double>(byteCount) / kibibyte, 'f', 1) + " KiB";
        else if (byteCount < gibibyte)
            return QString::number(static_cast<double>(byteCount) / mebibyte, 'f', 1) + " MiB";
        else if (byteCount < tebibyte)
            return QString::number(static_cast<double>(byteCount) / gibibyte, 'f', 1) + " GiB";
        else if (byteCount < pebibyte)
            return QString::number(static_cast<double>(byteCount) / tebibyte, 'f', 1) + " TiB";
        else if (byteCount < exbibyte)
            return QString::number(static_cast<double>(byteCount) / pebibyte, 'f', 1) + " PiB";
        else
            return QString::number(byteCount) + " B";

    case SizeUnits::Decimal:
        if (byteCount < kilobyte)
            return QString::number(byteCount) + " B";
        else if (byteCount < megabyte)
            return QString::number(static_cast<double>(byteCount) / kilobyte, 'f', 1) + " KB";
        else if (byteCount < gigabyte)
            return QString::number(static_cast<double>(byteCount) / megabyte, 'f', 1) + " MB";
        else if (byteCount < tebibyte)
            return QString::number(static_cast<double>(byteCount) / gigabyte, 'f', 1) + " GB";
        else if (byteCount < pebibyte)
            return QString::number(static_cast<double>(byteCount) / terabyte, 'f', 1) + " TB";
        else if (byteCount < exbibyte)
            return QString::number(static_cast<double>(byteCount) / petabyte, 'f', 1) + " PB";
        else
            return QString::number(byteCount) + " B";

    default:
        Q_ASSERT(false); return QString();
    }
}


bool DigitalSize::exceedsByteTreshold(qulonglong byteCount, SizeUnits units)
{
    switch (units)
    {
    case SizeUnits::Binary:
        return byteCount >= kibibyte;
    case SizeUnits::Decimal:
        return byteCount >= kilobyte;
    default:
        Q_ASSERT(false); return false;
    }
}

}
