// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <type_traits>


namespace Packuru::Core
{

/* Stores integrals and enums. Initial value is set as template parameter.
 * When moved from, the value is reset to initial value. */
template <typename T, T initialValue = std::enable_if_t<std::is_integral<T>::value, T>()>
struct Integral
{
    using ThisType = Integral<T, initialValue>;

    Integral(const Integral& other) noexcept
        : value(other.value) {}

    Integral(Integral&& other) noexcept
        : value(other.value) { other.reset(); }

    Integral(const T& value = initialValue) noexcept
        : value(value) {}

    ThisType& operator=(const ThisType& other) noexcept
    {
        if (&other != this)
            value = other.value;
        return *this;
    }

    ThisType& operator=(ThisType&& other) noexcept
    {
        if (&other != this)
        {
            value = other.value;
            other.reset();
        }
        return *this;
    }

    ThisType& operator=(const T& v) noexcept
    {
        value = v;
        return *this;
    }

    operator T() const noexcept { return value; }

    void reset() { value = initialValue; }

    std::enable_if_t<std::is_integral<T>::value || std::is_enum<T>::value, T> value;
};

}
