// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QStringList>

#include "utils/private/qttypehasher.h"


namespace Packuru::Core::ArchiveTypeModelAux
{

class Entry
{
public:
    QString archiveType; // From ArchiveType

    struct {
        QString currentPlugin;
        QStringList availablePlugins;
        bool changed = false;
    } read;

    struct {
        QString currentPlugin;
        QStringList availablePlugins;
        bool changed = false;
    } write;
};


struct ChangedValues
{
    QString newReadPlugin;
    QString newWritePlugin;
};


using ChangedValuesMap = std::unordered_map<QString, ChangedValues, Packuru::Utils::QtTypeHasher<QString>>;


struct InitData
{
    std::vector<Entry> entries;
    std::function<void(const ChangedValuesMap& changes)> onValuesChanged;
};


struct BackdoorData
{
    std::function<void()> saveChanges;
};

}
