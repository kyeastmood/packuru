// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <unordered_set>

#include <QObject>

#include "core/symbol_export.h"
#include "plugin-api/backendarchiveproperties.h"


class QFileInfo;


namespace Packuru::Core
{

class ArchivingTask;
class ArchivingTaskData;
class ExtractionTaskData;
class ExtractionTask;
class ReadTask;
class DeletionTask;
class DeletionTaskData;
class ReadTaskData;
enum class TaskType;
class TestTask;
class TestTaskData;
class ArchivingParameterHandlerFactory;
enum class ArchiveType;
struct PluginData;
class ArchiveTypeModel;
class ArchiveTypeModelAccessor;


class PACKURU_CORE_EXPORT ArchiveServices : public QObject
{
    Q_OBJECT
public:
    ArchiveServices(const std::vector<PluginData>& pluginData, QObject* parent = nullptr);
    ~ArchiveServices();

    std::vector<std::unique_ptr<ArchivingParameterHandlerFactory>>
    createArchivingParameterHandlerFactoryList() const;

    std::unique_ptr<ArchivingParameterHandlerFactory>
    createArchivingParameterHandlerFactory(ArchiveType archiveType) const;

    QString getExtractionBackendNameForType(ArchiveType archiveType) const;

    std::unordered_set<TaskType>
    getSupportedWriteTasksForArchive(ArchiveType archiveType, const BackendArchiveProperties& properties) const;

    ArchivingTask* createTask(const ArchivingTaskData& data);
    ExtractionTask* createTask(const ExtractionTaskData& data);
    ReadTask* createTask(const ReadTaskData& data);
    DeletionTask* createTask(const DeletionTaskData& data);
    TestTask* createTask(const TestTaskData& data);

    ArchiveTypeModelAccessor getArchiveTypeModelAccessor();

signals:
    void pluginConfigChanged();
    // Notifies other instances via D-Bus
    void pluginConfigChangedBroadcast(qint64 originProcessId);

private slots:
    void onSomeProcessPluginConfigChanged(qint64 originProcessId);

private:
    class ArchiveServicesPrivate* priv;
};

}
