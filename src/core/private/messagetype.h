// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once


namespace Packuru::Core
{

enum class MessageType
{
    CommandLineCreateArchives,
    CommandLineDefaultStartUp,
    CommandLineError,
    CommandLineExtractArchives,
    CommandLineHelp,
    CommandLineReadArchives,
    CommandLineTestArchives,

    ArchivingTaskData,
    DeletionTaskData,
    ExtractionTaskData,
    TestTaskData
};

}
