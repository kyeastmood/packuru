// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QString>

#include "nodetype.h"


namespace Packuru::Core
{

namespace
{
    const QString file("File");
    const QString folder("Folder");
    const QString link("Link");
}


QString toString(NodeType value)
{
    switch (value)
    {
    case NodeType::File:
        return file;
    case NodeType::Folder:
        return folder;
    case NodeType::Link:
        return link;
    default:
        Q_ASSERT(false); return QString();
    }
}

}
