// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>

#include "backendhandlerstate.h"


namespace Packuru::Core
{

bool isStateBusy(BackendHandlerState state)
{
    return (state > BackendHandlerState::___BUSY_STATES_BEGIN
            && state < BackendHandlerState::___BUSY_STATES_END);
}


bool isStateFinished(BackendHandlerState state)
{
    return (state > BackendHandlerState::___FINISHED_STATES_BEGIN
            && state < BackendHandlerState::___FINISHED_STATES_END);
}


bool isStateCompleted(BackendHandlerState state)
{
    return state == BackendHandlerState::Success
            || state == BackendHandlerState::Warnings;
}


bool isStateValid(BackendHandlerState state)
{
    return state == BackendHandlerState::ReadyToRun
            || isStateBusy(state)
            || isStateFinished(state);
}


char* toString(BackendHandlerState state)
{
    switch (state)
    {
    case BackendHandlerState::ReadyToRun: return qstrdup("ReadyToRun");
    case BackendHandlerState::InProgress: return qstrdup("InProgress");
    case BackendHandlerState::WaitingForPassword: return qstrdup("WaitingForPassword");
    case BackendHandlerState::Paused: return qstrdup("Paused");
    case BackendHandlerState::Success: return qstrdup("Success");
    case BackendHandlerState::Warnings: return qstrdup("Warnings");
    case BackendHandlerState::Errors: return qstrdup("Errors");
    case BackendHandlerState::Aborted: return qstrdup("Aborted");
    case BackendHandlerState::___INVALID: return qstrdup("___INVALID");
    default: Q_ASSERT(false); return qstrdup("");
    }
}

}
