// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QtGlobal>

#include "core/symbol_export.h"


namespace Packuru::Core
{

enum class ArchiveType;

/* Supported: 7z, lzip, rar, tar/lzip, zip
   Some names can be both single and multipart e.g. archive.rar can be a first part of old
   naming pattern where the second part is archive.r00. */
PACKURU_CORE_EXPORT bool isMultiPartArchiveName(const QString& filePath, ArchiveType type);

// The list may contain orphaned parts from other archives (unlikely but possible)
PACKURU_CORE_EXPORT std::vector<QString> getArchiveExistingPartPaths(const QString& filePath, ArchiveType type);

// Count may include orphaned parts from other archives (unlikely but possible)
PACKURU_CORE_EXPORT int getArchiveExistingPartCount(const QString& filePath, ArchiveType type);

// If partCount equals 0 all subsequent parts that exists on disk will be used,
// even if they are orphaned parts from other archives (unlikely but possible)
PACKURU_CORE_EXPORT quint64 computeArchiveSize(const QString& filePath, ArchiveType type, int partCount = 0);

PACKURU_CORE_EXPORT QString getFirstRarPart(const QString& filePath);

}
