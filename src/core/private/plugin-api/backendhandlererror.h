// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>
#include <QString>

#include "core/symbol_export.h"


namespace Packuru::Core
{

enum class BackendHandlerError
{
    /* In general TaskWorkers check files and folders for read/write problems
       and they report TaskError::AccessDenied before BackendHandlers are started.
       However it still might be safer and useful to detect when a backend reports
       AccessDenied. */
    AccessDenied,
    ArchivePartNotFound,
    ArchivingMethodNotSupported,
    Crash,
    // When archive contains a link could overwrite existing data (only 7-Zip)
    DangerousLink,
    // Includes: unrecognized data format, corrupted data, truncated archive
    DataError,
    DiskFull,
    // When backend executable could not be found; See also SubmoduleNotFound error
    ExecutableNotFound,
    // When adding files to archive and some of them could not be found (removed?)
    FileNotFound,
    InvalidCommandLine,
    /* When an archive contains file and folder with the same path, it can cause an
       error during extraction on Unix systems, unless backend supports auto-renaming
       existing or extracted items, even when they have different type. */
    /* For encrypted archives backends often print ambiguous error messages and it is
       difficult to distinguish corrupted data from invalid password. It is likely
       because it is not really possible to tell the difference from examining the archive.
       Recommendations:
       1. Clear messages like "CRC/checksum error in encrypted file" can be
          reported as DataError
       2. Cryptic messages like "Data error in encrypted file":
          a. For fully encypted archives report DataError as it is unlikely
             that the file is encrypted with different password then the master password
             (It is then important to know before e.g. testing archive if it is fully
             encrypted, which may require custom checks)
          b. For archives with content only encryption it is better to report
             InvalidPasswordOrDataError as it will allow user to provide a new password */
    InvalidPasswordOrDataError,
    ItemTypeMismatch,
    NotEnoughMemory,
    // When backend requires additional modules (libraries or executables) to perform a task
    SubmoduleNotFound,
    // When handling temporary archive during write operations
    TemporaryArchiveError,
    // Temporary destination folder must be empty before extraction is performed, mostly because
    // backend handlers might want to clean it on failure so there is a danger that user data
    // could be deleted.
    TemporaryFolderNotEmpty,
    TemporaryFolderNotFound,
    WriteError,

    ___INVALID
};

PACKURU_CORE_EXPORT QLatin1String toLatin1(BackendHandlerError value);

PACKURU_CORE_EXPORT char* toString(BackendHandlerError value);

}

Q_DECLARE_METATYPE(Packuru::Core::BackendHandlerError)
