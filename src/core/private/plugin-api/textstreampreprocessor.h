// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QObject>

#include "core/symbol_export.h"


namespace Packuru::Core
{

class PACKURU_CORE_EXPORT TextStreamPreprocessor : public QObject
{
    Q_OBJECT
public:
    TextStreamPreprocessor(QObject* parent = nullptr);

    void processBytes(const QByteArray& bytes);
    void clear();

signals:
    void fullLinesAvailable(const std::vector<QStringRef>& lines, int firstLine);
    void unfinishedLine(QString& line, int newDataPosition);

private:
    QString buffer;
    std::vector<QStringRef> fullLines;
};

}
