// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QHash>
#include <QString>
#include <QMetaType>
#include "core/symbol_export.h"


namespace Packuru::Core
{

enum class BackendHandlerState
{
    ReadyToRun,

    ___BUSY_STATES_BEGIN,
    InProgress,
    WaitingForPassword,
    Paused,
    ___BUSY_STATES_END,

    ___FINISHED_STATES_BEGIN,
    // No warnings and no errors must be present in Success state
    Success,
    // No errors must be present in Warnings state
    Warnings,
    // All read/write problems (inluding corrupted archive's header or missing files on disk)
    // must be reported as errors even if backends report them only as warnings.
    Errors,
    Aborted,
    ___FINISHED_STATES_END,

    ___INVALID
};

PACKURU_CORE_EXPORT bool isStateBusy(BackendHandlerState state);
PACKURU_CORE_EXPORT bool isStateFinished(BackendHandlerState state);
PACKURU_CORE_EXPORT bool isStateCompleted(BackendHandlerState state);
PACKURU_CORE_EXPORT bool isStateValid(BackendHandlerState state);

PACKURU_CORE_EXPORT char* toString(BackendHandlerState state);

}

Q_DECLARE_METATYPE(Packuru::Core::BackendHandlerState)
