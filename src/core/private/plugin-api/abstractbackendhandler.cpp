// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDir>

#include "abstractbackendhandler.h"
#include "backendextractiondata.h"
#include "backendarchivingdata.h"
#include "backenddeletiondata.h"
#include "diskspacewatcher.h"
#include "core/private/digitalsize.h"


namespace Packuru::Core
{

struct AbstractBackendHandler::Private
{

    bool startUpEnsureDiskNotFull();

    AbstractBackendHandler* publ = nullptr;
    BackendHandlerTaskType taskType;
    BackendHandlerState state = BackendHandlerState::ReadyToRun;
    int progress = -1;
    std::unordered_set<BackendHandlerWarning> warnings;
    std::unordered_set<BackendHandlerError> errors;
    QString log;
    bool stopRequest = false;

};


AbstractBackendHandler::AbstractBackendHandler(QObject *parent) :
    QObject(parent),
    diskWatcher(new DiskSpaceWatcher(this)),
    priv(new Private)
{
    priv->publ = this;

    connect(diskWatcher, &DiskSpaceWatcher::diskFull,
            this, [handler = this] () { handler->addError(BackendHandlerError::DiskFull); });

    connect(diskWatcher, &DiskSpaceWatcher::lowDiskSpace,
            this, [handler = this] ()
    {
        const QString treshold = DigitalSize::toString(handler->diskWatcher->lowDiskSpaceTreshold(),
                                                       SizeUnits::Decimal);
        handler->addWarning(BackendHandlerWarning::LowDiskSpace,
                            "Warning: There is less than " + treshold + " free space");
    });

    connect(this, &AbstractBackendHandler::stateChanged,
            this, [publ = this] (auto state)
    {
       if (isStateFinished(state))
           publ->diskWatcher->stop();
    });
}


AbstractBackendHandler::~AbstractBackendHandler()
{

}


void AbstractBackendHandler::readArchive(const BackendReadData& data)
{
    priv->taskType = BackendHandlerTaskType::Read;
    readArchiveImpl(data);
}


void AbstractBackendHandler::extractArchive(const BackendExtractionData& data)
{
    Q_ASSERT(!data.tempDestinationPath.isEmpty());

    priv->taskType = BackendHandlerTaskType::Extract;

    diskWatcher->setPath(data.tempDestinationPath);

    if (!priv->startUpEnsureDiskNotFull())
        return;

    QDir d(data.tempDestinationPath);
    d.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::Hidden);

    // These tests could be performed by ExtractionTaskWorker, however plugins are unit tested
    // in isolation without task workers and they should make sure themselves that they do not
    // overwrite user data (plugins are allowed to clean up their temporary destination folder).
    // On the other hand TaskWorker checks whether it's possible to create and write into
    // destination folder and if not, what is the cause (access denied, disk full).
    if (!d.exists())
    {
        addError(BackendHandlerError::TemporaryFolderNotFound,
                 Packuru::Core::AbstractBackendHandler::tr("Temporary folder not found: ") + d.path());
        return changeState(BackendHandlerState::Errors);
    }

    if (!d.isEmpty())
    {
        addError(BackendHandlerError::TemporaryFolderNotEmpty,
                 Packuru::Core::AbstractBackendHandler::tr("Temporary folder is not empty: ") + d.path());
        return changeState(BackendHandlerState::Errors);
    }

    diskWatcher->start();
    extractArchiveImpl(data);
}


void AbstractBackendHandler::addToArchive(const BackendArchivingData& data)
{
    priv->taskType = BackendHandlerTaskType::Add;
    diskWatcher->setPath(data.destinationPath);

    if (!priv->startUpEnsureDiskNotFull())
        return;

    diskWatcher->start();
    addToArchiveImpl(data);
}


void AbstractBackendHandler::deleteFiles(const BackendDeletionData& data)
{
    priv->taskType = BackendHandlerTaskType::Delete;
    diskWatcher->setPath(data.archiveInfo.absolutePath());

    if (!priv->startUpEnsureDiskNotFull())
        return;

    diskWatcher->start();
    deleteFilesImpl(data);
}


void AbstractBackendHandler::testArchive(const BackendTestData& data)
{
    priv->taskType = BackendHandlerTaskType::Test;
    testArchiveImpl(data);
}


void AbstractBackendHandler::stop()
{
    if (priv->stopRequest)
        return;

    priv->stopRequest = true;

    if (isBusy())
        stopImpl();
}


bool AbstractBackendHandler::stopRequested() const
{
    return priv->stopRequest;
}


BackendHandlerTaskType AbstractBackendHandler::getTaskType() const
{
    return priv->taskType;
}


BackendHandlerState AbstractBackendHandler::getState() const
{
    return priv->state;
}


bool AbstractBackendHandler::isReady() const
{
    return priv->state == BackendHandlerState::ReadyToRun;
}


bool AbstractBackendHandler::isBusy() const
{
    return (priv->state > BackendHandlerState::___BUSY_STATES_BEGIN
            && priv->state < BackendHandlerState::___BUSY_STATES_END);
}


bool AbstractBackendHandler::isFinished() const
{
    return (priv->state > BackendHandlerState::___FINISHED_STATES_BEGIN
            && priv->state < BackendHandlerState::___FINISHED_STATES_END);
}


bool AbstractBackendHandler::isCompleted() const
{
    return (priv->state == BackendHandlerState::Success
            || priv->state == BackendHandlerState::Warnings);
}


std::unordered_set<BackendHandlerError> AbstractBackendHandler::getErrors() const
{
    return priv->errors;
}


bool AbstractBackendHandler::hasErrors() const
{
    return !priv->errors.empty();
}


bool AbstractBackendHandler::hasError(BackendHandlerError error) const
{
    return priv->errors.find(error) != priv->errors.end();
}


std::unordered_set<BackendHandlerWarning> AbstractBackendHandler::getWarnings() const
{
    return priv->warnings;
}


bool AbstractBackendHandler::hasWarnings() const
{
    return !priv->warnings.empty();
}


bool AbstractBackendHandler::hasWarning(BackendHandlerWarning warning) const
{
    return priv->warnings.find(warning) != priv->warnings.end();
}


QString AbstractBackendHandler::clearLog()
{
    const auto result = priv->log;
    priv->log.clear();
    return result;
}


void AbstractBackendHandler::changeState(BackendHandlerState newState)
{
    if (newState != priv->state)
    {
        priv->state = newState;
        emit stateChanged(newState);
    }
}


void AbstractBackendHandler::changeProgress(int value)
{
    if (value != priv->progress)
    {
        priv->progress = value;
        emit progressChanged(value);
    }
}


void AbstractBackendHandler::addError(BackendHandlerError e)
{
    if (e != BackendHandlerError::___INVALID && !priv->errors.count(e))
    {
        priv->errors.insert(e);
        onNewError(e);
    }
}


void AbstractBackendHandler::addError(BackendHandlerError e, const QString& line)
{
    addError(e, line.midRef(0));
}


void AbstractBackendHandler::addError(BackendHandlerError e, QStringRef line)
{
    addError(e);
    addLogLine(line);
}


void AbstractBackendHandler::addWarning(BackendHandlerWarning w, const QString& line)
{
    addWarning(w, line.midRef(0));
}


void AbstractBackendHandler::addWarning(BackendHandlerWarning w, QStringRef line)
{
    priv->warnings.insert(w);
    addLogLine(line);
}


void AbstractBackendHandler::removeWarning(BackendHandlerWarning w)
{
    priv->warnings.erase(w);
}


void AbstractBackendHandler::addLogLine(const QString& line)
{
    addLogLine(QStringRef(&line));
}


void AbstractBackendHandler::addLogLine(QStringRef line)
{
    if (!line.isEmpty())
    {
        priv->log.append(line);
        priv->log.append('\n');
    }
}


void AbstractBackendHandler::addLogLines(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
        addLogLine(line);
}


QString AbstractBackendHandler::createSinglePartArchiveName(const QString& archiveBaseName, ArchiveType archiveType) const
{
    Q_UNUSED(archiveBaseName)
    Q_UNUSED(archiveType)
    return QString();
}


QString AbstractBackendHandler::createMultiPartArchiveName(const QString& archiveBaseName, ArchiveType archiveType) const
{
    Q_UNUSED(archiveBaseName)
    Q_UNUSED(archiveType)
    return QString();
}


QString AbstractBackendHandler::createMultiPartArchiveRegexNamePattern(const QString& archiveBaseName, ArchiveType archiveType) const
{
    Q_UNUSED(archiveBaseName)
    Q_UNUSED(archiveType)
    return QString();
}

bool AbstractBackendHandler::Private::startUpEnsureDiskNotFull()
{
    if (publ->diskWatcher->diskFullDetected())
    {
        publ->addError(BackendHandlerError::DiskFull, "Disk full, refusing to start backend");
        publ->changeState(BackendHandlerState::Errors);
        return false;
    }

    return true;
}

}
