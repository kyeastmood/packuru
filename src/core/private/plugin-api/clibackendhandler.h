// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include "abstractbackendhandler.h"


class QProcess;


namespace Packuru::Core
{

class TextStreamPreprocessor;
enum class BackendExitCodeInterpretation;


class CLIBackendHandler : public AbstractBackendHandler
{
    Q_OBJECT
public:
    void enterPassword(const QString&password) override;

    // Returns arguments (including password) from each invocation (backends may run multiple steps)
    const std::vector<QStringList>& getBackendArgs() const;

protected:
    CLIBackendHandler(QObject *parent = nullptr);
    ~CLIBackendHandler() override;

    void stopImpl() override;
    void onNewError(BackendHandlerError error) override;

    // Returns concluded recommended state, may report additional errors like Crash
    BackendHandlerState handleFinishedProcess(BackendExitCodeInterpretation exitCodeInter);

    // Logs the command for the user; args must not contain password
    void logCommand(const QString& executableName, const QStringList& args);

    QProcess* backendProcess;
    TextStreamPreprocessor* outPreprocessor;
    TextStreamPreprocessor* errPreprocessor;

    bool passwordReceived_ = false;

private:

    struct Private;
    std::unique_ptr<Private> priv;
};

}
