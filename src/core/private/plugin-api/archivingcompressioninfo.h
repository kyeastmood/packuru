// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QtGlobal>


namespace Packuru::Core
{

struct ArchivingCompressionInfo
{
    ArchivingCompressionInfo(int min = 0, int max = 0, int current = 0)
        :min(min), max(max), current(current)
    {
        Q_ASSERT(min >= 0 and max >= min and current >= min and current <= max);
    }

    ArchivingCompressionInfo(const ArchivingCompressionInfo& other)
        : min(other.min), max(other.max), current(other.current) {}

    ArchivingCompressionInfo& operator=(const ArchivingCompressionInfo& other)
    {
        if (&other != this)
        {
            const_cast<int&>(min) = other.min;
            const_cast<int&>(max) = other.max;
            current = other.current;
        }

        return *this;
    }

    void setCurrent(int level)
    {
        Q_ASSERT(level >= min and level <= max);
        current = level;
    }

    int getCurrent() const { return current; }

    const int min;
    const int max;

private:
    int current;
};

}
