// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QTimer>
#include <QStorageInfo>
#include <QFileInfo>

#include "diskspacewatcher.h"


namespace Packuru::Core
{

struct DiskSpaceWatcher::Private
{
    void refresh();

    DiskSpaceWatcher* publ = nullptr;
    QStorageInfo storageInfo;
    bool diskFull = false;
    bool lowDiskSpace = false;
    QTimer* timer = nullptr;
    static const quint64 lowSpaceThreshold = 100'000'000;
};


DiskSpaceWatcher::DiskSpaceWatcher(QObject *parent)
    : QObject(parent),
      priv(new Private)
{
    priv->publ = this;
    priv->timer = new QTimer(this);
    priv->timer->setInterval(1000);
    priv->timer->setSingleShot(false);

    connect(priv->timer, &QTimer::timeout,
            this, [publ = this] () { publ->refresh(); });
}


DiskSpaceWatcher::DiskSpaceWatcher(const QString& path, QObject *parent)
    : DiskSpaceWatcher(parent)
{
    setPath(path);
}


DiskSpaceWatcher::~DiskSpaceWatcher()
{

}


void DiskSpaceWatcher::setPath(const QString& path)
{
    priv->storageInfo.setPath(path);
    priv->diskFull = false;
    priv->lowDiskSpace = false;
    refresh();
}


void DiskSpaceWatcher::refresh()
{
    priv->refresh();
}


void DiskSpaceWatcher::start()
{
    if (!priv->diskFull)
        priv->timer->start();
}


void DiskSpaceWatcher::stop()
{
    priv->timer->stop();
}


bool DiskSpaceWatcher::diskFullDetected() const
{
    return priv->diskFull;
}


bool DiskSpaceWatcher::lowDiskSpaceDetected() const
{
    return priv->lowDiskSpace;
}


quint64 DiskSpaceWatcher::lowDiskSpaceTreshold() const
{
    return priv->lowSpaceThreshold;
}


void DiskSpaceWatcher::Private::refresh()
{
    if (diskFull)
        return;

    storageInfo.refresh();

    if (!lowDiskSpace && static_cast<quint64>(storageInfo.bytesAvailable()) < lowSpaceThreshold)
    {
        lowDiskSpace = true;
        emit publ->lowDiskSpace();
    }

    if (storageInfo.bytesAvailable() == 0)
    {
        diskFull = true;
        timer->stop();
        emit publ->diskFull();
    }
}

}
