// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QString>
#include <QCoreApplication>

#include "encryptionstate.h"


namespace Packuru::Core
{

QString toString(EncryptionState value)
{
     switch (value)
     {
     case EncryptionState::Disabled:
         return QCoreApplication::translate("EncryptionState", "Disabled");
     case EncryptionState::EntireArchive:
         return QCoreApplication::translate("EncryptionState", "Enabled (entire archive)");
     case EncryptionState::FilesContentOnly:
         return QCoreApplication::translate("EncryptionState", "Enabled (files content only)");
     default:
         Q_ASSERT(false); return QString();
     }
}

}
