// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_set>

#include <QObject>

#include "core/symbol_export.h"
#include "archivingparameterhandlerinitdata.h"


namespace qcs::core
{
class ControllerIdRowMapper;
class ControllerEngine;
}


namespace Packuru::Core
{

enum class EncryptionSupport;
class ArchivingCompressionInfo;
enum class ArchivingFilePaths;
enum class ArchiveType;


class PACKURU_CORE_EXPORT ArchivingParameterHandlerBase : public QObject
{
    Q_OBJECT

public:
    explicit ArchivingParameterHandlerBase(const ArchivingParameterHandlerInitData& initData, QObject *parent = nullptr);

    ~ArchivingParameterHandlerBase() override;

    ArchiveType getArchiveType() const;
    qcs::core::ControllerIdRowMapper* getMapper();
    std::unordered_set<ArchivingFilePaths> getSupportedFilePaths() const;

    virtual bool archiveSplittingSupport() const;
    virtual bool customInternalPathSupport() const;
    virtual ArchivingCompressionInfo getCompressionInfo() const;
    virtual void setCompressionLevel(int value);
    virtual EncryptionSupport getEncryptionSupport() const;
    virtual QString getEncryptionMethod() const;
    virtual QVariant getPrivateArchivingData() const;

signals:
    void sigCompressionInfoChanged();
    void sigEncryptionMethodChanged(const QString&);

protected:
    virtual std::unordered_set<ArchivingFilePaths> getAdditionalSupportedFilePaths() const;

    ArchivingParameterHandlerInitData initData;
    qcs::core::ControllerEngine* controllerEngine;
    qcs::core::ControllerIdRowMapper* controllerRowMapper;
};

}
