// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QFileInfo>
#include <QVariant>

#include "encryptionstate.h"
#include "archivingfilepaths.h"
#include "archivetype.h"
#include "core/symbol_export.h"


namespace Packuru::Core
{

struct BackendArchivingData
{
    /* When creating a new archive archiveName is only a base name without extension;
     * when adding files to existing archive archiveName is a full name with extension.
     * Overrides of BackendHandler::addToArchive() can handle archiveName in their own way. */
    QString archiveName;
    ArchiveType archiveType = ArchiveType::___NOT_SUPPORTED;
    QString destinationPath;
    std::vector<QFileInfo> inputItems;
    QString internalPath; // Can be empty
    qulonglong partSize = 0;
    ArchivingFilePaths filePaths = ArchivingFilePaths::RelativePaths;
    EncryptionState currentArchiveEncryption = EncryptionState::Disabled;
    EncryptionState newArchiveEncryption = EncryptionState::Disabled;
    QString password;
    bool createNewArchive = false;

    QVariant pluginPrivateData;
};

}

PACKURU_CORE_EXPORT QDataStream& operator<<(QDataStream& out, const Packuru::Core::BackendArchivingData& data);
PACKURU_CORE_EXPORT QDataStream& operator>>(QDataStream& in, Packuru::Core::BackendArchivingData& data);
