// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>
#include <QString>

#include "core/symbol_export.h"


namespace Packuru::Core
{

enum class EncryptionState
{
    Disabled,
    FilesContentOnly,
    EntireArchive,

    ___INVALID
};

PACKURU_CORE_EXPORT QString toString(EncryptionState value);

}

Q_DECLARE_METATYPE(Packuru::Core::EncryptionState);
