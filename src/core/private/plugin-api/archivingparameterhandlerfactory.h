// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <functional>

#include <QString>
#include <QMimeType>

#include "archivingparameterhandlerbase.h"
#include "archivetype.h"


namespace Packuru::Core
{

struct ArchivingParameterHandlerInitData;


class ArchivingParameterHandlerFactory
{
    friend class ArchiveServices;

public:
    using CreateHandlerFunction = std::function<std::unique_ptr<ArchivingParameterHandlerBase>(const ArchivingParameterHandlerInitData&)>;

    ArchivingParameterHandlerFactory(ArchiveType archiveType,
                                     const QString& archiveTypeAlias,
                                     uint pluginId,
                                     const CreateHandlerFunction& createHandler);

    ArchiveType getArchiveType() const { return archiveType_; }
    QString getArchiveTypeAlias() const { return archiveTypeAlias_; }
    uint getPluginId() const { return pluginId; }

    ArchivingParameterHandlerBase* createHandler(const ArchivingParameterHandlerInitData& initData);
    ArchivingParameterHandlerBase* getHandler() const { return handler.get(); }

private:
    ArchiveType archiveType_;
    QString archiveTypeAlias_;
    uint pluginId;
    CreateHandlerFunction createHandler_;
    std::unique_ptr<ArchivingParameterHandlerBase> handler;
};

}
