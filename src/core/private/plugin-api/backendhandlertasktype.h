// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once


namespace Packuru::Core
{

enum class BackendHandlerTaskType
{
    Add,
    Delete,
    Extract,
    Read,
    Test
};

//Returns whether task writes data to disk
bool isWritingTask(BackendHandlerTaskType type);

}
