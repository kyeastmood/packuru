// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>

#include "core/symbol_export.h"


namespace Packuru::Core
{

enum class ArchivingFilePaths
{
    AbsolutePaths,
    FullPaths,
    RelativePaths,
};

PACKURU_CORE_EXPORT QString toString(ArchivingFilePaths value);

}

Q_DECLARE_METATYPE(Packuru::Core::ArchivingFilePaths);
