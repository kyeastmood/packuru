// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "archivetype.h"
#include "encryptionstate.h"


namespace Packuru::Core
{

struct ArchivingParameterHandlerInitData
{
    ArchiveType archiveType = ArchiveType::___NOT_SUPPORTED;
    bool creatingNewArchive = false;
    EncryptionState currentArchiveEncryption = EncryptionState::___INVALID;
};

}
