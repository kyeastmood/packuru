// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <unordered_set>

#include <QtPlugin>

#include "core/symbol_export.h"
#include "backendarchiveproperties.h"
#include "archivingparameterhandlerbase.h"


namespace Packuru::Core
{

class AbstractBackendHandler;
enum class ArchiveType;
struct ArchivingParameterHandlerInitData;


struct PACKURU_CORE_EXPORT ArchiveHandlerInterface
{
    virtual ~ArchiveHandlerInterface() {}

    virtual std::unordered_set<ArchiveType> supportedTypesForReading() const { return {}; }
    virtual std::unordered_set<ArchiveType> supportedTypesForWriting() const { return {}; }

    virtual bool canAddFiles(ArchiveType archiveType, const BackendArchiveProperties& properties) const
    {
        Q_UNUSED(archiveType)
        Q_UNUSED(properties)
        return false;
    }

    virtual bool canDeleteFiles(ArchiveType archiveType, const BackendArchiveProperties& properties) const
    {
        Q_UNUSED(archiveType)
        Q_UNUSED(properties)
        return false;
    }

    virtual std::unique_ptr<AbstractBackendHandler> createBackendHandler() const = 0;

    virtual std::unique_ptr<ArchivingParameterHandlerBase>
    createArchivingParameterHandler(const ArchivingParameterHandlerInitData& initData) const
    {
        return std::make_unique<ArchivingParameterHandlerBase>(initData);
    }
};

}

Q_DECLARE_INTERFACE(Packuru::Core::ArchiveHandlerInterface, "Packuru::Core::ArchiveHandlerInterface")
