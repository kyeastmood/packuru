// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>

#include "../datastream/container_datastream.h"
#include "../datastream/qfileinfo_datastream.h"

#include "backendextractiondata.h"


using Packuru::Core::operator<<;
using Packuru::Core::operator>>;


QDataStream& operator<<(QDataStream& out, const Packuru::Core::BackendExtractionData& data)
{
    return out << data.archiveInfo.absoluteFilePath()
               << data.archiveType
               << data.tempDestinationPath
               << data.filesToExtract
               << data.extractionForPreview
               << data.encryptionState
               << data.password
               << data.pluginPrivateData;
}


QDataStream& operator>>(QDataStream& in, Packuru::Core::BackendExtractionData& data)
{
    return in >> data.archiveInfo
              >> data.archiveType
              >> data.tempDestinationPath
              >> data.filesToExtract
              >> data.extractionForPreview
              >> data.encryptionState
              >> data.password
              >> data.pluginPrivateData;
}
