// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>

#include "core/symbol_export.h"


namespace Packuru::Core
{

// Filled in by AbstractBackendHandler subclasses
enum class BackendArchiveProperty
{
    Comment, // QString
    EncryptionState, // EncryptionState
    Locked, // bool
    Methods, // QStringUnorderedSet
    Multipart, // bool
    RecoveryData, // bool
    Snapshots, // QString
    Solid, // bool
    PartCount, // int

    // Backend handlers need to set it only for multi-part archives
    TotalSize // qulonglong
};

PACKURU_CORE_EXPORT QLatin1String toLatin1(BackendArchiveProperty value);

PACKURU_CORE_EXPORT char* toString(BackendArchiveProperty value);

}
