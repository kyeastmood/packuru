// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QStringBuilder>

#include "textstreampreprocessor.h"


namespace Packuru::Core
{

namespace
{
    // Same as QStringRef::trimmed but space characters are not removed
    QStringRef customTrimmed(QStringRef s)
    {
        int begin = 0;
        int end = s.size();

        // Do not check final character here, it will be processed in the next loop
        while(begin < end - 1)
        {
            const auto& c = s[begin];
            if (c.isSpace() && c != QLatin1Char(' '))
                ++begin;
            else
                break;
        }

        while(begin < end)
        {
            const auto& c = s[end - 1];
            if (c.isSpace() && c != QLatin1Char(' '))
                --end;
            else
                break;
        }

        return s.mid(begin, end - begin);
    }
}

TextStreamPreprocessor::TextStreamPreprocessor(QObject* parent)
    : QObject(parent)
{

}


void TextStreamPreprocessor::processBytes(const QByteArray& bytes)
{
    if (bytes.isEmpty())
        return;

    int pos = buffer.size();
    int lineStart = 0;

    buffer = buffer % bytes;

    while (pos < buffer.size())
    {
        if (buffer.at(pos) == QLatin1Char('\n'))
        {
            const auto lineRef = customTrimmed(buffer.midRef(lineStart, pos - lineStart));

            fullLines.push_back(lineRef);

            ++pos;
            lineStart = pos;
        }
        else
            ++pos;
    }

    int newDataPosition = 0;

    if (!fullLines.empty())
    {
        emit fullLinesAvailable(fullLines, 0);
        fullLines.clear();

        const int unfinishedLineLength = pos - lineStart;

        if (unfinishedLineLength == 0)
        {
            buffer.clear();
        }
        else
        {
            buffer = buffer.right(unfinishedLineLength);
        }
    }
    else
        newDataPosition = buffer.size() - bytes.size();

    if (!buffer.isEmpty())
        emit unfinishedLine(buffer, newDataPosition);
}


void TextStreamPreprocessor::clear()
{
    buffer.clear();
}

}
