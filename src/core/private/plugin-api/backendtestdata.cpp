// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>

#include "../datastream/qfileinfo_datastream.h"

#include "backendtestdata.h"


using Packuru::Core::operator<<;
using Packuru::Core::operator>>;


QDataStream& operator<<(QDataStream& out, const Packuru::Core::BackendTestData& data)
{
    return out << data.archiveInfo
               << data.archiveType
               << data.encryptionState
               << data.password
               << data.pluginPrivateData;
}


QDataStream& operator>>(QDataStream& in, Packuru::Core::BackendTestData& data)
{   
    return in >> data.archiveInfo
              >> data.archiveType
              >> data.encryptionState
              >> data.password
              >> data.pluginPrivateData;
}
