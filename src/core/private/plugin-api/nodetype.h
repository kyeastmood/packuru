// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "core/symbol_export.h"


namespace Packuru::Core
{

enum class NodeType : unsigned char
{
    File,
    Folder,
    Link
};

PACKURU_CORE_EXPORT QString toString(NodeType  value);

}
