// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>

#include "../datastream/container_datastream.h"
#include "../datastream/qfileinfo_datastream.h"

#include "backendarchivingdata.h"


using Packuru::Core::operator<<;
using Packuru::Core::operator>>;


QDataStream& operator<<(QDataStream& out, const Packuru::Core::BackendArchivingData& data)
{
    return out << data.archiveName
               << data.archiveType
               << data.destinationPath
               << data.inputItems
               << data.internalPath
               << data.partSize
               << data.filePaths
               << data.currentArchiveEncryption
               << data.newArchiveEncryption
               << data.password
               << data.createNewArchive
               << data.pluginPrivateData;
}


QDataStream& operator>>(QDataStream& in, Packuru::Core::BackendArchivingData& data)
{
    return in >> data.archiveName
              >> data.archiveType
              >> data.destinationPath
              >> data.inputItems
              >> data.internalPath
              >> data.partSize
              >> data.filePaths
              >> data.currentArchiveEncryption
              >> data.newArchiveEncryption
              >> data.password
              >> data.createNewArchive
              >> data.pluginPrivateData;
}
