// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once


namespace Packuru::Core
{

enum class BackendHandlerWarning
{
    LowDiskSpace,
    // Invoked executable has a very old version and might not work as expected
    VersionDeprecated,
    // Invoked executable has a newer version that the one used during development
    VersionNotTested,

    ___INVALID
};

}
