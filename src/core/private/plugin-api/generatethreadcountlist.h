// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QList>


namespace Packuru::Core
{

// Used in controllers and QComboBox so must return QList
QList<int> generateThreadCountList(int maxThreadCount);

}
