// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archiveservicesdbusadaptor.h"


namespace Packuru::Core
{

ArchiveServicesDBusAdaptor::ArchiveServicesDBusAdaptor(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
    setAutoRelaySignals(true);
}

}
