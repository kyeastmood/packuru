// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <vector>


namespace Packuru::Core
{

class GlobalSettingsConverter;

using GlobalSettingsConverterList = std::vector<std::unique_ptr<GlobalSettingsConverter>>;

}
