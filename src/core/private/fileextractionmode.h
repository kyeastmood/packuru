// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


class QString;


namespace Packuru::Core
{

enum class FileExtractionMode
{
    AllFiles,
    SelectedFilesOnly
};


QString toString(FileExtractionMode value);

}

Q_DECLARE_METATYPE(Packuru::Core::FileExtractionMode)
