// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "privatestrings.h"
#include "core/appinfo.h"


namespace Packuru::Core
{

QString PrivateStrings::getString(UserString value, int count)
{
    Q_UNUSED(count)

    switch (value)
    {
    case UserString::Abort:
        return Packuru::Core::PrivateStrings::tr("Abort");

    case UserString::ActionAbout:
        return Packuru::Core::PrivateStrings::tr("About %1").arg(AppInfo::getAppName());

    case UserString::ActionExtract:
        return Packuru::Core::PrivateStrings::tr("Extract");

    case UserString::ActionNew:
        return Packuru::Core::PrivateStrings::tr("New");

    case UserString::ActionTest:
        return Packuru::Core::PrivateStrings::tr("Test");

    case UserString::ApplyToAll:
        return Packuru::Core::PrivateStrings::tr("Apply to all");

    case UserString::Destination:
        return Packuru::Core::PrivateStrings::tr("Destination");

    case UserString::DialogFileListEmpty:
        return Packuru::Core::PrivateStrings::tr("File list is empty");

    case UserString::FilterPlaceholderText:
        return Packuru::Core::PrivateStrings::tr("Filter...");

    case UserString::Miscellaneous:
        return Packuru::Core::PrivateStrings::tr("Miscellaneous");

    case UserString::Mode:
        return Packuru::Core::PrivateStrings::tr("Mode");

    case UserString::Password:
        return Packuru::Core::PrivateStrings::tr("Password");

    case UserString::Quit:
        return Packuru::Core::PrivateStrings::tr("Quit");

    case UserString::Retry:
        return Packuru::Core::PrivateStrings::tr("Retry");

    case UserString::Settings:
        return Packuru::Core::PrivateStrings::tr("Settings");

    case UserString::Skip:
        return Packuru::Core::PrivateStrings::tr("Skip");

    case UserString::Source:
        return Packuru::Core::PrivateStrings::tr("Source");

    default:
        Q_ASSERT(false); return "";
    }
}

}
