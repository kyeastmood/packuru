// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QFileInfo>
#include <QFile>
#include <QMimeDatabase>
#include <QRegularExpression>
#include <QDataStream>

#include <magic.h>

#include "core/private/plugin-api/archivetype.h"
#include "core/private/plugin-api/multipartarchivehelper.h"

#include "archivetypedetector.h"


namespace Packuru::Core
{

struct ArchiveTypeDetector::Private
{
    Private();

    ArchiveType fromMediaType(const QString& str) const;
    ArchiveType customExtensionDetector(const QString& name) const;
    ArchiveType customContentDetector(const QFileInfo& info) const;
    ArchiveType qtContentDetector(const QFileInfo& info) const;
    ArchiveType magicStandardContentDetector(const QFileInfo& info) const;
    ArchiveType magicCustomContentDetector(const QFileInfo& info) const;
    bool arrayContains(const QByteArray& array, const QLatin1String& str, int offsetFrom, int offsetLength = 0) const;

    QMimeDatabase mimeDb;
    using MagicPointer = std::unique_ptr<magic_set, decltype(&magic_close)>;
    // Returns media type string
    MagicPointer standardMagic;
    // Returns description string
    MagicPointer customMagic;
};


ArchiveTypeDetector::ArchiveTypeDetector()
    : priv(new Private)
{

}


ArchiveTypeDetector::~ArchiveTypeDetector()
{

}


ArchiveType ArchiveTypeDetector::detectType(const QString& absFilePath) const
{
    const QFileInfo info(absFilePath);

    if (!info.exists())
        return ArchiveType::___NOT_SUPPORTED;

    QString name = info.fileName();
    if (info.fileName().endsWith(QLatin1String(".part")))
        name.chop(5);

    QString extensionTypeStr = priv->mimeDb.mimeTypeForFile(name, QMimeDatabase::MatchExtension).name();
    ArchiveType extensionType = priv->fromMediaType(extensionTypeStr);

    if (extensionType == ArchiveType::___NOT_SUPPORTED
            // It can be a multi part tar/lzip (e.g. name.tar.lz0001.lz) and not just lzip
            || extensionType == ArchiveType::lzip)
        extensionType = priv->customExtensionDetector(name);

    using DetectorFunction = ArchiveType(ArchiveTypeDetector::Private::*)(const QFileInfo&) const;

    const std::initializer_list<DetectorFunction> expertDetectors {
        &ArchiveTypeDetector::Private::qtContentDetector,
                &ArchiveTypeDetector::Private::magicStandardContentDetector,
                &ArchiveTypeDetector::Private::magicCustomContentDetector
    };

    ArchiveType type = ArchiveType::___NOT_SUPPORTED;
    float confidence = 0.0;

    for (const auto& detector : expertDetectors)
    {
        ArchiveType tempType = (priv.get()->*detector)(info);

        float tempConfidence = 1.0;

        if (tempType == ArchiveType::arc)
        {
            tempConfidence = 0.9f; // It can be pak
        }
        else if (tempType == ArchiveType::brotli)
        {
            if (extensionType == ArchiveType::tar_brotli)
                tempType = extensionType;
        }
        else if (tempType == ArchiveType::bzip2)
        {
            if (extensionType == ArchiveType::tar_bzip2)
                tempType = extensionType;
            else
                tempConfidence = 0.9f; // It can be dmg
        }
        else if (tempType == ArchiveType::compress)
        {
            if (extensionType == ArchiveType::tar_compress)
                tempType = extensionType;
        }
        else if (tempType == ArchiveType::gzip)
        {
            if (extensionType == ArchiveType::tar_gzip)
                tempType = extensionType;
        }
        else if (tempType == ArchiveType::lrzip)
        {
            if (extensionType == ArchiveType::tar_lrzip)
                tempType = extensionType;
        }
        else if (tempType == ArchiveType::lz4)
        {
            if (extensionType == ArchiveType::tar_lz4)
                tempType = extensionType;
        }
        else if (tempType == ArchiveType::lzip)
        {
            if (extensionType == ArchiveType::tar_lzip)
                tempType = extensionType;
        }
        else if (tempType == ArchiveType::lzma)
        {
            if (extensionType == ArchiveType::tar_lzma)
                tempType = extensionType;
        }
        else if (tempType == ArchiveType::lzop)
        {
            if (extensionType == ArchiveType::tar_lzop)
                tempType = extensionType;
        }
        else if (tempType == ArchiveType::xz)
        {
            if (extensionType == ArchiveType::tar_xz)
                tempType = extensionType;
        }
        else if (tempType == ArchiveType::zstd)
        {
            if (extensionType == ArchiveType::tar_zstd)
                tempType = extensionType;
        }

        if (tempType != ArchiveType::___NOT_SUPPORTED && tempConfidence > confidence)
        {
            type = tempType;
            confidence = tempConfidence;
            if (confidence == 1.0f)
                break;
        }
    }

    if (type != ArchiveType::___NOT_SUPPORTED && confidence == 1.0f)
        return type;

    const auto customType = priv->customContentDetector(info);
    if (customType != ArchiveType::___NOT_SUPPORTED) // Takes priority over uncertain 'expert' type
        type = customType;

    if (type == ArchiveType::___NOT_SUPPORTED)
        type = extensionType; // Last resort: use file extension

    return type;
}


ArchiveTypeDetector::Private::Private()
    : standardMagic(magic_open(MAGIC_MIME_TYPE), magic_close),
      customMagic(magic_open(MAGIC_NONE), magic_close)
{
    magic_load(standardMagic.get(), nullptr);
    magic_load(customMagic.get(), nullptr);
}


ArchiveType ArchiveTypeDetector::Private::fromMediaType(const QString& str) const
{
    if (str.endsWith(QLatin1String("epub+zip")))
        return ArchiveType::zip;

    else if (str.endsWith(QLatin1String("gzip")))
        return ArchiveType::gzip;

    else if (str.endsWith(QLatin1String("pdf")))
        return ArchiveType::pdf;

    else if (str.endsWith(QLatin1String("vnd.adobe.flash.movie")))
        return ArchiveType::swf;

    else if (str.endsWith(QLatin1String("vnd.appimage")))
        return ArchiveType::appimage;

    else if (str.endsWith(QLatin1String("vnd.debian.binary-package")))
        return ArchiveType::deb;

    else if (str.endsWith(QLatin1String("vnd.ms-cab-compressed")))
        return ArchiveType::cab;

    else if (str.endsWith(QLatin1String("vnd.ms-htmlhelp")))
        return ArchiveType::chm;

    else if (str.endsWith(QLatin1String("vnd.oasis.opendocument.spreadsheet"))
             || str.endsWith(QLatin1String("vnd.oasis.opendocument.text"))
             || str.endsWith(QLatin1String("msword"))
             || str.endsWith(QLatin1String("vnd.openxmlformats-officedocument.wordprocessingml.document"))
             || str.endsWith(QLatin1String("vnd.ms-excel")))
        return ArchiveType::zip;

    else if (str.endsWith(QLatin1String("vnd.rar")) || str.endsWith(QLatin1String("x-rar")))
        return ArchiveType::rar;

    else if (str.endsWith(QLatin1String("vnd.squashfs")))
        return ArchiveType::squashfs;

    else if (str.endsWith(QLatin1String("x-7z-compressed")))
        return ArchiveType::_7z;

    else if (str.endsWith(QLatin1String("x-ace")))
        return ArchiveType::ace;

    else if (str.endsWith(QLatin1String("x-alz")))
        return ArchiveType::alz;

    else if (str.endsWith(QLatin1String("x-apple-diskimage")))
        return ArchiveType::dmg;

    else if (str.endsWith(QLatin1String("x-archive")))
        return ArchiveType::ar;

    else if (str.endsWith(QLatin1String("x-arc")))
        return ArchiveType::arc;

    else if (str.endsWith(QLatin1String("x-arj")))
        return ArchiveType::arj;

    else if (str.endsWith(QLatin1String("x-brotli")))
        return ArchiveType::brotli;

    else if (str.endsWith(QLatin1String("x-brotli-compressed-tar")))
        return ArchiveType::tar_brotli;

    else if (str.endsWith(QLatin1String("x-bzip")) || str.endsWith(QLatin1String("x-bzip2")))
        return ArchiveType::bzip2;

    else if (str.endsWith(QLatin1String("x-bzip2-compressed-tar")))
        return ArchiveType::tar_bzip2;

    else if (str.endsWith(QLatin1String("x-compress")))
        return ArchiveType::compress;

    else if (str.endsWith(QLatin1String("x-compressed-tar")))
        return ArchiveType::tar_gzip;

    else if (str.endsWith(QLatin1String("x-cpio")))
        return ArchiveType::cpio;

    else if (str.endsWith(QLatin1String("x-iso9660-image"))
             || str.endsWith(QLatin1String("x-cd-image")))
        return ArchiveType::iso9660;

    else if (str.endsWith(QLatin1String("x-lha"))
             || str.endsWith(QLatin1String("x-lzh-compressed")))
        return ArchiveType::lha;

    else if (str.endsWith(QLatin1String("x-lrzip")))
        return ArchiveType::lrzip;

    else if (str.endsWith(QLatin1String("x-lrzip-compressed-tar")))
        return ArchiveType::tar_lrzip;

    else if (str.endsWith(QLatin1String("x-lz4")))
        return ArchiveType::lz4;

    else if (str.endsWith(QLatin1String("x-lz4-compressed-tar")))
        return ArchiveType::tar_lz4;

    else if (str.endsWith(QLatin1String("x-lzip")))
        return ArchiveType::lzip;

    else if (str.endsWith(QLatin1String("x-lzip-compressed-tar")))
        return ArchiveType::tar_lzip;

    else if (str.endsWith(QLatin1String("x-lzma")))
        return ArchiveType::lzma;

    else if (str.endsWith(QLatin1String("x-lzma-compressed-tar")))
        return ArchiveType::tar_lzma;

    else if (str.endsWith(QLatin1String("x-lzop")))
        return ArchiveType::lzop;

    else if (str.endsWith(QLatin1String("x-ms-wim")))
        return ArchiveType::wim;

    else if (str.endsWith(QLatin1String("x-msi")))
        return ArchiveType::msi;

    else if (str.endsWith(QLatin1String("x-nrg")))
        return ArchiveType::nrg;

    else if (str.endsWith(QLatin1String("x-rpm")))
        return ArchiveType::rpm;

    else if (str.endsWith(QLatin1String("x-stuffit")))
        return ArchiveType::sit;

    else if (str.endsWith(QLatin1String("x-tar")))
        return ArchiveType::tar;

    else if (str.endsWith(QLatin1String("x-tarz")))
        return ArchiveType::tar_compress;

    else if (str.endsWith(QLatin1String("x-tzo")))
        return ArchiveType::tar_lzop;

    else if (str.endsWith(QLatin1String("x-virtualbox-vdi")))
        return ArchiveType::vdi;

    else if (str.endsWith(QLatin1String("x-virtualbox-vhd")))
        return ArchiveType::vhd;

    else if (str.endsWith(QLatin1String("x-virtualbox-vmdk")))
        return ArchiveType::vmdk;

    else if (str.endsWith(QLatin1String("x-xar")))
        return ArchiveType::xar;

    else if (str.endsWith(QLatin1String("x-xz")))
        return ArchiveType::xz;

    else if (str.endsWith(QLatin1String("x-xz-compressed-tar")))
        return ArchiveType::tar_xz;

    else if (str.endsWith(QLatin1String("x-zpaq")))
        return ArchiveType::zpaq;

    else if (str.endsWith(QLatin1String("zip")))
        return ArchiveType::zip;

    else if (str.endsWith(QLatin1String("x-zstd-compressed-tar")))
        return ArchiveType::tar_zstd;

    else if (str.endsWith(QLatin1String("zstd")))
        return ArchiveType::zstd;

    return ArchiveType::___NOT_SUPPORTED;
}


ArchiveType ArchiveTypeDetector::Private::customExtensionDetector(const QString& name) const
{
    ArchiveType result = ArchiveType::___NOT_SUPPORTED;

    if (name.endsWith(QLatin1String("tar.br")))
        result = ArchiveType::tar_brotli;
    else if (isMultiPartArchiveName(name, ArchiveType::tar_lzip))
        result = ArchiveType::tar_lzip;
    else if (name.endsWith(QLatin1String("tar.zst")) || name.endsWith(QLatin1String("tzst")))
        result = ArchiveType::tar_zstd;
    else if (name.endsWith(QLatin1String("zpaq")))
        result = ArchiveType::zpaq;

    return result;
}


// This detector checks for things that are not checked by libmagic and FreeDestop Shared MIME-info Database
// NOTE: Keep broad searches at the end
ArchiveType ArchiveTypeDetector::Private::customContentDetector(const QFileInfo& info) const
{
    QFile file(info.absoluteFilePath());
    file.open(QIODevice::ReadOnly);
    QDataStream in(&file);

    QByteArray frontBuffer;
    // RAR sfx module can take up to 1 MiB/MB:
    // https://www.rarlab.com/technote.htm#sfx
    // so this value is used as maximum size for front buffer
    const int frontBufferInitSize = 65536;
    const int frontBufferMaxSize = 1'048'576;

    frontBuffer.reserve(frontBufferMaxSize);

    const int readSize = in.readRawData(frontBuffer.data(), frontBufferInitSize);
    frontBuffer.resize(readSize);

    if (frontBuffer.size() == 0)
        return ArchiveType::___NOT_SUPPORTED;

    // Start checks here ====================
    if (frontBuffer.startsWith("MEDIA DESCRIPTOR"))
        return ArchiveType::mdx;

    { // Disk images
        const QLatin1String udfId("\x42\x45\x41\x30\x31\x01"); // .BEA01

        const int filesystemImageOffset = 16 * 2048 + 1;
        if (arrayContains(frontBuffer, udfId, filesystemImageOffset))
            return ArchiveType::udf;

        const int rawImgOffset = 16 * (2352 + 1) + 1;
        const QLatin1String iso9660Id("CD001");

        // There are many raw image formats so there is no fixed offset for ID
        if (arrayContains(frontBuffer, iso9660Id, rawImgOffset, 1024)
                || arrayContains(frontBuffer, udfId, rawImgOffset, 1024))
            return ArchiveType::raw_optical;
    }

    const QLatin1String linuxExecId("\x7f\x45\x4c\x46"); // .ELF
    const QLatin1String winExecId("\x4d\x5a"); // MZ

    if (frontBuffer.startsWith(winExecId.data()) || frontBuffer.startsWith(linuxExecId.data()))
    {
        if (!in.atEnd())
        {
            // Read remaining data from the stream, up to the buffer's capacity
            const auto readRemainderSize = in.readRawData(frontBuffer.data() + frontBuffer.size(),
                                                          frontBuffer.capacity() - frontBuffer.size());
            frontBuffer.resize(frontBuffer.size() + readRemainderSize);
        }

        const char _7zId[] = "\x37\x7a\xbc\xaf\x27\x1c"; // https://en.wikipedia.org/wiki/List_of_file_signatures
        const char cabId[] = "\x4d\x53\x43\x46"; // https://en.wikipedia.org/wiki/List_of_file_signatures
        const char rarId[] = "\x52\x61\x72\x21\x1a\x07"; // https://www.rarlab.com/technote.htm#rarsign

        const char* zipId = "\x50\x4b"; // PK

        const std::initializer_list<const char*> zipIds { "\x50\x4b\x03\x04", // PK**
                                                          "\x50\x4b\x05\x06",
                                                          "\x50\x4b\x07\x08",
                                                          "\x50\x4b\x30\x30"};

        if (frontBuffer.indexOf(_7zId) >= 0)
            return ArchiveType::_7z;

        if (frontBuffer.indexOf(cabId) >= 0)
            return ArchiveType::cab;

        if (frontBuffer.indexOf(rarId) >= 0)
            return ArchiveType::rar;

        int offset = -1;
        while (true)
        {
            offset = frontBuffer.indexOf(zipId, ++offset);
            if (offset >= 0)
            {
                for (const auto* id : zipIds)
                    if (arrayContains(frontBuffer, QLatin1String(id), offset))
                        return ArchiveType::zip;
            }
            else
                break;
        }
    }

    // Read the end of the file
    QByteArray backBuffer;

    if (frontBuffer.size() >= file.size())
        backBuffer = frontBuffer;
    else
    {
        // https://en.wikipedia.org/wiki/Apple_Disk_Image
        const int backBufferMaxSize = 512;
        backBuffer.resize(backBufferMaxSize);
        file.seek(file.size() - backBufferMaxSize);

        const int readRemainderSize = in.readRawData(backBuffer.data(), backBufferMaxSize);
        backBuffer.resize(readRemainderSize);
    }

    if (backBuffer.startsWith("koly"))
        return ArchiveType::dmg;

    return ArchiveType::___NOT_SUPPORTED;
}


ArchiveType ArchiveTypeDetector::Private::qtContentDetector(const QFileInfo& info) const
{
    const QString type = mimeDb.mimeTypeForFile(info, QMimeDatabase::MatchContent).name();
    return fromMediaType(type);
}


ArchiveType ArchiveTypeDetector::Private::magicStandardContentDetector(const QFileInfo& info) const
{
    const QString type = magic_file(standardMagic.get(), info.absoluteFilePath().toLocal8Bit().constData());
    return fromMediaType(type);
}


ArchiveType ArchiveTypeDetector::Private::magicCustomContentDetector(const QFileInfo& info) const
{
    const QString type = magic_file(customMagic.get(), info.absoluteFilePath().toLocal8Bit().constData());

    const QRegularExpression extPattern ("ext\\d filesystem data");

    if (type.contains(QLatin1String("RAR self-extracting")))
        return ArchiveType::rar;

    else if (type.startsWith(QLatin1String("ACE archive")))
        return ArchiveType::ace;

    else if (type.startsWith(QLatin1String("Squashfs filesystem")))
        return ArchiveType::squashfs;

    else if (type.contains(QLatin1String("cpio archive")))
        return ArchiveType::cpio;

    else if (type.startsWith(QLatin1String("Linux Compressed ROM File System data")))
        return ArchiveType::cramfs;

    else if (type.contains(extPattern))
        return ArchiveType::ext;

    else if (type.contains(QLatin1String("lzop compressed")))
        return ArchiveType::lzop;

    // can also contain "fat" in the description so check for ntfs first
    else if (type.contains(QLatin1String("NTFS")))
        return ArchiveType::ntfs;

    else if (type.contains(QLatin1String("FAT")))
        return ArchiveType::fat;

    // can also contain fat, ntfs, ext so first check for these file systems first
    else if (type.startsWith(QLatin1String("DOS/MBR boot sector")))
        return ArchiveType::mbr;

    else if (type.startsWith(QLatin1String("QEMU QCOW Image")))
        return ArchiveType::qcow;

    else if (type.startsWith(QLatin1String("PAK archive data")))
        return ArchiveType::pak;

    else if (type.startsWith(QLatin1String("VirtualBox Disk Image")))
        return ArchiveType::vdi;

    else if (type.startsWith(QLatin1String("Microsoft Disk Image")))
        return ArchiveType::vhd;

    else if (type.startsWith(QLatin1String("VMware4 disk image")))
        return ArchiveType::vmdk;

    else if (type.startsWith(QLatin1String("MS Windows HtmlHelp Data")))
        return ArchiveType::chm;

    else if (type.startsWith(QLatin1String("ZPAQ")))
        return ArchiveType::zpaq;

    return ArchiveType::___NOT_SUPPORTED;
}


bool ArchiveTypeDetector::Private::arrayContains(const QByteArray& array,
                                                 const QLatin1String& str,
                                                 int offsetFrom,
                                                 int offsetLength) const
{
    Q_ASSERT(offsetFrom >= 0 && offsetLength >= 0);

    if (str.isEmpty())
        return false;

    int strPos = 0;
    auto strStart = offsetFrom;
    const auto offsetMax = offsetFrom + offsetLength;

    for (auto i = offsetFrom; i < array.size() && strStart <= offsetMax; )
    {
        if (array.at(i) == str[strPos])
        {
            ++strPos;
            if (strPos == str.size())
                return true;
            ++i;
        }
        else
        {
            if (strPos == 0)
                ++i;
            else
                strPos = 0;
            strStart = i;
        }
    }

    return false;
}

}
