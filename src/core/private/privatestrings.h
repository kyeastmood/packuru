// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "core/symbol_export.h"


namespace Packuru::Core
{

class PACKURU_CORE_EXPORT PrivateStrings : public QObject
{
    Q_OBJECT
public:
    enum class UserString
    {
        ___INVALID,

        Abort,
        ActionAbout,
        ActionExtract,
        ActionNew,
        ActionTest,
        ApplyToAll,
        Destination,
        DialogFileListEmpty,
        FilterPlaceholderText,
        Miscellaneous,
        Mode,
        Password,
        Quit,
        Retry,
        Settings,
        Skip,
        Source
    };
    Q_ENUM(UserString)

    // Translations must be loaded first
    Q_INVOKABLE static QString getString(UserString value, int count = 0);
};

}
