// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QDataStream>
#include <QFileInfo>


namespace Packuru::Core
{

inline QDataStream& operator<<(QDataStream& out, const QFileInfo& info)
{
    return out << info.absoluteFilePath();
}


inline QDataStream& operator>>(QDataStream& in, QFileInfo& info)
{
    QString temp;
    in >> temp;
    info.setFile(temp);
    return in;
}

};
