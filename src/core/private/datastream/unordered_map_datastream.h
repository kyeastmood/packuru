// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>

#include <QDataStream>


namespace Packuru::Core
{

template <typename T, typename U>
QDataStream& operator<<(QDataStream& out, const std::unordered_map<T, U>& map)
{
    using MapSizeType = typename std::unordered_map<T, U>::size_type;
    using SerializedSizeType = quint64;

    static_assert(std::numeric_limits<MapSizeType>::max() <= std::numeric_limits<SerializedSizeType>::max());

    out << static_cast<SerializedSizeType>(map.size());

    for (const auto& it : map)
        out << it.first << it.second;

    return out;
}


template <typename T, typename U>
QDataStream& operator>>(QDataStream& in, std::unordered_map<T, U>& map)
{
    using MapSizeType = typename std::unordered_map<T, U>::size_type;
    using SerializedSizeType = quint64;

    static_assert(std::numeric_limits<MapSizeType>::max() <= std::numeric_limits<SerializedSizeType>::max());

    SerializedSizeType size = 0;
    in >> size;

    for (SerializedSizeType i = 0; i < size; ++i)
    {
        T key;
        in >> key;

        U value;
        in >> value;

        map[key] = value;
    }

    return in;
}

};
