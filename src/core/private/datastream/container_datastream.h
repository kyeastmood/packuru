// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <limits>
#include <type_traits>

#include <QDataStream>


namespace Packuru::Core
{

template <typename T, typename = void>
struct is_streamable_container : std::false_type {};

template <typename T>
struct is_streamable_container<T, std::void_t< typename T::size_type,
                                    typename T::value_type,
                                    decltype(std::declval<T>().begin()),
                                    decltype(std::declval<T>().end()),
                                    decltype(std::declval<T>().size()),
                                    decltype(std::declval<T>().push_back(typename T::value_type()))>>
                                     : std::true_type {};

template <typename T>
std::enable_if_t<is_streamable_container<T>::value, QDataStream&>
operator<<(QDataStream& out, const T& container)
{
    using ContainerSizeType = typename T::size_type;
    using SerializedSizeType = quint64;

    static_assert(std::numeric_limits<ContainerSizeType>::max() <= std::numeric_limits<SerializedSizeType>::max());

    out << static_cast<SerializedSizeType>(container.size());

    for (const auto& v : container)
        out << v;

    return out;
}


template <typename T>
std::enable_if_t<is_streamable_container<T>::value, QDataStream&>
operator>>(QDataStream& in, T& container)
{
    using ContainerSizeType = typename T::size_type;
    using SerializedSizeType = quint64;

    static_assert(std::numeric_limits<ContainerSizeType>::max() <= std::numeric_limits<SerializedSizeType>::max());

    SerializedSizeType size = 0;
    in >> size;

    using ContainerValueType = typename T::value_type;
    ContainerValueType temp;

    for (SerializedSizeType i = 0; i < size; ++i)
    {
        in >> temp;
        container.push_back(temp);
    }

    return in;
}


};
