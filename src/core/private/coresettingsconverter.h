// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "core/globalsettingsconverter.h"
#include "core/symbol_export.h"


namespace Packuru::Core
{

enum class SizeUnits;
enum class CompressionRatioMode;


class PACKURU_CORE_EXPORT CoreSettingsConverter : public GlobalSettingsConverter
{
public:
    CoreSettingsConverter();

    std::unordered_set<int> getSupportedKeys() const override;
    QString getKeyName(int key) const override;
    QVariant toSettingsType(int key, const QVariant& value) const override;
    QVariant fromSettingsType(int key, const QVariant& value) const override;

private:
    QString toSettingsType(SizeUnits value) const;
    void fromSettingsType(const QString& key, SizeUnits& out) const;

    QString toSettingsType(CompressionRatioMode value) const;
    void fromSettingsType(const QString& key, CompressionRatioMode& out) const;
};

}
