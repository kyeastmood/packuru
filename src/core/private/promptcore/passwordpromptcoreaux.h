// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>
#include <QObject>

#include "core/creationnameerrorpromptcore.h"


namespace Packuru::Core::PasswordPromptCoreAux
{

class Emitter : public QObject
{
    Q_OBJECT

public:
    explicit Emitter(QObject* parent = nullptr);

signals:
    void passwordEntered(const QString& value);
    void abortRequested();
    void finished();
};


struct Init
{
    QString archiveName;
};


struct Backdoor
{
    Emitter* emitter = nullptr;
};

}
