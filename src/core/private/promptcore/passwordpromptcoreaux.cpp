// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "passwordpromptcoreaux.h"


namespace Packuru::Core::PasswordPromptCoreAux
{

Emitter::Emitter(QObject* parent)
    : QObject(parent)
{

}

}
