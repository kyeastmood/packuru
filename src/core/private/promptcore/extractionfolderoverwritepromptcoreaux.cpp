// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "extractionfolderoverwritepromptcoreaux.h"


namespace Packuru::Core::ExtractionFolderOverwritePromptCoreAux
{

Emitter::Emitter(QObject* parent)
    : QObject(parent)
{

}

}
