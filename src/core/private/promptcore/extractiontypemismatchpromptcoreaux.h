// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QFileInfo>
#include <QObject>

#include "core/extractiontypemismatchpromptcore.h"


namespace Packuru::Core::ExtractionTypeMismatchPromptCoreAux
{

class Emitter : public QObject
{
    Q_OBJECT

public:
    explicit Emitter(QObject* parent = nullptr);

signals:
    void skipRequested(bool applyToAll);
    void retryRequested();
    void abortRequested();
};


struct Init
{
    QFileInfo source;
    QFileInfo destination;
};


struct Backdoor
{
    Emitter* emitter = nullptr;
};

}
