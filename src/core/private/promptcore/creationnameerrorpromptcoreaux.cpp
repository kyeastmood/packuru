// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "creationnameerrorpromptcoreaux.h"


namespace Packuru::Core::CreationNameErrorPromptCoreAux
{

Emitter::Emitter(QObject* parent)
    : QObject(parent)
{

}

}
