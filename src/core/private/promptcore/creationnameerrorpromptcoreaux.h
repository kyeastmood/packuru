// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>
#include <QObject>

#include "core/creationnameerrorpromptcore.h"


namespace Packuru::Core::CreationNameErrorPromptCoreAux
{

class Emitter : public QObject
{
    Q_OBJECT

public:
    explicit Emitter(QObject* parent = nullptr);

signals:
    void changeArchiveNameRequested(const QString& newName);
    void retryRequested();
    void abortRequested();
};


struct Init
{
    QString archiveName;
    QString errorInfo;
};


struct Backdoor
{
    Emitter* emitter = nullptr;
};

}
