// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "aboutdialogcore.h"
#include "appinfo.h"
#include "../core/private/privatestrings.h"


namespace Packuru::Core
{

AboutDialogCore::AboutDialogCore(QObject *parent)
    : QObject(parent)
{

}


QString AboutDialogCore::getString(UserString value)
{
    switch (value)
    {
    case UserString::AppInfo: return AppInfo::getAppInfo();
    case UserString::DialogTitle: return PrivateStrings::getString(PrivateStrings::UserString::ActionAbout);
    default: Q_ASSERT(false); return "";
    }
}

}
