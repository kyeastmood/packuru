// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <map>
#include <vector>

#include <QDir>

#include "../utils/private/tounderlyingtype.h"
#include "../utils/private/enumcast.h"

#include "../qcs-core/controllerengine.h"

#include "extractiondialogcore.h"
#include "extractiondialogcontrollertype.h"
#include "private/dialogcore/extractiondialog/extractiondialogcoreinitsingle.h"
#include "private/dialogcore/extractiondialog/extractiondialogcoreinitmulti.h"
#include "private/dialogcore/extractiondialog/topfoldermodectrl.h"
#include "private/dialogcore/extractiondialog/fileextractionmodectrl.h"
#include "private/dialogcore/extractiondialog/extractionfileoverwritectrl.h"
#include "private/dialogcore/extractiondialog/extractionfolderoverwritectrl.h"
#include "private/dialogcore/inputarchivesmodel.h"
#include "private/dialogcore/extractiondialog/extractiondestinationmodectrl.h"
#include "private/dialogcore/extractiondialog/extractiondestinationpathctrl.h"
#include "private/dialogcore/genericpasswordctrl.h"
#include "private/dialogcore/extractiondialog/extractiondestinationerrorctrl.h"
#include "private/dialogcore/extractiondialog/extractionopendestinationctrl.h"
#include "private/dialogcore/runinqueuectrl.h"
#include "private/dialogcore/acceptbuttonctrl.h"
#include "private/dialogcore/genericdialogerrorsctrl.h"
#include "private/tasks/extractiontaskdata.h"
#include "private/plugin-api/encryptionstate.h"
#include "commonstrings.h"


using namespace qcs::core;


namespace Packuru::Core
{

struct ExtractionDialogCore::Private
{
    Private(ExtractionDialogCore* publ,
            ExtractionDialogCore::DialogMode dialogMode,
            bool acceptsPassword);

    ~Private();

    void initControllers(const QString& password);

    ExtractionDialogCore* publ;
    ExtractionDialogCore::DialogMode dialogMode;
    ExtractionDialogCoreInitSingle initSingle;
    ExtractionDialogCoreInitMulti initMulti;
    InputArchivesModel* archivesModel = nullptr;
    ControllerEngine* engine = nullptr;
    bool accepted = false;
    const bool acceptsPassword;
};


ExtractionDialogCore::ExtractionDialogCore(ExtractionDialogCoreInitSingle initData,
                                           QObject* parent)
    : QObject(parent),
      priv(new Private(this,
                       DialogMode::SingleArchive,
                       initData.archiveEncryption == EncryptionState::FilesContentOnly
                       && initData.password.isEmpty()))
{
    Q_ASSERT(initData.isValid());
    priv->initSingle = std::move(initData);

    priv->initControllers(priv->initSingle.password);
}


ExtractionDialogCore::ExtractionDialogCore(ExtractionDialogCoreInitMulti initData,
                                           QObject* parent)
    : QObject(parent),
      priv(new Private(this, DialogMode::MultiArchive, true))
{
    Q_ASSERT(initData.isValid());
    priv->initMulti = std::move(initData);

    priv->archivesModel = new InputArchivesModel(priv->initMulti.guessArchiveType,
                                                 priv->initMulti.getBackend, this);
    priv->archivesModel->addArchives(priv->initMulti.archives);

    priv->initControllers(QString());
}


ExtractionDialogCore::~ExtractionDialogCore()
{
}


void ExtractionDialogCore::destroyLater()
{
    deleteLater();
}


QString ExtractionDialogCore::getString(ExtractionDialogCore::UserString value)
{
    switch (value)
    {
    case UserString::Cancel:
        return CommonStrings::getString(CommonStrings::UserString::Cancel);

    case UserString::DialogTitleMulti:
        return Packuru::Core::ExtractionDialogCore::tr("Extract archives");

    case UserString::DialogTitleSingle:
        return Packuru::Core::ExtractionDialogCore::tr("Extract");

    case UserString::TabArchives:
        return Packuru::Core::ExtractionDialogCore::tr("Archives");

    case UserString::TabOptions:
        return Packuru::Core::ExtractionDialogCore::tr("Options");

    default:
        Q_ASSERT(false); return "";
    }
}


QAbstractItemModel* ExtractionDialogCore::getArchivesModel() const
{
    return priv->archivesModel;
}


ControllerEngine* ExtractionDialogCore::getControllerEngine()
{
    return priv->engine;
}


ExtractionDialogCore::DialogMode ExtractionDialogCore::getMode() const
{
    return priv->dialogMode;
}


void ExtractionDialogCore::addArchives(const QList<QUrl> &archives)
{
    priv->archivesModel->addArchives(archives);
}


void ExtractionDialogCore::removeArchives(const QModelIndexList& indexes)
{
    priv->archivesModel->removeArchives(indexes);
}


void ExtractionDialogCore::accept()
{
    Q_ASSERT(!priv->accepted);
    priv->accepted = true;

    auto values = priv->engine->getAvailableCurrentValues<ExtractionDialogControllerType::Type>();

    ExtractionTaskData data;
    data.topFolderMode = Utils::getValueAs<ExtractionTopFolderMode>(values[ExtractionDialogControllerType::Type::TopFolderMode]);
    data.fileOverwriteMode = Utils::getValueAs<ExtractionFileOverwriteMode>(values[ExtractionDialogControllerType::Type::FileOverwriteMode]);
    data.folderOverwriteMode = Utils::getValueAs<ExtractionFolderOverwriteMode>(values[ExtractionDialogControllerType::Type::FolderOverwriteMode]);
    data.openDestinationOnCompletion = Utils::getValueAs<bool>(values[ExtractionDialogControllerType::Type::OpenDestinationOnCompletion]);
    data.runInQueue = Utils::getValueAs<bool>(values[ExtractionDialogControllerType::Type::RunInQueue]);
    data.backendData.password = Utils::getValueAs<QString>(values[ExtractionDialogControllerType::Type::Password]);

    if (priv->dialogMode == DialogMode::SingleArchive)
    {
        data.backendData.archiveInfo = priv->initSingle.archiveInfo;
        data.backendData.archiveType = priv->initSingle.archiveType;

        // QUrl will remove "file://" from path which is passed by qml
        const QUrl destUrl = Utils::getValueAs<QString>(values[ExtractionDialogControllerType::Type::DestinationPath]);
        data.destinationPath = destUrl.path();

        const auto fileExtractionMode = Utils::getValueAs<FileExtractionMode>(values[ExtractionDialogControllerType::Type::FileExtractionMode]);
        if (fileExtractionMode == FileExtractionMode::SelectedFilesOnly)
            data.backendData.filesToExtract =  priv->initSingle.selectedFiles;

        data.backendData.encryptionState =  priv->initSingle.archiveEncryption;

        priv->initSingle.onAccepted(data);
    }
    else if (priv->dialogMode == DialogMode::MultiArchive)
    {
        const auto archives = priv->archivesModel->getArchives();
        std::vector<ExtractionTaskData> dataList;

        const auto destinationMode = Utils::getValueAs<ExtractionDestinationMode>(values[ExtractionDialogControllerType::Type::DestinationMode]);

        for (const auto& archive : archives)
        {
            data.backendData.archiveInfo = archive.info;
            data.backendData.archiveType = archive.type;

            if (destinationMode == ExtractionDestinationMode::ArchiveParentFolder)
                data.destinationPath = archive.info.absolutePath();
            else if (destinationMode == ExtractionDestinationMode::CustomFolder)
            {
                // QUrl will remove "file://" from path which is passed by qml
                const QUrl destUrl = Utils::getValueAs<QString>(values[ExtractionDialogControllerType::Type::DestinationPath]);
                data.destinationPath = destUrl.path();
            }
            else
                Q_ASSERT(false);

            dataList.push_back(data);
        }

        priv->initMulti.onAccepted(dataList);
    }
    else
        Q_ASSERT(false);
}


ExtractionDialogCore::Private::Private(ExtractionDialogCore* publ,
                                       ExtractionDialogCore::DialogMode mode,
                                       bool acceptsPassword)
    : publ(publ),
      dialogMode(mode),
      engine(new ControllerEngine(publ)),
      acceptsPassword(acceptsPassword)

{
}


ExtractionDialogCore::Private::~Private()
{
}


void ExtractionDialogCore::Private::initControllers(const QString& password)
{
    const auto userDestinationOnly = dialogMode == ExtractionDialogCore::DialogMode::SingleArchive;

    auto destinationModeCtrl =
            engine->createTopController<ExtractionDestinationModeCtrl>(ExtractionDialogControllerType::Type::DestinationMode,
                                                                       userDestinationOnly);

    QString path;
    if (dialogMode == ExtractionDialogCore::DialogMode::MultiArchive)
        path = archivesModel->getCommonParentFolder();
    else if (dialogMode == ExtractionDialogCore::DialogMode::SingleArchive)
        path = initSingle.archiveInfo.absolutePath();
    else
        Q_ASSERT(false);

    if (path.isEmpty())
        path = QDir::homePath();

    auto destinationPathCtrl =
            engine->createController<ExtractionDestinationPathCtrl>(ExtractionDialogControllerType::Type::DestinationPath,
                                                                    path);

    auto destinationErrorCtrl
            = engine->createController<ExtractionDestinationErrorCtrl>(ExtractionDialogControllerType::Type::DestinationError);

    engine->createTopController<TopFolderModeCtrl>(ExtractionDialogControllerType::Type::TopFolderMode);
    engine->createTopController<FileExtractionModeCtrl>(ExtractionDialogControllerType::Type::FileExtractionMode,
                                                        !initSingle.selectedFiles.empty());
    engine->createTopController<ExtractionFileOverwriteCtrl>(ExtractionDialogControllerType::Type::FileOverwriteMode);
    engine->createTopController<ExtractionFolderOverwriteCtrl>(ExtractionDialogControllerType::Type::FolderOverwriteMode);
    engine->createTopController<GenericPasswordCtrl>(ExtractionDialogControllerType::Type::Password,
                                                     password, acceptsPassword);
    engine->createTopController<ExtractionOpenDestinationCtrl>(ExtractionDialogControllerType::Type::OpenDestinationOnCompletion);

    std::vector<ExtractionDialogControllerType::Type> errorIds
            = {ExtractionDialogControllerType::Type::DestinationError};

    // In Single Archive mode the model is not created
    const bool archiveListEmpty = archivesModel ? archivesModel->rowCount() == 0
                                                : false;

    using DialogErrorsCtrl = GenericDialogErrorsCtrl<ExtractionDialogControllerType::Type>;
    auto dialogErrorsCtrl = engine->createController<DialogErrorsCtrl>
            (ExtractionDialogControllerType::Type::DialogErrors,
             archiveListEmpty,
             errorIds);

    if (archivesModel)
    {
        QObject::connect(archivesModel, &InputArchivesModel::modelEmpty,
                         publ, [ctrl = dialogErrorsCtrl] (bool empty)
        { ctrl->setFileListEmpty(empty); });
    }

    using AcceptButtonCtrlType = AcceptButtonCtrl<ExtractionDialogControllerType::Type>;

    auto acceptButtonCtrl = engine->createController<AcceptButtonCtrlType>
            (ExtractionDialogControllerType::Type::AcceptButton,
             ExtractionDialogControllerType::Type::DialogErrors);

    const auto runInQueueMode =
            dialogMode == ExtractionDialogCore::DialogMode::MultiArchive
            ? RunInQueueCtrl::Mode::ForcedQueue
            : RunInQueueCtrl::Mode::Switchable;

    engine->createTopController<RunInQueueCtrl>
            (ExtractionDialogControllerType::Type::RunInQueue, runInQueueMode);

    destinationModeCtrl->addDependant(destinationPathCtrl);
    destinationPathCtrl->addDependant(destinationErrorCtrl);
    destinationErrorCtrl->addDependant(dialogErrorsCtrl);
    dialogErrorsCtrl->addDependant(acceptButtonCtrl);

    engine->updateAllControllers();
}

}
