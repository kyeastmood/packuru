// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../utils/boolflag.h"

#include "creationnameerrorpromptcore.h"
#include "commonstrings.h"
#include "private/privatestrings.h"
#include "private/promptcore/creationnameerrorpromptcoreaux.h"


namespace Packuru::Core
{

using CreationNameErrorPromptCoreAux::Init;
using CreationNameErrorPromptCoreAux::Backdoor;
using CreationNameErrorPromptCoreAux::Emitter;


struct CreationNameErrorPromptCore::Private
{
    Init initData;
    Utils::BoolFlag<false> finished;
    Emitter* emitter = nullptr;
};


CreationNameErrorPromptCore::CreationNameErrorPromptCore(const Init& initData, Backdoor& backdoorData,
                                                         QObject *parent)
    : QObject(parent),
      priv(new Private)
{
    priv->initData =initData;
    priv->emitter = new Emitter(this);
    backdoorData.emitter= priv->emitter;
}


CreationNameErrorPromptCore::~CreationNameErrorPromptCore()
{
    emit deleted();
}


QString CreationNameErrorPromptCore::getString(UserString value)
{
    switch (value)
    {
    case UserString::Abort:
        return PrivateStrings::getString(PrivateStrings::UserString::Abort);
    case UserString::CannotCreateArchive:
        return Packuru::Core::CreationNameErrorPromptCore::tr("Cannot create archive");
    case UserString::DialogTitle:
        return Packuru::Core::CreationNameErrorPromptCore::tr("File name conflict");
    case UserString::Name:
        return Packuru::Core::CreationNameErrorPromptCore::tr("Name");
    case UserString::Retry:
        return PrivateStrings::getString(PrivateStrings::UserString::Retry);
    case UserString::YouCanChangeArchiveName:
        return Packuru::Core::CreationNameErrorPromptCore::tr("You can change archive name and try again.");
    default:
        Q_ASSERT(false); return "";
    }
}


void CreationNameErrorPromptCore::retry(const QString &archiveName)
{
    if (!priv->finished.set())
        return;

    emit priv->emitter->changeArchiveNameRequested(archiveName);
    emit priv->emitter->retryRequested();
}


void CreationNameErrorPromptCore::abort()
{
    if (!priv->finished.set())
        return;

    emit priv->emitter->abortRequested();
}



QString CreationNameErrorPromptCore::archiveName() const
{
    return priv->initData.archiveName;
}


QString CreationNameErrorPromptCore::errorInfo() const
{
    return priv->initData.errorInfo;
}

}
