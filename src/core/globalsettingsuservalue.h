// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

/// @file

namespace Packuru::Core
{

/// @brief Minimal value to be used as a custom key in GlobalSettingsManager.
/// @sa GlobalSettingsManager, GlobalSettingsConverter
const int globalSettingsUserValue = 1000;

}
