// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{

/**
 @brief Identifies input widgets in Test Dialog.
 @headerfile "core/testdialogcontrollertype.h"
 */
class TestDialogControllerType : public QObject
{
    Q_OBJECT
public:
    enum class Type
    {
        AcceptButton, ///< Push button.
        DialogErrors, ///< Label.
        Password, ///< Line edit.
        RunInQueue, ///< Check box.
    };
    Q_ENUM(Type)
};

PACKURU_CORE_EXPORT uint qHash(TestDialogControllerType::Type value, uint seed = 0);

}
