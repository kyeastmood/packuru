// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QSortFilterProxyModel>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{

/**
 @brief Filters archive types in %ArchiveTypeModel.

 %ArchiveTypeFilterModel object must be manually created.

 @sa ArchiveTypeModel
 @headerfile "core/archivetypefiltermodel.h"
 */
class PACKURU_CORE_EXPORT ArchiveTypeFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT
    /** @brief Holds filter's text.
     *
     * The value is stored in settings and is remembered between application invocations.
     *
     * @sa getFilterFixedString()
     */
    Q_PROPERTY(QString filterFixedString READ getFilterFixedString WRITE setFilterFixedString)

public:
    ArchiveTypeFilterModel(QObject *parent = nullptr);
    ~ArchiveTypeFilterModel() override;

    /**
     * @brief Returns filter's text.
     *
     * Set the text by calling QSortFilterProxyModel::setFilterFixedString().
     * @sa filterFixedString
     */
    Q_INVOKABLE QString getFilterFixedString() const;

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const override;
};

}
