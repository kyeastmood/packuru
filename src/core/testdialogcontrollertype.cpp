// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../utils/private/enumhash.h"

#include "testdialogcontrollertype.h"


namespace Packuru::Core
{

uint qHash(TestDialogControllerType::Type value, uint seed)
{
    return Utils::enumHash(value, seed);
}

}
