// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QSettings>

#include "archivetypefiltermodel.h"


namespace Packuru::Core
{

ArchiveTypeFilterModel::ArchiveTypeFilterModel(QObject* parent)
    : QSortFilterProxyModel(parent)
{
    setFilterCaseSensitivity(Qt::CaseInsensitive);

    QSettings s;
    const QString regex = s.value("settings_archive_type_filter_regex").toString();
    setFilterFixedString(regex);
}


ArchiveTypeFilterModel::~ArchiveTypeFilterModel()
{
    QSettings s;
    s.setValue("settings_archive_type_filter_regex", filterRegExp().pattern());
}


QString ArchiveTypeFilterModel::getFilterFixedString() const
{
    return filterRegExp().pattern();
}


bool ArchiveTypeFilterModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
{
    const QModelIndex index = sourceModel()->index(source_row, 0, source_parent);
    return sourceModel()->data(index).toString().contains(filterRegExp());
}

}
