// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.5 as Kirigami

import PackuruMobileCore 1.0 as PackuruMobileCore
import ControllerSystem.QQC 1.0 as CS_QQC

import PackuruCoreQueue 1.0
import PackuruCore 1.0


PackuruMobileCore.SettingsActivity {
    id: root

    property QtObject queueSettingsDialogCore

    onAccepted: queueSettingsDialogCore.accept()


    Component.onCompleted: {
        mapper.setEngine(queueSettingsDialogCore.getControllerEngine())

        var page = queuePageComponent.createObject(root)
        insertCategory(1, "media-playback-start", page)
    }

    Component.onDestruction: {
        queueSettingsDialogCore.destroyLater()
    }

    CS_QQC.WidgetMapper {
        id: mapper
    }

    Component {
        id: queuePageComponent

        Kirigami.Page {
            title: queueSettingsDialogCore.getString(SettingsDialogCore.PageTitle)

            ColumnLayout {
                anchors.fill: parent

                GridLayout {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    columns: 2

                    Label {
                        id: autoCloseLabel
                        Layout.alignment: Qt.AlignRight
                    }
                    CheckBox {
                        id: autoCloseCheck
                    }

                    Label {
                        id: autoRemoveCompletedLabel
                        Layout.alignment: Qt.AlignRight
                    }
                    CheckBox {
                        id: autoRemoveCompletedCheck
                    }

                    Label {
                        id: abortEncryptedLabel
                        Layout.alignment: Qt.AlignRight
                    }
                    CheckBox {
                        id: abortEncryptedCheck
                    }
                    Label {
                        id: maxErrorCountLabel
                        Layout.alignment: Qt.AlignRight
                    }
                    SpinBox {
                        id: maxErrorCountSpinBox
                    }
                    Item {
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                    }

                }

                DialogButtonBox {
                    Layout.fillWidth: true
                    Button {
                        text: commonStrings.getString(CommonStrings.Back)
                        icon.name: "go-previous"
                        onClicked: pageRow.layers.pop()
                    }
                }
            }

            Component.onCompleted: {
                mapper.addLabelAndWidget(SettingsControllerType.AbortTasksRequiringPassword,
                                         abortEncryptedLabel,
                                         abortEncryptedCheck)
                mapper.addLabelAndWidget(SettingsControllerType.AutoCloseOnAllTasksCompletion,
                                         autoCloseLabel,
                                         autoCloseCheck)
                mapper.addLabelAndWidget(SettingsControllerType.AutoRemoveCompletedTasks,
                                         autoRemoveCompletedLabel,
                                         autoRemoveCompletedCheck)
                mapper.addLabelAndWidget(SettingsControllerType.MaxErrorCount,
                                         maxErrorCountLabel,
                                         maxErrorCountSpinBox)
                mapper.updateWidgets()
            }
        }
    }
}
