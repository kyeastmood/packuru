// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import PackuruMobileCore 1.0 as PackuruMobileCore
import PackuruCore 1.0


PackuruMobileCore.AppClosingDialog {
    id: root
    property int openedDialogs: 0
    property int unfinishedTasks: 0

    infoItem: ColumnLayout {
        Label {
            text: dialogCore.getString(AppClosingDialogCore.QueueUnfinishedTasksInfo, unfinishedTasks)
            visible: unfinishedTasks > 0
        }
        Label {
            text: dialogCore.getString(AppClosingDialogCore.QueueOpenedDialogsInfo, openedDialogs)
            visible: openedDialogs > 0
        }
    }
}
