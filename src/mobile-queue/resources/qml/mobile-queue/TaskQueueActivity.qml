// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0

import org.kde.kirigami 2.5 as Kirigami

import KirigamiActivities 1.0 as KirigamiActivities


KirigamiActivities.BasicActivity {
    id: root

    signal viewTaskLogRequested(string log)

    property alias taskQueueModel: queuePage.taskQueueModel

    pageRow.globalToolBar.style: Kirigami.ApplicationHeaderStyle.Auto

    pageRow.initialPage: TaskQueuePage {
        id: queuePage
        onViewTaskLogRequested: root.viewTaskLogRequested(log)
    }


}
