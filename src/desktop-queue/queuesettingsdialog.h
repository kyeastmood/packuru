// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../desktop-core/settingsdialog.h"


namespace Packuru::Core::Queue
{
class SettingsDialogCore;
}


class QueueSettingsDialog : public SettingsDialog
{
    Q_OBJECT

public:
    explicit QueueSettingsDialog(Packuru::Core::AppSettingsDialogCore* appSettingsCore,
                                 Packuru::Core::Queue::SettingsDialogCore* settingsCore,
                                 QWidget *parent = nullptr);
    ~QueueSettingsDialog() override;
};
