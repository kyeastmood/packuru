# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)
include(../common-desktop.pri)
include(../common-exec.pri)

MODULE_BUILD_NAME_SUFFIX = $$QUEUE_BUILD_NAME_SUFFIX
MODULE_BUILD_NAME = $$QUEUE_BUILD_NAME

include(../embed-qm-files.pri)

TARGET = $$MODULE_BUILD_NAME

LIBS += -L$${PROJECT_TARGET_LIB_DIR}
LIBS += -l$$QCS_QTWIDGETS_BUILD_NAME
LIBS += -l$${CORE_BUILD_NAME} -l$${CORE_QUEUE_BUILD_NAME} -l$${DESKTOP_CORE_BUILD_NAME}

TRANSLATIONS += \
    translations/$${MODULE_BUILD_NAME_SUFFIX}_en.ts \
    translations/$${MODULE_BUILD_NAME_SUFFIX}_nl.ts \

QM_FILES_RESOURCE_PREFIX = $${PROJECT_BUILD_NAME}/qm/$${MODULE_BUILD_NAME}

HEADERS += \
    queuepasswordprompt.h \
    archivecreationnameerrorprompt.h \
    queueitemdelegate.h \
    queuesettingsdialog.h \
    queuesettingspage.h \
    queuedesktop.h \
    queuemainwindow.h
    
SOURCES += \
        main.cpp \
    queuepasswordprompt.cpp \
    archivecreationnameerrorprompt.cpp \
    queueitemdelegate.cpp \
    queuesettingsdialog.cpp \
    queuesettingspage.cpp \
    queuedesktop.cpp \
    queuemainwindow.cpp

FORMS += \
    queuepasswordprompt.ui \
    archivecreationnameerrorprompt.ui \
    queuesettingspage.ui \
    queuemainwindow.ui
