// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QMessageBox>
#include <QTimer>
#include <QPushButton>
#include <QCoreApplication>

#include "utils/makeqpointer.h"

#include "core/appclosingdialogcore.h"
#include "core/testdialogcore.h"
#include "core/archivingdialogcore.h"
#include "core/extractiondialogcore.h"
#include "core/globalsettingsmanager.h"
#include "core/commonstrings.h"

#include "core-queue/queuecore.h"
#include "core-queue/mainwindowcore.h"
#include "core-queue/taskqueuemodel.h"

#include "desktop-core/archivingdialog.h"
#include "desktop-core/extractiondialog.h"
#include "desktop-core/showcommandlinedialog.h"
#include "desktop-core/testdialog.h"
#include "desktop-core/appclosingdialog.h"

#include "queuesettingsdialog.h"
#include "queuedesktop.h"
#include "queuemainwindow.h"


using Packuru::Utils::makeQPointer;
using namespace Packuru::Core::Queue;
using namespace Packuru::Core;


QueueDesktop::QueueDesktop(Packuru::Core::Queue::QueueCore* queueCore, QObject* parent)
    : QObject(parent),
      queueCore(queueCore),
      mainWindow(new QueueMainWindow(queueCore->getMainWindowCore()))
{
    connect(queueCore, &QueueCore::defaultStartUp,
            this, &QueueDesktop::showMainWindow);
    
    connect(queueCore, &QueueCore::commandLineHelpRequested,
            this, &QueueDesktop::onCommandLineHelpRequested);
    
    connect(queueCore, &QueueCore::commandLineError,
            this, &QueueDesktop::onCommandLineError);
    
    connect(queueCore, &QueueCore::newTasks,
            this, &QueueDesktop::showMainWindow);
    
    connect(queueCore, &QueueCore::archivingDialogCoreCreated,
            this, &QueueDesktop::onArchivingDialogCoreCreated);
    
    connect(queueCore, &QueueCore::extractionDialogCoreCreated,
            this, &QueueDesktop::onExtractionDialogCoreCreated);
    
    connect(queueCore, &QueueCore::testDialogCoreCreated,
            this, &QueueDesktop::onTestDialogCoreCreated);

    connect(mainWindow.get(), &QueueMainWindow::newArchiveDialogRequested,
            queueCore, &QueueCore::createArchivingDialogCore);
    
    connect(mainWindow.get(), &QueueMainWindow::extractionDialogRequested,
            queueCore, &QueueCore::createExtractionDialogCore);
    
    connect(mainWindow.get(), &QueueMainWindow::testDialogRequested,
            queueCore, &QueueCore::createTestDialogCore);
    
    connect(mainWindow.get(), &QueueMainWindow::closeRequested,
            this, &QueueDesktop::onCloseMainWindowRequested);
    
    connect(mainWindow.get(), &QueueMainWindow::settingsRequested,
            this, &QueueDesktop::onSettingsRequested);
    
    auto mainWindowCore = queueCore->getMainWindowCore();

    connect(mainWindowCore, &MainWindowCore::allTasksCompletedReadyToClose,
            this, [queueDesktop = this, window = makeQPointer(mainWindow.get())] ()
    {
        if (queueDesktop->openedDialogs == 0)
        {
            if (window)
                window->setEnabled(false);
            QTimer::singleShot(1000, queueDesktop, &QueueDesktop::quitApplication);
        }
    });

}


QueueDesktop::~QueueDesktop()
{
    if (mainWindow)
        mainWindow->deleteLater();
}


void QueueDesktop::onCommandLineError(const QString &info)
{
    showCommandLineDialog(mainWindow.get(),
                          CommonStrings::getString(CommonStrings::UserString::CommandLineErrorDialogTitle),
                          false,
                          info);
}


void QueueDesktop::onCommandLineHelpRequested(const QString& info)
{
    showCommandLineDialog(mainWindow.get(),
                          CommonStrings::getString(CommonStrings::UserString::CommandLineHelpDialogTitle),
                          false,
                          info);
}


void QueueDesktop::onArchivingDialogCoreCreated(ArchivingDialogCore* dialogCore)
{
    auto dialog = new ArchivingDialog(dialogCore);
    dialog->setWindowTitle(dialogCore->getString(ArchivingDialogCore::UserString::DialogTitleCreate));

    connect(dialog, &QDialog::finished, &QObject::deleteLater);
    connect(dialog, &QObject::destroyed,
            this, [publ = this] ()
    {
        --publ->openedDialogs;
    });
    
    ++openedDialogs;
    dialog->show();
}


void QueueDesktop::onExtractionDialogCoreCreated(ExtractionDialogCore* dialogCore)
{
    auto dialog = new ExtractionDialog(dialogCore);
    dialog->setWindowTitle(dialogCore->getString(ExtractionDialogCore::UserString::DialogTitleMulti));

    connect(dialog, &QDialog::finished, &QObject::deleteLater);
    connect(dialog, &QObject::destroyed,
            this, [publ = this] ()
    {
        --publ->openedDialogs;
    });
    
    ++openedDialogs;
    dialog->show();
}


void QueueDesktop::onTestDialogCoreCreated(TestDialogCore* dialogCore)
{
    auto dialog = new TestDialog(dialogCore);
    dialog->setWindowTitle(dialogCore->getString(TestDialogCore::UserString::DialogTitleMulti));

    connect(dialog, &QDialog::finished, &QObject::deleteLater);
    connect(dialog, &QObject::destroyed,
            this, [publ = this] ()
    {
        --publ->openedDialogs;
    });
    
    ++openedDialogs;
    dialog->show();
}


void QueueDesktop::onCloseMainWindowRequested()
{
    const int unfinishedTaskCount = queueCore->getMainWindowCore()->getTaskQueueModel()->unfinishedTaskCount();
    
    if (unfinishedTaskCount == 0 && openedDialogs == 0)
        emit quitApplication();
    else
    {
        AppClosingDialog d(mainWindow.get());
        
        QString text;
        
        if (unfinishedTaskCount > 0)
            text = AppClosingDialogCore::getString(AppClosingDialogCore::UserString::QueueUnfinishedTasksInfo,
                                                   unfinishedTaskCount);

        if (openedDialogs > 0)
        {
            if (!text.isEmpty())
                text += "\n";
            text += AppClosingDialogCore::getString(AppClosingDialogCore::UserString::QueueOpenedDialogsInfo,
                                                    openedDialogs);
        }
        
        d.setText(text);
        d.exec();

        if (d.quitRequested())
            emit quitApplication();
    }
}


void QueueDesktop::onLastDialogClosed()
{
    if (mainWindow->isHidden())
        emit quitApplication();
}


void QueueDesktop::onSettingsRequested()
{
    QueueSettingsDialog dialog(queueCore->createAppSettingsDialogCore(),
                               queueCore->createQueueSettingsDialogCore(),
                               mainWindow.get());
    
    connect(&dialog, &QObject::destroyed,
            this, [publ = this] ()
    {
        --publ->openedDialogs;
    });
    
    ++openedDialogs;
    dialog.exec();
}


void QueueDesktop::onDefaultStartUp()
{
    mainWindow->show();
    mainWindow->raise();
    mainWindow->activateWindow();
}


void QueueDesktop::onAnotherInstanceDefaultStartUp()
{
    mainWindow->raise();
    mainWindow->activateWindow();
}


void QueueDesktop::showMainWindow()
{
    mainWindow->show();
    mainWindow->activateWindow();
    mainWindow->raise();
}
