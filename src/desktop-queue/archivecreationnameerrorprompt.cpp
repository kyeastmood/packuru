// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/creationnameerrorpromptcore.h"

#include "archivecreationnameerrorprompt.h"
#include "ui_archivecreationnameerrorprompt.h"


using namespace Packuru::Core;


ArchiveCreationNameErrorPrompt::ArchiveCreationNameErrorPrompt(CreationNameErrorPromptCore* promptCore,
                                                               QWidget *parent)
    : QDialog(parent),
      ui(new Ui::ArchiveCreationNameErrorPrompt),
      promptCore(promptCore)
{
    ui->setupUi(this);

    setWindowTitle(promptCore->getString(CreationNameErrorPromptCore::UserString::DialogTitle));
    ui->archiveNameEdit->setText(promptCore->archiveName());

    ui->headerLabel->setText(promptCore->getString(CreationNameErrorPromptCore::UserString::CannotCreateArchive)
                             + QLatin1String(" '") + promptCore->archiveName() + "'.");

    ui->abortButton->setText(promptCore->getString(CreationNameErrorPromptCore::UserString::Abort));
    ui->retryButton->setText(promptCore->getString(CreationNameErrorPromptCore::UserString::Retry));
    ui->archiveNameLabel->setText(promptCore->getString(CreationNameErrorPromptCore::UserString::Name));
    ui->youCanChangeLabel->setText(promptCore->getString(CreationNameErrorPromptCore::UserString::YouCanChangeArchiveName));

    ui->informativeTextLabel->setText(promptCore->errorInfo());

    connect(ui->retryButton, &QPushButton::clicked,
            [archiveNameEdit = ui->archiveNameEdit, promptCore = promptCore] ()
    { promptCore->retry(archiveNameEdit->text()); });

    connect(ui->abortButton, &QPushButton::clicked,
            promptCore, &CreationNameErrorPromptCore::abort);

    connect(this, &QDialog::rejected,
            promptCore, &CreationNameErrorPromptCore::abort);

    connect(ui->archiveNameEdit, &QLineEdit::textChanged,
            [button = ui->retryButton] (const auto& text)
    { button->setEnabled(!text.isEmpty()); });

    connect(promptCore, &QObject::destroyed,
            this, &QObject::deleteLater);
}


ArchiveCreationNameErrorPrompt::~ArchiveCreationNameErrorPrompt()
{
    delete ui;
}
