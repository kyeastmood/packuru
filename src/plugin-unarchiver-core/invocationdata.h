// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QStringList>


namespace Packuru::Plugins::Unarchiver::Core
{

struct InvocationData
{
    QString executableName;
    QStringList options;
    QString archiveFilePath;
    QStringList items;
    QString password;
};

}
