// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QRegularExpression>
#include <QVector>
#include <QDirIterator>
#include <QStandardPaths>

#include "../core/private/plugin-api/textstreampreprocessor.h"
#include "../core/private/plugin-api/backendtestdata.h"
#include "../core/private/plugin-api/backendextractiondata.h"
#include "../core/private/plugin-api/backendreaddata.h"
#include "../core/private/plugin-api/backendarchiveproperty.h"
#include "../core/private/plugin-api/backendexitcodeinterpretation.h"
#include "../core/private/plugin-api/encryptionstate.h"
#include "../core/private/plugin-api/multipartarchivehelper.h"
#include "../core/private/plugin-api/diskspacewatcher.h"

#include "invocationdata.h"
#include "backendhandler.h"


using namespace Packuru::Core;


namespace Packuru::Plugins::Unarchiver::Core
{

namespace
{

int advanceToNonSpace(QStringRef line, int start);
qulonglong extractSize(QStringRef line);
void extractAttributes(ArchiveItem& item, QStringRef line, int startPos);

}


BackendHandler::BackendHandler(QObject* parent)
    : CLIBackendHandler(parent),
      archiveType(ArchiveType::___NOT_SUPPORTED)
{

}


bool BackendHandler::executableAvailable()
{
    return !QStandardPaths::findExecutable("lsar").isEmpty()
            && !QStandardPaths::findExecutable("unar").isEmpty();
}


void BackendHandler::enterPassword(const QString& password)
{
    Q_ASSERT(!password.isEmpty());
    lastInvocationData.password = password;
    startBackend(lastInvocationData);
}


void BackendHandler::readArchiveImpl(const BackendReadData& data)
{
    archiveFilePath = data.archiveInfo.absoluteFilePath();
    archiveType = data.archiveType;

    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &BackendHandler::onListProcessFinished);

    connect(outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::listHandler);

    connect(errPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::errStreamErrorLineHandler);

    InvocationData invocationData;
    invocationData.executableName = "lsar";
    invocationData.options << "-L";
    invocationData.archiveFilePath = data.archiveInfo.absoluteFilePath();
    /* Password should not be provided in InvocationData when reading archive because it is then
       not possible to distinguish archives with encrypted file names from archives with encrypted
       file content only. Instead helper variable is used and password is entered when needed
       which then means that file names are encrypted. */
    readPassword = data.password;

    startBackend(invocationData);
}


void BackendHandler::extractArchiveImpl(const BackendExtractionData& data)
{
    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, [hnd = this,tempDir = data.tempDestinationPath] (int exitCode, QProcess::ExitStatus exitStatus)
    { hnd->onExtractionProcessFinished(exitCode, exitStatus, tempDir); });

    connect(outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::outStreamErrorLineHandler);

    connect(errPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::errStreamErrorLineHandler);

    InvocationData invocationData;
    invocationData.executableName = "unar";
    invocationData.options << QLatin1String("-o")<< data.tempDestinationPath;

    /* This handles the case of multiple identical paths inside an archive.
       For formats that can store multiple versions of the same file we favor the option
       to overwrite everything so the last file version added to the archive will overwrite
       all previous versions during extraction. Same thing when extracting files fore preview.
       The downside is that if an archive contains two identical paths eg. dir1/dir2/item
       but the first is a directory and the second is a file there will be a 'Type mismatch' error.
       There is no perfect solution since for extraction's behavior uniformity sake
       everything is extracted to a temp dir and in plugins we don't handle backend
       overwrite prompts. */
    if (data.extractionForPreview || isTar(data.archiveType))
    {
        invocationData.options.push_back(QLatin1String("-f"));
    }
    else
    {
        /* The remaining formats do not store multiple versions of the files but still can have
           same paths with different types (dir/file). This is not a problem on systems that
           allow identical names for files and dirs (e.g. Windows) so we set the option to
           overwrite everything on these systems but there should not be really anything to overwrite in the
           temp dir. For Unix we set the option to automatically rename the items that are
           extracted from the archive.
           UPDATE: It seems that The Unarchiver will fail with 'Type mismatch' error anyway
           even when set to rename new files. */
#ifdef Q_OS_UNIX
        invocationData.options.push_back(QLatin1String("-r"));
#else
        invocationData.options.push_back(QLatin1String("-f"));
#endif
    }

    invocationData.options << "-D"; // Do not create containing directory
    invocationData.archiveFilePath = data.archiveInfo.absoluteFilePath();
    invocationData.password = data.password;

    /* File paths passed to The Unarchiver must be stripped of the leading '/' or './',
       even if they are absolute and were printed that way by lsar, and they will be correctly extracted.
       However when there are two equal paths, except that the first is absolute (/dir1/dir2/file)
       and the second is relative (dir1/dir2/file), the one that is extracted last will stay on disk. */
    for (const auto& item : data.filesToExtract)
    {
        auto path = item;

        if (path.startsWith(QLatin1Char('/')))
            path.remove(0, 1);
        else if (path.startsWith(QLatin1String("./")))
            path.remove(0, 2);

        // To extract dirs the paths must look like 'dir1/dir2/dir3/*'
        if (path.endsWith(QLatin1Char('/')))
            path.append(QLatin1Char('*'));

        invocationData.items.push_back(path);
    }

    startBackend(invocationData);
}


void BackendHandler::testArchiveImpl(const BackendTestData& data)
{
    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &BackendHandler::onProcessFinished);

    connect(outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::outStreamErrorLineHandler);

    connect(errPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::errStreamErrorLineHandler);

    InvocationData invocationData;
    invocationData.executableName = "lsar";
    invocationData.archiveFilePath = data.archiveInfo.absoluteFilePath();
    invocationData.password = data.password;
    invocationData.options << "-t";

    startBackend(invocationData);
}


void BackendHandler::startBackend(const InvocationData& data)
{
    changeState(BackendHandlerState::InProgress);

    lastInvocationData = data;

    logCommand(data.executableName, composeCommand(data, false));

    const auto args = composeCommand(data, true);

    backendProcess->start(data.executableName, args);
}


QStringList BackendHandler::composeCommand(const InvocationData& data, bool includePassword) const
{
    auto args = QStringList() << data.options;

    if (!data.password.isEmpty())
    {
        if (includePassword)
            args << QLatin1String("-p") << data.password;
        else
            args << QLatin1String("-p") << QLatin1String("*");
    }

    args << data.archiveFilePath << data.items;

    return args;
}


void BackendHandler::onProcessFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    Q_UNUSED(exitStatus)

    auto intepretation = BackendExitCodeInterpretation::Success;

    if (passwordRequired && lastInvocationData.password.isEmpty())
        intepretation = BackendExitCodeInterpretation::PasswordRequiredNotProvided;
    else if (exitCode != 0)
        intepretation = BackendExitCodeInterpretation::WarningsOrErrors;

    const BackendHandlerState state = handleFinishedProcess(intepretation);

    changeState(state);
}


void BackendHandler::onListProcessFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    if (passwordRequired && lastInvocationData.password.isEmpty() && !readPassword.isEmpty())
    {
        lastInvocationData.password = readPassword;
        readPassword.clear();
        return startBackend(lastInvocationData);
    }

    if (!tempItem.name.isEmpty())
    {
        items.emplace_back(std::move(tempItem));
        emit newItems(items);
    }

    auto encryptionState = EncryptionState::Disabled;

    if (!lastInvocationData.password.isEmpty())
        encryptionState = EncryptionState::EntireArchive;
    else if (encryptedFiles)
        encryptionState = EncryptionState::FilesContentOnly;

    archiveProperties[BackendArchiveProperty::EncryptionState].setValue(encryptionState);

    if (!methodsSet.empty())
    {
        auto& methods = archiveProperties[BackendArchiveProperty::Methods];
        auto set = methods.value<Utils::QStringUnorderedSet>();
        set.insert(methodsSet.begin(), methodsSet.end());

        if (archiveType == ArchiveType::zip && encryptedFiles)
        {
            if (set.find("AES") == set.end())
                set.insert("ZipCrypto");
        }

        methods.setValue(set);
    }

    auto it = archiveProperties.find(BackendArchiveProperty::PartCount);
    if (it != archiveProperties.end())
    {
        auto partCount = it->second.toInt();
        if (partCount > 1)
            archiveProperties[BackendArchiveProperty::Multipart] = true;

        archiveProperties[BackendArchiveProperty::TotalSize] = computeArchiveSize(archiveFilePath,
                                                                                  archiveType,
                                                                                  partCount);
    }

    if (archiveType == ArchiveType::_7z || archiveType == ArchiveType::rar)
    {
        archiveProperties[BackendArchiveProperty::Solid] = isSolid;
    }

    if (!archiveComment.isEmpty())
    {
        archiveComment.chop(1); // Remove final '\n'
        archiveProperties[BackendArchiveProperty::Comment] = archiveComment;
    }

    onProcessFinished(exitCode, exitStatus);
}


void BackendHandler::onExtractionProcessFinished(int exitCode,
                                                 QProcess::ExitStatus exitStatus,
                                                 const QString& tempDestinationPath)
{
    Q_UNUSED(exitStatus)
    Q_ASSERT(!tempDestinationPath.isEmpty());

    auto intepretation = BackendExitCodeInterpretation::Success;

    if (passwordRequired && lastInvocationData.password.isEmpty())
        intepretation = BackendExitCodeInterpretation::PasswordRequiredNotProvided;
    else if (exitCode != 0)
        intepretation = BackendExitCodeInterpretation::WarningsOrErrors;

    const BackendHandlerState state = handleFinishedProcess(intepretation);

    /* When extracting an archive with content only encryption in non interactive mode unar creates
       an empty file on disk and then reports that the archive requires password. Before we ask the
       user for password and start extraction again we have to remove this empty file together with
       its parent path, otherwise unar will rename the newly extracted file or folder.
       It is CRITICALLY IMPORTANT to ensure that the temp dir is empty before starting extraction
       to avoid deleting user files - this is what AbstractBackendHandler::extractArchive() does.*/
    if (state == BackendHandlerState::WaitingForPassword && !tempDestinationPath.isEmpty())
    {
        QDir tempDir(tempDestinationPath);
        tempDir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::Hidden);

        if (!tempDir.isEmpty())
        {
            QDirIterator it(tempDestinationPath, {}, QDir::AllEntries | QDir::Hidden | QDir::NoDotAndDotDot);

            while (it.hasNext())
            {
                const QFileInfo info(it.next());

                if (info.isDir())
                {
                    QDir dir(info.absoluteFilePath());
                    dir.removeRecursively();
                }
                else
                {
                    tempDir.remove(info.absoluteFilePath());
                }
            }
        }
    }

    changeState(state);
}


void BackendHandler::listHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        int valueStart = 0;
        int temp = 0;

        if ((temp = lineStartsWith(line, {QLatin1String("  Name:")})))
        {
            if (listingStarted)
                items.push_back(std::move(tempItem));
            else
                listingStarted = true;

            valueStart = advanceToNonSpace(line, temp);

            tempItem.name = line.right(line.size() - valueStart).toString();

            /* In fully encrypted archives the folders are not marked as encrypted
               (even though in reality they are). So we mark them as encrypted because
               it allows to quickly recognize the archive as fully encrypted without
               checking its properties. */
            if (!lastInvocationData.password.isEmpty())
                tempItem.encrypted = true;
        }
        else if (line.startsWith(QLatin1String("  Is a directory:")))
        {
            tempItem.nodeType = NodeType::Folder;
        }
        else if (line.startsWith(QLatin1String("  Is a link:")))
        {
            tempItem.nodeType = NodeType::Link;
        }
        else if (line.startsWith(QLatin1String("  Size:")))
        {
            tempItem.originalSize = extractSize(line);
        }
        else if (line.startsWith(QLatin1String("  Compressed size:")))
        {
            tempItem.packedSize = extractSize(line);
        }
        else if ((temp = lineStartsWith(line, {QLatin1String("  Last modified:")})))
        {
            valueStart = advanceToNonSpace(line, temp);
            const auto str = line.mid(valueStart, line.size() - valueStart).toString();
            tempItem.modified = QDateTime::fromString(str, Qt::ISODate);
        }
        else if ((temp = lineStartsWith(line, {QLatin1String("  Last accessed:")})))
        {
            valueStart = advanceToNonSpace(line, temp);
            const auto str = line.mid(valueStart, line.size() - valueStart).toString();
            tempItem.accessed = QDateTime::fromString(str, Qt::ISODate);
        }
        else if ((temp = lineStartsWith(line, {QLatin1String("  Created:")})))
        {
            valueStart = advanceToNonSpace(line, temp);
            const auto str = line.mid(valueStart, line.size() - valueStart).toString();
            tempItem.created = QDateTime::fromString(str, Qt::ISODate);
        }
        else if ((temp = lineStartsWith(line, {QLatin1String("  Comment:")})))
        {
            valueStart = advanceToNonSpace(line, temp);
            tempItem.comment = line.mid(valueStart, line.size() - valueStart).toString();
        }

        else if ((temp = lineStartsWith(line, {
                                        QLatin1String("  Unix permissions:"),
                                        QLatin1String("  DOS file attributes:"),
                                        QLatin1String("  Windows file attributes:")})))
        {
            valueStart = advanceToNonSpace(line, temp);
            extractAttributes(tempItem, line, valueStart);
        }
        // For zip archives compression type is set in "ZipCompressionMethod" string handler.
        else if (archiveType != ArchiveType::zip
                 && (temp = lineStartsWith(line, {QLatin1String("  Compression type:")})))
        {
            valueStart = advanceToNonSpace(line, temp);

            const auto methodStrRef = line.right(line.size() - valueStart);
            const auto methods = methodStrRef.split(QLatin1Char('+'));
            for (const auto& strRef : methods)
                methodsSet.insert(strRef.toString());

            tempItem.method = methodStrRef.toString();
        }
        else if (line.startsWith(QLatin1String("  Is encrypted:")))
        {
            tempItem.encrypted = true;
            encryptedFiles = true;
        }
        else if (!isSolid && line.startsWith(QLatin1String("  Is a solid archive file:")))
        {
            isSolid = true;
        }
        else if ((temp = lineStartsWith(line, {
                                        QLatin1String("  ZipCRC32:"),
                                        QLatin1String("  7zCRC32:"),
                                        QLatin1String("  RARCRC32:"),
                                        QLatin1String("  RAR5CRC32:")})))
        {
            valueStart = advanceToNonSpace(line, temp);
            valueStart += 2;
            tempItem.checksum = line.right(line.size() - valueStart).toString().toUpper();
        }
        else if ((temp = lineStartsWith(line, {
                                        QLatin1String("  ZipOSName:"),
                                        QLatin1String("  RAROSName:"),
                                        QLatin1String("  RAR5OSName:")})))
        {
            valueStart = advanceToNonSpace(line, temp);
            tempItem.hostOS = line.right(line.size() - valueStart).toString();
        }
        else if ((temp = lineStartsWith(line, {QLatin1String("  Unix user name:")})))
        {
            valueStart = advanceToNonSpace(line, temp);
            tempItem.user = line.right(line.size() - valueStart).toString();
        }
        else if ((temp = lineStartsWith(line, {QLatin1String("  Unix group name:")})))
        {
            valueStart = advanceToNonSpace(line, temp);
            tempItem.group = line.right(line.size() - valueStart).toString();
        }
        else if ((temp = lineStartsWith(line, {QLatin1String("  TARIsSparseFile:")})))
        {
            valueStart = advanceToNonSpace(line, temp);
            const bool isSparse = line.right(line.size() - valueStart) == QLatin1String("Yes");

            if (isSparse)
                tempItem.sparseState = SparseFileState::Sparse;
            else
                tempItem.sparseState = SparseFileState::NonSparse;
        }
        else if (line.startsWith(archiveFilePath + QLatin1Char(':'))
                 && line.endsWith(QLatin1String("volumes)")))
        {
            int index = line.lastIndexOf(QLatin1Char('('));
            ++index;
            if (index > 0)
            {
                QString temp;
                while (line[index].isDigit() && index < line.size())
                    temp += line[index++];
                bool ok = false;
                const int count = temp.toInt(&ok);

                if (ok)
                    archiveProperties[BackendArchiveProperty::PartCount] = count;
            }
        }
        else if ((temp = lineStartsWith(line, {
                                        QLatin1String("  ZipCompressionMethod:"),
                                        QLatin1String("  WinZipAESCompressionMethod:")})))
        {
            valueStart = advanceToNonSpace(line, temp);
            const auto methodId = line.right(line.size() - valueStart).toInt();
            tempItem.method = zipCompressionMethodName(methodId);
            methodsSet.insert(tempItem.method);
        }
        else if (line.startsWith("Archive comment:"))
        {
            processingArchiveComment = true;
        }
        else if (processingArchiveComment)
        {
            archiveComment += line + "\n";
        }
        else
            checkLineForError(line);
    }

    if (!items.empty())
        emit newItems(items);

    items.clear();
}


void BackendHandler::checkLineForError(QStringRef line)
{
    auto error = BackendHandlerError::___INVALID;

    // Often empty file
    if (line.endsWith(QLatin1String(": Couldn't recognize the archive format.")))
        error = BackendHandlerError::DataError;

    else if (line.endsWith(QLatin1String("Archive parsing failed! (Attempted to read more data than was available.)"))
             || line.endsWith(QLatin1String("Archive parsing failed! (Error on decrunching.)"))
             || line.endsWith(QLatin1String("Archive parsing failed! (Data is corrupted.)"))
             || line.endsWith(QLatin1String("Archive parsing failed! (Missing or wrong password.)"))
             || line.endsWith(QLatin1String("Archive parsing failed! (Unknown error.)"))
             || line.endsWith(QLatin1String("Checksum failed!"))
             || line.endsWith(QLatin1String("Failed! (Attempted to read more data than was available)"))
             || line.endsWith(QLatin1String("Failed! (Error on decrunching)"))
             || line.endsWith(QLatin1String("Failed! (Missing or wrong password)"))
             || line.endsWith(QLatin1String("Failed! (Unknown error)"))
             || line.endsWith(QLatin1String("Failed! (Wrong checksum)"))
             || line.endsWith(QLatin1String("Listing failed! (Attempted to read more data than was available.)"))
             || line.endsWith(QLatin1String("Unpacking failed!"))
             || line.endsWith(QLatin1String("Wrong password!")))
    {
        if (!lastInvocationData.password.isEmpty())
            error = BackendHandlerError::InvalidPasswordOrDataError;
        else
            error = BackendHandlerError::DataError;
    }
    // * When trying to open an item as a directory but it's a file
    // * Or it can mean 'Access denied'; unlikely, as it's checked in TaskWorker
    // * Or disk full
    else if (line.endsWith(QLatin1String("... Failed! (Could not create directory)")))
    {
        if (hasError(BackendHandlerError::DiskFull))
            addLogLine(line);
        else
        {
            diskWatcher->refresh();
            if (diskWatcher->diskFullDetected())
                addLogLine(line);
            else
                error = BackendHandlerError::WriteError;
        }
    }
    // When trying to write a file over existing directory (most likely)
    else if (line.endsWith(QLatin1String("... Failed! (Opening file failed)")))
    {
        error = BackendHandlerError::AccessDenied;
    }
    else if (line.endsWith(QLatin1String("... Failed! (Failed to write to file)")))
    {
        if (hasError(BackendHandlerError::DiskFull))
            addLogLine(line);
        else
        {
            diskWatcher->refresh();
            if (diskWatcher->diskFullDetected())
                addLogLine(line);
            else
                error = BackendHandlerError::WriteError;
        }
    }
    else if (line.endsWith(QLatin1String("... Unsupported!"))
             // When trying to extract sitx archive
             || line.endsWith(QLatin1String("... Failed! (File is not fully supported)")))
        error = BackendHandlerError::ArchivingMethodNotSupported;
    else if (line.endsWith(QLatin1String("This archive requires a password to unpack. Use the -p option to provide one.")))
        passwordRequired = true;

    if (error != BackendHandlerError::___INVALID)
        addError(error, line);
}


void BackendHandler::outStreamErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        checkLineForError(line);
    }
}


void BackendHandler::errStreamErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if  (line.startsWith(QLatin1String("This archive requires a password to unpack.")))
        {
            if (lastInvocationData.password.isEmpty())
                passwordRequired = true;
            else
                addError(BackendHandlerError::InvalidPasswordOrDataError, line);
        }
    }
}


int BackendHandler::lineStartsWith(QStringRef line, std::initializer_list<QLatin1String> strings)
{
    for (const auto& string : strings)
    {
        if (line.startsWith(string))
            return string.size();
    }

    return 0;
}


QLatin1String BackendHandler::zipCompressionMethodName(int id) const
{
    switch (id)
    {
    case 0: return QLatin1String("Store");
    case 8: return QLatin1String("Deflate");
    case 9: return QLatin1String("Deflate64");
    case 12: return QLatin1String("Bzip2");
    case 14: return QLatin1String("LZMA");
    case 19: return QLatin1String("LZ77");
    case 97: return QLatin1String("WavPack");
    case 98: return QLatin1String("PPMd");
    case 99: return QLatin1String("AES");
    default: return QLatin1String(QString::number(id).toStdString().c_str());
    }
}


namespace
{

int advanceToNonSpace(QStringRef line, int start)
{
    while (start < line.size() && line[start] == QLatin1Char(' '))
        ++start;

    return start;
}


qulonglong extractSize(QStringRef line)
{
    int valueEnd = line.lastIndexOf(QLatin1String("bytes"));
    --valueEnd;
    int valueStart = 0;
    for (auto pos = valueEnd - 1; pos >= 0; --pos)
    {
        if (line[pos] == QLatin1Char(' ') || line[pos] == QLatin1Char('('))
        {
            valueStart = pos + 1;
            break;
        }
    }

    return line.mid(valueStart, valueEnd - valueStart).toString().remove(QLatin1Char(',')).toULongLong();
}


void extractAttributes(ArchiveItem& item, QStringRef line, int startPos)
{
    int valueEnd = line.lastIndexOf(QLatin1Char('('));
    --valueEnd;
    if (!item.attributes.isEmpty())
        item.attributes.append(QLatin1Char(' '));
    item.attributes += line.mid(startPos, valueEnd - startPos).toString();
}

}

}
