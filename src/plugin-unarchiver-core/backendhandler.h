// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QProcess>

#include "../utils/private/qstringunorderedset.h"

#include "../core/private/plugin-api/clibackendhandler.h"

#include "invocationdata.h"


namespace Packuru::Core
{
enum class ArchiveType;
enum class BackendHandlerError;
}

namespace Packuru::Plugins::Unarchiver::Core
{

class BackendHandler : public Packuru::Core::CLIBackendHandler
{
public:
    BackendHandler(QObject* parent = nullptr);

    static bool executableAvailable();

    void enterPassword(const QString& password) override;

private:
    void readArchiveImpl(const Packuru::Core::BackendReadData& data) override;
    void extractArchiveImpl(const Packuru::Core::BackendExtractionData& data) override;
    void testArchiveImpl(const Packuru::Core::BackendTestData& data) override;

    void startBackend(const InvocationData& data);
    QStringList composeCommand(const InvocationData& data, bool includePassword) const;
    void onProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onListProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onExtractionProcessFinished(int exitCode,
                                     QProcess::ExitStatus exitStatus,
                                     const QString& tempDestinationPath);

    void listHandler(const std::vector<QStringRef>& lines);
    void checkLineForError(QStringRef line);
    void outStreamErrorLineHandler(const std::vector<QStringRef>& lines);
    void errStreamErrorLineHandler(const std::vector<QStringRef>& lines);

    int lineStartsWith(QStringRef line, std::initializer_list<QLatin1String> strings);
    QLatin1String zipCompressionMethodName(int id) const;

    bool listingStarted = false;
    Packuru::Core::ArchiveItem tempItem;
    QString archiveFilePath;
    Packuru::Core::ArchiveType archiveType;
    InvocationData lastInvocationData;
    bool passwordRequired = false;
    QString readPassword;
    bool encryptedFiles = false;
    bool isSolid = false;
    bool processingArchiveComment = false;
    QString archiveComment;
    Packuru::Utils::QStringUnorderedSet methodsSet;
};

}
