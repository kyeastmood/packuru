// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QQmlEngine>

#include "../qcs-qqcontrols/registertypes.h"

#include "../core/creationnameerrorpromptcore.h"
#include "../core/passwordpromptcore.h"
#include "../core/extractionfileoverwritepromptcore.h"
#include "../core/extractionfolderoverwritepromptcore.h"
#include "../core/extractiontypemismatchpromptcore.h"
#include "../core/extractiondialogcore.h"
#include "../core/extractiondialogcontrollertype.h"
#include "../core/archivingdialogcore.h"
#include "../core/archivingdialogcontrollertype.h"
#include "../core/testdialogcore.h"
#include "../core/testdialogcontrollertype.h"
#include "../core/appsettingsdialogcore.h"
#include "../core/appsettingscontrollertype.h"
#include "../core/plugindatamodel.h"
#include "../core/archivetypemodel.h"
#include "../core/archivetypefiltermodel.h"
#include "../core/commonstrings.h"
#include "../core/appclosingdialogcore.h"
#include "../core/aboutdialogcore.h"

#include "registertypes.h"
#include "private/aux.h"


using namespace Packuru::Core;

namespace Packuru::Mobile::Core {

void registerTypes()
{
    qcs::qqc::registerTypes();

    const QString qmlDir(MOBILE_CORE_QML_DIR);

    qmlRegisterType(QUrl(qmlDir + "/KirigamiActivities/AbstractActivity.qml"), "KirigamiActivities", 1, 0, "AbstractActivity");
    qmlRegisterType(QUrl(qmlDir + "/KirigamiActivities/BasicActivity.qml"), "KirigamiActivities", 1, 0, "BasicActivity");
    qmlRegisterType(QUrl(qmlDir + "/KirigamiActivities/SwitchableActivity.qml"), "KirigamiActivities", 1, 0, "SwitchableActivity");

    qmlRegisterType(QUrl(qmlDir + "/Dialogs/AboutDialog.qml"), "PackuruMobileCore", 1, 0, "AboutDialog");
    qmlRegisterType(QUrl(qmlDir + "/Dialogs/AppClosingDialog.qml"), "PackuruMobileCore", 1, 0, "AppClosingDialog");
    qmlRegisterType(QUrl(qmlDir + "/Dialogs/ArchiveTaskDialog.qml"), "PackuruMobileCore", 1, 0, "ArchiveTaskDialog");
    qmlRegisterType(QUrl(qmlDir + "/Dialogs/ArchiveTaskDialogListView.qml"), "PackuruMobileCore", 1, 0, "ArchiveTaskDialogListView");
    qmlRegisterType(QUrl(qmlDir + "/Dialogs/ArchivingDialog.qml"), "PackuruMobileCore", 1, 0, "ArchivingDialog");
    qmlRegisterType(QUrl(qmlDir + "/Dialogs/AutoResizableDialog.qml"), "PackuruMobileCore", 1, 0, "AutoResizableDialog");
    qmlRegisterType(QUrl(qmlDir + "/Dialogs/DialogComboBox.qml"), "PackuruMobileCore", 1, 0, "DialogComboBox");
    qmlRegisterType(QUrl(qmlDir + "/Dialogs/ExtractionDialog.qml"), "PackuruMobileCore", 1, 0, "ExtractionDialog");
    qmlRegisterType(QUrl(qmlDir + "/Dialogs/ExtractionFileOverwritePrompt.qml"), "PackuruMobileCore", 1, 0, "ExtractionFileOverwritePrompt");
    qmlRegisterType(QUrl(qmlDir + "/Dialogs/ExtractionFolderOverwritePrompt.qml"), "PackuruMobileCore", 1, 0, "ExtractionFolderOverwritePrompt");
    qmlRegisterType(QUrl(qmlDir + "/Dialogs/ExtractionTypeMismatchPrompt.qml"), "PackuruMobileCore", 1, 0, "ExtractionTypeMismatchPrompt");
    qmlRegisterType(QUrl(qmlDir + "/Dialogs/InformationDialog.qml"), "PackuruMobileCore", 1, 0, "InformationDialog");
    qmlRegisterType(QUrl(qmlDir + "/Dialogs/PasswordField.qml"), "PackuruMobileCore", 1, 0, "PasswordField");
    qmlRegisterType(QUrl(qmlDir + "/Dialogs/TabWidget.qml"), "PackuruMobileCore", 1, 0, "TabWidget");
    qmlRegisterType(QUrl(qmlDir + "/Dialogs/TestDialog.qml"), "PackuruMobileCore", 1, 0, "TestDialog");
    qmlRegisterType(QUrl(qmlDir + "/SettingsActivity.qml"), "PackuruMobileCore", 1, 0, "SettingsActivity");

    qmlRegisterUncreatableType<AppSettingsControllerType>("PackuruCore", 1, 0, "AppSettingsControllerType", "");

    qmlRegisterUncreatableType<CreationNameErrorPromptCore>("PackuruCore", 1, 0, "CreationNameErrorPromptCore", "");
    qmlRegisterUncreatableType<PasswordPromptCore>("PackuruCore", 1, 0, "PasswordPromptCore", "");
    qmlRegisterUncreatableType<ExtractionFileOverwritePromptCore>("PackuruCore", 1, 0, "ExtractionFileOverwritePromptCore", "");
    qmlRegisterUncreatableType<ExtractionFolderOverwritePromptCore>("PackuruCore", 1, 0, "ExtractionFolderOverwritePromptCore", "");
    qmlRegisterUncreatableType<ExtractionTypeMismatchPromptCore>("PackuruCore", 1, 0, "ExtractionTypeMismatchPromptCore", "");
    qmlRegisterUncreatableType<ArchivingDialogCore>("PackuruCore", 1, 0, "ArchivingDialogCore", "");
    qmlRegisterUncreatableType<ArchivingDialogControllerType>("PackuruCore", 1, 0, "ArchivingDialogControllerType", "");
    qmlRegisterUncreatableType<ExtractionDialogCore>("PackuruCore", 1, 0, "ExtractionDialogCore", "");
    qmlRegisterUncreatableType<ExtractionDialogControllerType>("PackuruCore", 1, 0, "ExtractionDialogControllerType", "");
    qmlRegisterUncreatableType<TestDialogCore>("PackuruCore", 1, 0, "TestDialogCore", "");
    qmlRegisterUncreatableType<TestDialogControllerType>("PackuruCore", 1, 0, "TestDialogControllerType", "");
    qmlRegisterUncreatableType<AppSettingsDialogCore>("PackuruCore", 1, 0, "AppSettingsDialogCore", "");
    qmlRegisterUncreatableType<PluginDataModel>("PackuruCore", 1, 0, "PluginDataModel", "");
    qmlRegisterUncreatableType<ArchiveTypeModel>("PackuruCore", 1, 0, "ArchiveTypeModel", "");
    qmlRegisterUncreatableType<CommonStrings>("PackuruCore", 1, 0, "CommonStrings", "");
    qmlRegisterType<AppClosingDialogCore>("PackuruCore", 1, 0, "AppClosingDialogCore");
    qmlRegisterType<AboutDialogCore>("PackuruCore", 1, 0, "AboutDialogCore");
    qmlRegisterType<ArchiveTypeFilterModel>("PackuruCore", 1, 0, "ArchiveTypeFilterModel");
    qmlRegisterSingletonType<Aux>("PackuruCore", 1, 0, "Aux", auxSingletonProvider);
}

}
