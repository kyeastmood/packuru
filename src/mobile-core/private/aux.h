// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>


class QQmlEngine;
class QJSEngine;


class Aux : public QObject
{
    Q_OBJECT
public:
    explicit Aux(QObject *parent = nullptr);

    Q_INVOKABLE bool containsUrlType(const QVariant& v) const;
    Q_INVOKABLE QString htmlLink(const QString& address, const QString& label) const;
    Q_INVOKABLE QString htmlLink(const QString& address) const;
};


static QObject* auxSingletonProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    return new Aux();
}
