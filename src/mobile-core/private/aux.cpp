// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "utils/qvariant_utils.h"

#include "core/htmllink.h"

#include "aux.h"


Aux::Aux(QObject *parent) : QObject(parent)
{

}


bool Aux::containsUrlType(const QVariant& v) const
{
    return Packuru::Utils::containsType<QUrl>(v);
}


QString Aux::htmlLink(const QString& address, const QString& label) const
{
    return Packuru::Core::htmlLink(address, label);
}


QString Aux::htmlLink(const QString& address) const
{
    return Packuru::Core::htmlLink(address);
}
