// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "symbol_export.h"


namespace Packuru::Mobile::Core
{

PACKURU_MOBILE_CORE_EXPORT void registerTypes();

}
