// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

FocusScope {
    id: root
    implicitHeight: rootLayout.implicitHeight
    implicitWidth: rootLayout.implicitWidth
    property alias tabBar: tabBar
    property alias swipeView: swipeView
    default property alias content: swipeView.contentItem

    ColumnLayout {
        id: rootLayout
        anchors.fill: parent
        TabBar {
            id: tabBar
            Layout.fillWidth: true
            onCurrentIndexChanged: swipeView.setCurrentIndex(currentIndex)
            visible: swipeView.count > 1
            Repeater {
                id: buttonFactory
                model: swipeView.count
                TabButton {
                    text: swipeView.contentChildren[index].title
                    visible: swipeView.contentChildren[index].visible
                    width: implicitWidth
                }
            }
        }

        SwipeView {
            id: swipeView
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            focus: root.focus
            onCurrentIndexChanged: tabBar.setCurrentIndex(currentIndex)
            property int pageMaxImplicitWidth: 0
            property int pageMaxImplicitHeight: 0
            implicitHeight: pageMaxImplicitHeight
            implicitWidth: pageMaxImplicitWidth
            contentItem.focus: true

            function addPage(page) {
                if (page.implicitWidth > pageMaxImplicitWidth)
                    pageMaxImplicitWidth = page.implicitWidth

                page.onImplicitWidthChanged.connect(recomputeImplicitWidth)

                if (page.implicitHeight > pageMaxImplicitHeight)
                    pageMaxImplicitHeight = page.implicitHeight

                page.onImplicitHeightChanged.connect(recomputeImplicitHeight)

                addItem(page)
            }

            function recomputeImplicitWidth() {
                var maxImplicitWidth = 0
                for (var i = 0; i < contentChildren.length; ++i) {
                    if (contentChildren[i].implicitWidth > maxImplicitWidth)
                        maxImplicitWidth = contentChildren[i].implicitWidth
                }
                pageMaxImplicitWidth = maxImplicitWidth
            }

            function recomputeImplicitHeight() {
                var maxImplicitHeight = 0
                for (var i = 0; i < contentChildren.length; ++i) {
                    if (contentChildren[i].implicitHeight > maxImplicitHeight)
                        maxImplicitHeight = contentChildren[i].implicitHeight
                }
                pageMaxImplicitHeight = maxImplicitHeight
            }
        }
    }
}
