// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.6 as Kirigami

import PackuruCore 1.0


FocusScope{
    id: root
    implicitHeight: col.implicitHeight
    implicitWidth: col.implicitWidth
    property QtObject promptCore
    property alias headerVisible: headerLabel.visible
    property int maxContentWidth: 0

    Connections {
        target: promptCore
        function onDeleted() {
            root.destroy()
        }
    }

    QtObject {
        id: priv
        readonly property string file: promptCore ? promptCore.getString(ExtractionFileOverwritePromptCore.File) : ""
        readonly property string location: promptCore ? promptCore.getString(ExtractionFileOverwritePromptCore.Location) : ""
        readonly property string modified: promptCore ? promptCore.getString(ExtractionFileOverwritePromptCore.Modified) : ""
        readonly property string size: promptCore ? promptCore.getString(ExtractionFileOverwritePromptCore.Size) : ""
    }

    ColumnLayout{
        id: col
        spacing: 10
        anchors.horizontalCenter: parent.horizontalCenter

        Label {
            id: headerLabel
            text: promptCore ? promptCore.getString(ExtractionFileOverwritePromptCore.FileAlreadyExists) + "." : ""
            font.bold: true
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
        }

        Item {
            height: Kirigami.Units.smallSpacing
        }

        Label {
            text: promptCore ? promptCore.getString(ExtractionFileOverwritePromptCore.Source) : ""
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }

        ColumnLayout {
            spacing: 0

            Label {
                text: priv.file
                opacity: 0.5
            }
            Kirigami.UrlButton {
                text: promptCore ? promptCore.sourceName() : null
                url: promptCore ? promptCore.sourceFilePath() : null
                wrapMode: Text.WrapAnywhere
                Layout.maximumWidth: root.maxContentWidth > 0 ? root.maxContentWidth : root.width
            }
        }

        ColumnLayout {
            spacing: 0
            Label {
                text: priv.location
                opacity: 0.5
            }
            Kirigami.UrlButton {
                url: promptCore ? promptCore.sourceLocation() : null
                wrapMode: Text.WrapAnywhere
                Layout.maximumWidth: root.maxContentWidth > 0 ? root.maxContentWidth : root.width
            }
        }

        Column {
            spacing: 0
            Label {
                text: priv.size
                opacity: 0.5
            }
            Label {
                text: promptCore ? promptCore.sourceSize() : null
            }
        }

        Column {
            spacing: 0
            Label {
                text: priv.modified
                opacity: 0.5
            }
            Label {
                text: promptCore ? promptCore.sourceModifiedDate() : null
            }
        }

        Item {
            height: Kirigami.Units.smallSpacing
        }

        Label {
            text: promptCore ? promptCore.getString(ExtractionFileOverwritePromptCore.Destination) : ""
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }

        ColumnLayout {
            spacing: 0
            Label {
                text: priv.file
                opacity: 0.5
            }
            Kirigami.UrlButton {
                text: promptCore ? promptCore.destinationName() : null
                url: promptCore ? promptCore.destinationFilePath() : null
                wrapMode: Text.WrapAnywhere
                Layout.maximumWidth: root.maxContentWidth > 0 ? root.maxContentWidth : root.width
            }
        }

        ColumnLayout {
            spacing: 0
            Label {
                text: priv.location
                opacity: 0.5
            }
            Kirigami.UrlButton {
                url: promptCore ? promptCore.destinationLocation() : null
                wrapMode: Text.WrapAnywhere
                Layout.maximumWidth: root.maxContentWidth > 0 ? root.maxContentWidth : root.width
            }
        }

        Column {
            spacing: 0
            Label {
                text: priv.size
                opacity: 0.5
            }
            Label {
                text: promptCore ? promptCore.destinationSize() : null
            }
        }

        Column {
            spacing: 0
            Label {
                text: priv.modified
                opacity: 0.5
            }
            Label {
                text: promptCore ? promptCore.destinationModifiedDate() : null
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter
            Button {
                text: promptCore ? promptCore.getString(ExtractionFileOverwritePromptCore.Skip) : ""
                icon.name: "go-next-skip"
                onClicked: promptCore.skip(applyToAllCheck.checked)
            }
            Button {
                text: promptCore ? promptCore.getString(ExtractionFileOverwritePromptCore.Overwrite) : ""
                icon.name: "download"
                onClicked: promptCore.overwrite(applyToAllCheck.checked)
            }
            CheckBox {
                id: applyToAllCheck
                text: promptCore ? promptCore.getString(ExtractionFileOverwritePromptCore.ApplyToAll) : ""
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter

            Button {
                text: promptCore ? promptCore.getString(ExtractionFileOverwritePromptCore.Retry) : ""
                icon.name: "view-refresh"
                onClicked: promptCore.retry()
            }
            Button {
                text: promptCore ? promptCore.getString(ExtractionFileOverwritePromptCore.Abort) : ""
                icon.name: "dialog-cancel"
                onClicked: promptCore.abort()
            }
        }
    }
}
