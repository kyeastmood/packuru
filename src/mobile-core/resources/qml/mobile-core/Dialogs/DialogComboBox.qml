// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3


ComboBox {
    id: root

    Layout.fillWidth: true;
    Layout.preferredWidth: priv.maxContentWidth + contentItem.leftPadding + contentItem.rightPadding
                           + indicator.implicitWidth

    // It was supposed to update max item width but it doesn't.
//    Connections {
//        target: contentItem

//        onContentWidthChanged: {
//            if (contentItem.contentWidth > priv.maxContentWidth)
//                priv.maxContentWidth = contentItem.contentWidth
//        }
//    }

    QtObject {
        id: priv
        property int maxContentWidth: 0
    }
}
