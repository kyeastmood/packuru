// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQml.Models 2.11

import org.kde.kirigami 2.0 as Kirigami


ListView {
    id: root
    boundsBehavior: Flickable.StopAtBounds
    clip:true

    property int delegateMaxImplicitWidth: 0
    property int delegateMaxImplicitHeight: 0
    property alias selectionModel: selectionModel
    property Component delegateContentItem

    delegate: Kirigami.AbstractListItem {
        id: delegateItem
        contentItem: Loader {
            property int row: index
            property var model: root.model
            property var decor: decoration
            sourceComponent: delegateContentItem
        }
        checkable: selectionModel.hasSelection
        onImplicitWidthChanged: {
            if (implicitWidth + leftPadding + rightPadding > root.delegateMaxImplicitWidth)
                root.delegateMaxImplicitWidth = implicitWidth  + leftPadding + rightPadding
        }
        onImplicitHeightChanged: {
            if (implicitHeight > root.delegateMaxImplicitHeight)
                root.delegateMaxImplicitHeight = implicitHeight
        }
        onPressAndHold: {
            if (selectionModel.hasSelection)
                return
            toggle()
        }
        onCheckedChanged: {
            var modelIndex = delegateModel.modelIndex(index)
            var toggle = checked ? ItemSelectionModel.Select : ItemSelectionModel.Deselect
            selectionModel.select(modelIndex, toggle)
        }
        Component.onCompleted: {
            if (selectionModel.hasSelection) {
                var modelIndex = delegateModel.modelIndex(index)
                if (selectionModel.isSelected(modelIndex))
                    toggle()
            }
        }
    }

    DelegateModel {
        id: delegateModel
        model: root.model

    }

    ItemSelectionModel {
        id: selectionModel
        model: root.model
    }
}
