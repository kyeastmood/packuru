// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQml.Models 2.11
import QtQuick.Dialogs 1.3

import org.kde.kirigami 2.5 as Kirigami

import ControllerSystem.QQC 1.0 as CS_QQC

import PackuruCore 1.0


ArchiveTaskDialog {
    id: root

    contentItem: TabWidget {
        id: tabWidget
        focus: true
    }

    onAccepted: dialogCore.accept()

    Component.onCompleted: {
        if (!dialogCore)
            return

        var fileListPage = fileListComponent.createObject(tabWidget.swipeView)
        tabWidget.swipeView.addPage(fileListPage)

        var createArchiveMode = dialogCore.getMode() === ArchivingDialogCore.DialogMode.CreateArchive
        if (createArchiveMode) {
            var archivePage = archivePageComponent.createObject(tabWidget.swipeView,
                                                                { "title": dialogCore.getString(ArchivingDialogCore.TabArchive)})
            tabWidget.swipeView.addPage(archivePage)
        }

        var optionsPage = optionsPageComponent.createObject(tabWidget.swipeView)
        tabWidget.swipeView.addPage(optionsPage)

        mapper.addWidgets(ArchivingDialogControllerType.RunInQueue, root.runInQueueCheck)
        mapper.addWidgets(ArchivingDialogControllerType.AcceptButton, root.acceptButton)
        mapper.addWidgets(ArchivingDialogControllerType.DialogErrors, root.errorsItem)

        mapper.setEngine(dialogCore.getControllerEngine())

        root.cancelButton.text = dialogCore.getString(ArchivingDialogCore.Cancel)

        priv.advancedPage = advancedPageComponent.createObject(root)
        priv.advancedPage.setCurrentPage(dialogCore.getPluginRowMapper())
        tabWidget.swipeView.addPage(priv.advancedPage)

        if (createArchiveMode) {
            if (dialogCore.getFileModel().rowCount() === 0)
                tabWidget.swipeView.setCurrentIndex(0)
            else
                tabWidget.swipeView.setCurrentIndex(1)
        }

    }

    CS_QQC.WidgetMapper {
        id: mapper
    }

    Connections {
        target: dialogCore
        function onPluginRowMapperChanged() {
            priv.advancedPage.setCurrentPage(dialogCore.getPluginRowMapper())
        }
    }

    QtObject {
        id: priv
        property QtObject advancedPage
    }

    Component {
        id: fileListComponent

        FocusScope {
            implicitHeight: fileListLayout.implicitHeight
            implicitWidth: fileListLayout.implicitWidth
            readonly property string title: dialogCore.getString(ArchivingDialogCore.TabFiles)

            ColumnLayout {
                id: fileListLayout
                anchors.fill: parent

                ArchiveTaskDialogListView {
                    id: fileListView
                    model: dialogCore ? dialogCore.getFileModel() : null
                    Layout.preferredHeight: delegateMaxImplicitHeight * count
                    Layout.minimumHeight: 300
                    Layout.preferredWidth: delegateMaxImplicitWidth
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    delegateContentItem: RowLayout {
                        Kirigami.Icon {
                            id: iconItem
                            source: decor
                            Layout.preferredHeight: parent.implicitHeight
                            Layout.preferredWidth: Layout.preferredHeight
                        }
                        ColumnLayout {
                            Layout.fillWidth: true

                            Label {
                                Layout.fillWidth: true
                                Component.onCompleted: {
                                    var index = fileListView.model.index(row, 0)
                                    text = fileListView.model.data(index)
                                }
                            }
                            Label {
                                opacity: 0.5
                                Layout.fillWidth: true
                                Component.onCompleted: {
                                    var index = fileListView.model.index(row, 1)
                                    text = fileListView.model.data(index)
                                }
                            }
                        }
                    }

                }

                RowLayout {
                    id: buttons
                    Button {
                        text: dialogCore.getString(ArchivingDialogCore.AddFolder)
                        Layout.fillWidth: true
                        focus: true
                        onClicked: {
                            fileDialogLoader.openFolder = true
                            fileDialogLoader.active = true
                        }
                    }
                    Button {
                        text: dialogCore.getString(ArchivingDialogCore.AddFiles)
                        Layout.fillWidth: true
                        focus: true
                        onClicked: {
                            fileDialogLoader.openFolder = false
                            fileDialogLoader.active = true
                        }
                    }
                    Button {
                        text: dialogCore.getString(ArchivingDialogCore.RemoveItems)
                        Layout.fillWidth: true
                        enabled: fileListView.selectionModel.hasSelection
                        onClicked: {
                            dialogCore.removeItems(fileListView.selectionModel.selectedIndexes)
                        }
                    }
                }

                Loader {
                    id: fileDialogLoader
                    active: false
                    property bool openFolder: false
                    sourceComponent: FileDialog {
                        selectMultiple: !openFolder
                        selectFolder: openFolder
                        Component.onCompleted: open()

                        onAccepted: {
                            var urls = []

                            if (openFolder) {
                                urls.push(fileUrl)
                            }
                            else {
                                for (var i = 0; i < fileUrls.length; ++i)
                                    urls.push(fileUrls[i])
                            }

                            dialogCore.addItems(fileUrls)
                            fileDialogLoader.active = false
                        }

                        onRejected: fileDialogLoader.active = false
                    }
                }
            }
        }
    }

    Component {
        id: archivePageComponent

        OptionsPage {
            property string title

            Label {
                id: archivingModeLabel
                Layout.alignment: Qt.AlignRight
            }
            DialogComboBox {
                id: archivingModeCombo
            }

            Label {
                id: archiveNameLabel
                Layout.alignment: Qt.AlignRight
            }
            TextField {
                id: archiveNameEdit
                Layout.fillWidth: true
            }

            Label {
                id: archiveTypeLabel
                Layout.alignment: Qt.AlignRight
            }
            DialogComboBox {
                id: archiveTypeCombo
            }

            Label {
                id: splitPresetLabel
                Layout.alignment: Qt.AlignRight
            }
            DialogComboBox {
                id: splitPresetCombo
            }

            Label {
                id: partSizeLabel
                Layout.alignment: Qt.AlignRight
            }
            SpinBox {
                id: partSizeSpinBox
                Layout.fillWidth: true
                to: 1000000
                stepSize: 10
                editable: true
            }

            Label {
                id: destinationModeLabel
                Layout.alignment: Qt.AlignRight
            }
            DialogComboBox {
                id: destinationModeCombo
            }

            Label {
                id: destinationPathLabel
                Layout.alignment: Qt.AlignRight
            }
            RowLayout {
                Layout.fillWidth: true
                TextField {
                    id: destinationPathEdit
                    Layout.fillWidth: true

                }
                Button {
                    id: destinationPathButton
                    icon.name: "document-open-folder"
                    Layout.preferredWidth: height
                    onClicked: fileDialogLoader.active = true
                    Loader {
                        id: fileDialogLoader
                        active: false
                        sourceComponent: FileDialog {
                            selectMultiple: false
                            selectFolder: true
                            onAccepted: {
                                destinationPathEdit.text = fileUrl
                                fileDialogLoader.active = false
                            }
                            onRejected: fileDialogLoader.active = false
                            Component.onCompleted: open()
                        }
                    }
                }
            }

            Label {
                text: dialogCore.getString(ArchivingDialogCore.OnCompletion)
                Layout.alignment: Qt.AlignRight
                font.bold: true
            }
            Item {
                Layout.preferredHeight: 1
            }

            Item {
                Layout.preferredHeight: 1
            }
            CheckBox {
                id: openNewArchiveCheck
            }

            Item {
                Layout.preferredHeight: 1
            }
            CheckBox {
                id: openDestinationCheck
            }
            Component.onCompleted: {
                mapper.addLabelAndWidget(ArchivingDialogControllerType.ArchivingMode,
                                         archivingModeLabel,
                                         archivingModeCombo)
                mapper.addLabelAndWidget(ArchivingDialogControllerType.ArchiveName,
                                         archiveNameLabel,
                                         archiveNameEdit)
                mapper.addLabelAndWidget(ArchivingDialogControllerType.ArchiveType,
                                         archiveTypeLabel,
                                         archiveTypeCombo)
                mapper.addLabelAndWidget(ArchivingDialogControllerType.SplitPreset,
                                         splitPresetLabel,
                                         splitPresetCombo)
                mapper.addLabelAndWidget(ArchivingDialogControllerType.SplitSize,
                                         partSizeLabel,
                                         partSizeSpinBox )
                mapper.addLabelAndWidget(ArchivingDialogControllerType.DestinationMode,
                                         destinationModeLabel,
                                         destinationModeCombo)
                mapper.addLabelAndWidget(ArchivingDialogControllerType.DestinationPath,
                                         destinationPathLabel,
                                         destinationPathEdit)
                mapper.addWidget(ArchivingDialogControllerType.OpenNewArchive, openNewArchiveCheck)
                mapper.addWidget(ArchivingDialogControllerType.OpenDestinationPath, openDestinationCheck)

                mapper.addBuddy(ArchivingDialogControllerType.DestinationPath, destinationPathButton)

                mapper.updateWidgets()
            }
        }
    }

    Component {
        id: optionsPageComponent

        OptionsPage {
            property string title: dialogCore.getString(ArchivingDialogCore.TabOptions)

            Label {
                text: dialogCore.getString(ArchivingDialogCore.Compression)
                Layout.alignment: Qt.AlignRight
                font.bold: true
            }
            Item {
                Layout.fillWidth: true
            }

            Label {
                id: compressionStateLabel
                Layout.alignment: Qt.AlignRight
            }
            Label {
                id: compressionStateInfo
                Layout.fillWidth: true
                font.italic: true
            }

            Label {
                id: compressionLevelLabel
                Layout.alignment: Qt.AlignRight
            }
            RowLayout {
                Label {
                    id: compressionMinLevelLabel
                    text: compressionLevelSlider.from
                }

                Slider {
                    id: compressionLevelSlider
                    Layout.fillWidth: true
                    stepSize: 1
                }
                Label {
                    id: compressionMaxLevelLabel
                    text: compressionLevelSlider.to
                }
            }

            Label {
                text: dialogCore.getString(ArchivingDialogCore.Encryption)
                Layout.alignment: Qt.AlignRight
                font.bold: true
            }
            Item {
                Layout.fillWidth: true
            }

            Label {
                id: encryptionStateLabel
                Layout.alignment: Qt.AlignRight
                font.italic: true
            }

            RowLayout {
                Layout.fillWidth: true

                Label {
                    id: encryptionStateInfo
                    font.italic: true
                }
                Label {
                    id: passwordMatchLabel
                    Layout.fillWidth: true
                    horizontalAlignment: Qt.AlignRight
                    font.italic: true
                }
            }

            Label {
                id: passwordLabel
                Layout.alignment: Qt.AlignRight
            }
            PasswordField {
                id: passwordEdit
                echoMode: TextInput.Password
                Layout.fillWidth: true
            }

            Label {
                id: passwordRepeatLabel
                Layout.alignment: Qt.AlignRight
            }
            TextField {
                id: passwordRepeatEdit
                echoMode: passwordEdit.echoMode
                Layout.fillWidth: true
            }

            Label {
                id: encryptionModeLabel
                Layout.alignment: Qt.AlignRight
            }
            DialogComboBox {
                id: encryptionModeCombo
            }

            Label {
                text: dialogCore.getString(ArchivingDialogCore.FilePaths)
                Layout.alignment: Qt.AlignRight
                font.bold: true
            }
            Item {
                Layout.fillWidth: true
            }

            Label {
                id: filePathsModeLabel
                Layout.alignment: Qt.AlignRight
            }
            DialogComboBox {
                id: filePathsModeCombo
            }

            Label {
                id: internalPathLabel
                Layout.alignment: Qt.AlignRight
            }
            TextField {
                id: internalPathEdit
                Layout.fillWidth: true
            }

            Component.onCompleted: {
                mapper.addLabelAndWidget(ArchivingDialogControllerType.CompressionState,
                                         compressionStateLabel,
                                         compressionStateInfo)
                mapper.addLabelAndWidget(ArchivingDialogControllerType.CompressionLevel,
                                         compressionLevelLabel,
                                         compressionLevelSlider)
                mapper.addLabelAndWidget(ArchivingDialogControllerType.EncryptionMode,
                                         encryptionModeLabel,
                                         encryptionModeCombo)
                mapper.addLabelAndWidget(ArchivingDialogControllerType.EncryptionState,
                                         encryptionStateLabel,
                                         encryptionStateInfo)
                mapper.addLabelAndWidget(ArchivingDialogControllerType.Password,
                                         passwordLabel,
                                         passwordEdit)
                mapper.addLabelAndWidget(ArchivingDialogControllerType.PasswordRepeat,
                                         passwordRepeatLabel,
                                         passwordRepeatEdit)
                mapper.addWidget(ArchivingDialogControllerType.PasswordMatch, passwordMatchLabel)
                mapper.addLabelAndWidget(ArchivingDialogControllerType.FilePaths,
                                         filePathsModeLabel,
                                         filePathsModeCombo)
                mapper.addLabelAndWidget(ArchivingDialogControllerType.InternalPath,
                                         internalPathLabel,
                                         internalPathEdit)

                mapper.updateWidgets()
            }
        }
    }

    Component {
        id: advancedPageComponent

        CS_QQC.PageStack {
            property string title: dialogCore.getString(ArchivingDialogCore.TabAdvanced)
        }
    }
}
