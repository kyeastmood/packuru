// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.6 as Kirigami

import PackuruCore 1.0


FocusScope{
    id: root
    implicitHeight: col.implicitHeight
    implicitWidth: col.implicitWidth
    property QtObject promptCore
    property alias headerVisible: headerLabel.visible
    property int maxContentWidth: 0

    Connections {
        target: promptCore
        function onDeleted() {
            root.destroy()
        }
    }

    QtObject {
        id: priv
        readonly property string content: promptCore ? promptCore.getString(ExtractionFolderOverwritePromptCore.Content) : ""
        readonly property string folder: promptCore ? promptCore.getString(ExtractionFolderOverwritePromptCore.Folder) : ""
        readonly property string modified: promptCore ? promptCore.getString(ExtractionFolderOverwritePromptCore.Modified) : ""
    }

    ColumnLayout{
        id: col
        spacing: 10
        anchors.horizontalCenter: parent.horizontalCenter

        Label {
            id: headerLabel
            text: promptCore ? promptCore.getString(ExtractionFolderOverwritePromptCore.FolderAlreadyExists) + (".") : ""
            font.bold: true
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
        }

        Item {
            height: Kirigami.Units.smallSpacing
        }

        Label {
            text: promptCore ? promptCore.getString(ExtractionFolderOverwritePromptCore.Source) : ""
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }

        ColumnLayout {
            spacing: 0
            Label {
                text: priv.folder
                opacity: 0.5
            }
            Kirigami.UrlButton {
                url: promptCore ? promptCore.sourcePath() : null
                wrapMode: Text.WrapAnywhere
                Layout.maximumWidth: root.maxContentWidth > 0 ? root.maxContentWidth : root.width
            }
        }

        Column {
            spacing: 0
            Label {
                text: priv.content
                opacity: 0.5
            }
            Label {
                text: promptCore ? promptCore.sourceContent() : null
            }
        }

        Column {
            spacing: 0
            Label {
                text: priv.modified
                opacity: 0.5
            }
            Label {
                text: promptCore ? promptCore.sourceModifiedDate() : null
            }
        }

        Item {
            height: Kirigami.Units.smallSpacing
        }

        Label {
            text: promptCore ? promptCore.getString(ExtractionFolderOverwritePromptCore.Destination) : ""
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }

        ColumnLayout {
            spacing: 0
            Label {
                text: priv.folder
                opacity: 0.5
            }
            Kirigami.UrlButton {
                url: promptCore ? promptCore.destinationPath() : null
                wrapMode: Text.WrapAnywhere
                Layout.maximumWidth: root.maxContentWidth > 0 ? root.maxContentWidth : root.width
            }
        }

        Column {
            spacing: 0
            Label {
                text: priv.content
                opacity: 0.5
            }
            Label {
                text: promptCore ? promptCore.destinationContent() : null
            }
        }

        Column {
            spacing: 0
            Label {
                text: priv.modified
                opacity: 0.5
            }
            Label {
                text: promptCore ? promptCore.destinationModifiedDate() : null
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter

            Button {
                text: promptCore ? promptCore.getString(ExtractionFolderOverwritePromptCore.Skip) : ""
                icon.name: "go-next-skip"
                onClicked: promptCore.skip(applyToAllCheck.checked)
            }
            Button {
                text: promptCore ? promptCore.getString(ExtractionFolderOverwritePromptCore.WriteInto) : ""
                icon.name: "download"
                onClicked: promptCore.overwrite(applyToAllCheck.checked)
            }
            CheckBox {
                id: applyToAllCheck
                text: promptCore ? promptCore.getString(ExtractionFolderOverwritePromptCore.ApplyToAll) : ""
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter

            Button {
                text: promptCore ? promptCore.getString(ExtractionFolderOverwritePromptCore.Retry) : ""
                icon.name: "view-refresh"
                onClicked: promptCore.retry()
            }
            Button {
                text: promptCore ? promptCore.getString(ExtractionFolderOverwritePromptCore.Abort) : ""
                icon.name: "dialog-cancel"
                onClicked: promptCore.abort()
            }
        }
    }
}
