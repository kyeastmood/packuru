// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.14
import QtQuick.Controls 2.14

import org.kde.kirigami 2.5 as Kirigami


TextField {
    id: root

    Kirigami.Icon  {
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        height: root.implicitHeight - 2 * Kirigami.Units.smallSpacing
        width: height
        source: root.echoMode == TextInput.Password ? "hint" : "visibility"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if (root.echoMode == TextInput.Password)
                    root.echoMode = TextInput.Normal
                else
                    root.echoMode = TextInput.Password
            }
        }
    }
}
