// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Layouts 1.3

FocusScope {
    implicitHeight: options.implicitHeight
    implicitWidth: options.implicitWidth
    property string title
    default property alias children: gridLayout.children

    Flickable {
        id: options
        contentHeight: gridLayout.implicitHeight
        contentWidth: gridLayout.implicitWidth
        implicitHeight: contentHeight
        implicitWidth: contentWidth
        anchors.fill: parent
        boundsBehavior: Flickable.StopAtBounds
        contentItem.anchors.left: contentItem.parent.left
        contentItem.anchors.right: contentItem.parent.right

        GridLayout {
            id: gridLayout
            columns: 2
            anchors.fill: parent
        }
    }
}
