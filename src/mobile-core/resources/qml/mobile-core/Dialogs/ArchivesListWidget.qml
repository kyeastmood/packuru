// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.3

import org.kde.kirigami 2.5 as Kirigami

import PackuruCore 1.0


FocusScope {
    implicitHeight: archives.implicitHeight
    implicitWidth: archives.implicitWidth
    property string title
    property alias model: archiveListView.model

    ColumnLayout {
        anchors.fill: parent

        id: archives

        ArchiveTaskDialogListView {
            id: archiveListView

            Layout.preferredHeight: delegateMaxImplicitHeight * count
            Layout.minimumHeight: 300
            Layout.preferredWidth: delegateMaxImplicitWidth
            Layout.fillHeight: true
            Layout.fillWidth: true

            delegateContentItem: RowLayout {
                Kirigami.Icon {
                    id: iconItem
                    source: decor
                    Layout.preferredHeight: parent.implicitHeight
                    Layout.preferredWidth: Layout.preferredHeight
                }
                ColumnLayout {
                    Layout.fillWidth: true
                    Label {
                        Layout.fillWidth: true
                        Component.onCompleted: {
                            var index = archiveListView.model.index(row, 0)
                            text = archiveListView.model.data(index)
                        }
                    }
                    RowLayout {
                        Label {
                            opacity: 0.5
                            Layout.alignment: Qt.AlignLeft
                            Component.onCompleted: {
                                var index = archiveListView.model.index(row, 2)
                                text = archiveListView.model.data(index)
                            }
                        }
                        Label {
                            opacity: 0.5
                            Layout.fillWidth: true
                            horizontalAlignment: Qt.AlignRight
                            Component.onCompleted: {
                                var index = archiveListView.model.index(row, 3)
                                text = archiveListView.model.data(index)
                            }
                        }
                    }
                }
            }

        }

        RowLayout {
            id: buttons
            Button {
                text: commonStrings.getString(CommonStrings.Add)
                Layout.fillWidth: true
                focus: true
                onClicked: fileDialogLoader.active = true
            }
            Button {
                text: commonStrings.getString(CommonStrings.Remove)
                Layout.fillWidth: true
                enabled: archiveListView.selectionModel.hasSelection
                onClicked: {
                    model.removeArchives(archiveListView.selectionModel.selectedIndexes)
                }
            }
        }

        Loader {
            id: fileDialogLoader
            active: false
            sourceComponent: FileDialog {
                selectMultiple: true
                Component.onCompleted: open()

                onAccepted: {
                    var urls = []
                    for (var i = 0; i < fileUrls.length; ++i) {
                        urls.push(fileUrls[i])
                    }
                    model.addArchives(fileUrls)
                    fileDialogLoader.active = false
                }

                onRejected: fileDialogLoader.active = false
            }
        }
    }
}
