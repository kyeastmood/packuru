// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.15
import QtQml.Models 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.5 as Kirigami

import KirigamiActivities 1.0 as KirigamiActivities

import PackuruCore 1.0

import ControllerSystem.QQC 1.0 as CS_QQC


KirigamiActivities.BasicActivity {
    id: root
    name: appSettingsDialogCore.getString(AppSettingsDialogCore.DialogTitle)
    globalDrawerAvailable: false
    contextDrawerAvailable: false
    pageRow.globalToolBar.style: Kirigami.ApplicationHeaderStyle.Auto

    property QtObject appSettingsDialogCore

    signal accepted()

    onAccepted: appSettingsDialogCore.accept()

    Component.onDestruction: {
        appSettingsDialogCore.destroyLater()
    }

    CS_QQC.WidgetMapper {
        id: mapper
    }

    Component.onCompleted: {
        mapper.setEngine(appSettingsDialogCore.getControllerEngine())
    }

    function insertCategory(index, icon, kirigamiPage) {
        var category = categoryComponent.createObject(root)
        category.icon = icon
        category.label = kirigamiPage.title
        category.page = kirigamiPage
        categoriesModel.insert(index, category)
    }

    Component {
        id: categoryComponent
        Kirigami.BasicListItem {
            separatorVisible: false
            property var page
            onClicked: pageRow.layers.push(page)
        }
    }

    pageRow.initialPage: Kirigami.Page {
        title: root.name

        ObjectModel {
            id: categoriesModel
            Kirigami.BasicListItem {
                label: appSettingsDialogCore.getString(AppSettingsDialogCore.CategoryGeneral)
                icon: "configure"
                separatorVisible: false
                onClicked: pageRow.layers.push(generalPage)
            }
            Kirigami.BasicListItem {
                label: appSettingsDialogCore.getString(AppSettingsDialogCore.CategoryPlugins)
                icon: "plugins"
                separatorVisible: false
                onClicked: pageRow.layers.push(pluginsPage)
            }
            Kirigami.BasicListItem {
                label: appSettingsDialogCore.getString(AppSettingsDialogCore.CategoryArchiveTypes)
                icon: "document-preview"
                separatorVisible: false
                onClicked: pageRow.layers.push(archiveTypesPage)
            }
        }


        ColumnLayout {
            anchors.fill: parent
            ListView {
                model: categoriesModel
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.margins: 0
                // Disable delegate highlightning
                keyNavigationEnabled: false
            }

            DialogButtonBox {
                id: buttons
                Layout.fillWidth: true
                alignment: Qt.AlignRight
                Button {
                    text: "OK"
                    icon.name: "dialog-ok"
                    DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
                }
                Button {
                    text: commonStrings.getString(CommonStrings.Cancel)
                    icon.name: "dialog-cancel"
                    DialogButtonBox.buttonRole: DialogButtonBox.RejectRole
                }

                onRejected: root.close()
                onAccepted: {
                    root.accepted()
                    root.close()
                }
            }
        }
    }

    property Item generalPage: Kirigami.Page {
        title: appSettingsDialogCore.getString(AppSettingsDialogCore.CategoryGeneral)

        ColumnLayout {
            anchors.fill: parent

            GridLayout {
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft
                columns: 2

                Label {
                    id: sizeUnitsLabel
                    Layout.alignment: Qt.AlignRight
                }
                ComboBox {
                    id: sizeUnitsCombo
                    Layout.fillWidth: true
                }
                Item {
                    Layout.fillWidth: true
                }
                Label {
                    id: sizeUnitsExampleLabel
                }
                Label {
                    id: compressionRatioLabel
                    Layout.alignment: Qt.AlignRight
                }
                ComboBox {
                    id: compressionRatioCombo
                    Layout.fillWidth: true
                }
                Item  {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                }
            }

            DialogButtonBox {
                Layout.fillWidth: true
                Button {
                    text: commonStrings.getString(CommonStrings.Back)
                    icon.name: "go-previous"
                    onClicked: pageRow.layers.pop()
                }
            }
        }

        Component.onCompleted: {
            mapper.addLabelAndWidget(AppSettingsControllerType.SizeUnits,
                                     sizeUnitsLabel,
                                     sizeUnitsCombo)
            mapper.addWidget(AppSettingsControllerType.SizeUnitsExample, sizeUnitsExampleLabel)
            mapper.addLabelAndWidget(AppSettingsControllerType.CompressionRatioMode,
                                     compressionRatioLabel,
                                     compressionRatioCombo)
            mapper.updateWidgets()
        }
    }

    property Item pluginsPage: Kirigami.Page {
        title: appSettingsDialogCore.getString(AppSettingsDialogCore.CategoryPlugins)
        ColumnLayout {
            anchors.fill: parent

            ListView {
                Layout.fillWidth: true
                Layout.fillHeight: true
                id: pluginListView
                model: appSettingsDialogCore.getPluginModel()
                // Disable delegate highlightning
                keyNavigationEnabled: false
                delegate: Kirigami.AbstractListItem {
                    Label {
                        text: display
                    }
                    separatorVisible: false
                    onClicked: {
                        var page = pageRow.layers.push(pluginInfoPageComponent)
                        page.title = display
                        var model = appSettingsDialogCore.getPluginModel()
                        page.descriptionLabel.text = model.getDescription(index)
                        page.backendHomepageLabel.text = Aux.htmlLink(model.getHomepage(index))
                        page.pathLabel.text = model.getPath(index)
                        page.readsLabel.text = model.supportedReadArchives(index)
                        page.writesLabel.text = model.supportedWriteArchives(index)
                    }
                }
            }

            DialogButtonBox {
                Layout.fillWidth: true
                Button {
                    text: commonStrings.getString(CommonStrings.Back)
                    icon.name: "go-previous"
                    onClicked: pageRow.layers.pop()
                }
            }
        }
    }


    property Item archiveTypesPage: Kirigami.Page {
        title: appSettingsDialogCore.getString(AppSettingsDialogCore.CategoryArchiveTypes)

        ColumnLayout {
            anchors.fill: parent

            RowLayout {

                TextField {
                    placeholderText: appSettingsDialogCore.getString(AppSettingsDialogCore.FilterPlaceholderText)
                    text: view.model.filterFixedString
                    Layout.fillWidth: true
                    selectByMouse: true
                    mouseSelectionMode: TextInput.SelectCharacters
                    onTextChanged: view.model.setFilterFixedString(text)
                }

                RowLayout {
                    Layout.fillWidth: true
                    Layout.minimumWidth: parent.implicitWidth / 2
                    Item {
                        Layout.fillWidth: true
                    }
                    Button {
                        id: toggleWarningButton
                        text: appSettingsDialogCore.getString(AppSettingsDialogCore.ReadWarning)
                        icon.name: "dialog-warning"
                        checkable: true
                    }
                }
            }

            Label {
                text: appSettingsDialogCore.getString(AppSettingsDialogCore.PluginConfigChangeWarning)
                wrapMode: Text.WordWrap
                visible: toggleWarningButton.checked
                Layout.preferredWidth: parent.width
            }

            ListView {
                id: view
                Layout.fillHeight: true
                Layout.fillWidth: true
                clip: true
                boundsBehavior: Flickable.StopAtBounds
                model: ArchiveTypeFilterModel {
                    sourceModel: appSettingsDialogCore.getArchiveTypeModel()
                }
                // Disable delegate highlightning
                keyNavigationEnabled: false
                delegate: Kirigami.AbstractListItem {
                    id: archiveTypeDelegate
                    hoverEnabled: false
                    ColumnLayout {
                        GridLayout {
                            columns: 2
                            Layout.fillWidth: true
                            Item {
                                Layout.preferredHeight: 1
                            }
                            Label {
                                horizontalAlignment: Text.AlignHCenter
                                text: display
                                Layout.fillWidth: true
                                font.bold: true
                            }
                            Label {
                                text: view.model ? view.model.headerData(1, Qt.Horizontal) : ""
                                Layout.alignment: Qt.AlignRight
                                opacity: 0.5
                            }
                            ComboBox {
                                id: readCombo
                                enabled: model.length > 1
                                Layout.fillWidth: true
                            }
                            Label {
                                text: view.model ? view.model.headerData(2, Qt.Horizontal) : ""
                                Layout.alignment: Qt.AlignRight
                                opacity: 0.5
                            }
                            ComboBox {
                                id: writeCombo
                                enabled: model.length > 1
                                Layout.fillWidth: true
                            }
                        }
                    }

                    Component.onCompleted: {
                        var filterModel =  ListView.view.model
                        var sourceModel =  ListView.view.model.sourceModel

                        var sourceIndex = filterModel.mapToSource(filterModel.index(index, 1))
                        readCombo.model = sourceModel.getReadPlugins(sourceIndex.row)
                        var text = sourceModel.getCurrentReadPlugin(sourceIndex.row)
                        readCombo.currentIndex = readCombo.find(text)
                        readCombo.currentTextChanged.connect(function() {
                            sourceModel.setReadPlugin(sourceIndex.row, readCombo.currentText)
                        })

                        sourceIndex = filterModel.mapToSource(filterModel.index(index, 2))
                        writeCombo.model = sourceModel.getWritePlugins(sourceIndex.row)
                        text = sourceModel.getCurrentWritePlugin(sourceIndex.row)
                        writeCombo.currentIndex = writeCombo.find(text)
                        writeCombo.currentTextChanged.connect(function() {
                            sourceModel.setWritePlugin(sourceIndex.row, writeCombo.currentText)
                        })
                    }
                }

            }

            DialogButtonBox {
                Layout.fillWidth: true
                Button {
                    text: commonStrings.getString(CommonStrings.Back)
                    icon.name: "go-previous"
                    onClicked: pageRow.layers.pop()
                }
            }
        }
    }

    Component {
        id: pluginInfoPageComponent
        Kirigami.Page {
            property alias descriptionLabel: descriptionLabel
            property alias backendHomepageLabel: backendHomepageLabel
            property alias pathLabel: pathLabel
            property alias readsLabel: readsLabel
            property alias writesLabel: writesLabel

            ColumnLayout {
                id: rootLayout
                anchors.fill: parent

                Flickable {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    contentWidth:  infoLayout.implicitWidth
                    contentHeight: infoLayout.implicitHeight
                    contentItem.anchors.left: contentItem.parent.left
                    contentItem.anchors.right: contentItem.parent.right
                    clip: true
                    boundsBehavior: Flickable.StopAtBounds
                    ColumnLayout {
                        id: infoLayout
                        anchors.fill: parent
                        spacing: 20
                        Column {
                            Layout.fillWidth: true
                            Label { text: appSettingsDialogCore.getString(AppSettingsDialogCore.PluginDescription); opacity: 0.5}
                            Label { id: descriptionLabel}
                        }
                        Column {
                            Layout.fillWidth: true
                            Label { text: appSettingsDialogCore.getString(AppSettingsDialogCore.PluginBackendHomepage); opacity: 0.5}
                            Label {
                                id: backendHomepageLabel;
                                onLinkActivated: {
                                    Qt.openUrlExternally(link)
                                }
                            }
                        }
                        Column {
                            Layout.fillWidth: true
                            Label { text: appSettingsDialogCore.getString(AppSettingsDialogCore.PluginFilePath); opacity: 0.5}
                            Label {
                                id: pathLabel;
                                wrapMode: Text.Wrap;
                                width: parent.width

                            }

                        }
                        Column {
                            Layout.fillWidth: true
                            Label { text: appSettingsDialogCore.getString(AppSettingsDialogCore.PluginReads); opacity: 0.5}
                            Label { id: readsLabel }
                        }
                        Column {
                            Layout.fillWidth: true
                            Label {
                                text: appSettingsDialogCore.getString(AppSettingsDialogCore.PluginWrites);
                                opacity: 0.5;
                                visible: writesLabel.text.length > 0
                            }
                            Label { id: writesLabel}
                        }
                    }

                }

                DialogButtonBox {
                    Layout.fillWidth: true
                    Button {
                        text: commonStrings.getString(CommonStrings.Back)
                        icon.name: "go-previous"
                        onClicked: pageRow.layers.pop()
                    }
                }
            }
        }
    }
}
