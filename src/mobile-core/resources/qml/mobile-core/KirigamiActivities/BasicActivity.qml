// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.11

import org.kde.kirigami 2.5 as Kirigami


AbstractActivity {
    property alias pageRow: __pageRow

    Kirigami.PageRow {
        id: __pageRow
        anchors.fill: parent
    }
}
