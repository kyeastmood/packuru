// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.11

import org.kde.kirigami 2.3 as Kirigami


AbstractActivity {
    id: root

    property AbstractActivity initialActivity
    property AbstractActivity currentActivity
    property AbstractActivity topActivity: currentActivity && currentActivity.topActivity ?
                                                  currentActivity.topActivity : currentActivity

    readonly property Item pageRow: currentActivity ? currentActivity.pageRow : null
    readonly property bool globalDrawerAvailable: currentActivity ? currentActivity.globalDrawerAvailable : true
    readonly property bool contextDrawerAvailable: currentActivity ? currentActivity.contextDrawerAvailable : true
    property int size: 0

    signal activityPushed(AbstractActivity activity)
    signal activityRemoved(AbstractActivity activity)

    function size() {
        return priv.activities.length
    }

    function pushActivity(activity, activateNow) {
        if (!activity)
            return

        priv.activities.push(activity)
        ++size

        activity.onActivationRequested.connect(priv.activateActivity)
        activity.onFinished.connect(priv.removeActivity)

        if (activateNow)
            priv.swapActivities(currentActivity, activity)

        activityPushed(activity)
    }

    QtObject {
        id: priv
        property var activities: []

        function activityIndex(activity) {
            for (var i = 0; i < activities.length; ++i)
                if (activities[i] === activity)
                    return i

            return -1
        }

        function removeActivity(activity) {
            var index = activityIndex(activity)

            if (index < 0)
                return

            activities.splice(index, 1)
            --root.size

            if (activity.active && activities.length > 0) {

                if (index === activities.length)
                    swapActivities(activity, activities[index - 1])
                else
                    swapActivities(activity, activities[index])
            }

            root.activityRemoved(activity)

            if (activities.length == 0) {
                root.close()
            }
        }

        function activateActivity(activity) {
            swapActivities(currentActivity, activity)
        }

        function swapActivities(oldActivity, newActivity) {
            if (oldActivity) {
                oldActivity.active = false
                oldActivity.current = false
                if (oldActivity.pageRow)
                    oldActivity.pageRow.visible = false
            }

            if (newActivity) {
                newActivity.active = Qt.binding( function() { return root.active } )
                newActivity.current = true
                if (newActivity.pageRow)
                    newActivity.pageRow.visible = true
            }
            currentActivity = newActivity
        }
    }

    Component.onCompleted: {
        pushActivity(initialActivity, true)
    }
}
