// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.11


Item {
    id: root
    visible: false

    property string name
    property string icon
    property int type
    property bool active: false
    property bool current: false
    property bool canClose: true
    property bool globalDrawerAvailable: true
    property bool contextDrawerAvailable: true
    property Item pageRow

    signal activationRequested(Item activity)
    signal finished(Item activity)

    function activate() {
        activationRequested(root)
    }

    function close() {
        if (canClose)
            finished(root)
    }

}
