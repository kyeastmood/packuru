// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QVariant>

#include "../qcs-core/controllerengine.h"
#include "../qcs-core/controlleridrowmapper.h"

#include "../core/private/plugin-api/archivingcompressioninfo.h"
#include "../core/private/plugin-api/archivingfilepaths.h"
#include "../core/private/plugin-api/archivetype.h"
#include "../core/private/plugin-api/encryptionsupport.h"

#include "archivingparametershandler.h"
#include "privatearchivingparameter.h"
#include "privatearchivingparametermapzpaq.h"
#include "fileattributesctrl.h"
#include "threadcountctrl.h"


using namespace Packuru::Core;


namespace Packuru::Plugins::ZPAQ::Core
{

ArchivingParametersHandler::ArchivingParametersHandler(const ArchivingParameterHandlerInitData& initData,
                                                       QObject* parent)
    : ArchivingParameterHandlerBase(initData, parent)
{

    compressionInfo = {0, 5 , 1};

    controllerEngine
            ->createTopController<FileAttributesCtrl>(PrivateArchivingParameter::FileAttributes);

    controllerRowMapper
            ->addController(Packuru::Plugins::ZPAQ::Core::ArchivingParametersHandler
                            ::tr("Store file attributes/permissions"),
                            PrivateArchivingParameter::FileAttributes);

    controllerEngine
            ->createTopController<ThreadCountCtrl>(PrivateArchivingParameter::ThreadCount);

    controllerRowMapper
            ->addController(Packuru::Plugins::ZPAQ::Core::ArchivingParametersHandler
                            ::tr("Thread count"),
                            PrivateArchivingParameter::ThreadCount);

    controllerEngine->updateAllControllers();
}


ArchivingParametersHandler::~ArchivingParametersHandler()
{

}


ArchivingCompressionInfo ArchivingParametersHandler::getCompressionInfo() const
{
    return compressionInfo;
}


void ArchivingParametersHandler::setCompressionLevel(int value)
{
    compressionInfo.setCurrent(value);
}


bool ArchivingParametersHandler::customInternalPathSupport() const
{
    return true;
}


EncryptionSupport ArchivingParametersHandler::getEncryptionSupport() const
{
    return EncryptionSupport::EntireArchiveOnly;
}


QString ArchivingParametersHandler::getEncryptionMethod() const
{
    return "AES-256";
}


QVariant ArchivingParametersHandler::getPrivateArchivingData() const
{
    auto map = controllerEngine->getAvailableCurrentValues<PrivateArchivingParameter>();
    map[PrivateArchivingParameter::CompressionLevel] = compressionInfo.getCurrent();

    QVariant result;
    result.setValue(map);

    return result;
}


std::unordered_set<ArchivingFilePaths> ArchivingParametersHandler::getAdditionalSupportedFilePaths() const
{
    return {ArchivingFilePaths::AbsolutePaths, ArchivingFilePaths::FullPaths};
}

}
