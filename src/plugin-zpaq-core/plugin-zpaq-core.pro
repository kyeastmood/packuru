# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)
include(../common-lib.pri)

MODULE_BUILD_NAME_SUFFIX = plugin-zpaq-core
MODULE_BUILD_NAME = $${PROJECT_BUILD_NAME}-$${MODULE_BUILD_NAME_SUFFIX}

include(../embed-qm-files.pri)

TARGET = $$qtLibraryTarget($$MODULE_BUILD_NAME)

LIBS += -L$${PROJECT_TARGET_LIB_DIR} -l$$QCS_CORE_BUILD_NAME -l$${CORE_BUILD_NAME}

TRANSLATIONS += \
    translations/$${MODULE_BUILD_NAME_SUFFIX}_en.ts \

HEADERS += \
    archivingparametershandler.h \
    backendhandler.h \
    fileattributesctrl.h \
    invocationdata.h \
    privatearchivingparameter.h \
    privatearchivingparametermapzpaq.h \
    registerstreamoperators.h \
    threadcountctrl.h

SOURCES += \
    archivingparametershandler.cpp \
    backendhandler.cpp \
    fileattributesctrl.cpp \
    registerstreamoperators.cpp \
    threadcountctrl.cpp
