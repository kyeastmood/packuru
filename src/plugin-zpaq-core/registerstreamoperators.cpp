// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/datastream/unordered_map_datastream.h"

#include "registerstreamoperators.h"
#include "privatearchivingparametermapzpaq.h"


using Packuru::Core::operator<<;
using Packuru::Core::operator>>;


namespace Packuru::Plugins::ZPAQ::Core
{

void registerStreamOperators()
{
    qRegisterMetaTypeStreamOperators<PrivateArchivingParameterMap>("Packuru::Plugins::ZPAQ::Core::PrivateArchivingParameterMap");
}

}
