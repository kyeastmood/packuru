// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QThread>

#include "core/private/plugin-api/generatethreadcountlist.h"

#include "threadcountctrl.h"


using namespace qcs::core;


namespace Packuru::Plugins::ZPAQ::Core
{

ThreadCountCtrl::ThreadCountCtrl()
{
    setControllerType(ControllerType::Type::ComboBox);

    const int minThreadCount = 1;
    int maxThreadCount = QThread::idealThreadCount();

    PrivateValueList privateList = Packuru::Core::generateThreadCountList(maxThreadCount);
    PublicValueList publicList;
    publicList.reserve(privateList.size());

    for (int value : privateList)
        publicList.push_back(QString::number(value));

    setValueLists(publicList, privateList);

    // Don't use all threads by default as the system can become unusable
    const int setValue = maxThreadCount / 2;

    setCurrentValue(QString::number(setValue), setValue);
    setEnabled(maxThreadCount - minThreadCount > 0);
}

}
