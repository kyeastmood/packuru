// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <memory>

#include <QDir>
#include <QTimer>
#include <QStandardPaths>
#include <QProcess>

#include "../utils/qvariant_utils.h"
#include "../utils/makeqpointer.h"
#include "../utils/private/unordered_map_utils.h"

#include "../core/private/plugin-api/backendreaddata.h"
#include "../core/private/plugin-api/backendextractiondata.h"
#include "../core/private/plugin-api/backendtestdata.h"
#include "../core/private/plugin-api/backendarchivingdata.h"
#include "../core/private/plugin-api/backenddeletiondata.h"
#include "../core/private/plugin-api/backendexitcodeinterpretation.h"
#include "../core/private/plugin-api/textstreampreprocessor.h"
#include "../core/private/plugin-api/backendarchiveproperty.h"
#include "../core/private/plugin-api/diskspacewatcher.h"

#include "privatearchivingparametermapzpaq.h"
#include "privatearchivingparameter.h"
#include "backendhandler.h"




using namespace Packuru::Core;
using Packuru::Utils::makeQPointer;


namespace Packuru::Plugins::ZPAQ::Core
{

struct FileOperationException
{
    QString error;
};


BackendHandler::BackendHandler(QObject* parent)
    : CLIBackendHandler(parent),
      snapshotDigits(4)
{
    connect(errPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
            this, &BackendHandler::errorLineHandler);
}


bool BackendHandler::executableAvailable()
{
    return !QStandardPaths::findExecutable("zpaq").isEmpty();
}


void BackendHandler::enterPassword(const QString&password)
{
    Q_ASSERT(!password.isEmpty());
    lastInvocationData.password = password;
    startBackend(lastInvocationData);
}


QString BackendHandler::createSinglePartArchiveName(const QString& archiveBaseName, ArchiveType archiveType) const
{
    Q_ASSERT(archiveType == ArchiveType::zpaq);
    return archiveBaseName + QLatin1String(".zpaq");
}


void BackendHandler::addToArchiveImpl(const BackendArchivingData& data)
{
    connect(outPreprocessor, &TextStreamPreprocessor::unfinishedLine,
            this, &BackendHandler::progressLineHandler);

    struct AddToArchiveBlackboard
    {
        AddToArchiveBlackboard(const BackendArchivingData& data_, QPointer<BackendHandler> handler)
            : data(data_), handler(handler)
        {
            if (!data.destinationPath.endsWith(QLatin1Char('/')))
                data.destinationPath += QLatin1Char('/');
        }
        BackendArchivingData data;
        QPointer<BackendHandler> handler;
        bool addingToTempArchive = false;
        QString tempFileName;
    };

    // Use bb->data in the rest of the function instead of one provided in function argument
    // as the destination path might have been changed
    auto bb = std::make_shared<AddToArchiveBlackboard>(data, makeQPointer(this));

    // If archive name contains any extension, ZPAQ will not add ".zpaq"
    if (bb->data.createNewArchive && !bb->data.archiveName.endsWith(QLatin1String(".zpaq")))
        bb->data.archiveName = createSinglePartArchiveName(bb->data.archiveName, ArchiveType::zpaq);

    const auto repackStep = [bb = bb,
            connectionToFinished = QMetaObject::Connection()] () mutable
    {
        auto handler = bb->handler;

        if (!handler)
            return;

        const auto finishedSignal = static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished);

        connectionToFinished = connect(bb->handler->backendProcess, finishedSignal,
                                       [bb = bb,
                                       &connectionToFinished = connectionToFinished]
                                       ()
        {
            auto handler = bb->handler;

            if (!handler)
                return;

            auto state = handler->handleFinishedProcess();

            if (state == BackendHandlerState::Success || state == BackendHandlerState::Warnings)
            {
                const auto disconnect = handler->backendProcess->disconnect(connectionToFinished);
                Q_ASSERT(disconnect);
            }
            else
            {
                Q_ASSERT(!bb->tempFileName.isEmpty());
                try
                {
                    // 'Add' step will not be executed so remove temp archive
                    handler->removeFile(bb->data.destinationPath + bb->tempFileName);
                }

                catch (FileOperationException& e)
                {
                    handler->addError(BackendHandlerError::TemporaryArchiveError, e.error);
                    state = BackendHandlerState::Errors;
                }
            }

            handler->onStepStateChanged(state);
        });

        const auto privateData = Utils::getValueAs<PrivateArchivingParameterMap>(bb->data.pluginPrivateData);
        const auto threadCount = Utils::getValueAs<int>(Utils::getValue(privateData,
                                                                        PrivateArchivingParameter::ThreadCount));

        InvocationData invData;
        invData.command = QLatin1String("x");
        invData.options.push_back(QLatin1String("-t") + QString::number(threadCount));
        invData.archiveFilePath = bb->data.destinationPath + bb->data.archiveName;

        invData.repackInfo.enabled = true;
        invData.repackInfo.newArchivePath = bb->data.destinationPath + bb->tempFileName;
        if (bb->data.newArchiveEncryption == EncryptionState::EntireArchive)
            invData.repackInfo.newPassword = bb->data.password;
        invData.repackInfo.options.push_back(QLatin1String("-all"));

        if (bb->data.currentArchiveEncryption == EncryptionState::EntireArchive)
            invData.password = bb->data.password;

        handler->startBackend(invData);
    };

    const auto addToArchiveStep = [bb = bb,
            connectionToFinished = QMetaObject::Connection()] () mutable
    {
        auto handler = bb->handler;

        if (!handler)
            return;

        const auto finishedSignal = static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished);

        connectionToFinished = connect(handler->backendProcess, finishedSignal,
                                       [bb = bb,
                                       &connectionToFinished = connectionToFinished]
                                       () mutable
        {
            auto handler = bb->handler;

            if (!handler)
                return;

            auto state = handler->handleFinishedProcess();

            if (state == BackendHandlerState::Success || state == BackendHandlerState::Warnings)
            {
                const auto disconnect = handler->backendProcess->disconnect(connectionToFinished);
                Q_ASSERT(disconnect);
            }
            else if (bb->addingToTempArchive)
            {
                Q_ASSERT(!bb->tempFileName.isEmpty());
                try
                {
                    handler->removeFile(bb->data.destinationPath + bb->tempFileName);
                }

                catch (FileOperationException& e)
                {
                    handler->addError(BackendHandlerError::TemporaryArchiveError, e.error);
                    state = BackendHandlerState::Errors;
                }
            }

            handler->onStepStateChanged(state);
        });

        const auto privateData = Utils::getValueAs<PrivateArchivingParameterMap>(bb->data.pluginPrivateData);
        const auto threadCount = Utils::getValueAs<int>(Utils::getValue(privateData,
                                                                        PrivateArchivingParameter::ThreadCount));

        InvocationData invData;
        invData.command = QLatin1String("a");
        invData.options.push_back(QLatin1String("-t") + QString::number(threadCount));

        if (bb->addingToTempArchive)
            invData.archiveFilePath = bb->data.destinationPath + bb->tempFileName;
        else
            invData.archiveFilePath = bb->data.destinationPath + bb->data.archiveName;

        for (const auto& item : bb->data.inputItems)
            invData.inputItems.push_back(item.absoluteFilePath());

        switch (bb->data.filePaths)
        {
        case ArchivingFilePaths::AbsolutePaths:
            // Nothing more to do
            break;
        case ArchivingFilePaths::FullPaths:
        {
            for (const auto& item : bb->data.inputItems)
            {
                auto filePath = item.absoluteFilePath();

#ifdef Q_OS_UNIX
                const int cut = 1; // Remove '/'
#elif defined Q_OS_WINDOWS
                const int cut = 3; // Remove 'C:\'
#else
                static_assert(false, "Platform not supported");
#endif

                invData.outputItems.push_back(bb->data.internalPath + filePath.right(filePath.length() - cut));
            }
            break;
        }
        case ArchivingFilePaths::RelativePaths:
        {
            for (const auto& item : bb->data.inputItems)
                invData.outputItems.push_back(bb->data.internalPath + item.fileName());
            break;
        }
        default:
            Q_ASSERT(false);
        }

        invData.options.push_back(QLatin1String("-s1")); // Do not print file names

        const auto compressionLevel = Utils::getValueAs<int>(Utils::getValue(privateData,
                                                                             PrivateArchivingParameter::CompressionLevel));

        Q_ASSERT(compressionLevel >= 0 && compressionLevel <= 5);
        invData.options.push_back(QLatin1String("-m") + QString::number(compressionLevel));

        const auto storeAttributes = Utils::getValueAs<bool>(Utils::getValue(privateData,
                                                                             PrivateArchivingParameter::FileAttributes));
        if (!storeAttributes)
            invData.options.push_back(QLatin1String("-noattributes"));

        invData.password = bb->data.password;

        handler->startBackend(invData);
    };

    const auto renameStep = [bb = bb] ()
    {
        auto handler = bb->handler;

        if (!handler)
            return;

        try
        {
            handler->swapArchives(bb->data.destinationPath + bb->tempFileName,
                                  bb->data.destinationPath + bb->data.archiveName);
        }

        catch (FileOperationException& e)
        {
            handler->addError(BackendHandlerError::TemporaryArchiveError, e.error);
            handler->onStepStateChanged(BackendHandlerState::Errors);
            return;
        }

        handler->onStepStateChanged(BackendHandlerState::Success);
    };

    if (!bb->data.createNewArchive)
    {
        bb->addingToTempArchive = true;
        bb->tempFileName = createTempArchiveName(bb->data.destinationPath, bb->data.archiveName);

        stepQueue.push_back(repackStep);
        stepQueue.push_back(addToArchiveStep);
        stepQueue.push_back(renameStep);
    }
    else
    {
        stepQueue.push_back(addToArchiveStep);
    }

    /* As of version 7.15 ZPAQ does not report disk full when compressing and
     * the process returns 0 (SUCCESS). It also continues compression despite the
     * disk being full.
     */

    startQueue();
}


void BackendHandler::extractArchiveImpl(const BackendExtractionData& data)
{
    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            [publ = makeQPointer(this),
            password = data.password]
            () mutable
    {
        if (!publ)
            return;

        publ->onExtractionProcessFinished(password);
    });

    connect(outPreprocessor, &TextStreamPreprocessor::unfinishedLine,
            this, &BackendHandler::progressLineHandler);

    InvocationData invData;
    invData.command = QLatin1String("x");
    invData.archiveFilePath = data.archiveInfo.absoluteFilePath();

    QString tempDestPath = data.tempDestinationPath;
    if (!tempDestPath.endsWith(QLatin1Char('/')))
        tempDestPath.append(QLatin1Char('/'));

    if (!data.filesToExtract.empty())
    {
        for (const auto& item : data.filesToExtract)
            invData.inputItems.push_back(item);

        for (const auto& item : data.filesToExtract)
            invData.outputItems.push_back(tempDestPath+ item);
    }
    else
        invData.itemsDestinationPath = tempDestPath;

    invData.options.push_back(QLatin1String("-force"));
    invData.options.push_back(QLatin1String("-s1")); // Do not print file names
    if (data.encryptionState == EncryptionState::EntireArchive)
        invData.password = data.password;

    startBackend(invData);
}


void BackendHandler::readArchiveImpl(const BackendReadData& data)
{
    /* Snapshots are listed first and then items. Unfortunately ZPAQ has to
       be invoked twice which doubles task time. However ZPAQ lists items almost
       twice as fast as 7-ZIP because items take only one line. */
    auto dataPtr = std::make_shared<BackendReadData>(data);

    auto listSnapshotsStep = [publ = makeQPointer(this),
            dataPtr = dataPtr,
            connectionToFinished = QMetaObject::Connection()] () mutable
    {
        if (!publ)
            return;

        publ->archiveProperties[BackendArchiveProperty::TotalSize] = dataPtr->archiveInfo.size();

        connect(publ->outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
                publ, &BackendHandler::listSnaphotsHandler);

        const auto finishedSignal = static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished);

        connectionToFinished = connect(publ->backendProcess, finishedSignal,
                                       [publ = publ,
                                       &connectionToFinished = connectionToFinished]
                                       () mutable
        {
            if (!publ)
                return;

            auto encryptionState = EncryptionState::Disabled;

            if (!publ->lastInvocationData.password.isEmpty())
                encryptionState = EncryptionState::EntireArchive;

            publ->archiveProperties[BackendArchiveProperty::EncryptionState].setValue(encryptionState);

            publ->snapshotInfo.chop(1); // remove final '\n'
            publ->archiveProperties[BackendArchiveProperty::Snapshots] = publ->snapshotInfo;

            const auto state = publ->handleFinishedProcess();

            if (state == BackendHandlerState::Success || state == BackendHandlerState::Warnings)
            {
                const auto disconnect1 = publ->backendProcess->disconnect(connectionToFinished);
                Q_ASSERT(disconnect1);

                const auto disconnect2 = QObject::disconnect(publ->outPreprocessor,
                                                             &TextStreamPreprocessor::fullLinesAvailable,
                                                             publ,
                                                             nullptr);
                Q_ASSERT(disconnect2);

                // Do not log the same errors again when reading items
                publ->logErrors = false;
            }

            publ->onStepStateChanged(state);
        });

        InvocationData invData;
        invData.command = QLatin1String("l");
        invData.archiveFilePath = dataPtr->archiveInfo.absoluteFilePath();
        invData.options.push_back(QLatin1String("-all"));
        invData.options.push_back(QString::number(publ->snapshotDigits));
        invData.options.push_back(QLatin1String("-not"));
        invData.options.push_back(QLatin1String("*/?*"));
        invData.password = dataPtr->password;

        publ->startBackend(invData);
    };

    auto listItemsStep = [publ = makeQPointer(this), dataPtr = dataPtr] ()
    {
        if (!publ)
            return;

        connect(publ->outPreprocessor, &TextStreamPreprocessor::fullLinesAvailable,
                publ, &BackendHandler::listItemsHandler);

        const auto finishedSignal = static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished);

        connect(publ->backendProcess, finishedSignal,
                [publ = publ]
                ()
        {
            if (!publ)
                return;

            const auto state = publ->handleFinishedProcess();

            publ->onStepStateChanged(state);
        });

        InvocationData invData;
        invData.command = QLatin1String("l");
        invData.archiveFilePath = dataPtr->archiveInfo.absoluteFilePath();
        invData.password = publ->lastInvocationData.password;

        publ->startBackend(invData);
    };

    stepQueue.push_back(listSnapshotsStep);
    stepQueue.push_back(listItemsStep);
    startQueue();
}


void BackendHandler::testArchiveImpl(const BackendTestData& data)
{
    connect(backendProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, [publ = this, password = data.password]
            () mutable
    {
        publ->onExtractionProcessFinished(password);
    });

    connect(outPreprocessor, &TextStreamPreprocessor::unfinishedLine,
            this, &BackendHandler::progressLineHandler);

    InvocationData invData;
    invData.command = QLatin1String("x");
    invData.archiveFilePath = data.archiveInfo.absoluteFilePath();
    invData.options.push_back(QLatin1String("-test"));
    invData.options.push_back(QLatin1String("-s1")); // Do not print file names
    if (data.encryptionState == EncryptionState::EntireArchive)
        invData.password = data.password;

    startBackend(invData);
}


void BackendHandler::startBackend(const InvocationData& data)
{
    changeState(BackendHandlerState::InProgress);

    logCommand("zpaq", composeCommand(data, false));

    lastInvocationData = data;

    backendProcess->start("zpaq", composeCommand(data, true));
}


QStringList BackendHandler::composeCommand(const InvocationData& data, bool includePassword) const
{
    auto args = QStringList() << data.command << data.archiveFilePath;

    // inputItems is not empty when extracting selected files or when adding files to archive
    if (!data.inputItems.isEmpty())
    {
        for (const auto& item : data.inputItems)
            args << item;

        // outputItems is empty when when archiving with absolute paths (as provided by intputItems)
        if (!data.outputItems.isEmpty())
        {
            Q_ASSERT(data.inputItems.size() == data.outputItems.size());

            args << QLatin1String("-to");

            for (const auto& item : data.outputItems)
                args << item;
        }
    }
    // inputItems is empty when extracting entire archive, testing or reading
    else
    {
        if (!data.itemsDestinationPath.isEmpty())
            args << QLatin1String("-to") << data.itemsDestinationPath;
    }

    if (data.repackInfo.enabled)
    {
        args << QLatin1String("-repack");
        args << data.repackInfo.newArchivePath;
        if (!data.repackInfo.newPassword.isEmpty())
        {
            if (includePassword)
                args << data.repackInfo.newPassword;
            else
                args << QLatin1String("*");
        }
        args << data.repackInfo.options;
    }

    args << data.options;

    if (!data.password.isEmpty())
    {
        if (includePassword)
            args << QLatin1String("-key") << data.password;
        else
            args << QLatin1String("-key") << QLatin1String("*");
    }

    return args;
}


BackendHandlerState BackendHandler::handleFinishedProcess()
{
    auto intepretation = BackendExitCodeInterpretation::Success;

    if (passwordRequired && lastInvocationData.password.isEmpty())
        intepretation = BackendExitCodeInterpretation::PasswordRequiredNotProvided;
    else if (backendProcess->exitCode() != 0)
        intepretation = BackendExitCodeInterpretation::WarningsOrErrors;

    return CLIBackendHandler::handleFinishedProcess(intepretation);
}


// Handles extraction, preview and test
void BackendHandler::onExtractionProcessFinished(QString& password)
{
    const auto state = handleFinishedProcess();

    /* If the password was provided but the task was created in Queue (without opening the archive)
       it is unknown whether the archive is actually encrypted. An if it's not and we set the
       password ZPAQ will try to open unencrypted archive with a password and it will fail and
       we would show password prompt. This behavior is different from 7-Zip and The Unarchiver
       which do not use password unless the the archive is encrypted. Therefore we invoke ZPAQ
       without a password first, unless the archive has been opened from the Browser and
       encryptionState indicates that it is encrypted. */
    if (state == BackendHandlerState::WaitingForPassword && !password.isEmpty())
    {
        lastInvocationData.password = password;

        /* Clear the password so it is used only once. Subsequent attempts are
           handled by the application by creating new BackendHandlers. */
        password.clear();
        return startBackend(lastInvocationData);
    }

    onStepStateChanged(state);
}


void BackendHandler::errorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line.endsWith(QLatin1String("zpaq error: password incorrect")))
        {
            if (!lastInvocationData.password.isEmpty())
                addError(BackendHandlerError::InvalidPasswordOrDataError, line);
            else
                passwordRequired = true;
        }
        else if (line.startsWith(QLatin1String("Skipping block at")))
        {
            if (logErrors) // Do not log the same errors twice when reading archive
                addError(BackendHandlerError::DataError, line);
        }
        else if (line == QLatin1String("Failed (extracted/total fragments, file):"))
        {
            if (hasError(BackendHandlerError::DiskFull))
                addLogLine(line);
            else if (logErrors) // Do not log the same errors twice when reading archive
                addError(BackendHandlerError::DataError, line);
        }
        else if (line.endsWith(QLatin1String(": No such file or directory")))
        {
            auto error = BackendHandlerError::___INVALID;

            if (hasError(BackendHandlerError::DiskFull))
                addLogLine(line);
            else
            {
                if (getTaskType() == BackendHandlerTaskType::Extract)
                {
                    diskWatcher->refresh();
                    if (diskWatcher->diskFullDetected())
                        error = BackendHandlerError::DiskFull;
                    // This message is printed also when archive could not be found,
                    // however task workers check archive existence before starting backend handlers
                    // so we can assume it is an unlikely scenario here.
                    else
                        error = BackendHandlerError::WriteError;
                }
                else if (getTaskType() == BackendHandlerTaskType::Add)
                    error = BackendHandlerError::FileNotFound; // Input file not found
            }

            if (error != BackendHandlerError::___INVALID)
                addError(error, line);
        }
        else if (line.endsWith(QLatin1String(": Permission denied")))
        {
            addError(BackendHandlerError::AccessDenied, line);
        }
        else if (!line.endsWith(QLatin1String("(all OK)"))
                 && !line.endsWith(QLatin1String("(with warnings)"))
                 && !line.endsWith(QLatin1String("(with errors)")))
            addLogLine(line);
    }
}


void BackendHandler::listSnaphotsHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line.startsWith(QLatin1String("- ")))
        {
            snapshotInfo += line.mid(41, snapshotDigits).toString() // Snapshot number
                    + QLatin1String(" | ")
                    + line.mid(2, 19)  + QLatin1String(" UTC") // Date
                    + QLatin1String(" |")
                    + line.mid(46) // Changes in snapshot
                    + QLatin1Char('\n');
        }
    }
}


void BackendHandler::listItemsHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line.startsWith(QLatin1String("- ")))
        {
            const auto dateTime = line.mid(2, 19);
            tempItem.modified = QDateTime::fromString(dateTime.toString(), Qt::ISODate);
            // zpaq archives store UTC time and CLI also prints UTC time
            tempItem.modified.setTimeSpec(Qt::UTC);
            // Currently it is recommended to store local time as it is shown to the user
            tempItem.modified = tempItem.modified.toLocalTime();
            const auto attr = line.mid(35, 5);
            tempItem.attributes = attr.toString();

            tempItem.name = line.mid(41).toString();

            if (tempItem.name.endsWith(QLatin1Char('/')))
            {
                tempItem.nodeType = NodeType::Folder;
            }
            else
            {
                const auto size = line.mid(21, 13);
                tempItem.originalSize = size.toULongLong();
            }

            tempItem.encrypted = !lastInvocationData.password.isEmpty();

            items.emplace_back(std::move(tempItem));
        }
    }

    if (!items.empty())
        emit newItems(items);
}


void BackendHandler::progressLineHandler(QString& line, int newDataPosition)
{   
    Q_UNUSED(newDataPosition)

    const int pos = line.indexOf(QLatin1Char('%'));

    if (pos > 0)
    {
        const auto str = line.leftRef(pos);
        const auto progress = str.toDouble();
        if (progress >= 0 && progress <= 100)
        {
            changeProgress(static_cast<int>(progress));
        }
    }

    line.clear();
}


void BackendHandler::startQueue()
{
    Q_ASSERT(!stepQueue.empty());
    stepQueue[currentStep]();
}


void BackendHandler::onStepStateChanged(BackendHandlerState state)
{
    if ((state == BackendHandlerState::Success || state == BackendHandlerState::Warnings)
            && currentStep + 1 < stepQueue.size())
    {
        outPreprocessor->clear();
        errPreprocessor->clear();

        ++currentStep;

        QTimer::singleShot(0, this, &BackendHandler::startQueue);

        return;
    }

    changeState(state);
}


void BackendHandler::removeFile(const QString& filePath)
{
    QDir d;

    if (!d.remove(filePath))
    {
        const QString error = Packuru::Plugins::ZPAQ::Core::BackendHandler
                ::tr("Cannot remove file %1").arg(filePath);
        throw FileOperationException{error};
    }
}


void BackendHandler::swapArchives(const QString& temporaryFilePath, const QString& originalFilePath)
{
    removeFile(originalFilePath);

    QDir d;
    if (!d.rename(temporaryFilePath, originalFilePath))
    {
        const QString error = Packuru::Plugins::ZPAQ::Core::BackendHandler
                ::tr("Cannot rename file %1 to %2")
                .arg(temporaryFilePath)
                .arg(originalFilePath);
        throw FileOperationException{error};
    }
}


QString BackendHandler::createTempArchiveName(const QString& path, const QString& originalName)
{
    QFileInfo info;

    for (int i = 0; ; ++i)
    {
        info.setFile(path + originalName + QLatin1String(".tmp") + QString::number(i));
        if (!info.exists())
            break;
    }

    return info.fileName();
}

}
