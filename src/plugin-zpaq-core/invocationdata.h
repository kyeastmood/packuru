// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QStringList>


namespace Packuru::Plugins::ZPAQ::Core
{

struct InvocationData
{
    QLatin1String command;
    QString archiveFilePath;
    QStringList inputItems;
    QStringList outputItems;
    QString itemsDestinationPath;
    QStringList options;
    // Repacking is used when adding to archive - a temporary archive is created by repacking.
    struct
    {
        bool enabled = false;
        QString newArchivePath;
        QString newPassword;
        QStringList options;
    } repackInfo;
    QString password;
};

}
