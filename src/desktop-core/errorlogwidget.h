// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QDialog>


namespace Ui {
class ErrorLogWidget;
}


class ErrorLogWidget : public QDialog
{
    Q_OBJECT
public:
    explicit ErrorLogWidget(QWidget *parent = nullptr);
    ~ErrorLogWidget() override;

    void setText(const QString& text);

private:
    Ui::ErrorLogWidget *ui;
};
