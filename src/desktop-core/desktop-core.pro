# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)
include(../common-lib.pri)
include(../common-desktop.pri)

MODULE_BUILD_NAME_SUFFIX = $$DESKTOP_CORE_BUILD_NAME_SUFFIX
MODULE_BUILD_NAME = $$DESKTOP_CORE_BUILD_NAME

include(../embed-qm-files.pri)

TARGET = $$qtLibraryTarget($$MODULE_BUILD_NAME)

LIBS += -L$${PROJECT_TARGET_LIB_DIR}
LIBS += -l$$QCS_QTWIDGETS_BUILD_NAME
LIBS += -l$${CORE_BUILD_NAME}

DEFINES += $${PROJECT_MACRO_NAME}_DESKTOP_CORE_LIBRARY

TRANSLATIONS += \
    translations/$${MODULE_BUILD_NAME_SUFFIX}_en.ts \
    translations/$${MODULE_BUILD_NAME_SUFFIX}_nl.ts \

HEADERS +=\
    passwordedithandler.h \
    private/archiveslistwidget.h \
    extractiondialog.h \
    private/filelistwidget.h \
    private/archivepage.h \
    extractionfolderoverwriteprompt.h \
    extractionfileoverwriteprompt.h \
    extractiontypemismatchprompt.h \
    errorlogwidget.h \
    globalsettings.h \
    settingsdialog.h \
    symbol_export.h \
    testdialog.h \
    aboutdialog.h \
    createcolumnconfigmenu.h \
    archivingdialog.h \
    showcommandlinedialog.h \
    private/archivetypemodeldelegate.h \
    appclosingdialog.h

SOURCES += \
    passwordedithandler.cpp \
    private/archiveslistwidget.cpp \
    extractiondialog.cpp \
    private/filelistwidget.cpp \
    private/archivepage.cpp \
    extractionfolderoverwriteprompt.cpp \
    extractionfileoverwriteprompt.cpp \
    extractiontypemismatchprompt.cpp \
    errorlogwidget.cpp \
    settingsdialog.cpp \
    testdialog.cpp \
    aboutdialog.cpp \
    archivingdialog.cpp \
    showcommandlinedialog.cpp \
    private/archivetypemodeldelegate.cpp \
    appclosingdialog.cpp

FORMS += \
    private/archiveslistwidget.ui \
    extractiondialog.ui \
    private/filelistwidget.ui \
    private/archivepage.ui \
    extractionfolderoverwriteprompt.ui \
    extractionfileoverwriteprompt.ui \
    extractiontypemismatchprompt.ui \
    errorlogwidget.ui \
    settingsdialog.ui \
    testdialog.ui \
    aboutdialog.ui \
    archivingdialog.ui
