// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QDialog>


namespace Ui {
class ExtractionFolderOverwritePrompt;
}


namespace Packuru::Core
{
class ExtractionFolderOverwritePromptCore;
}

class ExtractionFolderOverwritePrompt : public QWidget
{
    Q_OBJECT
public:
    explicit ExtractionFolderOverwritePrompt(Packuru::Core::ExtractionFolderOverwritePromptCore* promptCore,
                                             bool browserMode,
                                             QWidget *parent = nullptr);
    ~ExtractionFolderOverwritePrompt() override;

    void showAndSetup();

private:
    void closeEvent(QCloseEvent* event) override;
    bool eventFilter(QObject *watched, QEvent *event) override;

    Ui::ExtractionFolderOverwritePrompt *ui;
    Packuru::Core::ExtractionFolderOverwritePromptCore* promptCore;
};

