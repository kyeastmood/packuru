// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "symbol_export.h"


class QWidget;
class QString;


void PACKURU_DESKTOP_CORE_EXPORT showCommandLineDialog(QWidget* parent,
                                                         const QString& title,
                                                         bool modal,
                                                         const QString& text);
