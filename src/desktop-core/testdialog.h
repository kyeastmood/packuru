// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QDialog>


namespace Packuru::Core
{
class TestDialogCore;
}

namespace Ui {
class TestDialog;
}


class TestDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TestDialog(Packuru::Core::TestDialogCore* core,
                        QWidget *parent = nullptr);
    ~TestDialog();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

