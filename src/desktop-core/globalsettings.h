// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../core/globalsettingsuservalue.h"


namespace Packuru::Desktop
{

enum class GlobalSettings
{
    DoubleClickToOpenItems = Packuru::Core::globalSettingsUserValue // bool
};

static int toInt (GlobalSettings key) { return static_cast<int>(key); }

}
