// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCloseEvent>
#include <QFocusEvent>
#include <QAction>

#include "../core/extractionfolderoverwritepromptcore.h"
#include "../core/htmllink.h"
#include "../core/commonstrings.h"

#include "extractionfolderoverwriteprompt.h"
#include "ui_extractionfolderoverwriteprompt.h"


using namespace Packuru::Core;


ExtractionFolderOverwritePrompt::ExtractionFolderOverwritePrompt(ExtractionFolderOverwritePromptCore* promptCore, bool browserMode, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ExtractionFolderOverwritePrompt),
    promptCore(promptCore)
{
    ui->setupUi(this);

    const auto content = promptCore->getString(ExtractionFolderOverwritePromptCore::UserString::Content);
    const auto folder = promptCore->getString(ExtractionFolderOverwritePromptCore::UserString::Folder);
    const auto modified = promptCore->getString(ExtractionFolderOverwritePromptCore::UserString::Modified);

    ui->sourceLabel->setText(promptCore->getString(ExtractionFolderOverwritePromptCore::UserString::Source));
    ui->sourceFolderLabel->setText(folder);
    ui->sourceContentLabel->setText(content);
    ui->sourceModifiedLabel->setText(modified);

    ui->destinationLabel->setText(promptCore->getString(ExtractionFolderOverwritePromptCore::UserString::Destination));
    ui->destFolderLabel->setText(folder);
    ui->destContentLabel->setText(content);
    ui->destModifiedLabel->setText(modified);

    ui->applyToAllCheckBox->setText(promptCore->getString(ExtractionFolderOverwritePromptCore::UserString::ApplyToAll));
    ui->skipButton->setText(promptCore->getString(ExtractionFolderOverwritePromptCore::UserString::Skip));
    ui->overwriteButton->setText(promptCore->getString(ExtractionFolderOverwritePromptCore::UserString::WriteInto));
    ui->retryButton->setText(promptCore->getString(ExtractionFolderOverwritePromptCore::UserString::Retry));
    ui->abortButton->setText(promptCore->getString(ExtractionFolderOverwritePromptCore::UserString::Abort));

    if (!browserMode)
    {
        setWindowFlag(Qt::Dialog);
        ui->infoLabel->hide();
        layout()->setContentsMargins(9, 9, 9, 9);
    }

    const QString text(promptCore->getString(ExtractionFolderOverwritePromptCore::UserString::FolderAlreadyExists));
    setWindowTitle(text);
    ui->infoLabel->setText(text + QLatin1Char('.'));

    ui->sourcePath->setText(htmlLink("file://" + promptCore->sourcePath(), promptCore->sourcePath()));
    ui->sourceContent->setText(promptCore->sourceContent());
    ui->sourceModified->setText(promptCore->sourceModifiedDate());

    ui->destinationPath->setText(htmlLink("file://" + promptCore->destinationPath(), promptCore->destinationPath()));
    ui->destinationContent->setText(promptCore->destinationContent());
    ui->destinationModified->setText(promptCore->destinationModifiedDate());

    connect(ui->skipButton, &QPushButton::clicked,
            promptCore, [promptCore = promptCore, ui = ui] ()
    {
        promptCore->skip(ui->applyToAllCheckBox->isChecked());
    });

    connect(ui->overwriteButton, &QPushButton::clicked,
            promptCore, [promptCore = promptCore, ui = ui] ()
    {
        promptCore->overwrite(ui->applyToAllCheckBox->isChecked());
    });

    connect(ui->retryButton, &QPushButton::clicked,
            promptCore, &ExtractionFolderOverwritePromptCore::retry);

    connect(ui->abortButton, &QPushButton::clicked,
            promptCore, &ExtractionFolderOverwritePromptCore::abort);

    auto abortAction = new QAction(this);
    abortAction->setShortcut(Qt::Key_Escape);
    connect(abortAction, &QAction::triggered,
            promptCore, &ExtractionFolderOverwritePromptCore::abort);
    addAction(abortAction);

    connect(promptCore, &QObject::destroyed,
            this, &QObject::deleteLater);

    ui->applyToAllCheckBox->installEventFilter(this);
    ui->skipButton->installEventFilter(this);
    ui->overwriteButton->installEventFilter(this);
    ui->retryButton->installEventFilter(this);
    ui->abortButton->installEventFilter(this);

    setFocusProxy(ui->retryButton);
}


ExtractionFolderOverwritePrompt::~ExtractionFolderOverwritePrompt()
{
    delete ui;
}


void ExtractionFolderOverwritePrompt::showAndSetup()
{
    show();
    setFixedSize({width(), height()});
}


void ExtractionFolderOverwritePrompt::closeEvent(QCloseEvent* event)
{
    event->ignore();
}


bool ExtractionFolderOverwritePrompt::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::FocusIn)
    {
        const auto focusEvent = static_cast<QFocusEvent*>(event);
        Q_ASSERT(event);

        const auto reason = focusEvent->reason();

        if  (reason == Qt::MouseFocusReason
             || reason == Qt::TabFocusReason
             || reason == Qt::BacktabFocusReason)
        {
            auto widget = qobject_cast<QWidget*>(watched);
            Q_ASSERT(widget);
            setFocusProxy(widget);
        }
    }

    return false;
}
