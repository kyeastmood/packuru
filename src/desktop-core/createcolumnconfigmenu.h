// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QAbstractItemModel>
#include <QMenu>


template <typename ViewWithColumns, typename Callback>
void createColumnConfigMenu(ViewWithColumns* view,
                            QAbstractItemModel* model,
                            QWidget* parent,
                            const QPoint& pos,
                            const Callback& triggerCallback)
{
    auto menu = new QMenu(parent);
    auto columnCount = model->columnCount();
    for (int column = 0; column < columnCount; ++column)
    {
        auto action = new QAction;
        action->setCheckable(column > 0);
        action->setChecked(!view->header()->isSectionHidden(column));
        auto text = model->headerData(column, Qt::Horizontal, Qt::DisplayRole).toString();
        action->setText(text);

        QObject::connect(action, &QAction::toggled,
                         [view = view, section = column] (auto value)
        { view->header()->setSectionHidden(section, !value); });

        menu->addAction(action);
    }

    QObject::connect(menu, &QMenu::triggered,
                     triggerCallback);

    menu->popup(view->mapToGlobal(pos));
}
