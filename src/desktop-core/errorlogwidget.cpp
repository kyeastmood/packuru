// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QSettings>

#include "../core/commonstrings.h"

#include "errorlogwidget.h"
#include "ui_errorlogwidget.h"


using namespace Packuru::Core;


ErrorLogWidget::ErrorLogWidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ErrorLogWidget)
{
    ui->setupUi(this);
    setWindowTitle(CommonStrings::getString(CommonStrings::UserString::ErrorLogDialogTitle));

    QSettings s;
    const QSize size = s.value("error_log_widget_size").toSize();
    if (size.isValid())
        resize(size);
}


ErrorLogWidget::~ErrorLogWidget()
{
    QSettings s;
    s.setValue("error_log_widget_size", size());
    delete ui;
}


void ErrorLogWidget::setText(const QString& text)
{
    ui->plainTextEdit->setPlainText(text);
}
