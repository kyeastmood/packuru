// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once
#include <QWidget>


namespace Ui {
class ExtractionTypeMismatchPrompt;
}


namespace Packuru::Core
{
class ExtractionTypeMismatchPromptCore;
}


class ExtractionTypeMismatchPrompt : public QWidget
{
    Q_OBJECT
public:
    explicit ExtractionTypeMismatchPrompt(Packuru::Core::ExtractionTypeMismatchPromptCore* promptCore,
                                          bool browserMode,
                                          QWidget *parent = nullptr);
    ~ExtractionTypeMismatchPrompt() override;

    void showAndSetup();

private:
    void closeEvent(QCloseEvent* event) override;
    bool eventFilter(QObject *watched, QEvent *event) override;

    Ui::ExtractionTypeMismatchPrompt *ui;
    Packuru::Core::ExtractionTypeMismatchPromptCore* promptCore;
};
