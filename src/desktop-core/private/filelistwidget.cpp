// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QAction>

#include "qcs-qtwidgets/widgetmapper.h"

#include "core/archivingdialogcontrollertype.h"
#include "core/archivingdialogcore.h"

#include "filelistwidget.h"
#include "ui_filelistwidget.h"


using namespace Packuru::Core;


FileListWidget::FileListWidget(ArchivingDialogCore* dialogCore,
                               qcs::qtw::WidgetMapper* /*mapper*/,
                               bool foldersAllowed,
                               QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FileListWidget)
{
    ui->setupUi(this);

    ui->addFilesButton->setText(ArchivingDialogCore::getString(ArchivingDialogCore::UserString::AddFiles));
    ui->addFoldersButton->setText(ArchivingDialogCore::getString(ArchivingDialogCore::UserString::AddFolder));
    ui->removeButton->setText(ArchivingDialogCore::getString(ArchivingDialogCore::UserString::RemoveItems));

    ui->view->header()->setDefaultAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    ui->view->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->view->setModel(dialogCore->getFileModel());

    connect(ui->view->selectionModel(), &QItemSelectionModel::selectionChanged,
            this, &FileListWidget::onSelectionChanged);

    connect(ui->addFilesButton, &QPushButton::clicked,
            this, &FileListWidget::addFilesRequested);

    connect(ui->addFoldersButton, &QPushButton::clicked,
            this, &FileListWidget::addFolderRequested);

    connect(ui->removeButton, &QPushButton::clicked,
            this, &FileListWidget::removeItemsRequested);

    if (!foldersAllowed)
    {
        ui->addFoldersButton->hide();

        ui->horizontalLayout->removeItem(ui->addFoldersSpacer);
        delete ui->addFoldersSpacer;
        ui->addFoldersSpacer = nullptr;
    }

    setFocusProxy(ui->addFoldersButton);

    auto addFolderAction = new QAction(this);
    addFolderAction->setShortcut(Qt::Key_Insert);
    connect (addFolderAction, &QAction::triggered,
             this, &FileListWidget::addFolderRequested);
    ui->view->addAction(addFolderAction);

    auto addFileAction = new QAction(this);
    addFileAction->setShortcut(Qt::CTRL + Qt::Key_Insert);
    connect (addFileAction, &QAction::triggered,
             this, &FileListWidget::addFilesRequested);
    ui->view->addAction(addFileAction);

    auto removeAction = new QAction(this);
    removeAction->setShortcut(QKeySequence::Delete);
    connect (removeAction, &QAction::triggered,
             this, &FileListWidget::removeItemsRequested);
    ui->view->addAction(removeAction);
}


FileListWidget::~FileListWidget()
{
    delete ui;
}


void FileListWidget::onSelectionChanged(const QItemSelection &selected, const QItemSelection &)
{
    if (selected.indexes().empty())
        ui->removeButton->setEnabled(false);
    else
        ui->removeButton->setEnabled(true);
}


QModelIndexList FileListWidget::getSelectedIndexes() const
{
    return ui->view->selectionModel()->selectedRows();
}
