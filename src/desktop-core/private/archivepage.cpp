// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QFileDialog>

#include "qcs-qtwidgets/widgetmapper.h"

#include "core/archivingdialogcontrollertype.h"
#include "core/archivingdialogcore.h"

#include "archivepage.h"
#include "ui_archivepage.h"


using namespace Packuru::Core;


ArchivePage::ArchivePage(QDialog* dialog,
                         ArchivingDialogCore* dialogCore,
                         qcs::qtw::WidgetMapper* mapper,
                         QWidget *parent)
    : QWidget(parent),
      ui(new Ui::ArchivePage),
      dialog(dialog),
      dialogCore(dialogCore),
      mapper(mapper)
{
    ui->setupUi(this);

    ui->onCompletionLabel->setText(dialogCore->getString(ArchivingDialogCore::UserString::OnCompletion));

    using Type = ArchivingDialogControllerType::Type;

    mapper->addLabelAndWidget(Type::ArchiveName,
                              ui->archiveNameLabel,
                              ui->archiveNameEdit);

    mapper->addWidgets(Type::ArchiveNameError,
                       ui->archiveNameErrorsLabel);

    mapper->addLabelAndWidget(Type::ArchiveType,
                              ui->archiveTypeLabel,
                              ui->archiveTypeComboBox);

    mapper->addLabelAndWidget(Type::ArchivingMode,
                              ui->archivingModeLabel,
                              ui->archivingModeCombo);

    mapper->addLabelAndWidget(Type::DestinationMode,
                              ui->destinationModeLabel,
                              ui->destinationModeCombo);

    mapper->addLabelAndWidget(Type::DestinationPath,
                              ui->destinationPathLabel,
                              ui->destinationPathEdit);

    mapper->addWidgets(Type::DirectoryError, ui->destinationErrorsLabel);

    mapper->addWidgets(Type::OpenDestinationPath, ui->openDestinationCheck);
    mapper->addWidgets(Type::OpenNewArchive, ui->openNewArchiveCheck);

    mapper->addLabelAndWidget(Type::SplitPreset,
                              ui->splitLabel,
                              ui->splitPresets);

    mapper->addLabelAndWidget(Type::SplitSize,
                              ui->partSizeLabel,
                              ui->splitSizeValue);

    mapper->addBuddies(Type::DestinationPath,
                       ui->destinationPathButton);

    if (auto gridLayout = qobject_cast<QGridLayout*>(layout()))
    {
        gridLayout->setColumnStretch(0, 0);
        gridLayout->setColumnStretch(1, 1);
        gridLayout->setColumnStretch(2, 1);
        gridLayout->setColumnStretch(3, 1);
    }

    connect(ui->archiveTypeComboBox, static_cast<void(QComboBox::*)(int)>(&QComboBox::activated),
            this, &ArchivePage::archiveTypeChanged);

    connect(ui->destinationPathButton, &QPushButton::clicked,
                     [priv = this] () { priv->onDestinationPathButtonClicked(); });

    setFocusProxy(ui->archiveNameEdit);
}


ArchivePage::~ArchivePage()
{
    delete ui;
}


void ArchivePage::onDestinationPathButtonClicked()
{
    QString folder = QFileDialog::getExistingDirectory(dialog,
                                                       QString(),
                                                       ui->destinationPathEdit->text());

    if (folder.isEmpty())
        return;

    ui->destinationPathEdit->setText(folder);
}
