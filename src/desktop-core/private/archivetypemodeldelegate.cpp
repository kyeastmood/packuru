// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QComboBox>
#include <QWheelEvent>

#include "core/archivetypemodel.h"
#include "core/archivetypefiltermodel.h"

#include "archivetypemodeldelegate.h"


using namespace Packuru::Core;


ArchiveTypeModelDelegate::ArchiveTypeModelDelegate(QObject* parent)
    : QStyledItemDelegate(parent)
{

}


QWidget* ArchiveTypeModelDelegate::createEditor(QWidget* parent,
                                                const QStyleOptionViewItem& option,
                                                const QModelIndex& index) const
{
    Q_UNUSED(option)
    auto editor = new QComboBox(parent);

    const auto* const filterModel = qobject_cast<const ArchiveTypeFilterModel*>(index.model());
    Q_ASSERT(filterModel);

    const auto* const model = qobject_cast<const ArchiveTypeModel*>(filterModel->sourceModel());
    Q_ASSERT(model);

    const auto sourceIndex = filterModel->mapToSource(index);
    const QStringList set = model->getValueSet(sourceIndex);
    editor->addItems(set);
    editor->setCurrentText(model->data(sourceIndex, Qt::DisplayRole).toString());
    if (set.size() < 2)
        editor->setEnabled(false);
    else
    {
        connect(editor, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                [editor = editor,
                model = const_cast<ArchiveTypeModel*>(model),
                index= sourceIndex] ()
        { model->setData(index, editor->currentText(), Qt::EditRole); });
    }

    return editor;
}


void ArchiveTypeModelDelegate::updateEditorGeometry(QWidget* editor,
                                                    const QStyleOptionViewItem& option,
                                                    const QModelIndex& index) const
{
    Q_UNUSED(index)
    editor->setGeometry(option.rect);
}
