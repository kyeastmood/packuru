// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QFileDialog>
#include <QSettings>

#include "../qcs-qtwidgets/widgetmapper.h"

#include "../core/extractiondialogcontrollertype.h"
#include "../core/extractiondialogcore.h"
#include "../core/commonstrings.h"

#include "extractiondialog.h"
#include "ui_extractiondialog.h"
#include "private/archiveslistwidget.h"
#include "passwordedithandler.h"


using namespace Packuru::Core;


struct ExtractionDialog::Private
{
    Private(ExtractionDialog* publ,
            ExtractionDialogCore* core,
            Ui::ExtractionDialog* ui);
    ~Private();

    void onAddArchivesRequested();
    void onRemoveArchivesRequested();
    void onDestinationPathButtonClicked();

    ExtractionDialog* publ = nullptr;
    ExtractionDialogCore* core = nullptr;
    Ui::ExtractionDialog* ui = nullptr;
    qcs::qtw::WidgetMapper* widgetMapper = nullptr;
    ArchivesListWidget* archivesListWidget = nullptr;
};


ExtractionDialog::ExtractionDialog(ExtractionDialogCore *core, QWidget* parent)
    : QDialog(parent),
      ui(new Ui::ExtractionDialog)
{
    ui->setupUi(this);

    ui->topTabWidget->setTabText(0, core->getString(ExtractionDialogCore::UserString::TabOptions));
    ui->rejectButton->setText(core->getString(ExtractionDialogCore::UserString::Cancel));

    priv.reset(new Private(this, core, ui));

    if (core->getMode() == ExtractionDialogCore::DialogMode::MultiArchive)
    {
        priv->archivesListWidget = new ArchivesListWidget(priv->core->getArchivesModel(),this);

        connect(priv->archivesListWidget, &ArchivesListWidget::sigAddArchivesRequested,
                [priv = priv.get()] () { priv->onAddArchivesRequested(); });

        connect(priv->archivesListWidget, &ArchivesListWidget::sigRemoveArchivesRequested,
                [priv = priv.get()] () { priv->onRemoveArchivesRequested(); });

        ui->topTabWidget->insertTab(0,
                                    priv->archivesListWidget,
                                    core->getString(ExtractionDialogCore::UserString::TabArchives));

        if (priv->core->getArchivesModel()->rowCount() == 0)
            ui->topTabWidget->setCurrentIndex(0);
    }

    connect(ui->destinationButton, &QPushButton::clicked,
            [priv = priv.get()] () { priv->onDestinationPathButtonClicked(); });

    connect(this, &QDialog::accepted,
            core, &ExtractionDialogCore::accept);

    QSettings s;
    const QSize size = s.value("extraction_dialog_size").toSize();
    if (size.isValid())
        resize(size);
}


ExtractionDialog::~ExtractionDialog()
{
    QSettings s;
    s.setValue("extraction_dialog_size", size());
    delete ui;
}


ExtractionDialog::Private::Private(ExtractionDialog* publ,
                                   ExtractionDialogCore* core,
                                   Ui::ExtractionDialog* ui)
    : publ(publ),
      core(core),
      ui(ui),
      widgetMapper(new qcs::qtw::WidgetMapper(publ)),
      archivesListWidget(nullptr)
{
    core->setParent(publ);

    using Type = ExtractionDialogControllerType::Type;

    widgetMapper->addWidgets(Type::DestinationError, ui->destinationPathErrorLabel);
    widgetMapper->addLabelAndWidget(Type::DestinationMode,
                                    ui->destinationModeLabel,
                                    ui->destinationModeCombo);
    widgetMapper->addLabelAndWidget(Type::DestinationPath,
                                    ui->destinationPathLabel,
                                    ui->destinationPathEdit);
    widgetMapper->addLabelAndWidget(Type::FileExtractionMode,
                                    ui->fileExtractionModeLabel,
                                    ui->fileExtractionModeBox);
    widgetMapper->addLabelAndWidget(Type::FileOverwriteMode,
                                    ui->fileOverwriteLabel,
                                    ui->fileOverwriteCombo);
    widgetMapper->addLabelAndWidget(Type::FolderOverwriteMode,
                                    ui->folderOverwriteLabel,
                                    ui->folderOverwriteCombo);
    widgetMapper->addWidgets(Type::OpenDestinationOnCompletion,
                             ui->openDestinationCheck);
    widgetMapper->addLabelAndWidget(Type::Password,
                                    ui->passwordLabel,
                                    ui->passwordEdit);
    widgetMapper->addLabelAndWidget(Type::TopFolderMode,
                                    ui->topFolderLabel,
                                    ui->topFolderBox);
    widgetMapper->addWidgets(Type::RunInQueue, ui->runInQueueCheck);
    widgetMapper->addWidgets(Type::DialogErrors, ui->errorLabel);
    widgetMapper->addWidgets(Type::AcceptButton, ui->acceptButton);

    widgetMapper->addBuddies(Type::DestinationPath, ui->destinationButton);

    widgetMapper->setEngine(core->getControllerEngine());

    const auto icon = QIcon::fromTheme("dialog-error");
    ui->errorLabel->setPixmap(icon.pixmap({22, 22}));

    if (widgetMapper->isSetVisible(Type::Password))
    {
        ui->passwordEdit->setFocus();
        new PasswordEditHandler(ui->passwordEdit, PasswordEditHandler::WidgetRole::Main, publ);
    }
    else
        ui->runInQueueCheck->setFocus();

    ui->acceptButton->setDefault(true);

    QObject::connect(ui->acceptButton, &QPushButton::clicked,
                     publ, &QDialog::accept);

    QObject::connect(ui->rejectButton, &QPushButton::clicked,
                     publ, &QDialog::reject);
}


ExtractionDialog::Private::~Private()
{

}


void ExtractionDialog::Private::onAddArchivesRequested()
{
    const auto archives = QFileDialog::getOpenFileUrls(publ);
    core->addArchives(archives);
}


void ExtractionDialog::Private::onRemoveArchivesRequested()
{
    const auto indexes = archivesListWidget->getSelectedIndexes();

    if (indexes.size() > 0)
        core->removeArchives(indexes);
}


void ExtractionDialog::Private::onDestinationPathButtonClicked()
{
    const QString folder = QFileDialog::getExistingDirectory(publ,
                                                             QString(),
                                                             ui->destinationPathEdit->text());

    if (folder.isEmpty())
        return;

    ui->destinationPathEdit->setText(folder);
}
