// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCloseEvent>
#include <QFocusEvent>
#include <QAction>

#include "../core/extractionfileoverwritepromptcore.h"
#include "../core/htmllink.h"
#include "../core/commonstrings.h"

#include "extractionfileoverwriteprompt.h"
#include "ui_extractionfileoverwriteprompt.h"


using namespace Packuru::Core;


ExtractionFileOverwritePrompt::ExtractionFileOverwritePrompt(ExtractionFileOverwritePromptCore* promptCore,
                                                             bool browserMode,
                                                             QWidget *parent)
    : QWidget(parent),
      ui(new Ui::ExtractionFileOverwritePrompt),
      promptCore(promptCore)
{
    ui->setupUi(this);

    const auto file = promptCore->getString(ExtractionFileOverwritePromptCore::UserString::File);
    const auto location = promptCore->getString(ExtractionFileOverwritePromptCore::UserString::Location);
    const auto modified = promptCore->getString(ExtractionFileOverwritePromptCore::UserString::Modified);
    const auto size = promptCore->getString(ExtractionFileOverwritePromptCore::UserString::Size);

    ui->sourceLabel->setText(promptCore->getString(ExtractionFileOverwritePromptCore::UserString::Source));
    ui->sourceNameLabel->setText(file);
    ui->sourceLocationLabel->setText(location);
    ui->sourceSizeLabel->setText(size);
    ui->sourceModifiedLabel->setText(modified);

    ui->destinationLabel->setText(promptCore->getString(ExtractionFileOverwritePromptCore::UserString::Destination));
    ui->destinationNameLabel->setText(file);
    ui->destinationLocationLabel->setText(location);
    ui->destinationSizeLabel->setText(size);
    ui->destinationModifiedLabel->setText(modified);

    ui->applyToAllCheckBox->setText(promptCore->getString(ExtractionFileOverwritePromptCore::UserString::ApplyToAll));
    ui->skipButton->setText(promptCore->getString(ExtractionFileOverwritePromptCore::UserString::Skip));
    ui->overwriteButton->setText(promptCore->getString(ExtractionFileOverwritePromptCore::UserString::Overwrite));
    ui->retryButton->setText(promptCore->getString(ExtractionFileOverwritePromptCore::UserString::Retry));
    ui->abortButton->setText(promptCore->getString(ExtractionFileOverwritePromptCore::UserString::Abort));

    if (!browserMode)
    {
        setWindowFlag(Qt::Dialog);
        ui->infoLabel->hide();
        layout()->setContentsMargins(9, 9, 9, 9);
    }

    const QString text(promptCore->getString(ExtractionFileOverwritePromptCore::UserString::FileAlreadyExists));
    setWindowTitle(text);
    ui->infoLabel->setText(text + QLatin1Char('.'));

    ui->sourceName->setText(htmlLink("file://" + promptCore->sourceFilePath(), promptCore->sourceName()));
    ui->sourceLocation->setText(htmlLink("file://" + promptCore->sourceLocation(), promptCore->sourceLocation()));
    ui->sourceSize->setText(promptCore->sourceSize());
    ui->sourceModified->setText(promptCore->sourceModifiedDate());

    ui->destinationName->setText(htmlLink("file://" + promptCore->destinationFilePath(), promptCore->destinationName()));
    ui->destinationLocation->setText(htmlLink("file://" + promptCore->destinationLocation(), promptCore->destinationLocation()));
    ui->destinationSize->setText(promptCore->destinationSize());
    ui->destinationModified->setText(promptCore->destinationModifiedDate());

    connect(ui->skipButton, &QPushButton::clicked,
            promptCore, [promptCore = promptCore, ui = ui] ()
    {
        promptCore->skip(ui->applyToAllCheckBox->isChecked());
    });

    connect(ui->overwriteButton, &QPushButton::clicked,
            promptCore, [promptCore = promptCore, ui = ui] ()
    {
        promptCore->overwrite(ui->applyToAllCheckBox->isChecked());
    });

    connect(ui->retryButton, &QPushButton::clicked,
            promptCore, &ExtractionFileOverwritePromptCore::retry);

    connect(ui->abortButton, &QPushButton::clicked,
            promptCore, &ExtractionFileOverwritePromptCore::abort);

    auto abortAction = new QAction(this);
    abortAction->setShortcut(Qt::Key_Escape);
    connect(abortAction, &QAction::triggered,
            promptCore, &ExtractionFileOverwritePromptCore::abort);
    addAction(abortAction);

    connect(promptCore, &QObject::destroyed,
            this, &QObject::deleteLater);

    ui->applyToAllCheckBox->installEventFilter(this);
    ui->skipButton->installEventFilter(this);
    ui->overwriteButton->installEventFilter(this);
    ui->retryButton->installEventFilter(this);
    ui->abortButton->installEventFilter(this);

    setFocusProxy(ui->retryButton);
}


ExtractionFileOverwritePrompt::~ExtractionFileOverwritePrompt()
{
    delete ui;
}


void ExtractionFileOverwritePrompt::showAndSetup()
{
    show();
    setFixedSize({width(), height()});
}


void ExtractionFileOverwritePrompt::closeEvent(QCloseEvent* event)
{
    event->ignore();
}


bool ExtractionFileOverwritePrompt::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::FocusIn)
    {
        const auto focusEvent = static_cast<QFocusEvent*>(event);
        Q_ASSERT(event);

        const auto reason = focusEvent->reason();

        if  (reason == Qt::MouseFocusReason
             || reason == Qt::TabFocusReason
             || reason == Qt::BacktabFocusReason)
        {
            auto widget = qobject_cast<QWidget*>(watched);
            Q_ASSERT(widget);
            setFocusProxy(widget);
        }
    }

    return false;
}
