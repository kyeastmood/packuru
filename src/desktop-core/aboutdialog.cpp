// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QSettings>

#include "../core/aboutdialogcore.h"

#include "aboutdialog.h"
#include "ui_aboutdialog.h"


using namespace Packuru::Core;


AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);

    setWindowTitle(AboutDialogCore::getString(AboutDialogCore::UserString::DialogTitle));

    ui->info->setMarkdown(AboutDialogCore::getString(AboutDialogCore::UserString::AppInfo));

    ui->buttonBox->setFocus();

    QSettings s;
    const QSize size = s.value("about_dialog_size").toSize();
    if (size.isValid())
        resize(size);
}

AboutDialog::~AboutDialog()
{
    QSettings s;
    s.setValue("about_dialog_size", size());
    delete ui;
}
