// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QPushButton>

#include "../core/commonstrings.h"
#include "../core/appclosingdialogcore.h"

#include "appclosingdialog.h"

using namespace Packuru::Core;


AppClosingDialog::AppClosingDialog(QWidget* parent)
    : QMessageBox(parent)
{
    setWindowModality(Qt::WindowModal);
    setWindowTitle(AppClosingDialogCore::getString(AppClosingDialogCore::UserString::DialogTitle));
    setInformativeText(AppClosingDialogCore::getString(AppClosingDialogCore::UserString::QuitQuestion));

    QAbstractButton* quitButton = addButton(AppClosingDialogCore::getString(AppClosingDialogCore::UserString::Quit),
                                            QMessageBox::AcceptRole);

    connect(quitButton, &QAbstractButton::clicked,
            [&flag = quitRequested_] () { flag = true;});

    addButton(QMessageBox::Cancel);
    setDefaultButton(QMessageBox::Cancel);

}
