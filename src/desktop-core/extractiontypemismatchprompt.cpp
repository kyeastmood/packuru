// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCloseEvent>
#include <QFocusEvent>
#include <QFrame>
#include <QAction>

#include "../core/extractiontypemismatchpromptcore.h"
#include "../core/htmllink.h"
#include "../core/commonstrings.h"

#include "extractiontypemismatchprompt.h"
#include "ui_extractiontypemismatchprompt.h"


using namespace Packuru::Core;


static QLabel* createHeader(const QString& text);
static QLabel* createValueLabel(const QString& text);


ExtractionTypeMismatchPrompt::ExtractionTypeMismatchPrompt(ExtractionTypeMismatchPromptCore* promptCore, bool browserMode, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ExtractionTypeMismatchPrompt),
    promptCore(promptCore)
{
    ui->setupUi(this);

    const auto file = promptCore->getString(ExtractionTypeMismatchPromptCore::UserString::File);
    const auto folder = promptCore->getString(ExtractionTypeMismatchPromptCore::UserString::Folder);
    const auto link = promptCore->getString(ExtractionTypeMismatchPromptCore::UserString::Link);
    const auto location = promptCore->getString(ExtractionTypeMismatchPromptCore::UserString::Location);

    ui->applyToAllCheckBox->setText(promptCore->getString(ExtractionTypeMismatchPromptCore::UserString::ApplyToAll));
    ui->skipButton->setText(promptCore->getString(ExtractionTypeMismatchPromptCore::UserString::Skip));
    ui->retryButton->setText(promptCore->getString(ExtractionTypeMismatchPromptCore::UserString::Retry));
    ui->abortButton->setText(promptCore->getString(ExtractionTypeMismatchPromptCore::UserString::Abort));

    if (!browserMode)
    {
        setWindowFlag(Qt::Dialog);
        ui->infoLabel->hide();
        layout()->setContentsMargins(9, 9, 9, 9);
    }

    QString text(promptCore->getString(ExtractionTypeMismatchPromptCore::UserString::ItemsTypeMismatch));

    setWindowTitle(text);
    ui->infoLabel->setText(text + QLatin1Char('.'));

    auto formLayout = ui->formLayout;

    formLayout->addRow(createHeader(promptCore->getString(ExtractionTypeMismatchPromptCore::UserString::Source)),
                       static_cast<QWidget*>(nullptr));

    const auto sourceType = promptCore->sourceFileType();

    if (sourceType == ExtractionTypeMismatchPromptCore::FileType::Dir)
    {
        const auto path = promptCore->sourceFilePath();
        const auto value = createValueLabel(htmlLink("file://" + path, path));
        formLayout->addRow(folder, value);
    }
    else
    {
        QString typeKey;

        if (sourceType == ExtractionTypeMismatchPromptCore::FileType::File)
            typeKey = file;
        else if (sourceType == ExtractionTypeMismatchPromptCore::FileType::Link)
            typeKey = link;

        const auto fileNameLabel = createValueLabel(htmlLink("file://" + promptCore->sourceFilePath(),
                                                               promptCore->itemFileName()));
        formLayout->addRow(typeKey, fileNameLabel);

        const auto sourcePath = promptCore->sourcePath();
        const auto locationLabel = createValueLabel(htmlLink("file://" + sourcePath, sourcePath));
        formLayout->addRow(location, locationLabel);
    }

    auto spacer = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

    formLayout->addItem(spacer);

    formLayout->addRow(createHeader(promptCore->getString(ExtractionTypeMismatchPromptCore::UserString::Destination)),
                       static_cast<QWidget*>(nullptr));

    const auto destType = promptCore->destinationFileType();

    if (destType == ExtractionTypeMismatchPromptCore::FileType::Dir)
    {
        const auto path = promptCore->destinationFilePath();
        const auto value = createValueLabel(htmlLink("file://" + path, path));
        formLayout->addRow(folder, value);
    }
    else
    {
        QString typeKey;

        if (destType == ExtractionTypeMismatchPromptCore::FileType::File)
            typeKey = file;
        else if (destType == ExtractionTypeMismatchPromptCore::FileType::Link)
            typeKey = link;

        const auto fileNameLabel = createValueLabel(htmlLink("file://" + promptCore->destinationFilePath(),
                                                               promptCore->itemFileName()));
        formLayout->addRow(typeKey, fileNameLabel);

        const auto destPath = promptCore->destinationPath();
        const auto locationLabel = createValueLabel(htmlLink("file://" + destPath, destPath));
        formLayout->addRow(location, locationLabel);
    }

    connect(ui->skipButton, &QPushButton::clicked,
            promptCore, [promptCore = promptCore, ui = ui] ()
    {
        promptCore->skip(ui->applyToAllCheckBox->isChecked());
    });

    connect(ui->retryButton, &QPushButton::clicked,
            promptCore, &ExtractionTypeMismatchPromptCore::retry);

    connect(ui->abortButton, &QPushButton::clicked,
            promptCore, &ExtractionTypeMismatchPromptCore::abort);

    auto abortAction = new QAction(this);
    abortAction->setShortcut(Qt::Key_Escape);
    connect(abortAction, &QAction::triggered,
            promptCore, &ExtractionTypeMismatchPromptCore::abort);
    addAction(abortAction);

    connect(promptCore, &QObject::destroyed,
            this, &QObject::deleteLater);

    ui->applyToAllCheckBox->installEventFilter(this);
    ui->skipButton->installEventFilter(this);
    ui->retryButton->installEventFilter(this);
    ui->abortButton->installEventFilter(this);

    setFocusProxy(ui->retryButton);
}


ExtractionTypeMismatchPrompt::~ExtractionTypeMismatchPrompt()
{
    delete ui;
}


void ExtractionTypeMismatchPrompt::showAndSetup()
{
    show();
    setFixedSize({width(), height()});
}


void ExtractionTypeMismatchPrompt::closeEvent(QCloseEvent* event)
{
    event->ignore();
}


bool ExtractionTypeMismatchPrompt::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::FocusIn)
    {
        const auto focusEvent = static_cast<QFocusEvent*>(event);
        Q_ASSERT(event);

        const auto reason = focusEvent->reason();

        if  (reason == Qt::MouseFocusReason
             || reason == Qt::TabFocusReason
             || reason == Qt::BacktabFocusReason)
        {
            auto widget = qobject_cast<QWidget*>(watched);
            Q_ASSERT(widget);
            setFocusProxy(widget);
        }
    }

    return false;
}


QLabel* createHeader(const QString& text)
{
    auto label = new QLabel(text);
    QFont font1;
    font1.setBold(true);
    font1.setWeight(75);
    label->setFont(font1);
    return label;
}


QLabel* createValueLabel(const QString& text)
{
    auto label = new QLabel(text);
    label->setOpenExternalLinks(true);
    return label;
}
