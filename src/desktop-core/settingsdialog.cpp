// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QListWidgetItem>
#include <QIcon>
#include <QSettings>

#include "../qcs-qtwidgets/widgetmapper.h"

#include "../core/plugindatamodel.h"
#include "../core/archivetypemodel.h"
#include "../core/htmllink.h"
#include "../core/appsettingsdialogcore.h"
#include "../core/appsettingscontrollertype.h"
#include "../core/archivetypefiltermodel.h"
#include "../core/commonstrings.h"

#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "private/archivetypemodeldelegate.h"


using namespace Packuru::Core;


struct SettingsDialog::Private
{
    void openEditors(ArchiveTypeModel* typeModel,
                     ArchiveTypeFilterModel* typeFilterModel,
                     QAbstractItemView* view);

    std::unique_ptr<Ui::SettingsDialog> ui;
    AppSettingsDialogCore* settingsCore;
    qcs::qtw::WidgetMapper* mapper = nullptr;
};


SettingsDialog::SettingsDialog(AppSettingsDialogCore* settingsCore,
                               QWidget *parent)
    : QDialog(parent),
      priv(new Private)
{
    auto& ui = priv->ui;

    ui.reset(new Ui::SettingsDialog);
    ui->setupUi(this);

    priv->settingsCore = settingsCore;
    priv->settingsCore->setParent(this);

    setWindowTitle(settingsCore->getString(AppSettingsDialogCore::UserString::DialogTitle));

    ui->pluginDescriptionLabel->setText(settingsCore->getString(AppSettingsDialogCore::UserString::PluginDescription));
    ui->pluginHomepageLabel->setText(settingsCore->getString(AppSettingsDialogCore::UserString::PluginBackendHomepage));
    ui->pluginPathLabel->setText(settingsCore->getString(AppSettingsDialogCore::UserString::PluginFilePath));
    ui->pluginReadsLabel->setText(settingsCore->getString(AppSettingsDialogCore::UserString::PluginReads));
    ui->pluginWritesLabel->setText(settingsCore->getString(AppSettingsDialogCore::UserString::PluginWrites));
    ui->warningLabel->setText(settingsCore->getString(AppSettingsDialogCore::UserString::PluginConfigChangeWarning));
    ui->readWarningButton->setText(AppSettingsDialogCore::getString(AppSettingsDialogCore::UserString::ReadWarning));

    ui->warningLabel->setVisible(ui->readWarningButton->isChecked());

    priv->mapper = new qcs::qtw::WidgetMapper(this);

    priv->mapper->addLabelAndWidget(AppSettingsControllerType::Type::CompressionRatioDisplayMode,
                                    ui->compressionRatioLabel,
                                    ui->compressionRatioCombo);

    priv->mapper->addLabelAndWidget(AppSettingsControllerType::Type::SizeUnits,
                                    ui->sizeUnitsLabel,
                                    ui->sizeUnitsCombo);

    priv->mapper->addWidgets(AppSettingsControllerType::Type::SizeUnitsExample,
                             ui->sizeUnitsExample);

    priv->mapper->setEngine(priv->settingsCore->getControllerEngine());

    auto pluginModel = settingsCore->getPluginModel();
    ui->pluginView->setModel(pluginModel);

    connect(ui->pluginView->selectionModel(), &QItemSelectionModel::currentChanged,
            [model = settingsCore->getPluginModel(), ui = ui.get()] (const auto& index)
    {
        ui->pluginDetails->setEnabled(true);
        ui->descriptionValue->setText(model->getDescription(index.row()));
        ui->locationValue->setText(model->getPath(index.row()));
        auto homepage = model->getHomepage(index.row());
        ui->homepageValue->setText(htmlLink(homepage, homepage));
        ui->readTypeTextEdit->setPlainText(model->supportedReadArchives(index.row()));
        ui->writeTypeTextEdit->setPlainText(model->supportedWriteArchives(index.row()));
    });

    ui->pluginView->setCurrentIndex(pluginModel->index(0, 0));

    ArchiveTypeModel* typeModel = priv->settingsCore->getArchiveTypeModel();
    auto typeFilterModel = new ArchiveTypeFilterModel(this);
    typeFilterModel->setSourceModel(typeModel);
    ui->archiveTypeView->setModel(typeFilterModel);
    ui->archiveTypeView->setItemDelegate(new ArchiveTypeModelDelegate(this));
    ui->archiveTypeView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->archiveTypeView->horizontalHeader()->setSectionsClickable(false);

    priv->openEditors(typeModel, typeFilterModel, ui->archiveTypeView);

    ui->archiveTypeFilterEdit->setPlaceholderText(settingsCore->getString(AppSettingsDialogCore::UserString::FilterPlaceholderText));
    ui->archiveTypeFilterEdit->setText(typeFilterModel->getFilterFixedString());

    connect(ui->archiveTypeFilterEdit, &QLineEdit::textChanged,
            [priv = priv.get(),
            typeModel = typeModel,
            typeFilterModel = typeFilterModel,
            view = ui->archiveTypeView] (const QString& text)
    {
        typeFilterModel->setFilterFixedString(text);
        priv->openEditors(typeModel, typeFilterModel, view);
    });

    connect(this, &QDialog::accepted, priv->settingsCore, &AppSettingsDialogCore::accept);

    const QString generalTitle = AppSettingsDialogCore::getString(AppSettingsDialogCore::UserString::CategoryGeneral);
    const QString pluginsTitle = AppSettingsDialogCore::getString(AppSettingsDialogCore::UserString::CategoryPlugins);
    const QString typesTitle = AppSettingsDialogCore::getString(AppSettingsDialogCore::UserString::CategoryArchiveTypes);

    new QListWidgetItem(QIcon::fromTheme("configure"), generalTitle, ui->listWidget);
    new QListWidgetItem(QIcon::fromTheme("plugins"), pluginsTitle, ui->listWidget);
    new QListWidgetItem(QIcon::fromTheme("document-preview"), typesTitle, ui->listWidget);

    ui->generalPageTitle->setText(generalTitle);
    ui->pluginsPageTitle->setText(pluginsTitle);
    ui->archiveTypesPageTitle->setText(typesTitle);

    QFont font;
    font.setPointSizeF(font.pointSizeF() * 1.2);
    font.setBold(true);

    ui->generalPageTitle->setFont(font);
    ui->pluginsPageTitle->setFont(font);
    ui->archiveTypesPageTitle->setFont(font);

    connect(ui->listWidget, &QListWidget::currentRowChanged,
            [stack = ui->stackedWidget] (int row)
    { stack->setCurrentIndex(row); });

    ui->splitter->setSizes({140});

    ui->listWidget->setCurrentRow(0);

    QSettings s;
    const QSize size = s.value("settings_dialog_size").toSize();
    if (size.isValid())
        resize(size);
}


SettingsDialog::~SettingsDialog()
{
    QSettings s;
    s.setValue("settings_dialog_size", size());
    s.setValue("settings_dialog_last_page", priv->ui->listWidget->currentRow());
}


void SettingsDialog::insertSettingsPage(int index, const QIcon& icon, const QString& label, QWidget* widget)
{
    auto item = new QListWidgetItem(icon, label);
    priv->ui->listWidget->insertItem(index, item);
    priv->ui->stackedWidget->insertWidget(index, widget);

    QSettings s;
    int lastPage = s.value("settings_dialog_last_page").toInt();

    auto listWidget = priv->ui->listWidget;
    if (lastPage >= listWidget->count())
        lastPage = 0;

    listWidget->setCurrentRow(lastPage);
}


void SettingsDialog::Private::openEditors(ArchiveTypeModel* typeModel,
                                          ArchiveTypeFilterModel* typeFilterModel,
                                          QAbstractItemView* view)
{
    for (int row = 0; row < typeFilterModel->rowCount(); ++row)
        for (int column = 0; column < typeFilterModel->columnCount(); ++column)
        {
            const auto filterIndex = typeFilterModel->index(row, column);
            const auto sourceIndex = typeFilterModel->mapToSource(filterIndex);

            if (typeModel->flags(sourceIndex) & Qt::ItemIsEditable)
                view->openPersistentEditor(filterIndex);
        }
}
