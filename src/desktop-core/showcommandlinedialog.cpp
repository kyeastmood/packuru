// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QMessageBox>
#include <QTimer>

#include "showcommandlinedialog.h"


void showCommandLineDialog(QWidget* parent,
                           const QString& title,
                           bool modal,
                           const QString& text)
{
    auto dialogFactory = [parent = parent, title = title, text = text, modal = modal] ()
    {
        auto dialog = new QMessageBox(parent);
        dialog->setWindowTitle(title);
        dialog->setText(text);
        dialog->setStandardButtons(QMessageBox::Ok);
        dialog->setModal(modal);
        dialog->show();

        QObject::connect(dialog, &QMessageBox::finished, &QObject::deleteLater);
    };

    parent->show();
    // It is needed to wait until parent becomes visible to center the dialog over parent
    QTimer::singleShot(100, dialogFactory);
}
