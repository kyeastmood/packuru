// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QPushButton>
#include <QSettings>
#include <QFileDialog>

#include "../qcs-qtwidgets/widgetmapper.h"

#include "../core/testdialogcore.h"
#include "../core/testdialogcontrollertype.h"
#include "../core/commonstrings.h"

#include "private/archiveslistwidget.h"
#include "testdialog.h"
#include "ui_testdialog.h"
#include "passwordedithandler.h"


using namespace Packuru::Core;


struct TestDialog::Private
{
    Private(TestDialog* publ,
            TestDialogCore* core);

    void onAddArchivesRequested();
    void onRemoveArchivesRequested();

    TestDialog* publ;
    TestDialogCore* core;
    std::unique_ptr<Ui::TestDialog> ui;
    qcs::qtw::WidgetMapper* widgetMapper = nullptr;
    ArchivesListWidget* archivesListWidget = nullptr;
    const QString settingsKey;
};


TestDialog::TestDialog(TestDialogCore* core, QWidget *parent)
    : QDialog(parent),
      priv(new Private(this, core))
{

    if (priv->core->getMode() == TestDialogCore::DialogMode::SingleArchive)
        priv->ui->optionsPage->layout()->setContentsMargins(0, 0, 0, 0);

    QSettings s;
    const QSize size = s.value(priv->settingsKey).toSize();
    if (size.isValid())
        resize(size);
}


TestDialog::~TestDialog()
{
    QSettings s;
    s.setValue(priv->settingsKey, size());
}


TestDialog::Private::Private(TestDialog* publ, TestDialogCore* core)
    : publ(publ),
      core(core),
      ui(new Ui::TestDialog),
      widgetMapper(new qcs::qtw::WidgetMapper(publ)),
      settingsKey(core->getMode() == TestDialogCore::DialogMode::SingleArchive ? "test_dialog_single_geometry"
                                                                               : "test_dialog_multi_geometry")
{
    ui->setupUi(publ);

    core->setParent(publ);

    ui->tabWidget->setTabText(0, core->getString(TestDialogCore::UserString::TabOptions));

    ui->rejectButton->setText(core->getString(TestDialogCore::UserString::Cancel));

    if (core->getMode() == TestDialogCore::DialogMode::MultiArchive)
    {
        archivesListWidget = new ArchivesListWidget(core->getArchivesModel(), publ);

        QObject::connect(archivesListWidget, &ArchivesListWidget::sigAddArchivesRequested,
                         [priv = this] () { priv->onAddArchivesRequested(); });

        QObject::connect(archivesListWidget, &ArchivesListWidget::sigRemoveArchivesRequested,
                         [priv = this] () { priv->onRemoveArchivesRequested(); });

        ui->tabWidget->insertTab(0, archivesListWidget, core->getString(TestDialogCore::UserString::TabArchives));

        if (core->getArchivesModel()->rowCount() == 0)
            ui->tabWidget->setCurrentIndex(0);
    }

    using Type = TestDialogControllerType::Type;

    widgetMapper->addLabelAndWidget(Type::Password,
                                    ui->passwordLabel,
                                    ui->passwordEdit);
    widgetMapper->addWidgets(Type::RunInQueue, ui->runInQueueCheck);
    widgetMapper->addWidgets(Type::DialogErrors, ui->errorLabel);
    widgetMapper->addWidgets(Type::AcceptButton, ui->acceptButton);

    widgetMapper->setEngine(core->getControllerEngine());

    if (widgetMapper->isSetVisible(Type::Password))
    {
        ui->passwordEdit->setFocus();
        new PasswordEditHandler(ui->passwordEdit, PasswordEditHandler::WidgetRole::Main, publ);
    }
    else
    {
        ui->runInQueueCheck->setFocus();
        publ->adjustSize();
    }

    const auto icon = QIcon::fromTheme("dialog-error");
    ui->errorLabel->setPixmap(icon.pixmap({22, 22}));

    ui->acceptButton->setDefault(true);

    QObject::connect(ui->acceptButton, &QPushButton::clicked,
                     publ, &QDialog::accept);

    QObject::connect(ui->rejectButton, &QPushButton::clicked,
                     publ, &QDialog::reject);

    QObject::connect(publ, &TestDialog::accepted,
                     core, &TestDialogCore::accept);
}


void TestDialog::Private::onAddArchivesRequested()
{
    const auto archives = QFileDialog::getOpenFileUrls(publ);
    core->addArchives(archives);
}


void TestDialog::Private::onRemoveArchivesRequested()
{
    const auto indexes = archivesListWidget->getSelectedIndexes();

    if (indexes.size() > 0)
        core->removeArchives(indexes);
}
