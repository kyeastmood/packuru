// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QDialog>

#include "symbol_export.h"


namespace Packuru::Core
{
class AppSettingsDialogCore;
}


class PACKURU_DESKTOP_CORE_EXPORT SettingsDialog : public QDialog
{
    Q_OBJECT
public:
    ~SettingsDialog() override;

protected:
    explicit SettingsDialog(Packuru::Core::AppSettingsDialogCore* settingsCore,
                            QWidget *parent = nullptr);

    void insertSettingsPage(int index, const QIcon& icon, const QString& label, QWidget* widget);

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

