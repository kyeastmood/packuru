// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QPointer>

/// @file

namespace Packuru::Utils
{

/**
 @brief Creates QPointer for an object of a QObject derived class.
 @param obj Raw pointer to an object.
 @return QPointer for a given object.

 @par Example

 @code
 auto qptr = makeQPointer(obj); // Class name can be omitted
 @endcode
 */
template <typename T>
QPointer<T> makeQPointer(T* obj)
{
    return QPointer<T>(obj);
}

}
