// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>

#include <QtGlobal>


namespace Packuru::Utils
{

/*
 Returns an element of unordered map mapped to the specific key.

 The behavior is undefined unless the map contains element with the specific key.
 */
template <typename KeyType, typename ValueType, typename Hash>
ValueType getValue(const std::unordered_map<KeyType, ValueType, Hash>& map,
                   const KeyType& key)
{
    const auto it = map.find(key);

    Q_ASSERT(it != map.end());

    return it->second;
}


/*
 If the map contains an element mapped to the specific key, the element is returned; otherwise
 a specified value is return which defaults to ValueType{}.
 */
template <typename KeyType, typename ValueType, typename Hash>
ValueType tryValue(const std::unordered_map<KeyType, ValueType, Hash>& map,
                   const KeyType& key,
                   ValueType defaultValue = ValueType{})
{
    const auto it = map.find(key);

    if (it == map.end())
        return defaultValue;

    return it->second;
}

} 
