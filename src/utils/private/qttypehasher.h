// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <cstddef>

#include <QHash>


namespace Packuru::Utils
{

/*
 A function object that computes object's hash value using qHash() function.

 Useful when using QString as a key in standard library associative containers,
 e.g. std::unordered_map or std::unordered_set.

 Example:
 std::unordered_map<QString, QVariant, QtTypeHasher<QString>> map;
 */
template <typename T>
struct QtTypeHasher {
    inline std::size_t operator() (const T& value) const
    {
        return qHash(value);
    }
};

}
