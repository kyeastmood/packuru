// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <type_traits>

#include <QHash>


namespace Packuru::Utils
{

// Computes and returns the hash value of an enumeration.
template <typename EnumType>
inline std::enable_if_t<std::is_enum_v<EnumType>, uint>
enumHash(EnumType value, uint seed = 0)
{
    using UnderlyingType = typename std::underlying_type_t<EnumType>;
    return qHash(static_cast<UnderlyingType>(value), seed);
}

}
