// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <type_traits>


namespace Packuru::Utils
{

// Returns the value of an enumeration as its underlying type.
template <typename Enum>
constexpr
std::enable_if_t<std::is_enum_v<Enum>, std::underlying_type_t<Enum>>
toUnderlyingType(Enum value)
{
    return static_cast<std::underlying_type_t<Enum>>(value);
}

}
