# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)

MODULE_BUILD_NAME_SUFFIX = utils

QT += core

TEMPLATE = aux

CONFIG += file_copies
COPIES += public_headers public_headers_qte public_headers_stde

HEADERS_TARGET_DIR = $${PROJECT_API_TARGET_DIR}/$${MODULE_BUILD_NAME_SUFFIX}

public_headers.files = $$files(*.h)
public_headers.path = $$HEADERS_TARGET_DIR

public_headers_qte.files = $$files(qte/*.h)
public_headers_qte.path = $${HEADERS_TARGET_DIR}/qte

public_headers_stde.files = $$files(stde/*.h)
public_headers_stde.path = $${HEADERS_TARGET_DIR}/stde

HEADERS += \
    boolflag.h \
    localholder.h \
    makeqpointer.h \
    qvariant_utils.h \
    scopeguard.h \
    typetraits.h \
    private/enumcast.h \
    private/enumhash.h \
    private/qstringunorderedset.h \
    private/qttypehasher.h \
    private/qvariantunorderedmap.h \
    private/tounderlyingtype.h \
    private/unordered_map_utils.h
