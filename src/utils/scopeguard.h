// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <type_traits>
#include <utility>

/// @file

namespace Packuru::Utils
{

/**
 @brief Stores a callable object and invokes it in the destructor.

 When it goes out of scope its destructor calls the internal callable object.
 It is useful mostly in functions with multiple exit paths where it's difficult
 to ensure that some state is always set on exit.
 The advantage is that if lambda is stored internally, no heap access is required.

 @par Example

 @code
 {
    bool inProgess = false;
    bool updateSuccess = false;

    {
        inProgress = true;
        int tasksCompleted = 0;

        // Create ScopeGuard with a lambda inside and invoke the lambda when out of scope
        const auto sg = createScopeGuard([&] () {
                                             inProgress = false;
                                             if (tasksCompleted > 0)
                                                 updateSuccess = true;
                                         });

        // tasksCompleted possibly incremented...
    }
 }
 @endcode

 @headerfile "utils/scopeguard.h"
 @sa createScopeGuard(), LocalHolder
 */
template <typename Callable>
class ScopeGuard
{
public:
    ScopeGuard(Callable&& callable)
        : c(std::forward<Callable>(callable)) {}

    ~ScopeGuard() { c(); }

private:
    const Callable c;
};

/**
 @brief Facilitates creation of ScopeGuard.
 @param callable A reference to a callable object.
 @return A ScopeGuard object.
 @par Example

 @code
 {
    // Creates ScopeGuard with a lambda inside
    const auto sg = createScopeGuard([&] () {// Set state...);

    // sg goes out of scope and calls lambda in destructor
 }
 @endcode

 @relatedalso ScopeGuard
 */
template <typename Callable>
constexpr auto createScopeGuard(Callable&& callable)
{
    return ScopeGuard<std::decay_t<Callable>> ( std::forward<Callable>(callable) );
}

}
